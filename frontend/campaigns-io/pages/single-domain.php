<?php
	$gtMetrix_hrefAttr = '#';
	$gtMetrix_classAttr = ' class="gtm-view-more-link btn btn-xs btn-primary mt-10 mb-20 pull-right"';
	$gtMetrix_styleAttr = ' style="display:none;"';

	if( isset( $domain['gtmetrix']['metrix']['report_url'] ) && '' !== trim( $domain['gtmetrix']['metrix']['report_url'] ) ){
		$gtMetrix_hrefAttr = $domain['gtmetrix']['metrix']['report_url'];
		$gtMetrix_styleAttr = '';
	}
	
	$gtMetrix_hrefAttr = ' href="' . $gtMetrix_hrefAttr . '"';
?>

<div class="row row-eq-height mb-15">
	<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
		<div class="panel panel-default card-view height100">
			<div class="panel-heading website-traffic">
				<div class="pull-left">
					<a href="<?php echo $domain['url']['wp_login']; ?>" target="_blank"><button class="btn btn-success">WP LOGIN</button></a>
				</div>
				<div class="pull-right">
					<h6 class="panel-title txt-dark mt-10">WEBSITE TRAFFIC</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body mt-20">
					<canvas id="webTrafficChart" height="70"></canvas>						
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
		<div class="panel panel-default card-view height100">
			<?php single_domain_gtmetrix_content( $domain['url']['site'], $domain['gtmetrix'] ); ?>
			<a title="" target="_blank" <?php echo $gtMetrix_hrefAttr . $gtMetrix_classAttr . $gtMetrix_styleAttr; ?>><span class="white">VIEW MORE</span></a>						
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
		<div class="panel panel-default card-view panel-refresh">
			<div class="refresh-container">
				<div class="la-anim-1"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
          <div class="row">
            <div class="col-sm-4 col-lg-3">
              <div class="row">
                <div class="col-sm-12 pr-0 mt-25">
                  <div class="col-sm-10 pa-0">
                    <canvas id="chart_8" height="164"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-8 col-lg-9">
              <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="panel panel-default card-view" style="background: #3A9623">
                    <div class="panel-heading" style="padding: 5px;">
                      <div class="text-center">
                        <span style="font-size: 11px;color: #daffd1; text-transform: uppercase;">Total clicks</span>
                        <h6 style="color: #daffd1;"><?php echo $domain['search_console_avg']['clicks']; ?></h6>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>  
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="panel panel-default card-view" style="background: #207ABF">
                    <div class="panel-heading" style="padding: 5px;">
                      <div class="text-center">
                        <span style="font-size: 11px;color: #e0f2ff; text-transform: uppercase;">Total impressions</span>
                        <h6 style="color: #e0f2ff;"><?php echo $domain['search_console_avg']['impressions']; ?></h6>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>  
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="panel panel-default card-view" style="background: #FEAF94">
                    <div class="panel-heading" style="padding: 5px;">
                      <div class="text-center">
                        <span style="font-size: 11px;color: #B44823; text-transform: uppercase;">Average CTR</span>
                        <h6 style="color: #B44823"><?php echo $domain['search_console_avg']['average_ctr']; ?></h6>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>  
                </div>  
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="panel panel-default card-view" style="background: #B5A7FD">
                    <div class="panel-heading" style="padding: 5px;">
                      <div class="text-center">
                        <span style="font-size: 11px;color: #4733AD; text-transform: uppercase;">Average Position</span>
                        <h6 style="color: #4733AD"><?php echo $domain['search_console_avg']['positions']; ?></h6>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>  
                </div>  
              </div>
              <div class="row">
                <canvas id="searchConsoleChart" height="80"></canvas>
              </div>
            </div>
          </div>                  
        </div>  	
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
		<div class="panel panel-default card-view panel-refresh">
			<div class="refresh-container">
				<div class="la-anim-1"></div>
			</div>
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">SEO DATA</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body row">
					<div class="col-sm-6 pa-0">
						<canvas id="chart_7" height="164"></canvas>
					</div>
					<div class="col-sm-6 pr-0 pt-25">
						<div class="label-chatrs">
							<div class="mb-5">
								<span class="clabels inline-block bg-yellow mr-5"></span>
								<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $domain['seo']['month_clicks']; ?> Clicks this month</span>
							</div>
							<div class="mb-5">
								<span class="clabels inline-block bg-pink mr-5"></span>
								<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $domain['seo']['keywords']['changes']['positive']; ?> Moved up</span>
							</div>
							<div class="mb-5">
								<span class="clabels inline-block bg-blue mr-5"></span>
								<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $domain['seo']['keywords']['changes']['negative']; ?> Moved down</span>
							</div>
							<div class="">
								<span class="clabels inline-block bg-green mr-5"></span>
								<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $domain['seo']['keywords']['changes']['nochange']; ?> No changes</span>
							</div>												
						</div>
					</div>										
				</div>	
			</div>
		</div>
		<div class="row row-eq-height">
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view sm-data-box-3 height100">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark" style="font-size: 14px;">KEYWORD POSITIONS</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body row">
							<div class="table-wrap mt-5">
								<div class="table-responsive">
									<table class="table mb-5">
										<tbody>
											<tr>
												<td>Top 10</td>
												<td><span class="label label-warning pull-right"><?php echo $domain['seo']['keywords']['position']['top10']; ?></span></td>
											</tr>
											<tr>
												<td>Top 20</td>
												<td><span class="label label-danger pull-right"><?php echo $domain['seo']['keywords']['position']['top20']; ?></span></td>
											</tr>
											<tr>
												<td>Top 50</td>
												<td><span class="label label-success pull-right"><?php echo $domain['seo']['keywords']['position']['top50']; ?></span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view sm-data-box-3 height100">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title" style="font-size: 14px;">WORDPRESS</h6>
						</div>
						<div class="pull-right">
							<a href="<?php echo $domain['url']['wp_login']; ?>" class="btn btn-xs btn-primary btn-rounded pl-5 pr-5" target="_blank"><i class="fa fa-wordpress txt-dark font-12"></i></a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body pb-10 row">
							<div class="table-responsive">
								<table class="table mb-0">
									<tbody>
										<tr>
											<td>Core update</td>
											<td><span class="label label-warning pull-right"><?php echo $domain['wp_updates']['core']; ?></span></td>
										</tr>
										<tr>
											<td>Plugins updates</td>
											<td><span class="label label-danger pull-right"><?php echo $domain['wp_updates']['plugins']; ?></span></td>
										</tr>
										<tr>
											<td>Themes updates</td>
											<td><span class="label label-success pull-right"><?php echo $domain['wp_updates']['themes']; ?></span></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="text-center">
								<a href="<?php echo $domain['url']['wp_dashboard']; ?>" class="btn btn-xs btn-primary mt-10">VIEW MORE</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="content-row">

	<div class="content-column w-100 w-two-thirds-l">
		
		<div class="content-column-main content-col">

	        <div class="title website-traffic-title">

	        	<div class="relative fl w-100 dib dn-ns mb3 cf">
	        		<h3>WEBSITE TRAFFIC</h3>
	        	</div>

	        	<div class="left-pos">
	        		<a href="<?php echo $domain['url']['analytics']; ?>" title="" class="btn-color f7 no-underline pv1 ph3 br1"><span class="white">VIEW MORE</span></a>
	        	</div>

	        	<div class="dn dtc-ns ph-2 tc-ns">
	        		<h3>WEBSITE TRAFFIC</h3>
	        	</div>

	        	<div class="right-pos">
	        		<a href="<?php echo $domain['url']['wp_login']; ?>" target="_blank" title="" class="btn-color f7 no-underline pv1 ph3 br1"><span class="white">WP LOGIN</span></a>
	        	</div>

	        </div>
	    
	        <div class="content-column-inner">
	        	<canvas id="webTrafficChart"></canvas>
	        </div>
	    
	    </div>
	</div>
	
	<div class="content-column w-100 w-50-ns w-third-half-l">
		<div class="content-column-main">

			<div class="title">
				<div class="left-pos"><h3>SEO DATA</h3></div>
			</div>

			<div class="content-column-inner rmv-left-padd rmv-right-padd">
				
				<div class="circle-stat">
					<div class="aspect-ratio aspect-ratio--1x1">
						<div class="dt aspect-ratio--object">
							<div class="dtc tc v-mid"><span><?php echo $domain['seo']['week_clicks']; ?></span>TRAFFIC THIS<br>WEEK</div>
						</div>
					</div>
				</div>

				<div class="inline-stat-num">
					<span class="stat-num campaignsio-admin-green month-clicks-val"><?php echo $domain['seo']['month_clicks']; ?></span>
					<span class="stat-label">Clicks this month</span>
				</div>

				<div class="inline-stat-num">	
					<span class="stat-num campaignsio-admin-yellow moved-up-clicks-val"><?php echo $domain['seo']['keywords']['changes']['positive']; ?></span>
					<span class="stat-label">Moved up</span>
				</div>

				<div class="inline-stat-num">
					<span class="stat-num campaignsio-admin-orange moved-down-clicks-val"><?php echo $domain['seo']['keywords']['changes']['negative']; ?></span>
					<span class="stat-label">Moved down</span>
				</div>

				<div class="inline-stat-num">
					<span class="stat-num campaignsio-admin-action-color no-changes-val"><?php echo $domain['seo']['keywords']['changes']['nochange']; ?></span>
					<span class="stat-label">No changes</span>
				</div>
			</div>
		</div>
	</div>

	<div class="content-column w-100 w-50-ns w-third-half-l">
		<div class="content-column-main">
			<div class="title">
				<div class="left-pos"><h3>KEYWORD POSITION</h3></div>
			</div>
			<?php  ?>
			<div class="content-column-inner">
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-green keyword-pos-top-10-val"><?php echo $domain['seo']['keywords']['position']['top10']; ?></span>
					<span class="stat-label">Top 10</span>
				</div>
				<hr>
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-yellow keyword-pos-top-20-val"><?php echo $domain['seo']['keywords']['position']['top20']; ?></span>
					<span class="stat-label">Top 20</span>
				</div>
				<hr>
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-orange keyword-pos-top-50-val"><?php echo $domain['seo']['keywords']['position']['top50']; ?></span>
					<span class="stat-label">Top 50</span>
				</div>
			</div>
			<?php  ?>
		</div>
	</div>

</div>

<div class="content-row">
	
	<div class="content-column w-100 w-two-thirds-l">
	    <div class="content-column-main content-col">

		    <?php
		    	$gtMetrix_hrefAttr = '#';
	        	$gtMetrix_classAttr = ' class="gtm-view-more-link btn-color f7 no-underline pv1 ph3 br1"';
	        	$gtMetrix_styleAttr = ' style="display:none;"';

	        	if( isset( $domain['gtmetrix']['metrix']['report_url'] ) && '' !== trim( $domain['gtmetrix']['metrix']['report_url'] ) ){
	        		$gtMetrix_hrefAttr = $domain['gtmetrix']['metrix']['report_url'];
	        		$gtMetrix_styleAttr = '';
	        	}
	        	
	        	$gtMetrix_hrefAttr = ' href="' . $gtMetrix_hrefAttr . '"';
		    ?>

		    <div class="title">

		    	<div class="relative fl w-100 dib dn-ns mb3 cf">
	        		<h3>GT METRIX</h3>
	        	</div>

	        	<?php /* ?>
	        	<div class="left-pos">
	        		<a title="" target="_blank" <?php echo $gtMetrix_hrefAttr . $gtMetrix_classAttr . $gtMetrix_styleAttr; ?>><span class="white">VIEW MORE</span></a>
	        	</div>

	        	<div class="dn dtc-ns ph-2 tc-ns">
	        		<h3>GT METRIX</h3>
	        	</div>
				
				<div class="right-pos">
	        		<span href="#" title="" class="rescan-gtmetrix btn-color f7 no-underline pv1 ph3 br1"><span class="white">RESCAN NOW</span></span>
	        	</div>
	        	<?php */ ?>

	        	<div class="left-pos">
	        		<h3>GT METRIX</h3>
	        	</div>
				
				<div class="right-pos">
	        		<a title="" target="_blank" <?php echo $gtMetrix_hrefAttr . $gtMetrix_classAttr . $gtMetrix_styleAttr; ?>><span class="white">VIEW MORE</span></a>
	        	</div>

	        </div>
			
			<div class="relative w-100 dib">
				<div class="gtmetrix_wrapper w-100 dib">
		        	<?php single_domain_gtmetrix_content( $domain['url']['site'], $domain['gtmetrix'] ); ?>
		    	</div>
		    	<div class="gtmetrix-loader-overlay serp-report-loader hidden invisible"><span class="loading-icon"><span><i class="material-icons">&#xE86A;</i></span></span></div>
		    </div>

	    </div>
	</div>
	
	<div class="content-column w-100 w-50-ns w-third-half-l">
	    <div class="content-column-main content-col">

	        <div class="title">
	        	<div class="left-pos">
	        		<h3>GOOGLE SEARCH CONSOLE</h3>
	        	</div>
	        </div>

	        <div class="content-column-inner rmv-left-padd rmv-right-padd">
				
				<br/>

				<div class="circle-stat">
					<div class="aspect-ratio aspect-ratio--1x1">
						<div class="dt aspect-ratio--object">
							<div class="dtc tc v-mid"><span><?php echo $domain['search_console']['average_ctr']; ?><small>%</small></span>AVERAGE <br/>CLICK THROUGH</div>
						</div>
					</div>
				</div>

				<div class="inline-stat-num">
					<span class="stat-num campaignsio-admin-green"><?php echo $domain['search_console']['clicks']; ?></span>
					<span class="stat-label">Total Clicks</span>
				</div>

				<div class="inline-stat-num">	
					<span class="stat-num campaignsio-admin-yellow"><?php echo $domain['search_console']['impressions']; ?></span>
					<span class="stat-label">Total Impressions</span>
				</div>

			</div>

	    </div>
	</div>

	<div class="content-column w-100 w-50-ns w-third-half-l">
	    <div class="content-column-main content-col">

	        <div class="title">
	        	<div class="left-pos">
	        		<h3>WORDPRESS</h3>
	        	</div>
	        </div>

	        <div class="content-column-inner">
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-green keyword-pos-top-10-val"><?php echo $domain['wp_updates']['core']; ?></span>
					<span class="stat-label">Core update</span>
				</div>
				<hr>
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-yellow keyword-pos-top-20-val"><?php echo $domain['wp_updates']['plugins']; ?></span>
					<span class="stat-label">Plugins updates</span>
				</div>
				<hr>
				<div class="bold-stat-num">
					<span class="stat-num campaignsio-admin-orange keyword-pos-top-50-val"><?php echo $domain['wp_updates']['themes']; ?></span>
					<span class="stat-label">Themes updates</span>
				</div>
				<hr/>
				<a href="<?php echo $domain['url']['wp_dashboard']; ?>" title="" class="btn-color f7 no-underline pv2 ph3 br1 fr"><span class="white">VIEW MORE</span></a>
			</div>

	    </div>
	</div>

</div> -->


<?php if( $domain['enabled_monitor_uptime'] ){ ?>

<!-- <div class="content-row">

	<div class="content-column w-100 w-50-l">
	    <div class="content-column-main content-col">
	        <div class="title">
	        	<div class="left-pos"><h3>DOMAIN UPTIME &amp; PERFORMANCE</h3></div>
	        	<div class="right-pos">
	        		<span class="domain-status white">Status: <?php
	        		if( 'down' === $domain['down_time']['server_status'] ){ ?>
	        			<span class="br1 campaignsio-admin-bg-orange"><small>DOWN</small></span><?php 
	        		}
	        		elseif( 'up' === $domain['down_time']['server_status'] ){ ?>
	        			<span class="br1 campaignsio-admin-bg-green"><small>UP</small></span><?php 
	        		}
	        		else{ ?>
	        			<span class="br1 campaignsio-admin-bg-yellow"><small>N/A</small></span><?php 
	        		}
	        		?></span></span>
	        	</div>
	        </div>
	        <div class="content-column-inner">
	        	<canvas id="responseAndUpTimeChart"></canvas>
	        </div>
	    </div>
	</div>

	<div class="content-column w-100 w-50-l">
	    <div class="content-column-main content-col">
	        <div class="title">
	        	<div class="left-pos"><h3>UPTIME</h3></div>
	        </div>
	        <div class="content-column-inner">
	        	<canvas id="uptimeChart"></canvas>
	        </div>
	    </div>
	</div>
</div> -->

<?php } ?>

<input type="hidden" name="is_dashboard_page" value="1" />

<?php echo form_hidden( 'base_url', base_url() ); ?>
<?php echo form_hidden( 'domain_id', $domain['id'] ); ?>
<?php echo form_hidden( 'domain_url', $domain['url']['site'] ); ?>
<?php echo form_hidden( 'gtmetrix_rescan_url', $domain['gtmetrix_rescan_url'] ); ?>

<input type="hidden" name="csrf_hash" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="csrf_token" value="<?php echo $this->security->get_csrf_token_name(); ?>" />

<?php echo form_hidden( 'id_website_traffic_data', json_encode( $domain['traffic']['payload'] ) ); ?>

<?php if( $domain['enabled_monitor_uptime'] ){
	echo form_hidden( 'id_uptime_performance_data', json_encode( $domain['up_time']['performance'] ) );
	echo form_hidden( 'id_uptime_data', json_encode( $domain['up_time']['data'] ) ); 
} ?>



<script>
	$( document ).ready(function() {
    "use strict";

	if( $('#chart_7').length > 0 ){
		var ctx7 = document.getElementById("chart_7").getContext("2d");
		var data7 = {
			 labels: [
				"lab 1",
				"lab 2",
				"lab 3",
				"lab 4"
				],
			datasets: [
			{
				data: [100, 100, 100, 100],
				backgroundColor: [
					"rgba(220,70,102,1)",
					"rgba(23,126,193,1)",
					"rgba(70,148,8,1)",
					"rgba(230,154,42,1)"
				],
				hoverBackgroundColor: [
					"rgba(220,70,102,1)",
					"rgba(23,126,193,1)",
					"rgba(70,148,8,1)",
					"rgba(230,154,42,1)"
				]
			}]
		};
		
		var doughnutChart = new Chart(ctx7, {
			type: 'doughnut',
			data: data7,
			options: {
				animation: {
					duration:	3000
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				},
				responsive: true,
				maintainAspectRatio:false,
				percentageInnerCutout: 50,
				legend: {
					display:false
				},
				tooltips: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Roboto'"
				},
				cutoutPercentage: 70,
				segmentShowStroke: false
			}
		});
	}

	if( $('#chart_8').length > 0 ){
		var ctx8 = document.getElementById("chart_8").getContext("2d");
		var data8 = {
			 labels: [
			"lab 1",
			"lab 2",
			"lab 3",
			"lab 4"
		],
		datasets: [
			{
				data: [100, 100, 100, 100],
				backgroundColor: [
					"rgba(220,70,102,1)",
					"rgba(23,126,193,1)",
					"rgba(70,148,8,1)",
					"rgba(230,154,42,1)"
				],
				hoverBackgroundColor: [
					"rgba(220,70,102,1)",
					"rgba(23,126,193,1)",
					"rgba(70,148,8,1)",
					"rgba(230,154,42,1)"
				]
			}]
		};
		
		var doughnutChart = new Chart(ctx8, {
			type: 'doughnut',
			data: data8,
			options: {
				animation: {
					duration:	3000
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				},
				responsive: true,
				maintainAspectRatio:false,
				percentageInnerCutout: 50,
				legend: {
					display:false
				},
				tooltips: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Roboto'"
				},
				cutoutPercentage: 70,
				segmentShowStroke: false
			}
		});
	}

	if( $('#searchConsoleChart').length > 0 ){
		var chartData = <?php echo json_encode($domain['search_console_data']); ?>;
		var impressions = [];
		var clicks = [];
		var labels = [];
		for ( var i = 0; i < chartData.length; i++) {
			labels[i] = chartData[i]['date'];
			impressions[i] = chartData[i]['impression'] - chartData[i]['click'];
			clicks[i] = chartData[i]['click'];
			
		}
		console.log(clicks);
		var ctx1 = document.getElementById("searchConsoleChart").getContext("2d");
		var data1 = {
			labels: labels,
			datasets: [
			{
				label: "clicks",
				backgroundColor: "#3B9623",
				borderColor: "#3B9623",
				borderWidth: 2,
				data: clicks,
				fill: false,
			},
			{
				label: "impressions",
				backgroundColor: "#207ABF",
				borderColor: "#207ABF",
				borderWidth: 2,
				data: impressions,
				fill: false,
			},			
		]
		};
		
		var areaChart = new Chart(ctx1, {
			type:"line",
			data:data1,
			options: {
				tooltips: {
					enabled:false
				},
				elements:{
					point: {
						hitRadius:50
					}
				},
				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#878787",
							lineWidth: 0.3
						},
						ticks: {
							fontFamily: "Roboto",
							fontColor:"#878787"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#878787",
							lineWidth: 0.3
						},
						ticks: {
							fontFamily: "Roboto",
							fontColor:"#878787"
						}
					}]
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					display: false,
				},
			}
		});
	}

});
</script>