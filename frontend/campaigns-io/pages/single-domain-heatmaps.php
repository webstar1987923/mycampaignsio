<?php
	// var_dump( $heatmaps['current']['id'] );
?>
<!-- <div class="row heading-bg">
	<div class="col-lg-12">
		<div class="pull-right">
			<a href="<?php echo base_url( 'domains/' . $domain['id'] . '/heatmaps/add' ); ?>" title="" class="btn btn-md btn-success"><i class="material-icons white">&#xE145;</i><small class="white">ADD HEATMAP</small></a>
		</div>
	</div>
</div> -->

<div class="row">
	<div class="col-lg-12 col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div  class="tab-struct custom-tab-1">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<?php
							foreach ($heatmaps as $key => $value) { ?>
								<li class="<?php echo $key==0 ? 'active' : '';?>" role="presentation">
									<a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_7" href="#heatmap_<?php echo $key;?>" onclick="iframe_load(<?php echo $key;?>, `<?php echo $value['heatmapViewUrl'];?>`)">
									<?php echo $value['name'];?>
									</a>
								</li>
							<?php
							}
							?>						
						</ul>
						<div class="tab-content" id="myTabContent_7">
							<?php
							if (!empty($heatmaps)) {
							foreach ($heatmaps as $key => $value) { ?>
							<div  id="heatmap_<?php echo $key;?>" class="tab-pane fade <?php echo $key==0 ? 'active' : '';?> in" role="tabpanel">
								<div class="aspect-ratio aspect-ratio--4x3">
									<div  id="parent_<?php echo $key;?>" class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f3 f2-ns">
										<?php if( empty( $value['heatmapViewUrl'] ) ){ ?>
										<span class="dtc v-mid pa3">None available heatmap</span>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php
							}} else {?>
							<div class="tab-pane fade active in" role="tabpanel">
								<div class="aspect-ratio aspect-ratio--4x3">
									<div class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f3 f2-ns">
										<span class="dtc v-mid pa3">None available heatmap</span>
									</div>
								</div>
							</div>
							<?php };?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$('body > div').addClass('slide-nav-toggle');

function iframe_load (id, url) 
{
	var iframes = document.querySelectorAll('iframe');
	for (var i = 0; i < iframes.length; i++) {
		iframes[i].parentNode.removeChild(iframes[i]);
	}

	if (url) {
		var iframe = document.createElement("iframe");
		var ifm = "ifm_" + id;
		iframe.setAttribute("id", ifm);
		iframe.setAttribute("class", "w-100 h-100");
		iframe.src = "<?php echo $this->config->config['piwik']['api_url'] . '/' ; ?>" + url;
		iframe.onload = function(){
			$("#" + ifm).contents().find(".enrichedHeadline").remove();
		};
		document.getElementById("parent_" + id).appendChild(iframe);
	}
};

iframe_load(0, "<?php echo $heatmaps[0]['heatmapViewUrl'] ;?>");

</script>