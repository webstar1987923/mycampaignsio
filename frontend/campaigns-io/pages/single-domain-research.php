<?php
$selected_tab = [];

if ($_POST['search_type'] && $_POST['se'] && $_POST['query']) {
	$selected_tab['3'] = true;
}
else if ($_POST['serp_keyword'] && $_POST['serp_se']) {
	$selected_tab['5'] = true;
}
else if ($_POST['serp_domain'] && $_POST['serp_country']) {
	$selected_tab['6'] = true;
}
else {
	$selected_tab['1'] = true;
}
?>

<div class="row pt-25">
	<div class="col-lg-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div  class="tab-struct custom-tab-1">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<li class="<?php echo $selected_tab['1'] ? 'active' : '';?>" role="presentation" class=""><a  data-toggle="tab" id="tab_1" role="tab" href="#1" aria-expanded="false">OVERVIEW</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_2" role="tab" href="#2" aria-expanded="false">POSITIONS</a></li>
							<li class="<?php echo $selected_tab['3'] ? 'active' : '';?>" role="presentation" class=""><a  data-toggle="tab" id="tab_3" role="tab" href="#3" aria-expanded="false">RESEARCH</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_4" role="tab" href="#4" aria-expanded="false">DOMAIN VS DOMAIN</a></li>
							<li class="<?php echo $selected_tab['5'] ? 'active' : '';?>" role="presentation" class=""><a  data-toggle="tab" id="tab_5" role="tab" href="#5" aria-expanded="false">TOP COMPETITORS</a></li>
							<li class="<?php echo $selected_tab['6'] ? 'active' : '';?>" role="presentation" class=""><a  data-toggle="tab" id="tab_6" role="tab" href="#6" aria-expanded="false">KEYWORDS FOR DOMAIN</a></li>
							<li class="<?php echo $selected_tab['7'] ? 'active' : '';?>" role="presentation" class=""><a  data-toggle="tab" id="tab_7" role="tab" href="#7" aria-expanded="false">KEYWORDS FOR KEYWORDS</a></li>
							
						</ul>
						<div class="tab-content" id="myTabContent_7">
							<div id="1" class="tab-pane fade <?php echo $selected_tab['1'] ? 'active' : '';?> in" role="tabpanel">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="circle-stat">
											<div class="aspect-ratio aspect-ratio--1x1">
												<div class="dt aspect-ratio--object">
													<div class="dtc tc v-mid"><span><small><?php echo $overview['info']['visibility']; ?></small></span>VISIBILITY</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="circle-stat campaignsio-admin-border-yellow" style="margin-bottom:0">
											<div class="aspect-ratio aspect-ratio--1x1">
												<div class="dt aspect-ratio--object">
													<div class="dtc tc v-mid"><span><small><?php echo $overview['info']['traffic']; ?></small></span>SE TRAFFIC</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="circle-stat campaignsio-admin-border-orange" style="margin-bottom:0">
											<div class="aspect-ratio aspect-ratio--1x1">
												<div class="dt aspect-ratio--object">
													<div class="dtc tc v-mid"><span><small><?php echo $overview['info']['organic_keywords']; ?></small></span>ORGANIC <br/>KEYWORDS</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="circle-stat campaignsio-admin-border-action-color" style="margin-bottom:0">
											<div class="aspect-ratio aspect-ratio--1x1">
												<div class="dt aspect-ratio--object">
													<div class="dtc tc v-mid"><span><small><?php echo $overview['info']['ppc']; ?></small></span>KEYWORDS <br/>IN PPC</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6">
								    <div class="panel panel-default border-panel card-view">
								    	<div class="panel-heading">
								    		<h5>ORGANIC KEYWORDS</h5>
								    	</div>

								    	<div class="panel-wrapper">
								    		<div class="panel-body">
								    			<div class="table-wrap">
								    				<div class="table-reponsive">
								    					<table class="table table-hover">
										        		<thead>
										        			<tr>
									            				<th class="tl">KEYWORDS</th>
									            				<th>POSITION</th>
									            				<th>GOOGLE VOLUME</th>
									            				<th class="nowrap">CPC $</th>
										        			</tr>
										        		</thead>
										        		<tbody><?php 
										        			if( empty( $overview['organic_keywords'] ) ){ ?>
										        				<tr><td colspan="4">No data available</td></tr>
																<?php
										        			}
										        			else{
										        				foreach( $overview['organic_keywords'] as $k => $v ){?>
											        				<tr>
												        				<td class="tl"><?php echo $v['keyword']; ?></td>
												        				<td><?php echo $v['position']; ?></td>
												        				<td><?php echo $v['concurrency']; ?></td>
												        				<td><?php echo $v['cost']; ?></td>
											        				</tr><?php
											        			}
										        			} ?>
										        		</tbody>
										        	</table>
								    				</div>
								    			</div>
								    		</div>
								    	</div>
								    </div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="panel panel-default border-panel card-view">
											<div class="panel-heading">
												<h5>ADS KEYWORDS</h5>
											</div>
											<div class="panel-wrapper">
								    		<div class="panel-body">
								    			<div class="table-wrap">
								    				<div class="table-reponsive">
								    					<table class="table table-hover">
								    						<thead>
										        			<tr>
									            				<th class="tl">KEYWORD</th>
									            				<th>TITLE</th>
									            				<th>POSITION</th>
									            				<th>CONCURRENCY</th>
									            				<th>FOUND RESULTS</th>
										        			</tr>
										        		</thead>
										        		<tbody><?php 
										        			if( empty( $overview['add_keywords'] ) ){ ?>
										        				<tr><td colspan="5">No data available</td></tr>
																<?php
										        			}
										        			else{
											        			foreach( $overview['add_keywords'] as $k => $v ){?>
											        				<tr>
												        				<td class="tl"><?php echo $v['keyword']; ?></td>
												        				<td><?php echo $v['title']; ?></td>
												        				<td><?php echo $v['position']; ?></td>
												        				<td><?php echo $v['concurrency']; ?></td>
												        				<td><?php echo $v['found_results']; ?></td>
											        				</tr><?php
											        			}
										        			} ?>
										        		</tbody>
								    					</table>
								    				</div>
								    			</div>
								    		</div>
								    	</div>
								    </div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6">
								    <div class="panel panel-default border-panel card-view">
								    	<div class="panel-heading">
								    		<h5>KEYWORD POSITION DISTRIBUTION</h5>
								    	</div>
								    	<div class="panel-wrapper">
								    		<div class="panel-body">
								    			<div class="aspect-ratio aspect-ratio--16x9">
									        	<div class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f4 f3-ns">
									        		<span class="dtc v-mid pa3">
									        			<canvas id="keyword_position_distribution_graph"></canvas>
									        		</span>
									        	</div>
													</div>
								    		</div>
								    	</div>
								    </div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-6">
									  <div class="panel panel-default border-panel card-view">
								   		<div class="panel-heading">
								   			<h5>SUBDOMAINS</h5>
								   		</div>
								   		<div class="panel-wrapper">
								   			<div class="panel-body">
								   				<div class="table-wrap">
								    				<div class="table-reponsive">
								    					<table class="table table-hover">
								    						<thead>
										        			<tr>
									            				<th class="tl">SUBDOMAIN</th>
									            				<th>NUMBER OF KEYWORDS</th>
										        			</tr>
										        		</thead>
										        		<tbody>
										        			<tr><td colspan="2">No data available</td></tr>
										        			<?php /* ?>
										        			<tr>
									            				<td class="tl">subdomain1.domain.com</td>
									            				<td>3</td>
										        			</tr>
										        			<tr>
									            				<td class="tl">subdomain2.domain.com</td>
									            				<td>2</td>
										        			</tr>
										        			<tr>
									            				<td class="tl">subdomain3.domain.com</td>
									            				<td>5</td>
										        			</tr>
										        			<tr>
									            				<td class="tl">subdomain4.domain.com</td>
									            				<td>7</td>
										        			</tr>
										        			<tr>
									            				<td class="tl">subdomain5.domain.com</td>
									            				<td>5</td>
										        			</tr>
										        			<tr>
									            				<td class="tl">subdomain6.domain.com</td>
									            				<td>2</td>
										        			</tr>
										        			<?php */ ?>
										        		</tbody>
								    					</table>
								    				</div>
								    			</div>
								   			</div>
								   		</div>
								    </div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12">
									  <div class="panel panel-default border-panel card-view">
									  	<div class="panel-heading">
									  		<h5>COMPETITORS IN ORGANIC SEARCH</h5>
									  	</div>
									  	<div class="panel-wrapper">
									  		<div class="panel-body">
									  			<div class="table-wrap">
								    				<div class="table-reponsive">
								    					<table class="table table-hover">
								    						<thead>
										        			<tr>
										        				<th style="width:1px;">#</th>
									            				<th class="tl">DOMAIN</th>
									            				<th>ALL KEYWORDS</th>
									            				<th>TRAFFIC DYNAMIC</th>
									            				<th>VISIBLE DYNAMIC</th>
									            				<th>KEYWORD DYNAMIC</th>
										        			</tr>
										        		</thead>
										        		<tbody><?php 
										        			if( empty( $overview['competitors_trend'] ) ){ ?>
										        				<tr><td colspan="6">No data available</td></tr>
																<?php
										        			}
										        			else{
										        				$cntr = 1;
											        			foreach( $overview['competitors_trend'] as $k => $v ){?>
											        				<tr>
												        				<td><?php echo $cntr; ?></td>
																        <td class="tl"><a href="http://<?php echo $v['domain']; ?>" title="" target="_blank" class="link white"><?php echo $v['domain']; ?></a></td>
																        <td><?php echo $v['keywords']; ?></td>
																        <td><?php echo $v['traff_dynamic']; ?></td>
																        <td><?php echo round( $v['visible_dynamic'], 9 ); ?></td>
																        <td><?php echo $v['keywords_dynamic']; ?></td>
											        				</tr><?php
											        				$cntr++;
											        			}
										        			} ?>
										        		</tbody>
								    					</table>
								    				</div>
								    			</div>
									  		</div>
									  	</div>
									   </div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12">
									  <div class="panel panel-default border-panel card-view">
									  	<div class="panel-heading">
									  		<h5>COMPETITORS GRAPH</h5>
									  	</div>
									  	<div class="panel-wrapper">
									  		<div class="panel-body">
									  			<div class="aspect-ratio aspect-ratio--16x9">
									        	<div class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f4 f3-ns">
									        		<span class="dtc v-mid pa3">
									        			<canvas id="competitors_graph_data" style="max-height:100rem !important;"></canvas>
									        		</span>
									        	</div>
													</div>
									  		</div>
									  	</div>
									  </div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
									  <div class="panel panel-default border-panel card-view">
									  	<div class="panel-heading">
									  		<h5>VISIBILITY TREND</h5>
									  	</div>
									  	<div class="panel-wrapper">
									  		<div class="panel-body">
									  			<div class="row">
									  				<div class="col-lg-6 mb-10">
										  				<div class="bold-stat-num">
				                        <small>
					                        <span class="stat-num campaignsio-admin-green"><small><?php echo $overview['trends']['visibility']['max_trend']; ?></small></span>
					                        <span class="stat-label">MAXIMUM NUMBER: <?php echo $overview['trends']['visibility']['max_trend_date']; ?></span>
				                        </small>
					                    </div>
										  			</div>
										  			<div class="col-lg-6">
										  				<div class="bold-stat-num mb-10">
				                        <small>
				                        	<span class="stat-num campaignsio-admin-action-color"><small><?php echo $overview['trends']['visibility']['min_trend']; ?></small></span>
				                        	<span class="stat-label">MINIMUM NUMBER: <?php echo $overview['trends']['visibility']['min_trend_date']; ?></span>
				                        </small>
								              </div>
										  			</div>
									  			</div>
									  			<div class="row">
									  				<div class="col-lg-12">
									  					<div class="aspect-ratio aspect-ratio--4x3">
											        	<div class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f4 f3-ns">
											        		<span class="dtc v-mid pa3">
											        			<canvas id="visibility_trend_chart"></canvas>
											        		</span>
											        	</div>
															</div>
									  				</div>
									  			</div>
									  		</div>
									  	</div>
									  </div>
									</div>

									<div class="col-lg-6">
									  <div class="panel panel-default border-panel card-view">
									  	<div class="panel-heading">
									  		<h5>KEYWORDS TREND</h5>
									  	</div>
									  	<div class="panel-wrapper">
									  		<div class="panel-body">
									  			<div class="row">
									  				<div class="col-lg-6">
									  					<div class="bold-stat-num mb-10">
				                        <small>
					                        <span class="stat-num campaignsio-admin-green"><small><?php echo $overview['trends']['keywords']['max_trend']; ?></small></span>
					                        <span class="stat-label">MAXIMUM NUMBER: <?php echo $overview['trends']['keywords']['max_trend_date']; ?></span>
				                        </small>
					                    </div>
									  				</div>
									  				<div class="col-lg-6">
									  					<div class="bold-stat-num mb-10">
				                        <small>
				                        	<span class="stat-num campaignsio-admin-action-color"><small><?php echo $overview['trends']['keywords']['min_trend']; ?></small></span>
				                        	<span class="stat-label">MINIMUM NUMBER: <?php echo $overview['trends']['keywords']['min_trend_date']; ?></span>
				                        </small>
				                    	</div>
									  				</div>
									  			</div>
									  			<div class="row">
									  				<div class="col-lg-12">
									  					<div class="aspect-ratio aspect-ratio--4x3">
											        	<div class="aspect-ratio--object dt tc black-70 bg-white-10 fw1 f4 f3-ns">
											        		<span class="dtc v-mid pa3">
											        			<canvas id="keywords_trend_chart"></canvas>
											        		</span>
											        	</div>
															</div>
									  				</div>
									  			</div>
									  		</div>
									  	</div>
									  </div>
									</div>
								</div>
							</div>

							<div id="2" class="tab-pane fade in" role="tabpanel">
								<?php if( ! empty( $available_values['search_engines'] ) ){ ?>
									<?php
									foreach ( $available_values['search_engines'] as $key => $val ) {
										?><li><a href="#" tile="" class="br-pill" data-engine-id="<?php echo $key; ?>"><img src="<?php  echo $val['flag']; ?>" alt="" class="br-pill" /><span><?php  echo $val['title']; ?></span></a></li><?php
									} ?>
								</ul>
								<?php } ?>
								<?php echo $positions['serp_report_html']; ?>
							</div>

							<div id="3" class="tab-pane fade <?php echo $selected_tab['3'] ? 'active' : '';?> in" role="tabpanel">
								<div class="row heading-bg" style="margin-top: 10px;">
									<div class="col-lg-12 col-sm-12">
										<div class="col-lg-3">
											<h5 class="txt-dark">KEYWORD RESEARCH</h5>
										</div>
										<?php echo form_open('domains/'.$domain['id'].'/research', array("class" => "form-vertical")); ?>
											<div class="col-lg-2">
												<select name="search_type" class="form-control select2 required" required>
													<option value="related" <?php echo $_POST['search_type'] == 'related' ? 'selected' : '';?>>Related keywords</option>
													<!-- <option value="US" <?php echo $_POST['search_type'] == 'US' ? 'selected' : '';?>>Suggestions</option> -->
													<option value="similar" <?php echo $_POST['search_type'] == 'similar' ? 'selected' : '';?>>Similar</option>
													<option value="ranked" <?php echo $_POST['search_type'] == 'ranked' ? 'selected' : '';?>>Ranked</option>
												</select>
											</div>
											<div class="col-lg-3">
												<input type="text" name="query" class="form-control" placeholder="query" value="<?php echo $_POST['query'];?>" required>
											</div>
											<div class="col-lg-2">
												<select name="se" class="form-control select2 required" required>
													<option value="GB" <?php echo $_POST['se'] == 'GB' ? 'selected' : '';?>>United Kingdom</option>
													<option value="US" <?php echo $_POST['se'] == 'US' ? 'selected' : '';?>>United States</option>
													<option value="CA" <?php echo $_POST['se'] == 'CA' ? 'selected' : '';?>>Canada</option>
													<option value="AU" <?php echo $_POST['se'] == 'AU' ? 'selected' : '';?>>Australia</option>
													<option value="DE" <?php echo $_POST['se'] == 'DE' ? 'selected' : '';?>>Germany</option>
												</select>
											</div>
											<div class="col-lg-2">
												<button type="submit" class="btn btn-success">Search</button>
											</div>
										<?php echo form_close(); ?>
									</div>
								</div>
								<div style="padding-top: 10px"></div>
								<?php if ($_POST['search_type'] && $_POST['se'] && $_POST['query']) { ?>
									<div class="table-wrap">
										<div class="table-reponsive">
											<table id="datable_3" class="table table-hover">
												<thead>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>Keyword</th>
														<th>Volume(this month)</th>
														<th>Volume(last month)</th>
														<th>CPC</th>
														<th>Competition</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>Keyword</th>
														<th>Volume(this month)</th>
														<th>Volume(last month)</th>
														<th>CPC</th>
														<th>Competition</th>
													</tr>
												</tfoot>
												<tbody>
													<?php foreach ($overview['keywords_selection'] as $key => $val) { ?>
														<tr>
															<td><?php echo ($key + 1);?></td>
															<td><?php echo $val['key'];?></td>
															<td><?php echo number_format($val['search_volume'], 0, '.', ',');?></td>
															<td><?php echo number_format($val['history'][0]['search_volume'], 0, '.', ',');?></td>
															<td><?php echo number_format($val['cpc'], 2, '.', ',');?></td>
															<td><?php echo number_format($val['competition'], 2, '.', ',');?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								<?php } ?>

							</div>

							<div id="4" class="tab-pane fade in" role="tabpanel">
								<div class="panel panel-default border-panel card-view">
									<div class="panel-heading">
										<h5>COMPARE DOMAIN WITH COMPETITOR</h5>
									</div>
									<div class="panel-wrapper">
										<div class="panel-body">
											<?php echo form_open('domains/'.$domain['id'].'/research', array("class" => "form-vertical")); ?>
												<div class="form-group">
													<div class="row">
														<div class="col-sm-3">
															<input type="text" name="domain_compare_input" placeholder="Competitor's Domain" class="form-control">
														</div>
														<div class="col-sm-3">
															<button class="btn btn-success">COMPARE</button>
														</div>
													</div>
												</div>
											<?php echo form_close(); ?>
											<span class="domains-comparison-loader serp-report-loader pt4 hidden invisible">
												<span class="loading-icon"><span><i class="material-icons">&#xE86A;</i></span></span>
											</span>
										</div>
									</div>
								</div>
						    </div>

							<div id="5" class="tab-pane fade <?php echo $selected_tab['5'] ? 'active' : '';?> in" role="tabpanel">
								<div class="panel panel-default border-panel card-view">
									<div class="panel-heading">
										<h5>SERP Tasks by Keyword</h5>
									</div>
									<div class="panel-wrapper">
										<div class="panel-body">
											<?php echo form_open('domains/'.$domain['id'].'/research', array("class" => "form-vertical")); ?>
												<div class="form-group">
													<div class="row">
														<div class="col-sm-1"></div>
														<div class="col-sm-3">
															<input type="text" name="serp_keyword" placeholder="Keyword" value="<?php echo $_POST['serp_keyword'];?>" required class="form-control">
														</div>
														<div class="col-lg-3">
															<select name="serp_se" class="form-control select2 required" required>
																<option value="en-us" <?php echo $_POST['serp_se'] == 'en-us' ? 'selected' : '';?>>google.com</option>
																<option value="en-uk" <?php echo $_POST['serp_se'] == 'en-uk' ? 'selected' : '';?>>google.co.uk</option>
																<option value="en-ca" <?php echo $_POST['serp_se'] == 'en-ca' ? 'selected' : '';?>>google.ca</option>
																<option value="en-au" <?php echo $_POST['serp_se'] == 'en-au' ? 'selected' : '';?>>google.com.au</option>
																<option value="en-ie" <?php echo $_POST['serp_se'] == 'en-ie' ? 'selected' : '';?>>google.ie</option>
															</select>
														</div>	
														<div class="col-sm-3">
															<button class="btn btn-success">SEARCH</button>
														</div>
													</div>
												</div>
											<?php echo form_close(); ?>
										</div>
									</div>
								</div>
								<div style="padding-top: 10px"></div>
								<?php if ($_POST['serp_keyword'] && $_POST['serp_se']) { ?>
									<div class="table-wrap">
										<div class="table-reponsive">
											<table id="datable_4" class="table table-hover">
												<thead>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>URL</th>
														<th>Title</th>
														<th>Result</th>
														<th>Snippet</th>
														<th>Extra</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>URL</th>
														<th>Title</th>
														<th>Result</th>
														<th>Snippet</th>
														<th>Extra</th>
													</tr>
												</tfoot>
												<tbody>
													<?php foreach ($overview['serp_result'] as $key => $val) { ?>
														<tr>
															<td><?php echo ($key + 1);?></td>
															<td><?php echo $val['result_url'];?></td>
															<td><?php echo $val['result_title'];?></td>
															<td><?php echo number_format($val['results_count'], 0, '.', ',');?></td>
															<td><?php echo $val['result_snippet'];?></td>
															<td><?php echo $val['result_extra'];?></td>
														</tr>
													<?php if (key==99) {break;}} ?>
												</tbody>
											</table>
										</div>
									</div>
								<?php } ?>
						    </div>

							<div id="6" class="tab-pane fade <?php echo $selected_tab['6'] ? 'active' : '';?> in" role="tabpanel">
								<?php if (count($overview['keywords_for_domain']) == 0) { ?>
									<div class="panel panel-default border-panel card-view">
									<?php if (!$overview['add_keywords_domain']) { ?>
										<div class="panel-heading">
											<span>Add <strong><?php echo $overview['domain_url'];?></strong> to DataForSEO for keywords suggestions</span>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<a class="btn btn-success" href="<?php echo (base_url() .'domains/'. $overview['domain_id'] . '/research?add_keywords_domain=1') ;?>">ADD</a>
											</div>
										</div>
									</div>
									<?php } else { ?>
										<h6>Comming soon...</h6>
									<?php } ?>
								<?php } else { ?>
									<div class="table-wrap">
										<div class="table-reponsive">
											<table id="datable_5" class="table table-hover">
												<thead>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>KEY</th>
														<th>CMP</th>
														<th>CPC</th>
														<th>SV</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>KEY</th>
														<th>CMP</th>
														<th>CPC</th>
														<th>SV</th>
													</tr>
												</tfoot>
												<tbody>
													<?php
													foreach ($overview['keywords_for_domain'] as $key => $val) { ?>
														<tr>
															<td><?php echo ($key + 1);?></td>
															<td><?php echo $val['key'];?></td>
															<td><?php echo number_format($val['cmp'], 2, '.', ',');?></td>
															<td><?php echo number_format($val['cpc'], 2, '.', ',');?></td>
															<td><?php echo number_format($val['sv'], 2, '.', ',');?></td>
														</tr>
													<?php if (key==99) {break;}} ?>
												</tbody>
											</table>
										</div>
									</div>
								<?php } ?>
							</div>

							<div id="7" class="tab-pane fade <?php echo $selected_tab['7'] ? 'active' : '';?> in" role="tabpanel">
								<?php if (count($overview['keywords_for_keywords']) == 0) { ?>
									<div class="panel panel-default border-panel card-view">
									<?php if (!$overview['add_keywords_keywords']) { ?>
										<div class="panel-heading">
											<span>Add a keyword to DataForSEO for keywords suggestions</span>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<?php echo form_open('domains/'.$domain['id'].'/research', array("class" => "form-vertical")); ?>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-1"></div>
															<div class="col-sm-3">
																<input type="text" name="serp_keyword" placeholder="Keyword" value="<?php echo $_POST['serp_keyword'];?>" required class="form-control">
															</div>	
															<div class="col-sm-3">
																<button name="serp_keywords_keywords_add" class="btn btn-success">ADD</button>
															</div>
														</div>
													</div>
												<?php echo form_close(); ?>
											</div>
										</div>
									</div>
									<?php } else { ?>
										<h6>Comming soon...</h6>
									<?php } ?>
								<?php } else { ?>
									<div class="table-wrap">
										<div class="table-reponsive">
											<table id="datable_7" class="table table-hover">
												<thead>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>KEY</th>
														<th>CMP</th>
														<th>CPC</th>
														<th>SV</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th data-sortable="false" class="tl">#</th>
														<th>KEY</th>
														<th>CMP</th>
														<th>CPC</th>
														<th>SV</th>
													</tr>
												</tfoot>
												<tbody>
													<?php
													foreach ($overview['keywords_for_keywords'] as $key => $val) { ?>
														<tr>
															<td><?php echo ($key + 1);?></td>
															<td><?php echo $val['key'];?></td>
															<td><?php echo number_format($val['cmp'], 2, '.', ',');?></td>
															<td><?php echo number_format($val['cpc'], 2, '.', ',');?></td>
															<td><?php echo number_format($val['sv'], 2, '.', ',');?></td>
														</tr>
													<?php if (key==99) {break;}} ?>
												</tbody>
											</table>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
$( document ).ready(function() {
	$('#datable_3').DataTable();
	$('#datable_4').DataTable();
	$('#datable_5').DataTable();
	$('#datable_7').DataTable();
});
</script>

<?php echo form_hidden( 'domain_compare_url', $domain_compare_url ); ?>
<?php echo form_hidden( 'competitors_graph_data', $overview['competitors_graph_data'] ); ?>
<?php echo form_hidden( 'keyword_position_distribution', $overview['keyword_position_distribution'] ); ?>


<?php echo form_hidden( 'trends_date_data', json_encode( $overview['trends']['dates'] ) ); ?>
<?php echo form_hidden( 'trends_visibility_data', json_encode( $overview['trends']['visibility']['results'] ) ); ?>
<?php echo form_hidden( 'trends_keywords_data', json_encode( $overview['trends']['keywords']['results'] ) ); ?>

<?php 
function number_format_short( $n, $precision = 1 ) {
	if ($n < 999) {
		$n_format = number_format($n, $precision);
		$suffix = '';
	} else if ($n < 999999) {
		$n_format = number_format($n / 1000, $precision);
		$suffix = 'K';
	} else if ($n < 999999999) {
		$n_format = number_format($n / 1000000, $precision);
		$suffix = 'M';
	} else if ($n < 999999999999) {
		$n_format = number_format($n / 1000000000, $precision);
		$suffix = 'B';
	} else {
		$n_format = number_format($n / 1000000000000, $precision);
		$suffix = 'T';
	}
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
	if ( $precision > 0 ) {
		$dotzero = '.' . str_repeat( '0', $precision );
		$n_format = str_replace( $dotzero, '', $n_format );
	}
	return $n_format . $suffix;
}
?>