<?php
// print_r( $ecom_data );
?>

<div class="row mt-15">
  <div class="col-lg-12">
    <div class="panel panel-default card-view">
      <div class="panel-wrapper">
        <div class="panel-body">
          <div class="col" style="display: flex; flex: 1; flex-flow: row nowrap;">
            <div class="col-info">
              <div class="top">
                <span class="title">Traffic <i class="ds-refresh"></i></span>
              </div>
              <?php 
                $traffic_num = $ecom_data['total_traffic'] - $ecom_data['total_traffic_prev'];
               
               ?>

              <div class="bottom">
                <span class="data"><?php echo $ecom_data['total_traffic']; ?></span> 
                <span class="diff <?php if($traffic_num<0)echo 'down';else echo 'up'; ?>"> <?php echo round($traffic_num,2); ?></span>
              </div>
            </div>
            <div class="col-info">
              <div class="top">
                <span class="title">Transactions <i class="ds-refresh"></i></span>
              </div>
              <?php 
                $trans_num = $ecom_data['current_month']['total_orders'] - $ecom_data['last_month']['total_orders'];
               ?>

              <div class="bottom">
                <span class="data"><?php echo $ecom_data['current_month']['total_orders']; ?></span> <span class="diff <?php if($trans_num>0)echo 'up';else echo 'down'; ?>"><?php echo round($trans_num,2); ?></span>
              </div>
            </div>
            <div class="col-info">
              <div class="top">
                <span class="title">Conversion Rate <i class="ds-refresh"></i></span>
              </div>

              <?php 
                if($ecom_data['current_month']['total_orders'] !=0) {
                  $curent_month_converstion = round($ecom_data['total_traffic']/$ecom_data['current_month']['total_orders'],2);
                  $last_month_converstion = round($ecom_data['total_traffic_prev']/$ecom_data['last_month']['total_orders'],2);
                  $conversion_diff = $current_month_conversion - $last_month_converstion;  
                } else {
                  $conversion_diff = 0;
                }
                
               ?>

              <div class="bottom">
                <span class="data"><?php echo number_format($ecom_data['total_traffic']/max($ecom_data['current_month']['total_orders'],2),1); ?>%</span> <span class="diff <?php if($conversion_diff<0)echo 'down';else echo 'up'; ?>"> <?php echo $conversion_diff; ?></span>
              </div>
            </div>
            <div class="col-info">
              <div class="top">
                <span class="title">Abandonded Cart <i class="ds-refresh"></i></span>
              </div>
              <div class="bottom">
                <span class="data"></span> <span class="diff up"></span>
              </div>
            </div>
            <div class="col-info">
              <div class="top">
                <span class="title">Average order value <i class="ds-refresh"></i></span>
              </div>
              <div class="bottom">
                <?php 

                   $average_per_num = ($ecom_data['current_month']['total_sales']/max($ecom_data['current_month']['total_orders'],1)) - ($ecom['last_month']['average_sales']/max($ecom_data['last_month']['total_orders'],1));


                   $average_per = (abs($average_per_num*100)) / max($ecom_data['last_month']['total_sales'],1); 
                 ?>

                <span class="data"><?php echo round($ecom_data['current_month']['total_sales']/max($ecom_data['current_month']['total_orders'],1)); ?></span> <span class="diff <?php if($average_per_num<0)echo 'down';else echo 'up'; ?>"><?php echo round($average_per,2); ?>%</span>

              </div>
            </div>
          </div>
          <div class="revenue-box">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col" style="display: flex; flex: 1; flex-wrap: wrap; margin: 0 -5px">
                  <div class="revenue-box-child">
                    <span class="top">Total Sales</span>
                    <span class="bottom"><?php echo $ecom_data['current_month']['total_sales']; ?></span>
                  </div>
                  <div class="revenue-box-child">
                    <span class="top">Last Month</span>
                    <?php 
                      if ($ecom_data['last_month']['total_sales'] !=0) {
                        $calc = $ecom_data['current_month']['total_sales'] - $ecom_data['last_month']['total_sales'];
                        $x = (abs($calc)*100)/$ecom_data['last_month']['total_sales'];
                      } else {
                        $x = 0;
                      }
                      
                     ?>
                    <span class="bottom"><?php echo $ecom_data['last_month']['total_sales']; ?> <span class="diff <?php if($calc > 0)echo 'up';else echo 'down'; ?>"><?php echo round($x,2); ?>%</span></span>
                  </div>
                  <div class="revenue-box-child">
                    <span class="top">Refunds</span>
                    <span class="bottom"><?php echo $ecom_data['current_month']['total_refunds']; ?></span>
                  </div>
                  <div class="revenue-box-child">
                    <span class="top">Shipping</span>
                    <span class="bottom"><?php echo $ecom_data['current_month']['total_shipping']; ?></span>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
              </div>
            </div>
          </div>

          <div  class="tab-struct custom-tab-1 mt-20">
            <ul role="tablist" class="nav nav-tabs" id="myTabs_7">
              <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="com_tab_overview" href="#com_overview">Overview</a></li>
              <li role="presentation" class=""><a  data-toggle="tab" id="com_tab_customers" role="tab" href="#com_customers" aria-expanded="false">Customers</a></li>
              <li role="presentation" class=""><a  data-toggle="tab" id="com_tab_traffic" role="tab" href="#com_traffic" aria-expanded="false">Traffic Sources</a></li>
              <li role="presentation" class=""><a  data-toggle="tab" id="com_tab_products" role="tab" href="#com_products" aria-expanded="false">Products</a></li>
              <li role="presentation" class=""><a  data-toggle="tab" id="com_tab_revenue" role="tab" href="#com_revenue" aria-expanded="false">Daily Revenue</a></li>
              <li role="presentation" class=""><a  data-toggle="tab" id="com_tab_gateways" role="tab" href="#com_gateways" aria-expanded="false">Payment Gateways</a></li>
            </ul>
            <div class="tab-content" id="myTabContent_7">
              <div id="com_overview" class="tab-pane fade active in" role="tabpanel">
                <div class="table-wrap">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered mb-0">
                      <thead>
                        <tr>
                          <th>PRODUCT NAME</th>
                          <th>REVENUE</th>
                          <th>QUANTITY</th>
                          <th>ORDERS</th>
                          <th>AVERAGE PRICE</th>
                          <th>AVERAGE QUALITY</th>
                          <th>CONVERSION RATE</th>
                        </tr>
                      </thead>
                      <tbody><?php
                          if( empty( $product_data ) ){
                            ?><tr><td colspan="7">No data found</td></tr><?php
                          }
                          else{
                            foreach($product_data as $k => $v){ ?>
                            <tr>
                              <td><?php echo $v['label']; ?></td>
                              <td><?php echo $v['revenue']; ?></td>
                              <td><?php echo $v['quantity']; ?></td>
                              <td><?php echo $v['orders']; ?></td>
                              <td><?php echo $v['avg_price']; ?></td>
                              <td><?php echo $v['avg_quantity']; ?></td>
                              <td><?php echo $v['conversion_rate']; ?></td>
                            </tr><?php
                          }
                        } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div id="com_customers" class="tab-pane fade" role="tabpanel">
                <div class="table-wrap">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered mb-0">
                      <thead>
                        <tr>
                          <th>REFERRER TYPE</th>
                          <th>VISITS</th>
                          <th>ECOMERCE ORDERS</th>
                          <th>TOTAL REVENUE</th>
                          <th>ECOMMERCE ORDER CONVERSION RATE</th>
                          <th>AVERGAE ORDER VALUE</th>
                          <th>PURCHASED PRODUCTS</th>
                          <th>REVENUE PER VISIT</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="8">N/A</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div id="com_traffic" class="tab-pane fade" role="tabpanel">
                <div class="table-wrap">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered mb-0">
                      <thead>
                        <tr>
                          <th>REFERRER TYPE</th>
                          <th>VISITS</th>
                          <th>ECOMERCE ORDERS</th>
                          <th>TOTAL REVENUE</th>
                          <th>ECOMMERCE ORDER CONVERSION RATE</th>
                          <th>AVERGAE ORDER VALUE</th>
                          <th>PURCHASED PRODUCTS</th>
                          <th>REVENUE PER VISIT</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="8">N/A</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?php echo form_hidden( 'id_total_visits_data', json_encode( $domain['total_visits'] ) ); ?>
