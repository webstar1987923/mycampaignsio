<?php 
$facebook_tab = 'facebook_tab' . $domain['id'];
$fb_oauth_login = 'fb_oauth_login' . $domain['id'];

$users = 0;
foreach ($domain['unique_visits'] as $key => $value) {
	$users = $users + $value;
}

$labels = array();
$userData = array();
$i=0;
foreach ($domain['unique_visits'] as $key => $value) {
	if ($value > 0) {
		$d = date_parse_from_format("Y-m-d", $key);
		if ($d["day"]) {
			$labels[$i] = $d["day"];
		} else {
			$labels[$i] = date('Y-m-d',strtotime('this day'));
		}
		
		$userData[$i] = $value;
		$i++;
	}
}

$channels = array();
$sessionsByChannel = array();
$i=0;
foreach ($domain['deviceType'] as $arr) {
	foreach ($arr as $key => $value) {
		if ($value > 0) {
			$channels[$i] = $value['label'];
			$sessionsByChannel[$i] = $value['nb_visits'];
			$i++;
		}
	}
	break;
}

$referrers = 0;
foreach ($domain['referrers'] as $arr) {
	foreach ($arr as $key => $value) {
		$referrers = $referrers + $value['nb_visits'];
	}
	break;
}

foreach ($domain['rangeData'] as $key => $value) {
	$page_view = $value['nb_pageviews'];
	$sessions = $value['nb_visits'];
	$bounce_rate = $value['bounce_rate'];
	$page_per_visit = $value['nb_actions_per_visit'];
	$conversion_count = $value['nb_conversions'];
	if ($sessions > 0) {
		$conversion_rate = $value['nb_conversions'] / $sessions * 100;
	} else {
		$conversion_rate = 'n/a';
	}
	$seconds = $value['avg_time_on_site'];
	break;
}

foreach ($domain['goals'] as $key => $value) {
	$revenue = $value['revenue'];
	break;
}

$minutes = floor($seconds/60);
$seconds = $seconds%60;
$hours = floor($minutes/60);
$minutes = $minutes%60;
$duration = '';
if($hours > 0) $duration = $hours . 'hrs ';
if($minutes > 0) $duration = $duration . $minutes . ' min ';
if($seconds > 0) $duration = $duration . $seconds . 's';
if($duration == '') $duration = 'n/a';

?>
<div class="row mt-10">
	<div class="col-lg-12 col-sm-12">
		<div class="panel panel-default panel-analytics card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div  class="tab-struct custom-tab-1">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<li <?php if (!$_SESSION[$facebook_tab]) {echo('class="active"');}?> role="presentation" class=""><a  data-toggle="tab" id="tab_google" role="tab" href="#google" aria-expanded="false">Analytics</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_search" role="tab" href="#search" aria-expanded="false">Search Console</a></li>
							<li <?php if ($_SESSION[$facebook_tab]) {echo('class="active"');}?> role="presentation" class=""><a  data-toggle="tab" id="tab_facebook" role="tab" href="#facebook" aria-expanded="false">Facebook</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_facebook_page" role="tab" href="#facebook_page" aria-expanded="false">Facebook Page</a></li>

							<div class="pull-right">
								<?php if( isset( $page_data_disabled_ga ) && $page_data_disabled_ga ){ ?>
								<span class="mv3-s ml0-s fl w-100 w-auto-ns fr-ns"><small class="dib mv3 mv0-ns mr2" style="color:#c9c9c9;">Google account is disabled</small> 
									<?php if( isset( $page_data_edit_settings_link ) && $page_data_edit_settings_link ){ ?>
									<a href="<?php echo $page_data_edit_settings_link; ?>" title="" class="btn btn-xs btn-primary btn-rounded"><span class="white">EDIT SETTINGS</span></a></span>
									<?php } ?>
								<?php } ?>

								<?php if( isset( $page_data_last_update ) && $page_data_last_update ){ ?>
								<span class="mv3-s ml0-s fl w-100 w-auto-ns fr-ns"><small class="dib mv3 mv0-ns mr2" style="color:#c9c9c9;">Last analytics check: <?php echo $page_data_last_update; ?></small> 
									<?php if( isset( $page_data_update_now_link ) && $page_data_update_now_link ){ ?>
									<a href="<?php echo $page_data_update_now_link; ?>" title="" class="btn btn-xs btn-success btn-rounded"><span class="white">CHECK NOW</span></a></span>
									<?php } ?>
								<?php } ?>
							</div>
						</ul>
						<div class="tab-content" id="myTabContent_7">
							<div id="google" class="tab-pane fade <?php if (!$_SESSION[$facebook_tab]) {echo('active');}?> in" role="tabpanel">
								<div class="row">
								    <div class="col-lg-4 col-md-4 col-sm-6">
								        <div class="panel panel-default panel-kpi border-panel card-view">
								            <div class="panel-heading">
							                <div class="pull-left"><h5>Audience Overview </h5></div>
							                <div class="pull-right">
							                	<div class="dropdown inline-block">
							                		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button">
							                			<span>
																			<?php $current_page = $_SERVER['QUERY_STRING'];  ?>
																			<?php 
																			switch ($current_page) {
																			    case "period=year&date=1":
																			        echo "Year to Date";
																			        break;
																			    case "period=month&date=1":
																			        echo "This Month";
																			        break;
																			    case "period=week&date=1":
																			        echo "This Week";
																			        break;
																			    case "period=day&date=1":
																			        echo "Today";
																			        break;
																			    case "period=month&date=2":
																			        echo "Last Month";
																			        break;
																			    default:
																			        echo "Month to date";
																			}?>
																		</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
							                		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
							                			<li role="presentation"><a href="<?php echo base_url('domains/' . $domain['id'] . '/analytics/?period=year&date=1');?>" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Year to Date</a></li>
							                			<li role="presentation"><a href="<?php echo base_url('domains/' . $domain['id'] . '/analytics/?period=month&date=1');?>" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>This Month</a></li>
							                			<li role="presentation"><a href="<?php echo base_url('domains/' . $domain['id'] . '/analytics/?period=week&date=1');?>" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>This Week</a></li>
							                			<li role="presentation"><a href="<?php echo base_url('domains/' . $domain['id'] . '/analytics/?period=day&date=1');?>" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Today</a></li>
							                			<li role="presentation"><a href="<?php echo base_url('domains/' . $domain['id'] . '/analytics/?period=month&date=2');?>" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last Month</a></li>
							                		</ul>
							                	</div>
							                </div>
							                <div class="clearfix"></div>
								            </div>
								            <div class="panel-wrapper collapse in">
								            	<div class="panel-body row pt-0 pb-0">
								            		<div class="table-wrap">
								            			<div class="table-responsive do-nicescroll" style="height: 340px;">
								            				<table class="table mb-0 scroll-me">
								            					
								          						<tbody>
								          						  <tr>
								          							<td class="font-16 txt-dark">Users</td>
								          							<td class="font-18 txt-dark"><?php echo $users != 0 ? number_format($users, 0, '.', ',') : 'n/a';?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Sessions</td>
								          							<td class="font-18 txt-dark"><?php echo $sessions != 0 ? number_format($sessions, 0, '.', ',') : 'n/a';?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Pages / Session</td>
								          							<td class="font-18 txt-dark"><?php echo $page_per_visit != 0 ? number_format($page_per_visit, 1, '.', ',') : 'n/a';?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Avg. Session Duration</td>
								          							<td class="font-18 txt-dark"><?php echo $duration;?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Bounce Rate</td>
								          							<td class="font-18 txt-dark"><?php echo $bounce_rate != 0 ? number_format($bounce_rate, 0, '.', ',') . '%' : 'n/a';?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Pageviews</td>
								          							<td class="font-18 txt-dark"><?php echo $page_view != 0 ? number_format($page_view, 0, '.', ',') : 'n/a';?></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-16 txt-dark">Conversion Rate</td>
								          							<td class="font-18 txt-dark"><?php echo $conversion_rate != 0 ? number_format($conversion_rate, 1, '.', ',') . '%' : 'n/a';?></td>
								          						  </tr>
								          						</tbody>
								            				</table>
								            			</div>
								            		</div>
								            	</div>
								            </div>
								        </div>
										<div class="panel panel-default panel-kpi border-panel card-view">
											<div class="panel-heading">
												<div class="pull-left"><h5>Referring sites</h5></div>
												<div class="clearfix"></div>
											</div>
											<div class="panel-wrapper collapse in">
												<div class="panel-body row pt-0 pb-0">
													<div class="table-wrap">
														<div class="table-responsive do-nicescroll2" style="height: 244px;">
															<table class="table mb-0 scroll-me2">
																<tbody class="subwide-verticle">
																<?php
																	foreach ($domain['websites'] as $websites) {
																		foreach ($websites as $key => $value) {
																			?>
																			<tr>
																				<td class="font-12 txt-dark dont-break-out" ><?php echo $value['label'];?></td>
																				<td class="font-18 txt-dark"><?php echo number_format($value['nb_visits'], 0, '.', ',');?></td>
																				<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> <?php echo number_format(($value['nb_visits']/$referrers*100), 1, '.', ',');?>%</span></td>
																			</tr>
																		<?php
																		}
																		break;
																		}
																		?>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
								    </div>

								    <div class="col-lg-3 col-md-3 col-sm-6">
		    	        		<div class="panel panel-default panel-kpi border-panel card-view">
		    	        			 <div class="panel-heading">
		    	        			 	<h5 class="text-center">Users</h5>
		    	        			 </div>
		    	        			 <div class="panel-wrapper">
		    	        			 	<div class="panel-body">
		    	        			 		<div class="col text-center mt-0 mb-10">
		    	        			 			<span class="font-24 txt-dark text-center" style="line-height: 0;"><?php echo number_format($users, 0, '.', ',');?></span>
		    	        			 		</div>
		    	        			 		<div>
		    											<canvas id="chart_users_sessions" height="217"></canvas>	
		    										</div>

		    										<!-- <ul class="flex-stat google-stat mt-10">
		    											<li>
		    												<span class="block txt-dark font-18 mb-10">Users</span>
		    												<span class="block txt-success weight-500 font-18"><i class="zmdi zmdi-caret-up font-21 ml-5 vertical-align-middle"></i><span class="counter-anim"> 11.1</span>%</span>
		    											</li>
		    											<li>
		    												<span class="block txt-dark font-18 mb-10">Sessions</span>
		    												<span class="block txt-success weight-500 font-18"><i class="zmdi zmdi-caret-up font-21 ml-5 vertical-align-middle"></i><span class="counter-anim"> 11.1</span>%</span>
		    											</li>
		    										</ul> -->
		    	        			 	</div>
		    	        			 </div>
		    	        		</div>

		    	        		<div class="panel panel-default panel-kpi card-view">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Sessions By Channel</span>	
		    										<canvas id="chart_sessions_by_channel" height="300" class="mt-20 mb-15"></canvas>
		    									</div>
		    	              </div>
		    	          	</div>
								    </div>

								    <div class="col-lg-2 col-md-2 col-sm-6">
		    							<div class="panel panel-default panel-kpi card-view bg-blue" style="height: 169px">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Goal Overview</span>	
		    										<ul class="flex-stat google-stat mt-30">
		    											<li>
		    												<span class="block txt-dark font-18 mb-10"><?php echo $conversion_rate != 0 ? number_format($conversion_rate, 1, '.', ',') . '%' : 'n/a';?></span>
		    											</li>
		    											<li>
		    												<span class="block txt-dark font-18 mb-10"><?php echo $conversion_count != 0 ? number_format($conversion_count, 0, '.', ',') : 'n/a';?></span>
		    											</li>
		    										</ul>
		    									</div>
		    	              </div>
		    	          	</div>

        							<div class="panel panel-default panel-kpi card-view bg-red" style="height: 170px">
        								<div class="panel-wrapper collapse in">
        	                <div class="panel-body sm-data-box-1">
        										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Bounce Rate</span>	
        										<div class="text-center medium-point txt-dark mt-20">
        											<span class="counter-anim "><?php echo $bounce_rate != 0 ? number_format($bounce_rate, 0, '.', ',') . '%' : 'n/a';?></span>
        										</div>
        									</div>
        	              </div>
        	          	</div>

        	          	<div class="panel panel-default panel-kpi card-view bg-green" style="height: 170px">
												<div class="panel-wrapper collapse in">
					                <div class="panel-body sm-data-box-1">
														<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Avg. Pages/Session</span>	
														<div class="text-center medium-point txt-dark mt-20">
															<span class="counter-anim"><?php echo $page_per_visit != 0 ? number_format($page_per_visit, 1, '.', ',') : 'n/a';?></span>
														</div>
													</div>
					              </div>
					          	</div>

					          	<div class="panel panel-default panel-kpi card-view" style="height: 170px">
												<div class="panel-wrapper collapse in">
					                <div class="panel-body sm-data-box-1">
														<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Avr. Session Duration</span>	
														<div class="text-center font-24 txt-dark mt-5">
															<span class="counter-anim"><?php echo $duration;?></span>
														</div>
													</div>
					              </div>
					          	</div>
								    </div>

								    <div class="col-lg-3 col-md-3 col-sm-6">
											<div class="panel panel-default panel-kpi card-view bg-blue">
		    	        			<div class="panel-heading">
		    	        				<h5 class="text-center">Goal Income</h5>
		    	        				<div class="clearfix"></div>
		    	        			</div>
		    	        			<div class="panel-wrapper collapse in">
		    	        				<div  class="panel-body">
											<div class="text-center medium-point txt-dark mt-10">
												<?php if ($revenue != 0) { ?>
												<span class="font-24"><?php echo $revenue != 0 ? '�' . number_format($revenue, 2, '.', ',') : 'n/a';?></span>
												<?php } else { ?>
													<a data-target="#addgoal" data-toggle="modal" class="btn btn-xs btn-success"><span class="white">Add Goal</span></a>
												<?php } ?>
											</div>
		    	        				</div>
		    	        			</div>
		    	        		</div>

		    	        		<div class="panel panel-default panel-kpi border-panel card-view">
		    	        		    <div class="panel-heading">
		    	      		        <h5 class="text-center">Pages by Pageviews </h5>
		    	      		        <div class="clearfix"></div>
		    	        		    </div>
		    	        		    <div class="panel-wrapper collapse in">
		    	        		    	<div class="panel-body row pt-0 pb-0">
		    	        		    		<div class="table-wrap">
		    	        		    			<div class="table-responsive do-nicescroll2" style="height: 505px;">
		    	        		    				<table class="table mb-0 scroll-me2">
		    	        		    					<tbody class="subwide-verticle">
																<?php
																	foreach ($domain['pageUrls'] as $pageUrls) {
																		foreach ($pageUrls as $value) {
																?>
																	<tr>
																		<td class="font-12 txt-dark dont-break-out" ><?php echo $value['label'];?></td>
																		<td class="font-18 txt-dark"><?php echo number_format($value['nb_hits'], 0, '.', ',');?></td>
																		<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> <?php echo number_format(($value['nb_hits']/$page_view*100), 1, '.', ',');?>%</span></td>
																	</tr>
																<?php
																		}
																		break;
																	}
																?>
		    	        		    					</tbody>
		    	        		    				</table>
		    	        		    			</div>
		    	        		    		</div>
		    	        		    	</div>
		    	        		    </div>
		    	        		</div>

								    </div>
								</div>
							</div>
							<div id="search" class="tab-pane fade in" role="tabpanel">
								<div class="row">
								    <div class="col-lg-4 col-md-4 col-sm-12">
								        <div class="panel panel-default panel-search-console border-panel card-view">
								            <div class="panel-heading">
								                <div class="pull-left"><h5>Query(By Clicks) </h5></div>
								                <div class="clearfix"></div>
								            </div>
								            <div class="panel-wrapper collapse in">
								            	<div class="panel-body row pt-0 pb-0">
								            		<div class="table-wrap">
								            			<div class="table-responsive do-nicescroll" style="height: 669px;">
								            				<table class="table mb-0 scroll-me">
								            					<thead>
								            					  <tr>
																					<th class="font-10">Query</th>
																					<th class="font-10">Clicks</th>
								            					  </tr>
								            					</thead>
								          						<tbody>
																				<?php foreach ($domain['console_query_by_clicks'] as $key => $value) { ?>
																					<tr>
																						<td class="font-16 txt-dark"><?php echo $value['query'];?></td>
																						<td class="font-18 txt-dark"><?php echo $value['clicks'];?></td>
																					</tr>
																				<?php } ?>
								          						</tbody>
								            				</table>
								            			</div>
								            		</div>
								            	</div>
								            </div>
								        </div>
								    </div>

								    <div class="col-lg-8 col-md-8 col-sm-12">
								        <div class="row">
								        	<div class="col-lg-5 col-md-5 col-sm-12">
								        		<div class="panel panel-default panel-search-console border-panel card-view">
								        		    <div class="panel-heading">
								      		        <div class="pull-left"><h5>Pages(By Clicks) </h5></div>
								      		        <div class="clearfix"></div>
								        		    </div>
								        		    <div class="panel-wrapper collapse in">
								        		    	<div class="panel-body row pt-0 pb-0">
								        		    		<div class="table-wrap">
								        		    			<div class="table-responsive do-nicescroll2" style="height: 553px;">
																				<table class="table mb-0 scroll-me2">
								        		    					<thead>
								        		    					  <tr>
																							<th class="font-10">Query</th>
																							<th class="font-10">Clicks</th>
								        		    					  </tr>
								        		    					</thead>
																					<tbody>
																						<?php foreach ($domain['console_page_by_clicks'] as $key => $value) { ?>
																							<tr>
																								<td class="font-12 txt-dark"><?php echo $value['page'];?></td>
																								<td class="font-16 txt-dark"><?php echo $value['clicks'];?></td>
																							</tr>
																						<?php
																						if ($key == 19) break;
																						}
																						?>
																					</tbody>
								        		    				</table>
								        		    			</div>
								        		    		</div>
								        		    	</div>
								        		    </div>
								        		</div>

								        		<div class="row">
								        			<div class="col-lg-6 col-sm-6">
																<div class="panel panel-default panel-search-console card-view">
																	<div class="panel-wrapper collapse in">
										                <div class="panel-body sm-data-box-1">
																			<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Impressions</span>	
																			<div class="text-center font-24 txt-dark mt-5">
																				<span class="counter-anim"><?php echo $domain['search_console_avg']['impressions']; ?></span>
																			</div>
																		</div>
										              </div>
										          	</div>
								        			</div>
								        			<div class="col-lg-6 col-sm-6">
																<div class="panel panel-default panel-search-console card-view">
																	<div class="panel-wrapper collapse in">
										                <div class="panel-body sm-data-box-1">
																			<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Clicks</span>	
																			<div class="text-center font-24 txt-dark mt-5">
																				<span class="counter-anim"><?php echo $domain['search_console_avg']['clicks']; ?></span>
																			</div>
																		</div>
										              </div>
										          	</div>
								        			</div>
								        		</div>
								        	</div>
								        	<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="panel panel-default panel-search-console card-view">
															<div class="panel-wrapper collapse in">
																<div class="panel-body sm-data-box-1">
																	<h5 class="text-center">Crawl Errors</h5>
																	<div class="text-center font-24 txt-dark mt-5">
																		<?php foreach ($domain['console_crawl_errors'] as $value) {
																			if($value['count'] == 0) continue;
																		?>
																			<span class="font-16"><?php echo $value['count'] . ' ' . $value['category'];?>&nbsp&nbsp</span>
																		<?php
																		}?>
																	</div>
																</div>
															</div>
														</div>

								        		<div class="panel panel-default panel-search-console border-panel card-view">
								        			 <div class="panel-heading">
								        			 	<h5 class="text-center">Clicks</h5>
								        			 </div>
								        			 <div class="panel-wrapper">
								        			 	<div class="panel-body">
								        			 		<div class="col text-center mt-15 mb-10">
								        			 			<span class="temprature text-center" style="line-height: 0;">595</span>
								        			 		</div>
								        			 		<div>
																		<canvas id="searchConsoleChart" height="254"></canvas>	
																	</div>
								        			 	</div>
								        			 </div>
								        		</div>

								        		<div class="row">
								        			<div class="col-lg-4  col-sm-6">
																<div class="panel panel-default panel-search-console card-view">
																	<div class="panel-wrapper collapse in">
										                <div class="panel-body sm-data-box-1">
																			<span class="uppercase-font weight-500 font-14 block text-center txt-dark">CTR</span>	
																			<div class="text-center font-24 txt-dark mt-5">
																				<span class="counter-anim"><?php echo $domain['search_console_avg']['average_ctr'] * 100; ?></span><span>%</span>
																			</div>
																		</div>
										              </div>
										          	</div>
								        			</div>
								        			<div class="col-lg-4 col-sm-6">
																<div class="panel panel-default panel-search-console card-view">
																	<div class="panel-wrapper collapse in">
										                <div class="panel-body sm-data-box-1">
																			<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Avr. Position</span>	
																			<div class="text-center font-24 txt-dark mt-5">
																				<span class="counter-anim"><?php echo $domain['search_console_avg']['positions']; ?></span>
																			</div>
																		</div>
										              </div>
										          	</div>
								        			</div>
								        		</div>
								        	</div>
								        </div>
								    </div>
								</div>
							</div>
							<div id="facebook" class="tab-pane fade <?php if ($_SESSION[$facebook_tab]) {echo('active');}?> in" role="tabpanel">
								<?php if ($_SESSION[$fb_oauth_login]) {
								?>
								<div class="row">
								    <div class="col-lg-4">
								        <div class="panel panel-default panel-facebook border-panel card-view">
						        			<div class="panel-heading">
						        				<div class="pull-left"><h6>Page Visits </h6></div>
						    	            <div class="pull-right">
						    	            	<div class="dropdown inline-block">
						    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
						    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
						    	            		</ul>
						    	            	</div>
						    	            </div>
						    	            <div class="clearfix"></div>
						        			 </div>
						        			 <div class="panel-wrapper">
						        			 	<div class="panel-body">
						        			 		<div class="col text-center mt-15 mb-10">
						        			 			<span class="temprature text-center" style="line-height: 0;">595</span>
						        			 			<span class="text-center txt-success"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 11.1%</span>
						        			 		</div>
						        			 		<div>
																<canvas id="chart_page_visit" height="201"></canvas>
															</div>
						        			 	</div>
						        			 </div>
						        		</div>
								    </div>

								    <div class="col-lg-3">
								    	<div class="panel panel-default panel-facebook border-panel card-view">
								            <div class="panel-heading">
								                <div class="pull-left"><h6>Page Summary </h6></div>
								                <div class="pull-right">
								                	<div class="dropdown inline-block">
								                		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
								                		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
								                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
								                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
								                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
								                		</ul>
								                	</div>
								                </div>
								                <div class="clearfix"></div>
								            </div>
								            <div class="panel-wrapper collapse in">
								            	<div class="panel-body row pt-0 pb-0">
								            		<div class="table-wrap">
								            			<div class="table-responsive do-nicescroll" style="height: 320px;">
								            				<table class="table mb-0 scroll-me">
								            					<thead>
								            					  <tr>
								            						<th class="font-10">Metric</th>
								            						<th class="font-10">Month to Date</th>
								            						<th class="font-10"><span class="pull-right">%</span></th>
								            					  </tr>
								            					</thead>
								          						<tbody>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						</tbody>
								            				</table>
								            			</div>
								            		</div>
								            	</div>
								            </div>
								        </div>
								    </div>

								    <div class="col-lg-3">
								    		<div class="panel panel-default panel-facebook border-panel card-view">
						    	        <div class="panel-heading">
						    	            <div class="pull-left"><h6>Post Overview </h6></div>
						    	            <div class="pull-right">
						    	            	<div class="dropdown inline-block">
						    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
						    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
						    	            		</ul>
						    	            	</div>
						    	            </div>
						    	            <div class="clearfix"></div>
						    	        </div>
						    	        <div class="panel-wrapper collapse in">
						    	        	<div class="panel-body row pt-0 pb-0">
						    	        		<div class="table-wrap">
						    	        			<div class="table-responsive do-nicescroll" style="height: 320px;">
						    	        				<table class="table mb-0 scroll-me">
						    	        					<thead>
						    	        					  <tr>
						    	        						<th class="font-10">Metric</th>
						    	        						<th class="font-10">Month to Date</th>
						    	        						<th class="font-10"><span class="pull-right">%</span></th>
						    	        					  </tr>
						    	        					</thead>
						    	      						<tbody>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						    	      						  </tr>
						    	      						</tbody>
						    	        				</table>
						    	        			</div>
						    	        		</div>
						    	        	</div>
						    	        </div>
						    	    </div>
								    </div>

								    <div class="col-lg-2">
		    							<div class="panel panel-default panel-facebook card-view" style="height: 180px">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Page Likes</span>	
		    										<div class="block text-center">
		    	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
		    										</div>	
		    										<div class="text-center font-24 txt-dark mt-5">
		    											<span class="temprature counter-anim">550</span>
		    										</div>
		    									</div>
		    	              </div>
		    	          	</div>

		    	          	<div class="panel panel-default panel-facebook card-view" style="height: 185px">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Page Likes/Unlikes</span>	
		    										<div class="block text-center">
		    	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
		    										</div>	
		    										<div class="row">
		    											<div class="col-sm-6">
		    												<div class="text-center font-24 txt-dark mt-5">
		    													<span class="counter-anim">550</span>
		    												</div>
		    												<div class="flex-stat text-center mt-5">
																	<span class="block txt-dark font-12"><span class="text-danger"><i class="zmdi zmdi-caret-down font-21 ml-5 vertical-align-middle"></i> 8%</span> Likes</span>
																</div>
		    											</div>
		    											<div class="col-sm-6">
		    												<div class="text-center font-24 txt-dark mt-5">
		    													<span class="counter-anim">550</span>
		    												</div>
		    												<div class="flex-stat text-center mt-5">
																	<span class="block txt-dark font-12"><span class="text-danger"><i class="zmdi zmdi-caret-down font-21 ml-5 vertical-align-middle"></i> 8%</span> Unikes</span>
																</div>
		    											</div>
		    										</div>
		    									</div>
		    	              </div>
		    	          	</div>
								    </div>
								</div>

								<div class="row">
								    <div class="col-lg-4">
								        <div class="panel panel-default panel-facebook border-panel card-view">
						        			<div class="panel-heading">
						        				<div class="pull-left"><h6>Post Reach </h6></div>
						    	            <div class="pull-right">
						    	            	<div class="dropdown inline-block">
						    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
						    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
						    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
						    	            		</ul>
						    	            	</div>
						    	            </div>
						    	            <div class="clearfix"></div>
						        			 </div>
						        			 <div class="panel-wrapper">
						        			 	<div class="panel-body">
						        			 		<div class="col text-center mt-15 mb-10">
						        			 			<span class="temprature text-center" style="line-height: 0;">595</span>
						        			 			<span class="text-center txt-success"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 11.1%</span>
						        			 		</div>
						        			 		<div>
																<canvas id="chart_post_reach" height="201"></canvas>
															</div>
						        			 	</div>
						        			 </div>
						        		</div>
								    </div>

								    <div class="col-lg-3">
								    	<div class="panel panel-default panel-facebook border-panel card-view">
								            <div class="panel-heading text-center">
								                <h6>Click Action on Page </h6>
							                	<div class="dropdown inline-block">
							                		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
							                		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
							                		</ul>
							                	</div>
								                <div class="clearfix"></div>
								            </div>
								            <div class="panel-wrapper collapse in">
								            	<div class="panel-body row pt-0 pb-0">
								            		<div class="table-wrap">
								            			<div class="table-responsive do-nicescroll" style="height: 298px;">
								            				<table class="table mb-0 scroll-me">
								            					<thead>
								            					  <tr>
								            						<th class="font-10">Action</th>
								            						<th class="font-10">Month to Date</th>
								            						<th class="font-10"><span class="pull-right">%</span></th>
								            					  </tr>
								            					</thead>
								          						<tbody>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						  <tr>
								          							<td class="font-14 txt-dark">Miss Lia Jast</td>
								          							<td class="font-18 txt-dark">979</td>
								          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
								          						  </tr>
								          						</tbody>
								            				</table>
								            			</div>
								            		</div>
								            	</div>
								            </div>
								        </div>
								    </div>

								    <div class="col-lg-3">
								    		<div class="panel panel-default panel-facebook border-panel card-view">
						    	        <div class="panel-heading text-center">
						    	            <h6>Most Recent Posts </h6>
					    	            	<div class="dropdown inline-block">
					    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
					    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
					    	            		</ul>
					    	            	</div>
						    	            <div class="clearfix"></div>
						    	        </div>
						    	        <div class="panel-wrapper collapse in">
						    	        	<div class="panel-body row pt-0 pb-0">
						    	        		<div class="table-wrap">
						    	        			<div class="table-responsive do-nicescroll" style="height: 298px;">
						    	        				<table class="table mb-0 scroll-me">
						    	        					<thead>
						    	        					  <tr>
						    	        						<th class="font-10">#</th>
						    	        						<th class="font-10">Name</th>
						    	        						<th class="font-10">Clicks</th>
						    	        						<th class="font-10">Reach</th>
						    	        					  </tr>
						    	        					</thead>
						    	      						<tbody>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">1</td>
						    	      							<td class="font-12 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-16 txt-dark">979</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">2</td>
						    	      							<td class="font-12 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-16 txt-dark">979</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">3</td>
						    	      							<td class="font-12 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-16 txt-dark">979</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">4</td>
						    	      							<td class="font-12 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-16 txt-dark">979</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      						  </tr>
						    	      						  <tr>
						    	      							<td class="font-14 txt-dark">5</td>
						    	      							<td class="font-12 txt-dark">Miss Lia Jast</td>
						    	      							<td class="font-16 txt-dark">979</td>
						    	      							<td class="font-18 txt-dark">979</td>
						    	      						  </tr>
						    	      						  <tr>
						    	      						</tbody>
						    	        				</table>
						    	        			</div>
						    	        		</div>
						    	        	</div>
						    	        </div>
						    	    </div>
								    </div>

								    <div class="col-lg-2">
		    							<div class="panel panel-default panel-facebook card-view" style="height: 180px">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Fans by Gender</span>	
		    										<div class="block text-center mb-5">
		    	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
		    										</div>	
		    										<canvas id="chart_7" height="200"></canvas>
		    									</div>
		    	              </div>
		    	          	</div>

		    	          	<div class="panel panel-default panel-facebook card-view" style="height: 185px">
		    								<div class="panel-wrapper collapse in">
		    	                <div class="panel-body sm-data-box-1">
		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Fans by Country</span>	
		    										<div class="block text-center mb-5">
		    	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
		    										</div>	
		    										<canvas id="chart_8" height="200"></canvas>
		    									</div>
		    	              </div>
		    	          	</div>
								    </div>
									<div style="position: absolute;width: 100%;">
								<center>
									<div style="width: 450px; height: 300px; opacity: 0.7;background: #f3f3f3; margin-top: -300px; border-radius: 10px; border: 1px solid #fff">
										<h3 style="color: #000;padding-top: 80px;">Coming Soon...</h3>
									</div>
								</center>
								</div>
								</div>
								
							<?php } ?>

							<?php if (!$_SESSION[$fb_oauth_login]) { ?>
							<div class="row">
								<div class="col-lg-12">
								<div class="panel panel-default panel-facebook border-panel card-view">
								<div style="padding-bottom: 300px"></div>
									<center>
										<a href="<?php echo base_url('facebook/index/' . $domain['id']); ?>" title="" class="btn btn-success btn-rounded mb-10" style="height: 60px; width: 250px;display: flex;justify-content: center; align-items: center;"><span class="white" style="font-size: 16px;">Login with Facebook</span></a></span>
									</center>
								
								<div style="padding-bottom: 300px"></div>
								</div>
								</div>
							</div>
							<?php }
							unset($_SESSION[$facebook_tab]);
							?>

						</div>
							<div id="facebook_page" class="tab-pane fade in" role="tabpanel">
								<div class="row">
							    <div class="col-lg-9 col-md-9">
							    	<div class="row">
									    <div class="col-lg-5 col-md-5 col-sm-6">
									        <div class="panel panel-default panel-facebook-page border-panel card-view">
							        			<div class="panel-heading">
							        				<div class="pull-left"><h6>Post Overview </h6></div>
							    	            <div class="pull-right">
							    	            	<div class="dropdown inline-block">
							    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
							    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
							    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
							    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
							    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
							    	            		</ul>
							    	            	</div>
							    	            </div>
							    	            <div class="clearfix"></div>
							        			 </div>
						        			   <div class="panel-wrapper collapse in">
						        			   	<div class="panel-body row pt-0 pb-0">
						        			   		<div class="table-wrap">
						        			   			<div class="table-responsive do-nicescroll" style="height: 429px;">
						        			   				<table class="table mb-0 scroll-me">
						        			   					<thead>
						        			   					  <tr>
						        			   						<th class="font-10">Metric</th>
						        			   						<th class="font-10">Month to Date</th>
						        			   						<th class="font-10"><span class="pull-right">%</span></th>
						        			   					  </tr>
						        			   					</thead>
						        			 						<tbody>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						  <tr>
						        			 							<td class="font-14 txt-dark">Miss Lia Jast</td>
						        			 							<td class="font-18 txt-dark">979</td>
						        			 							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
						        			 						  </tr>
						        			 						</tbody>
						        			   				</table>
						        			   			</div>
						        			   		</div>
						        			   	</div>
						        			   </div>
							        		</div>
									    </div>

									    <div class="col-lg-4 col-md-4 col-sm-6">
									    	<div class="panel panel-default panel-facebook-page border-panel card-view">
							            <div class="panel-heading">
							                <div class="pull-left"><h6>Page Summary </h6></div>
							                <div class="pull-right">
							                	<div class="dropdown inline-block">
							                		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
							                		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
							                			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
							                		</ul>
							                	</div>
							                </div>
							                <div class="clearfix"></div>
							            </div>
							            <div class="panel-wrapper collapse in">
							            	<div class="panel-body row pt-0 pb-0">
							            		<div class="table-wrap">
							            			<div class="table-responsive do-nicescroll" style="height: 429px;">
							            				<table class="table mb-0 scroll-me">
							            					<thead>
							            					  <tr>
							            						<th class="font-10">Metric</th>
							            						<th class="font-10">Month to Date</th>
							            						<th class="font-10"><span class="pull-right">%</span></th>
							            					  </tr>
							            					</thead>
							          						<tbody>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-success pull-right weight-500"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						  <tr>
							          							<td class="font-14 txt-dark">Miss Lia Jast</td>
							          							<td class="font-18 txt-dark">979</td>
							          							<td><span class="inline-block txt-danger pull-right weight-500"><i class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i> 25%</span></td>
							          						  </tr>
							          						</tbody>
							            				</table>
							            			</div>
							            		</div>
							            	</div>
							            </div>
								        </div>
									    </div>

	    						    <div class="col-lg-3 col-md-3 col-sm-12">
	    						    	<div class="row">
	    						    		<div class="col-md-12 col-sm-6">
	    						    			<div class="panel panel-default panel-facebook-page card-view" style="height: 250px">
			        								<div class="panel-wrapper collapse in">
			        	                <div class="panel-body sm-data-box-1">
			        										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Post Published</span>	
			        										<div class="block text-center">
			        	      		        	<div class="dropdown inline-block">
								    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
								    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
								    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
								    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
								    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
								    	            		</ul>
								    	            	</div>
			        										</div>	
			        										<div class="text-center font-24 txt-dark mt-5">
			        											<span class="temprature counter-anim">550</span>
			        										</div>
			        										<div class="flex-stat text-center">
																		<span class="block txt-dark font-14"><span class="text-danger"><i class="zmdi zmdi-caret-down font-21 ml-5 vertical-align-middle"></i> 8%</span></span>
																	</div>
			        									</div>
			        	              </div>
			        	          	</div>
	    						    		</div>

	    						    		<div class="col-md-12 col-sm-6">
	    						    			<div class="panel panel-default panel-facebook-page card-view">
			        								<div class="panel-wrapper collapse in">
			        	                <div class="panel-body sm-data-box-1">
			        										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Page Likes/Unlikes</span>	
			        										<div class="block text-center mb-15">
			        	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
			        										</div>	
			        										<canvas id="chart_fans_by_gender" height="220"></canvas>
			        									</div>
			        	              </div>
			        	          	</div>
	    						    		</div>
	    						    	</div>
	    						    </div>
							    	</div>
							    	<div class="row">
							    		<div class="col-lg-4 col-md-4 col-sm-6">
			    	          	<div class="panel panel-default panel-facebook-page card-view" style="height: 185px">
			    	          		<div class="panel-heading mb-20">
		    	                	<div class="pull-left">
		    	                		<span class="uppercase-font weight-500 font-14 block txt-dark">Net Likes</span>
		    	                	</div>

		    	                	<div class="pull-right">
		    	                		<div class="dropdown inline-block">
		    	      		        		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
		    	      		        		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
		    	      		        		</ul>
		    	      		        	</div>
		    	                	</div>
			    	          		</div>
			    								<div class="panel-wrapper collapse in">
			    	                <div class="panel-body sm-data-box-1">
			    	                	<div class="col text-center mt-0 mb-10">
			    	        			 			<span class="font-24 txt-dark text-center" style="line-height: 0;">595</span>
			    	        			 			<span class="text-center txt-success"><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i> 11.1%</span>
			    	        			 		</div>
			    										<canvas id="chart_net_likes" height="100"></canvas>
			    									</div>
			    	              </div>
			    	          	</div>
							    		</div>
  						    		<div class="col-lg-4 col-md-4 col-sm-6">
  		    	          	<div class="panel panel-default panel-facebook-page card-view" style="height: 185px">
  		    								<div class="panel-wrapper collapse in">
  		    	                <div class="panel-body sm-data-box-1">
  		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Page Likes/Unlikes Overview</span>	
  		    										<div class="block text-center">
  		    	      		        	<div class="dropdown inline-block">
  		    	      		        		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
  		    	      		        		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
  		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
  		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
  		    	      		        			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
  		    	      		        		</ul>
  		    	      		        	</div>
  		    										</div>	
  		    										<div class="row">
  		    											<div class="col-sm-6 mt-15">
  		    												<div class="text-center medium-point txt-dark mt-5">
  		    													<span class="counter-anim">550</span>
  		    												</div>
  		    												<div class="flex-stat text-center mt-5">
  																	<span class="block txt-dark font-12"><span class="text-danger"><i class="zmdi zmdi-caret-down font-21 ml-5 vertical-align-middle"></i> 8%</span> Likes</span>
  																</div>
  		    											</div>
  		    											<div class="col-sm-6 mt-15">
  		    												<div class="text-center medium-point txt-dark mt-5">
  		    													<span class="counter-anim">550</span>
  		    												</div>
  		    												<div class="flex-stat text-center mt-5">
  																	<span class="block txt-dark font-12"><span class="text-danger"><i class="zmdi zmdi-caret-down font-21 ml-5 vertical-align-middle"></i> 8%</span> Unikes</span>
  																</div>
  		    											</div>
  		    										</div>
  		    									</div>
  		    	              </div>
  		    	          	</div>
  						    		</div>
  						    		<div class="col-lg-4 col-md-4 col-sm-12">
  		    	          	<div class="panel panel-default panel-facebook-page card-view" style="height: 185px">
  		    								<div class="panel-wrapper collapse in">
  		    	                <div class="panel-body sm-data-box-1">
  		    										<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Page Likes</span>	
  		    										<div class="block text-center">
  		    	      		        	<span class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-12">All Time</span>
  		    										</div>	
  		    										<div class="text-center medium-point txt-dark mt-20">
		    													<span class="counter-anim">550</span>
		    												</div>
  		    									</div>
  		    	              </div>
  		    	          	</div>
  						    		</div>
							    	</div>
							    </div>

							    <div class="col-lg-3 col-md-3">
							    		<div class="panel panel-default panel-facebook-page border-panel card-view">
					    	        <div class="panel-heading">
					    	            <div class="pull-left"><h6>Most Recent Posts </h6></div>
					    	            <div class="pull-right">
					    	            	<div class="dropdown inline-block">
					    	            		<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle border-none pa-0 font-16" type="button"><span>Month to date</span><i class="zmdi  zmdi-chevron-down ml-15"></i></button>
					    	            		<ul class="dropdown-menu bullet dropdown-menu-left"  role="menu">
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 30 Days</a></li>
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 15 Days</a></li>
					    	            			<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Last 7 Days</a></li>
					    	            		</ul>
					    	            	</div>
					    	            </div>
					    	            <div class="clearfix"></div>
					    	        </div>
					    	        <div class="panel-wrapper collapse in">
					    	        	<div class="panel-body row pt-0 pb-0">
					    	        		<div class="table-wrap">
					    	        			<div class="table-responsive do-nicescroll" style="height: 604px;">
					    	        				<table class="table mb-0 scroll-me">
					    	        					<thead>
					    	        					  <tr>
					    	        						<th class="font-10">#</th>
					    	        						<th class="font-10">Name</th>
					    	        						<th class="font-10">Clicks</th>
					    	        						<th class="font-10"><span class="pull-right">Reach</span></th>
					    	        					  </tr>
					    	        					</thead>
					    	      						<tbody>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">1</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">2</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">3</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">4</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">5</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">6</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">7</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">8</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">9</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">10</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">11</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">12</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">13</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">14</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						  <tr>
					    	      							<td class="font-14 txt-dark">15</td>
					    	      							<td class="font-14 txt-dark">Miss Lia Jast</td>
					    	      							<td class="font-18 txt-dark">979</td>
					    	      							<td><span class="inline-block txt-success pull-right weight-500">815</span></td>
					    	      						  </tr>
					    	      						</tbody>
					    	        				</table>
					    	        			</div>
					    	        		</div>
					    	        	</div>
					    	        </div>
					    	    </div>
							    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="addgoal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-warning">
	<?php echo form_open('domains/'.$domain['id'].'/analytics/addgoal', array("class" => "form-vertical","id"=>"addgoal")); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
				<h3 class="modal-title text-modal">
					Add Goal
				</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="form-group col-md-6 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label class="control-label mb-5">Goal Name</label>
								<div class="input-group"><span class="input-group-addon"></span>
									<input required type="text" name="name" value="" class="form-control" id="name"/ placeholder="Goal Name">
								</div>
							</div>
							<div class="form-group col-md-6 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label class="control-label mb-5">Revenue</label>
								<div class="input-group"><span class="input-group-addon"><i class="fa fa-gbp"></i></span>
									<input type="text" name="revenue" value="" class="form-control" id="revenue"/ placeholder="Revenue">
								</div>
							</div>
							<!-- <div class="form-group col-md-12 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label class="control-label mb-5">Description</label>
								<div class="input-group"><span class="input-group-addon"></span>
									<input type="text" name="description" value="" class="form-control" id="description"/ placeholder="Description">
								</div>
							</div> -->
							<div class="form-group col-md-6 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label for="matchAttribute" class="control-label mb-5">Goal Tirgger</label>
								<div class="add-on-edit">
									<select name="matchAttribute" class="form-control select2">
										<option value="url">Visit a given URL</option>
										<option value="title">Visit a given Page Title</option>
										<option value="event">Send an event</option>
										<option value="file">Download a file</option>
										<option value="external_website">Click on a Link to an external website</option>
									</select>
								</div>
							</div>
							<div class="form-group col-md-6 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label for="patternType" class="control-label mb-5">Where the URL?</label>
								<div class="add-on-edit">
									<select name="patternType" class="form-control select2">
										<option value="contains">contains</option>
										<option value="exact">is exactly</option>
										<option value="regex">matches the expression</option>
									</select>
								</div>
							</div>
							<div class="form-group col-md-12 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<label class="control-label mb-5">Pattern</label>
								<div class="input-group"><span class="input-group-addon"></span>
									<input type="text" name="pattern" value="" class="form-control" id="pattern"/ placeholder="Pattern">
								</div>
							</div>
							<div class="form-group col-md-12 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<div class="checkbox checkbox-success checkbox-circle pull-left">
									<input name="caseSensitive" class="success-check" id="caseSensitive" type="checkbox" value="1">
									<label for="publivs" class="control-label text-left">
										Case Sensitive match
									</label>
								</div>
							</div>
							<div class="form-group col-md-12 xs-pt-10 xs-pb-10 xs-pr-0 xs-pl-0">
								<div class="checkbox checkbox-success checkbox-circle pull-left">
									<input name="allowMultipleConversionsPerVisit" class="success-check" id="allowMultipleConversionsPerVisit" type="checkbox" value="1">
									<label for="publivs" class="control-label text-left">
										Allow Multiple Conversions
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-right">
					<button type="button" data-dismiss="modal" class="btn btn-danger modal-close">
						Cancel
					</button>
					<button type="submit" class="btn btn-success modal-close">
						ADD
					</button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<?php
if (isset($pageData)) {
?>
<div id="selectPage" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-warning">
	<?php echo form_open('facebook/getData/'.$domain['id'], array("class" => "form-vertical")); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-left">
					<h3>Select a Page</h3>
				</div>
				<div class="pull-right">
					<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="page">Facebook Pages</label>
							<div class="add-on-edit">
								<select name="pageID" class="form-control select2 required" required>
									<?php
										foreach ($pageData as $key => $page) {
											echo '<option value="'.$key.'">'.$page.'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">Cancel</button>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<script>
$(window).on('load',function(){
	$('#selectPage').modal('show');
});
</script>
<?php
}
?>

<script>
$(document).ready(function(){
if( $('#chart_users_sessions').length > 0 ){
	var ctx1 = document.getElementById("chart_users_sessions").getContext("2d");
	var labels = <?php echo json_encode($labels); ?>;
	var userData = <?php echo json_encode($userData); ?>;
	
	var data1 = {
		labels: labels,
		datasets: [
			{
				label: "sec",
				backgroundColor: "rgba(70,148,8,0.4)",
				borderColor: "rgba(70,148,8,0.4)",
				pointBorderColor: "rgb(70,148,8)",
				pointBackgroundColor: "rgba(70,148,8,0.4)",
				data: userData,
			}
		]
	};

	var areaChart = new Chart(ctx1, {
		type:"line",
		data:data1,
		
		options: {
			tooltips: {
				mode:"label"
			},
			elements:{
				point: {
					// hitRadius:90
				}
			},
			
			scales: {
				yAxes: [{
					stacked: true,
					gridLines: {
						color: "#878787",
					},
					ticks: {
						fontFamily: "Roboto",
						fontColor:"#878787"
					}
				}],
				xAxes: [{
					stacked: true,
					gridLines: {
						color: "#878787",
					},
					ticks: {
						fontFamily: "Roboto",
						fontColor:"#878787"
					}
				}]
			},
			animation: {
				duration: 3000
			},
			responsive: true,
			legend: {
				display: false,
			},
			tooltip: {
				backgroundColor:'rgba(33,33,33,1)',
				cornerRadius:0,
				footerFontFamily:"'Roboto'"
			}
			
		}
	});
}

if( $('#chart_sessions_by_channel').length > 0 ){
	var ctx7 = document.getElementById("chart_sessions_by_channel").getContext("2d");
	var labels = <?php echo json_encode($channels); ?>;
	var sessionsByChannel = <?php echo json_encode($sessionsByChannel); ?>;
	console.log(labels);
	console.log(sessionsByChannel);
	var data7 = {
			labels: labels,
	datasets: [
		{
			data: sessionsByChannel,
			backgroundColor: [
				"rgba(255,0,0,.8)",
				"rgba(255,128,51,.8)",
				"rgba(255,255,102,.8)",
				"rgba(128,255,0,.8)",
				"rgba(0,255,0,.8)"
			],
			hoverBackgroundColor: [
				"rgba(255,0,0,.8)",
				"rgba(255,128,0,.8)",
				"rgba(255,255,51,.8)",
				"rgba(128,255,0,.8)",
				"rgba(0,255,0,.8)"
			]
		}]
	};
	
	var doughnutChart = new Chart(ctx7, {
		type: 'doughnut',
		data: data7,
		options: {
			animation: {
				duration: 3000
			},
			responsive: true,
			legend: {
				display: false,
				labels: {
				fontFamily: "Roboto",
				fontColor:"#878787"
				}
			},
			tooltip: {
				backgroundColor:'rgba(33,33,33,1)',
				cornerRadius:0,
				footerFontFamily:"'Roboto'"
			},
			elements: {
				arc: {
					borderWidth: 0
				}
			}
		}
	});
}

	if( $('#searchConsoleChart').length > 0 ){
		var chartData = <?php echo json_encode($domain['search_console_data']); ?>;
		var impressions = [];
		var clicks = [];
		var labels = [];
		var ctrs = [];
		var positions = [];
		for ( var i = 0; i < chartData.length; i++) {
			labels[i] = chartData[i]['date'];
			impressions[i] = chartData[i]['impression'];
			clicks[i] = chartData[i]['click'] * 300;
			ctrs[i] = chartData[i]['ctr'] * 300;
			positions[i] = chartData[i]['position'] * 500;
		}
		console.log(clicks);
		var ctx1 = document.getElementById("searchConsoleChart").getContext("2d");
		var data1 = {
			labels: labels,
			datasets: [
			{
				label: "clicks",
				backgroundColor: "#3B9623",
				borderColor: "#3B9623",
				borderWidth: 2,
				data: clicks,
				fill: false,
			},
			{
				label: "impressions",
				backgroundColor: "#207ABF",
				borderColor: "#207ABF",
				borderWidth: 2,
				data: impressions,
				fill: false,
			},
			{
				label: "ctrs",
				backgroundColor: "#E04E38",
				borderColor: "#E04E38",
				borderWidth: 2,
				data: ctrs,
				fill: false,
			},
			{
				label: "positions",
				backgroundColor: "#FFB655",
				borderColor: "#FFB655",
				borderWidth: 2,
				data: positions,
				fill: false,
			},			
		]
		};
		
		var areaChart = new Chart(ctx1, {
			type:"line",
			data:data1,
			options: {
				tooltips: {
					enabled:true
				},
				elements:{
					point: {
						hitRadius:50
					}
				},
				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#878787",
							lineWidth: 0.3
						},
						ticks: {
							fontFamily: "Roboto",
							fontColor:"#878787"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#878787",
							lineWidth: 0.3
						},
						ticks: {
							fontFamily: "Roboto",
							fontColor:"#878787"
						}
					}]
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					display: false,
				},
			}
		});
	}

 });

</script>

<input type="hidden" name="is_domain_analytics_page" value="1" />

<?php echo form_hidden( 'id_total_visits_data', json_encode( $domain['total_visits'] ) ); ?>
<?php echo form_hidden( 'id_unique_visits_data', json_encode( $domain['unique_visits'] ) ); ?>
<?php echo form_hidden( 'id_page_per_visit_data', json_encode( $domain['page_per_visit'] ) ); ?>
<?php echo form_hidden( 'id_referrer_visits_data', json_encode( $domain['referrer_visits_array'] ) ); ?>
<?php echo form_hidden( 'id_referrer_visits_graph_data', json_encode( $domain['referrer_visits_graph_array'] ) ); ?>