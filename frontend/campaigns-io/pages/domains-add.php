<div class="row heading-bg mb-0">
    <div class="col-lg-12">
        <div class="txt-dark">
            <h5>ADD NEW SITE</h5>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php edit_domain_form_component( $domain['id'], $user['id'], $domain['form_data'], $available, $google_client_id, $google_oauth_redirect_uri ); ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#id-is_ecommerce").change(function() {
            if ($("#id-is_ecommerce")[0].checked) {
                $(".woo_api_fields").show();
            } else {
                $(".woo_api_fields").hide();
            }
        });

        $("#id-monitor_website_uptime").change(function() {
            if ($("#id-monitor_website_uptime")[0].checked) {
                $(".monitor_website_uptime").show();
            } else {
                $(".monitor_website_uptime").hide();
            }
        });

        $("#id-connect_to_google").change(function() {
            if ($("#id-connect_to_google")[0].checked) {
                $(".connect_to_google").show();
            } else {
                $(".connect_to_google").hide();
            }
        });
    });
</script>