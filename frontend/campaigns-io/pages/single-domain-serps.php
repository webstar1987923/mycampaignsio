<div class="row">
	<div class="col-lg-12 col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div  class="tab-struct custom-tab-1">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<li class="active" role="presentation" class=""><a  data-toggle="tab" id="tab_week" role="tab" href="#week" aria-expanded="false">WEEK</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_month" role="tab" href="#month" aria-expanded="false">MONTH</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="tab_year" role="tab" href="#year" aria-expanded="false">YEAR</a></li>
							
							<li class="pull-right">
								<a href="#" class="serp-keyword-add"><button class="btn btn-sm btn-primary">ADD KEYWORDS</button></a>
							</li>
							<li role="presentation" class="pull-right"><a  data-toggle="tab" id="tab_link_webmaster_tools" role="tab" href="#link_webmaster_tools" aria-expanded="false">Link Webmaster Tools</a></li>
							<li role="presentation" class="pull-right"><a  data-toggle="tab" id="tab_edit_keywords" role="tab" href="#edit_keywords" aria-expanded="false">Edit Keywords</a></li>
							
						</ul>
						<div class="tab-content" id="myTabContent_7">
							<div id="week" class="tab-pane fade active in" role="tabpanel">
								<div class="row mr-0 ml-0">
									<div class="col-lg-9 col-md-12 col-sm-12">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default card-view bg-yellow">
													<div class="panel-wrapper">
														<div class="panel-body ma-0 pa-0">
															<div class="row-eq-height bg-yellow row border-right-yellow">
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['week']['top1_latest']; ?></span> <span class="uppercase-font">First Place</span></span>
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['week']['top5_latest']; ?></span><span class="uppercase-font">Top 5</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['week']['top10_latest']; ?></span><span class="uppercase-font">Top 10</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['week']['top20_latest']; ?></span><span class="uppercase-font">Top 20</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['week']['ranked_latest']; ?></span><span class="uppercase-font">Ranked</span></span>
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default card-view">
													<div class="panel-heading">
														<div class="pull-left">
															<h6>Search Console</h6>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="panel-wrapper">
														<div class="panel-body">
															<div id="webTrafficChart" class="morris-chart" style="height: 277px;"></div>	
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-12 col-sm-12">
										<div class="panel panel-default card-view">
											<div class="panel-heading relative">
												<h6>Top Keywords</h6>
												<div class="head-overlay"></div>
											</div>
											<div class="panel-wrapper">
												<div class="panel-body row pa-0">
													<div class="table-wrap" style="min-height: 407px;">
														<div class="table-responsive">
														  <table class="table table-hover mb-0">
														  	<thead>
														  		<tr>
														  			<th>Keyword</th>
														  			<th>Position</th>
														  		</tr>
														  	</thead>
															<tbody>

															<?php 
															if($serps['serp_data_today']):
															foreach($serps['serp_data_today'] as $s_today): ?>	
															  <tr>
																<td><?php echo $s_today['keyword']; ?></td>
																<td><?php echo $s_today['position']; ?></td>
																<td>86</td>
															  </tr>
															  <?php endforeach;
															endif;
															   ?>
															</tbody>
														  </table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="month" class="tab-pane fade in" role="tabpanel">
								<div class="row mr-0 ml-0">
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default card-view bg-yellow">
													<div class="panel-wrapper">
														<div class="panel-body ma-0 pa-0">
															<div class="row-eq-height bg-yellow row border-right-yellow">
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['month']['top1_latest']; ?></span> <span class="uppercase-font">First Place</span></span>
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['month']['top5_latest']; ?></span><span class="uppercase-font">Top 5</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['month']['top10_latest']; ?></span><span class="uppercase-font">Top 10</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['month']['top20_latest']; ?></span><span class="uppercase-font">Top 20</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['month']['ranked_latest']; ?></span><span class="uppercase-font">Ranked</span></span>
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default card-view">
													<div class="panel-heading">
														<div class="pull-left">
															<h6>Search Console</h6>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="panel-wrapper">
														<div class="panel-body">
															<div id="webTrafficChart" class="morris-chart" style="height: 277px;"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="panel panel-default card-view">
											<div class="panel-heading relative">
												<h6>Top Keywords</h6>
												<div class="head-overlay"></div>
											</div>
											<div class="panel-wrapper">
												<div class="panel-body row pa-0">
													<div class="table-wrap" style="min-height: 407px;">
														<div class="table-responsive">
														  <table class="table table-hover mb-0">
														  	<thead>
														  		<tr>
														  			<th>Keyword</th>
														  			<th>Position</th>
														  		</tr>
														  	</thead>
															<tbody>
															<?php 
															if($serps['serp_data_month']):
															foreach($serps['serp_data_month'] as $s_month): ?>	
															  <tr>
																<td><?php echo $s_month['keyword']; ?></td>
																<td><?php echo $s_month['position']; ?></td>
															  </tr>
															  <?php endforeach;
															endif;
															   ?>
															</tbody>
														  </table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="year" class="tab-pane fade in" role="tabpanel">
								<div class="row mr-0 ml-0">
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default card-view bg-yellow">
													<div class="panel-wrapper">
														<div class="panel-body ma-0 pa-0">
															<div class="row-eq-height bg-yellow row border-right-yellow">
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['year']['top1_latest']; ?></span> <span class="uppercase-font">First Place</span></span>
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['year']['top5_latest']; ?></span><span class="uppercase-font">Top 5</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['year']['top10_latest']; ?></span><span class="uppercase-font">Top 10</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['year']['top20_latest']; ?></span><span class="uppercase-font">Top 20</span></span>
																	
																</div>
																<div class="col-20 text-center">
																	<span class="txt-light counter"><span class="weight-500 counter-anim font-24 mr-5"><?php echo $serps['stats']['year']['ranked_latest']; ?></span><span class="uppercase-font">Ranked</span></span>
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="panel panel-default card-view">
											<div class="panel-heading relative">
												<h6>Top Keywords</h6>
												<div class="head-overlay"></div>
											</div>
											<div class="panel-wrapper">
												<div class="panel-body row pa-0">
													<div class="table-wrap" style="min-height: 407px;">
														<div class="table-responsive">
														  <table class="table table-hover mb-0">
														  	<thead>
														  		<tr>
														  			<th>Keyword</th>
														  			<th>Position</th>
														  		</tr>
														  	</thead>
															<tbody>
															<?php 
															if($serps['serp_data_year']):
															foreach($serps['serp_data_year'] as $s_year): ?>	
															  <tr>
																<td><?php echo $s_year['keyword']; ?></td>
																<td><?php echo $s_year['position']; ?></td>
															  </tr>
															  <?php endforeach;
															endif;
															   ?>
															</tbody>
														  </table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="edit_keywords" class="tab-pane fade in" role="tabpanel">
								<div class="row mr-0 ml-0">
									<div class="col-lg-12">
										<div class="panel panel-default card-view">
											<div class="panel-wrapper">
												<div class="panel-body">
													<?php serp_edit_keywords_table( $serps['edit_keywords'] )  ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="link_webmaster_tools" class="tab-pane fade in" role="tabpanel">
								<div class="row mr-0 ml-0">
									<div class="col-lg-12">
										<div class="panel panel-default card-view">
											<div class="panel-wrapper">
												<div class="panel-body">
          								<div class="row form-wrap">
          									<form>
          										<div class="col-lg-4 col-md-4 col-sm-4">
          											<a href="#" title="" class="serp-keyword-webmaster-add mb-4">
          		            				<button class="btn btn-primary">ADD WEBMASTER KEYWORDS</button>
          		            			</a>
          										</div>
          										<div class="col-lg-4 col-md-4 col-sm-4">
          											<div class="row">
          												<div class="col-lg-6">
          													<div class="form-group">
          														<label class="control-label mb-10 text-left">Start date</label>
          														<input type="text" name="key-search-start-date" value="<?php echo $keys_filters['selected']['start_date']; ?>" class="form-control" />
          													</div>
          												</div>
          												<div class="col-lg-6">
          													<div class="form-group">
          														<label class="control-label mb-10 text-left">End date</label>
          														<input type="text" name="key-search-end-date" value="<?php echo $keys_filters['selected']['end_date']; ?>" class="form-control" />
          													</div>
          												</div>
          											</div>
          										</div>
          										<div class="col-lg-4 col-md-4 col-sm-4">
          											<div class="row">
          												<div class="col-lg-6">
          													<div class="form-group">
          														<label class="control-label mb-10 text-left">Dimension</label>
          														<select name="key-search-dimension" class="form-control"><?php
          															foreach($keys_filters['available']['search_dimensions'] as $k => $v){
          																echo '<option value="' . $k . '"' . ( $k === $keys_filters['selected']['search_dimensions'] ? ' selected' : '') . '>' . $v . '</option>';
          															} ?>
          														</select>
          													</div>
          												</div>
          												<div class="col-lg-6">
          													<div class="form-group">
          														<label class="control-label mb-10 text-left">Type</label>
          														<select name="key-search-type" class="form-control"><?php
          															foreach($keys_filters['available']['search_types'] as $k => $v){
          																echo '<option value="' . $k . '"' . ( $k === $keys_filters['selected']['search_types'] ? ' selected' : '') . '>' . $v . '</option>';
          															} ?>
          														</select>
          													</div>
          												</div>
          											</div>
          										</div>
          									</form>
          								</div>

					            		<hr/>

					            		<div id="serp-keywords-table-wrapper" class="list-table-wrap" style="min-height:140px;">
					            			<?php echo serp_keywords_table_html( $serps['keywords'], 'query' ); ?>
									        </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					<div class="">
						<div class="col-lg-3 col-md-3 col-sm-6">

							<div class="panel panel-default card-view" style="background: #4487EF;">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Total clicks: <?php echo $serps['search_console_avg']['clicks']; ?></h6>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>	
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">

							<div class="panel panel-default card-view" style="background: #1BBBD1;">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Total impressions: <?php echo $serps['search_console_avg']['impressions']; ?></h6>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>	
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6">

							<div class="panel panel-default card-view" style="background: #179587;">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Average CTR: <?php echo $serps['search_console_avg']['average_ctr']; ?></h6>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>	
						</div>	
						<div class="col-lg-3 col-md-3 col-sm-6">

							<div class="panel panel-default card-view" style="background: #6642B2;">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Average Position: <?php echo $serps['search_console_avg']['positions']; ?></h6>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="row mr-0 ml-0">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Ranking</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper">
									<div class="panel-body">
										<div id="ranking_chart" class="morris-chart" style="height:243px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Backlinks</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper">
									<div class="panel-body">
										<div id="keywords_chart" class="morris-chart" style="height:243px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>Click Data</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper">
									<div class="panel-body">
										<div id="click_data_chart" class="morris-chart" style="height:243px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6>top performing keywords </h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
											  <table class="table table-hover mb-0">
											  	<thead>
											  		<tr>
											  			<th>Keyword</th>
											  			<th>Position</th>
											  		</tr>
											  	</thead>
												<tbody>
													<?php foreach($serps['keywords'] as $key=>$keywords):
													if($key<=9):
													 ?>
												  <tr>
													<td><?php echo $keywords['keyword']; ?></td>
													<td><?php echo $keywords['position']; ?></td>
												  </tr>
												<?php
											endif;
												 endforeach; ?>
												  
												</tbody>
											  </table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php echo form_hidden( 'report_url', $serps['report_url'] ); ?>
<?php echo form_hidden( 'keyword_remove_url', $serps['keyword_remove_url'] ); ?>
<?php echo form_hidden( 'keyword_overall_url', $serps['keyword_overall_url'] ); ?>
<?php echo form_hidden( 'keyword_add_url', $serps['keyword_add_url'] ); ?>
<?php echo form_hidden( 'keyword_add_webmaster_tool_url', $serps['keyword_add_webmaster_tool_url'] ); ?>
<?php echo form_hidden( 'filter_keywords_url', $serps['filter_keywords_url'] ); ?>

<script>
	$(function() {
	"use strict";

		if($('#morris_extra_bar_chart').length > 0)
			// Morris bar chart
			Morris.Bar({
			element: 'morris_extra_bar_chart',
			data: [{
				y: '2006',
				a: 100,
				b: 90,
				c: 60
			}],
			xkey: 'y',
			ykeys: ['a', 'b', 'c'],
			labels: ['A', 'B', 'C'],
			barColors:['#e69a2a', '#ea6c41', '#177ec1'],
			hideHover: 'auto',
			gridLineColor: '#878787',
			resize: true,
			barGap:7,
			gridTextColor:'#878787',
			gridTextFamily:"Roboto"
		});

		if($('#ranking_chart').length > 0)
		   // Bar Chart
			Morris.Bar({
				element: 'ranking_chart',
				data: <?php echo $serps['serp_6month_graph']; ?>,
				xkey: 'keyword',
				ykeys: ['rank'],
				labels: ['Rank'],
				barRatio: 0.4,
				xLabelAngle: 35,
				pointSize: 1,
				pointStrokeColors:['#e69a2a'],
				behaveLikeLine: true,
				gridLineColor: '#878787',
				gridTextColor:'#878787',
				hideHover: 'auto',
				barColors: ['#e69a2a'],
				resize: true,
				gridTextFamily:"Roboto"
			});

		if($('#keywords_chart').length > 0)
		   // Bar Chart
			Morris.Bar({
				element: 'keywords_chart',
				data: [],
				xkey: 'keyword',
				ykeys: ['rank'],
				labels: ['Rank'],
				barRatio: 0.4,
				xLabelAngle: 35,
				pointSize: 1,
				pointStrokeColors:['#e69a2a'],
				behaveLikeLine: true,
				gridLineColor: '#878787',
				gridTextColor:'#878787',
				hideHover: 'auto',
				barColors: ['#e69a2a', '#ea6c41', '#177ec1', '#e69a2a', '#ea6c41', '#177ec1','#e69a2a', '#e69a2a', '#e69a2a', '#e69a2a'],
				resize: true,
				gridTextFamily:"Roboto"
			});

		if($('#click_data_chart').length > 0)
		   // Bar Chart
			Morris.Bar({
				element: 'click_data_chart',
				data: <?php echo $serps['top5_clicked']; ?>,
				xkey: 'keyword',
				ykeys: ['rank'],
				labels: ['Clicks'],
				barRatio: 0.4,
				xLabelAngle: 35,
				pointSize: 1,
				pointStrokeColors:['#e69a2a'],
				behaveLikeLine: true,
				gridLineColor: '#878787',
				gridTextColor:'#878787',
				hideHover: 'auto',
				barColors: ['#e69a2a', '#ea6c41', '#177ec1', '#e69a2a', '#ea6c41', '#177ec1'],
				resize: true,
				gridTextFamily:"Roboto"
			});

		if($('#webTrafficChart').length > 0){
			Morris.Line({
			  element: 'webTrafficChart',
			  data:<?php echo $serps['console_data']; ?>,
			  xkey: 'date',
			  ykeys: ['click', 'impression' ,'ctr'],
			  labels: ['Click', 'Impression', 'Ctr'],
			  resize: true,
			});        
		};
		var data = <?php echo $serps['console_data']; ?>;
		console.log(data);
	});
</script>		

