	
	<?php echo form_hidden( 'current_campaigns_page', $current_page ); ?>


<script type="text/javascript">var site_url = '<?php echo base_url();?>';</script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/popper.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/doodle/js/jquery.slimscroll.js"></script>

<!-- Fancy Dropdown JS -->
<script src="<?php echo base_url(); ?>assets/doodle/js/dropdown-bootstrap-extended.js"></script>

<!-- Jasny-bootstrap JS -->
<script src="<?php echo base_url(); ?>assets/lib/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<!--Nestable js -->
<script src="<?php echo base_url(); ?>assets/lib/nestable2/jquery.nestable.js"></script>

<!-- Owl JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Ion.RangeSlider -->
<script src="<?php echo base_url(); ?>assets/lib/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/raphael/raphael.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/morris.js/morris.min.js"></script>
  
<!-- Chartist JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/chartist/dist/chartist.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/chart.js/Chart.min.js"></script>

<!-- Easy Pie Chart JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script><!-- Moment JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/moment/min/moment-with-locales.min.js"></script>

<!-- Bootstrap Colorpicker JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

<!-- Switchery JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/switchery/dist/switchery.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/select2/dist/js/select2.full.min.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Bootstrap Tagsinput JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- Multiselect JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/multiselect/js/jquery.multi-select.js"></script>

<!-- JQ Steps JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/jquery.steps/build/jquery.steps.min.js"></script>

<!-- Nice Scroll JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/jquery.nicescroll/jquery.nicescroll.min.js"></script>
 
<!-- Bootstrap Switch JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>


<script src="<?php echo site_url(); ?>assets/doodle/js/jquery.dataTables.min.js"></script>


<!-- Bootstrap Datetimepicker JavaScript -->
<script src="<?php echo site_url(); ?>assets/lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo site_url(); ?>assets/doodle/js/autoNumeric.js"></script>
<script src="<?php echo site_url(); ?>assets/doodle/js/jquery.alphanumeric.js"></script>
<script src="<?php echo site_url(); ?>assets/doodle/js/jquery.tools.js"></script>
<script src="<?php echo site_url(); ?>assets/doodle/js/bootstrap-editable.min.js"></script>


<script src='<?php echo site_url(); ?>frontend/site/default/js/custom/campaigns-io-script.js'></script>
<script src="<?php echo site_url(); ?>assets/doodle/js/init.js"></script>

<?php enqueue_html_scripts( $document_data['bottom_scripts'] ); ?>

</body>

<script>
 "use strict";
 $(document).ready(function(){
 
  //nice scroll intialize
  $(".do-nicescroll").niceScroll(".scroll-me");
  $(".do-nicescroll2").niceScroll(".scroll-me2");

  //nice scroll reinitialized for tab change
  $('a[data-toggle="tab"]').on("click",function(){
    setTimeout(function(){ 
        $(".do-nicescroll").getNiceScroll().remove();
        $(".do-nicescroll2").getNiceScroll().remove();
        $(".do-nicescroll").niceScroll(".scroll-me");
        $(".do-nicescroll2").niceScroll(".scroll-me2");
       }, 1000);
    
  });


  

  if( $('#chart_page_visit').length > 0 ){
      var ctx1 = document.getElementById("chart_page_visit").getContext("2d");
      var data1 = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
        {
          label: "fir",
          backgroundColor: "rgba(220,70,102,0.4)",
          borderColor: "rgba(220,70,102,0.4)",
          pointBorderColor: "rgb(220,70,102)",
          pointHighlightStroke: "rgba(220,70,102,1)",
          data: [0, 59, 80, 58, 20, 55, 40]
        },
        {
          label: "sec",
          backgroundColor: "rgba(70,148,8,0.4)",
          borderColor: "rgba(70,148,8,0.4)",
          pointBorderColor: "rgb(70,148,8)",
          pointBackgroundColor: "rgba(70,148,8,0.4)",
          data: [28, 48, 40, 19, 86, 27, 90],
        }
        
      ]
      };
      
      var areaChart = new Chart(ctx1, {
        type:"line",
        data:data1,
        
        options: {
          tooltips: {
            mode:"label"
          },
          elements:{
            point: {
              hitRadius:90
            }
          },
          
          scales: {
            yAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }],
            xAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }]
          },
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          }
          
        }
      });
    }

  if( $('#chart_net_likes').length > 0 ) {
    Chart.defaults.global.defaultFontColor = 'white';
    Chart.defaults.global.defaultFontSize = 10;
    var ctx = document.getElementById("chart_net_likes").getContext('2d');
    var chart_net_likes = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Jan 10", "12", "15", "18", "20", "25"],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: 'rgba(255, 255, 255, 1)',
                borderWidth: 0
            }]
        },
        scaleFontColor: 'white',
        options: {
          legend: {
            display: false,
              labels: {
                  // This more specific font property overrides the global property
                  fontColor: 'white',
              }
          }
        }
    });
  }


  if( $('#chart_post_reach').length > 0 ){
      var ctx1 = document.getElementById("chart_post_reach").getContext("2d");
      var data1 = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
        {
          label: "fir",
          backgroundColor: "rgba(220,70,102,0.4)",
          borderColor: "rgba(220,70,102,0.4)",
          pointBorderColor: "rgb(220,70,102)",
          pointHighlightStroke: "rgba(220,70,102,1)",
          data: [0, 59, 80, 58, 20, 55, 40]
        },
        {
          label: "sec",
          backgroundColor: "rgba(70,148,8,0.4)",
          borderColor: "rgba(70,148,8,0.4)",
          pointBorderColor: "rgb(70,148,8)",
          pointBackgroundColor: "rgba(70,148,8,0.4)",
          data: [28, 48, 40, 19, 86, 27, 90],
        }
        
      ]
      };
      
      var areaChart = new Chart(ctx1, {
        type:"line",
        data:data1,
        
        options: {
          tooltips: {
            mode:"label"
          },
          elements:{
            point: {
              hitRadius:90
            }
          },
          
          scales: {
            yAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }],
            xAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }]
          },
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          }
          
        }
      });
    }




  if( $('#chart_clicks').length > 0 ){
      var ctx1 = document.getElementById("chart_clicks").getContext("2d");
      var data1 = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
        {
          label: "fir",
          backgroundColor: "rgba(220,70,102,0.4)",
          borderColor: "rgba(220,70,102,0.4)",
          pointBorderColor: "rgb(220,70,102)",
          pointHighlightStroke: "rgba(220,70,102,1)",
          data: [0, 59, 80, 58, 20, 55, 40]
        },
        {
          label: "sec",
          backgroundColor: "rgba(70,148,8,0.4)",
          borderColor: "rgba(70,148,8,0.4)",
          pointBorderColor: "rgb(70,148,8)",
          pointBackgroundColor: "rgba(70,148,8,0.4)",
          data: [28, 48, 40, 19, 86, 27, 90],
        }
        
      ]
      };
      
      var areaChart = new Chart(ctx1, {
        type:"line",
        data:data1,
        
        options: {
          tooltips: {
            mode:"label"
          },
          elements:{
            point: {
              hitRadius:90
            }
          },
          
          scales: {
            yAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }],
            xAxes: [{
              stacked: true,
              gridLines: {
                color: "#878787",
              },
              ticks: {
                fontFamily: "Roboto",
                fontColor:"#878787"
              }
            }]
          },
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          }
          
        }
      });
    }



    if( $('#chart_users').length > 0 ){
        var ctx1 = document.getElementById("chart_users").getContext("2d");
        var data1 = {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
          {
            label: "fir",
            backgroundColor: "rgba(220,70,102,0.4)",
            borderColor: "rgba(220,70,102,0.4)",
            pointBorderColor: "rgb(220,70,102)",
            pointHighlightStroke: "rgba(220,70,102,1)",
            data: [0, 59, 80, 58, 20, 55, 40]
          },
          {
            label: "sec",
            backgroundColor: "rgba(70,148,8,0.4)",
            borderColor: "rgba(70,148,8,0.4)",
            pointBorderColor: "rgb(70,148,8)",
            pointBackgroundColor: "rgba(70,148,8,0.4)",
            data: [28, 48, 40, 19, 86, 27, 90],
          }
          
        ]
        };
        
        var areaChart = new Chart(ctx1, {
          type:"line",
          data:data1,
          
          options: {
            tooltips: {
              mode:"label"
            },
            elements:{
              point: {
                hitRadius:90
              }
            },
            
            scales: {
              yAxes: [{
                stacked: true,
                gridLines: {
                  color: "#878787",
                },
                ticks: {
                  fontFamily: "Roboto",
                  fontColor:"#878787"
                }
              }],
              xAxes: [{
                stacked: true,
                gridLines: {
                  color: "#878787",
                },
                ticks: {
                  fontFamily: "Roboto",
                  fontColor:"#878787"
                }
              }]
            },
            animation: {
              duration: 3000
            },
            responsive: true,
            legend: {
              display: false,
            },
            tooltip: {
              backgroundColor:'rgba(33,33,33,1)',
              cornerRadius:0,
              footerFontFamily:"'Roboto'"
            }
            
          }
        });
      }
  
    

  if( $('#chart_7').length > 0 ){
      var ctx7 = document.getElementById("chart_7").getContext("2d");
      var data7 = {
         labels: [
        "Male",
        "Female"
      ],
      datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "rgba(220,70,102,.8)",
            "rgba(230,154,42,.8)"
          ],
          hoverBackgroundColor: [
            "rgba(220,70,102,.8)",
            "rgba(230,154,42,.8)"
          ]
        }]
      };
      
      var doughnutChart = new Chart(ctx7, {
        type: 'doughnut',
        data: data7,
        options: {
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
            labels: {
            fontFamily: "Roboto",
            fontColor:"#878787"
            }
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          },
          elements: {
            arc: {
              borderWidth: 0
            }
          }
        }
      });
    } 

  if( $('#chart_8').length > 0 ){
      var ctx7 = document.getElementById("chart_8").getContext("2d");
      var data7 = {
         labels: [
        "UK",
        "America",
        "Canada",
        "Australia",
        "Japan"
      ],
      datasets: [
        {
          data: [300, 50, 80, 70, 40],
          backgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,51,.8)",
            "rgba(255,255,102,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ],
          hoverBackgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,0,.8)",
            "rgba(255,255,51,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ]
        }]
      };
      
      var doughnutChart = new Chart(ctx7, {
        type: 'doughnut',
        data: data7,
        options: {
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
            labels: {
            fontFamily: "Roboto",
            fontColor:"#878787"
            }
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          },
          elements: {
            arc: {
              borderWidth: 0
            }
          }
        }
      });
    }


    if( $('#chart_session_by_channel').length > 0 ){
      var ctx7 = document.getElementById("chart_session_by_channel").getContext("2d");
      var data7 = {
         labels: [
        "UK",
        "America",
        "Canada",
        "Australia",
        "Japan"
      ],
      datasets: [
        {
          data: [300, 50, 80, 70, 40],
          backgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,51,.8)",
            "rgba(255,255,102,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ],
          hoverBackgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,0,.8)",
            "rgba(255,255,51,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ]
        }]
      };
      
      var doughnutChart = new Chart(ctx7, {
        type: 'doughnut',
        data: data7,
        options: {
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
            labels: {
            fontFamily: "Roboto",
            fontColor:"#878787"
            }
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          },
          elements: {
            arc: {
              borderWidth: 0
            }
          }
        }
      });
    }

    if( $('#chart_fans_by_gender').length > 0 ){
      var ctx7 = document.getElementById("chart_fans_by_gender").getContext("2d");
      var data7 = {
         labels: [
        "UK",
        "America",
        "Canada",
        "Australia",
        "Japan"
      ],
      datasets: [
        {
          data: [300, 50, 80, 70, 40],
          backgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,51,.8)",
            "rgba(255,255,102,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ],
          hoverBackgroundColor: [
            "rgba(255,0,0,.8)",
            "rgba(255,128,0,.8)",
            "rgba(255,255,51,.8)",
            "rgba(128,255,0,.8)",
            "rgba(0,255,0,.8)"
          ]
        }]
      };
      
      var doughnutChart = new Chart(ctx7, {
        type: 'doughnut',
        data: data7,
        options: {
          animation: {
            duration: 3000
          },
          responsive: true,
          legend: {
            display: false,
            labels: {
            fontFamily: "Roboto",
            fontColor:"#878787"
            }
          },
          tooltip: {
            backgroundColor:'rgba(33,33,33,1)',
            cornerRadius:0,
            footerFontFamily:"'Roboto'"
          },
          elements: {
            arc: {
              borderWidth: 0
            }
          }
        }
      });
    }

    $('#datable_1').DataTable();
    $('#datable_2').DataTable();


});
</script>

</html>