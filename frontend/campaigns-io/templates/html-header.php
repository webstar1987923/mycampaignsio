<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo $document_data['title']; ?></title>
    <?php enqueue_html_stylesheets( $document_data['styles'] ); ?>
    <?php enqueue_html_scripts( $document_data['top_scripts'] ); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- bootstrap-select CSS -->
    <link href="<?php echo base_url(); ?>assets/doodle/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/doodle/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    
    <link href="<?php echo base_url(); ?>assets/doodle/css/morris.css" rel="stylesheet" type="text/css"/>	
  	<link href="<?php echo base_url(); ?>assets/doodle/css/chartlist.min.css" rel="stylesheet" type="text/css"/>	
    <link href="<?php echo base_url(); ?>assets/doodle/css/switchery.min.css" rel="stylesheet" type="text/css"/>  
    <link href="<?php echo base_url(); ?>assets/doodle/css/wizard.css" rel="stylesheet" type="text/css"/> 
  	<link href="<?php echo base_url(); ?>assets/doodle/css/jquery.steps.css" rel="stylesheet" type="text/css"/>	
  	<link href="<?php echo base_url(); ?>assets/doodle/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>	
  	<!-- <link href="<?php echo base_url(); ?>/doodle/css/jquery.toast.min.css" rel="stylesheet" type="text/css">		 -->
  	<link href="<?php echo base_url(); ?>assets/doodle/css/select2.min.css" rel="stylesheet" type="text/css">		
  	<!-- Custom CSS -->
  	<link href="<?php echo base_url(); ?>assets/doodle/css/style.css" rel="stylesheet" type="text/css">
  	<link href="<?php echo base_url(); ?>assets/doodle/css/custom-style.css" rel="stylesheet" type="text/css">

  	<script src="<?php echo base_url(); ?>assets/doodle/js/jquery.min.js"></script>

</head>

<body>