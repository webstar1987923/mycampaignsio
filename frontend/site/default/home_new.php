<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view( get_template_directory() . 'header' );
?>
<!--  Site Content -->
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default card-view pa-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-green">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                    <span class="txt-light block counter"><span class="counter-anim"><?php echo $total_domains['totalDomain']; ?></span></span>
                                    <span class="weight-500 uppercase-font txt-light block font-13">Total Domains</span>
                                </div>
                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                    <i class="zmdi zmdi-male-female txt-light data-right-rep-icon"></i>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default card-view pa-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-blue">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                    <span class="txt-light block counter"><span class="counter-anim"><?php echo $up_domains['totalUpdomains']; ?></span></span>
                                    <span class="weight-500 uppercase-font txt-light block">Sites Up</span>
                                </div>
                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                    <i class="zmdi zmdi-redo txt-light data-right-rep-icon"></i>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default card-view pa-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-red">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                    <span class="txt-light block counter"><span class="counter-anim"><?php echo $down_domains; ?></span></span>
                                    <span class="weight-500 uppercase-font txt-light block">Sites Down</span>
                                </div>
                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                    <i class="zmdi zmdi-file txt-light data-right-rep-icon"></i>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default card-view pa-0">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                    <div class="sm-data-box bg-yellow">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                    <span class="txt-light block counter"><span class="counter-anim">46.43</span></span>
                                    <span class="weight-500 uppercase-font txt-light block">Wordpress</span>
                                </div>
                                <div class="col-xs-6 text-center  pl-0 pr-0 pt-25  data-wrap-right">
                                    <div id="sparkline_4" style="width: 100px; overflow: hidden; margin: 0px auto;"></div>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Row -->
<div class="row">
    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
        <div class="panel panel-default card-view">
            <div class="refresh-container">
                <div class="la-anim-1"></div>
            </div>
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">ALL WEBSITES</h6>
                </div>
                <div class="pull-right">
                    <a href="<?php echo base_url('domains/add'); ?>" type="button" class="pull-left btn btn-primary">
						ADD NEW DOMAIN
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body row pa-0">
                    <div class="table-wrap">
                        <div class="table-responsive pb-15">
                            <table id="datable_3" class="table table-hover display pb-10">
                                <thead>
                                    <tr>
                                        <th>WEBSITE NAME</th>
                                        <th>ANALYTICS ID</th>
                                        <th>STATUS</th>
                                        <th>ACTION</th>                                       
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>WEBSITE NAME</th>
                                        <th>ANALYTICS ID</th>
                                        <th>STATUS</th>
                                        <th>ACTION</th>                                       
                                    </tr>
                                </tfoot>
                                <tbody id="domainlist">                                   
                                </tbody>
                            </table>
                        </div>
                    </div>	
                </div>	
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
        <div class="panel panel-default card-view">
            <div class="refresh-container">
                <div class="la-anim-1"></div>
            </div>
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Downtime report</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">                
                <div class="panel-body row pa-0">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr role="row">
                                        <th>Domain</th>
                                        <th>Duration</th>
                                    </tr>
                                </thead>
                                <tbody id="incidentreport">                                   
                                    <tr>
                                        <td colspan="2" style="text-align: center;">Loading...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>	
                </div>
            </div>	
        </div>	
    </div>	
</div>	

<script type="text/javascript">
    function escapeHtml(unsafe) {
        return unsafe.replace(/&/g, "&amp;")
                     .replace(/</g, "&lt;")
                     .replace(/>/g, "&gt;")
                     .replace(/"/g, "&quot;")
                     .replace(/'/g, "&#039;");
    }

    function openPiwikCode(url){
        var myWindow = window.open(url, "", "width=600,height=400");
    }

    function searchAllDomain() {
        var domain = $('#searchDomain').val();
        if( domain.length > 2 ) {
            getAllDomains( domain );  
        }
        else if( '' === domain) {
            getAllDomains();
        }
    }

    function getAllDomains(domain, page) {
        
        domain = 'undefined' === typeof domain ? null : domain;
        page = ! page ? 2 : page;

        console.log(domain, page);

        // Getting user domains.      
        $.post( '<?php echo base_url(); ?>auth/home/alldomains', { domain:domain, page:page }, function(data){

            var html = '';

            if( data.status ){

                var i, url, tootltipTitle, tootltipTitleClassname, pagination;
                console.log(data.domain);
                for(i=0;i<data.domain.length;i++){

                    // url = ("/analytics/code/" + data.domain[i].id );
                    var piwik = (data.domain[i].piwik_site_id != null)?data.domain[i].piwik_site_id:'-';
                    html += '<tr>';
                    html += '<td class="txt-dark">' + data.domain[i].domain_name + '</td>';
                    html += '<td class="txt-dark">' + piwik+ '</td>';
                    html += '<td>';

                    switch( data.domain[i].server_status ){
                        case "UP":
                            tootltipTitle = 'Site is Up';
                            tootltipTitleClassname = 'label label-success ml-5';
                            break;
                        case "DOWN":
                            tootltipTitle = 'Site is Down';
                            tootltipTitleClassname = '';
                            break;
                        default:
                            tootltipTitle = 'Status not available';
                            tootltipTitleClassname = '';
                    }

                    html += '<span data-toggle="tooltip" data-placement="top" title="" class=" ml-10" data-original-title="Site Load Speed">' + parseFloat(data.domain[i].avg_load_time/1000).toFixed(2) + '</span>';
                    html += '<span data-toggle="tooltip" data-placement="top" title="" class="' + tootltipTitleClassname + '" data-original-title="' + tootltipTitle + '"> </span>';
                    // html += '<span data-toggle="tooltip" data-placement="top" title="" class="" data-original-title=""> </span>';
                    html += '</td>';
                    html += '<td>';
                    html += '<a href="<?php echo base_url(); ?>domains/' + data.domain[i].id + '/wordpress/login" data-toggle="tooltip" data-placement="top" title="" target="_blank" data-original-title="Login Wordpress admin"><i class="fa fa-wordpress"></i></a>';
                    html += '<a href="<?php echo base_url(); ?>domains/' + data.domain[i].id + '" data-toggle="tooltip" data-placement="top" title="" class="ml-5" data-original-title="View Site Stats"><i class="icon-eye"></i></a>';
                    <?php if(!isset($user_session['parent_id'])): ?>
                        html += '<a href="<?php echo base_url(); ?>domains/' + data.domain[i].id + '/edit" data-toggle="tooltip" data-placement="top" title="" class="ml-5" data-original-title="Edit Site"><i class="icon-pencil"></i></a>';
                        html += '<a href="<?php echo base_url(); ?>domains/' + data.domain[i].id + '/delete" data-toggle="tooltip" data-placement="top" title="" class="ml-5" onclick="return deleteDomain();" data-original-title="Delete Site"><i class="icon-trash"></i></a>';
                    <?php endif; ?>
                    html += '<a href="//' + data.domain[i].domain_name + '" data-toggle="tooltip" data-placement="top" title="" target="_blank"  class="ml-5" data-original-title="Visit Site"><i class="icon-share-alt"></i></a>';
                    html += '</td>';
                    html += '</tr>';
                }
                $('#domainlist').html( html );                
                $('#datable_3').DataTable();
                $('[data-toggle="tooltip"]').tooltip();
            }
            else{
                html += ' <tr><td colspan="4" style="text-align: center;">Loading...</td></tr>';
                $('#domainlist').html( html );
                $('[data-toggle="tooltip"]').tooltip();
            }
        },'json');   
    }

    function deleteDomain(){
        if( confirm( 'Are you sure you want to delete this domain?' ) ){
            return true;
        }
        else {
            return false;
        }
    }
    
    getAllDomains();

    $.post( '/auth/home/incidentreport', function(data) {
        var i, addclass, tooltipdata, html = '';

        if( 'success' === data.type ){
            addclass = 'odd';
            for( i=0; i < data.payload.length; i++ ){

                tooltipdata = escapeHtml( data.payload[i].error );

                html += '<tr>';
                html += '<td>';
                html += '<span data-toggle="tooltip" data-placement="top" title="' + tooltipdata + '" class="btn btn-sm btn-rounded btn-danger pull-right m-l-10"><i class="icon-info"></i></span>';
                html+= data.payload[i].domain_name;
                // html += '<td>'+data.payload[i].downtime+' </td>';
                html += '</td>';
                html += '<td>' + data.payload[i].totaloutagetime + '</td>';
                html += '</tr>'; 
            }
        }
        else {
            html += '<tr>';
            html += '<td colspan="4" style="text-align: center;">Loading...</td>';
            html += '</tr>';
        }

        $('#incidentreport').html( html );
        $('[data-toggle="tooltip"]').tooltip();
    },'json');
</script>

<?php $this->load->view( get_template_directory() . 'footer_new'); ?>
