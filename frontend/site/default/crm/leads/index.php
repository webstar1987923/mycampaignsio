<?php $page = 'leads'; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
		<div class="col-md-8 col-lg-9">
			<div class="panel panel-default border-panel card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h5><?php echo lang('leads'); ?></h5>
						<span class="panel-subtitle">
						<?php echo lang('leaddesc'); ?>
					</span>
					</div>
					<div class="pull-right">
						<div class="counter pull-right btn btn-success ml-10 label-lead-area-stat">
							<?php echo $tcl ?> CONVERTED
						</div>
						<div class="counter pull-right btn btn-danger ml-10 label-lead-area-stat">
							<?php echo $tll ?> LOST
						</div>
						<div class="counter pull-right btn btn-warning ml-10 label-lead-area-stat">
							<?php echo $tjl ?> JUNK
						</div>
						<div style="width: 200px" class="pull-right">
							<select id="" class="select2" onChange="window.location.href=this.value">
								<optgroup label="Firs Filter">
									<option value='?'>
										<?php echo lang('filter') ?>
									</option>
									<option value='?'>
										<?php echo lang('all') ?>
									</option>
									<option value='?filter-staff=<?php echo $this->session->userdata(' logged_in_staff_id '); ?>'>
										<?php echo lang('myleads') ?>
									</option>
								</optgroup>
								<optgroup label="Filter by Status">
									<?php
	                  foreach ($leadsstatuses as $status) {
	                      $selected = ($status[ 'id' ] == $this->input->post('source')) ? ' selected="selected"' : null;
	                      echo '<option value="?filter-status=' . $status[ 'id' ] . '" ' . $selected . '>' . $status[ 'name' ] . '</option>';
	                  }
	                ?>
								</optgroup>
								<optgroup label="Filter by Source">
									<?php
	                  foreach ($leadssources as $source) {
	                      $selected = ($source[ 'id' ] == $this->input->post('source')) ? ' selected="selected"' : null;
	                      echo '<option value="?filter-source=' . $source[ 'id' ] . '" ' . $selected . '>' . $source[ 'name' ] . '</option>';
	                  }
	                ?>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="datable_1" class="table table-hover display  pb-30" >
									<thead>
										<tr>
											<th width="7%">
												<?php echo lang('id')?>
											</th>
											<th>
												<?php echo lang('name')?>
											</th>
											<th>
												<?php echo lang('company')?>
											</th>
											<th>
												<?php echo lang('email')?>
											</th>
											<th>
												<?php echo lang('phone')?>
											</th>
											<th>
												<?php echo lang('assigned')?>
											</th>
											<th>
												<?php echo lang('status')?>
											</th>
											<th>
												<?php echo lang('source')?>
											</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th width="7%">
												<?php echo lang('id')?>
											</th>
											<th>
												<?php echo lang('name')?>
											</th>
											<th>
												<?php echo lang('company')?>
											</th>
											<th>
												<?php echo lang('email')?>
											</th>
											<th>
												<?php echo lang('phone')?>
											</th>
											<th>
												<?php echo lang('assigned')?>
											</th>
											<th>
												<?php echo lang('status')?>
											</th>
											<th>
												<?php echo lang('source')?>
											</th>
										</tr>
									</tfoot>
									<tbody>
										<?php foreach ($leads as $lead) { ?>
										<tr data-filterable data-filter-status="<?php echo $lead['status']?>" data-filter-source="<?php echo $lead['source']?>" data-filter-staff="<?php echo $lead['staffid']?>" class="ciuis-254325232344" onclick="window.location='<?php echo base_url('leads/lead/'.$lead['id'].'')?>'">
											<td>
												<?php echo $lead['id']; ?>
											</td>
											<td>
												<strong>
													<?php echo $lead['leadname']; ?>
												</strong>
											</td>
											<td>
												<?php echo $lead['company']; ?>
											</td>
											<td>
												<?php echo $lead['leadmail']; ?>
											</td>
											<td>
												<?php echo $lead['leadphone']; ?>
											</td>
											<td class="user-avatar"> <img src="<?php echo base_url('uploads/staffavatars/'.$lead['assignedavatar'].'')?>" alt="Avatar">
												<?php echo $lead['leadassigned']; ?>
											</td>
											<td>
												<span class="badge" style="border-color: #fff;background-color: <?php echo $lead['color']; ?>;">
													<?php echo $lead['statusname']; ?>
												</span>
											</td>
											<td>
												<span class="badge" style="border-color: #fff;background-color: <?php echo $lead['color']; ?>;">
													<?php echo $lead['sourcename']; ?> </span>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="btn-group btn-space pull-right"> <button type="button" class="btn btn-primary btn-lable-wrap left-label" data-target="#import-lead" data-toggle="modal">
				<span class="btn-label"><i class="icon-arrow-up-circle"></i></span>
				<span class="btn-text"><?php echo lang('importleads'); ?></span>
				</button>
			</div>
		</div>
	<div class="col-md-4 col-lg-3">
		<div class="panel panel-default card-view lead-manager-head">
			<div class="panel-heading">
					<div class="">
						<button data-target="#addlead" data-toggle="modal" class="btn btn-sm btn-primary pl-10 pr-10"><span class="btn-text">Add <?php echo lang('lead')?></span></button>
						<button data-target="#add_status" data-toggle="modal" class="btn btn-sm btn-primary pl-10 pr-10"><span class="btn-text">Add <?php echo lang('status')?></span></button>
						<button data-target="#add_source" data-toggle="modal" class="btn btn-sm btn-primary pl-10 pr-10"><span class="btn-text">Add <?php echo lang('source')?></span></button>
					</div>
				<a href="<?php echo base_url('leads/kanban')?>" class="btn btn-lg btn-default show-kanban col-md-12 hidden">
					<?php echo lang('showkanban');?>
				</a>
			</div>
			<div class="panel-body lead-manager">
				<table id="table" class="table table-striped table-hover table-fw-widget">
					<thead>
						<tr class="text-muted">
							<th>
								<?php echo lang('leadssources')?>
							</th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<?php foreach ($leadssources as $source) { ?>
					<tr>
						<td>
							<b class="text-muted">
								<?php echo $source['name']; ?>
							</b>
						</td>
						<td>
							<div class="pull-right">
								<button type="button" data-target="#update_source<?php echo $source['id']; ?>" data-toggle="modal" data-placement="left" title="" class="btn btn-xs btn-warning pl-10 pr-10" data-original-title="Update Source"><i class="icon-note"></i></button>
								<a href="<?php echo site_url('leads/removesource/'.$source['id']); ?>"><button class="btn btn-danger btn-xs pl-10 pr-10"><i class="icon-trash"></i></button></a>
							</div>
						</td>
					</tr>
					<div id="update_source<?php echo $source['id']; ?>" tabindex="-1" role="content" class="modal fade">
						<div class="modal-dialog">
							<?php echo form_open('leads/update_source/'.$source['id']); ?>
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h5 class="modal-title">
										<?php echo lang('updatesource'); ?>
									</h5>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label for="name" class="control-label mb-10 text-left">
											<?php echo lang('name'); ?>
										</label>
										<div class="input-group"><span class="input-group-addon"><i class="icon-user"></i></span>
											<input type="text" name="name" value="<?php echo($this->input->post('name') ? $this->input->post('name') : $source['name']); ?>" class="form-control" id="name" placeholder="name"/>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
										<?php echo lang('cancel'); ?>
									</button>
									<button type="submit" class="btn btn-success">
										<?php echo lang('save'); ?>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
					<?php } ?>
				</table>
				<table id="table" class="table table-striped table-hover table-fw-widget">
					<thead>
						<tr class="text-muted">
							<th>
								<?php echo lang('leadsstatuses')?>
							</th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<?php foreach ($leadsstatuses as $status) { ?>
					<tr>
						<td>
							<b class="text-muted">
								<?php echo $status['name']; ?>
							</b>
						</td>
						<td>
							<div class="pull-right">
								<button type="button" data-target="#update_status<?php echo $status['id']; ?>" data-toggle="modal" data-placement="left" title="" class="btn btn-xs btn-warning pl-10 pr-10" data-original-title="Update Expense Category"><i class="icon-note"></i></button>
								<a href="<?php echo site_url('leads/removestatus/'.$status['id']); ?>"><button class="btn btn-xs btn-danger pl-10 pr-10"><i class="icon-trash"></i></button></a>
							</div>
							
							
						</td>
					</tr>
					<div id="update_status<?php echo $status['id']; ?>" tabindex="-1" role="content" class="modal fade">
						<div class="modal-dialog">
							<?php echo form_open('leads/update_status/'.$status['id']); ?>
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h5 class="modal-title">
										<?php echo lang('updatestatus'); ?>
									</h5>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label for="name" class="control-label mb-10 text-left">
											<?php echo lang('name'); ?>
										</label>
										<div class="input-group"><span class="input-group-addon"><i class="icon-user"></i></span>
											<input type="text" name="name" value="<?php echo($this->input->post('name') ? $this->input->post('name') : $status['name']); ?>" class="form-control" id="name" placeholder="name"/>
										</div>
									</div>
									<div class="form-group">
										<label for="color" class="control-label mb-10 text-left">
											<?php echo lang('color'); ?>
										</label>
										<div class="input-group"><span class="input-group-addon"><i class="icon-info"></i></span>
											<input type="text" name="color" value="<?php echo($this->input->post('color') ? $this->input->post('color') : $status['color']); ?>" class="form-control" id="color" placeholder="color"/>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
										<?php echo lang('cancel'); ?>
									</button>
									<button type="submit" class="btn btn-success">
										<?php echo lang('save'); ?>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
					<?php } ?>
				</table>
				<br>
			</div>
		</div>
	</div>

	
</div>


<div id="add_status" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('leads/add_status'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">
					<?php echo lang('addstatus')?>
				</h5>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="control-label mb-10 text-left">
						<?php echo lang('name'); ?>
					</label>
					<div class="input-group"><span class="input-group-addon"><i class="icon-user"></i></span>
						<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" placeholder="name"/>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="control-label mb-10 text-left">
						<?php echo lang('color'); ?>
					</label>
					<div class="input-group"><span class="input-group-addon"><i class="icon-info"></i></span>
						<input type="text" name="color" value="<?php echo $this->input->post('color'); ?>" class="form-control" id="color" placeholder="color"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
					<?php echo lang('cancel'); ?>
				</button>
				<button type="submit" class="btn btn-success">
					<?php echo lang('save'); ?>
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="import-lead" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open_multipart('leads/importcsv'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">
					<?php echo lang('importleads')?>
				</h5>
			</div>
			<div class="modal-body">
				<div class="form-group mb-30">
					<label for="name" class="control-label mb-10 text-left"><?php echo lang('choosecsvfile'); ?></label>
					<div class="fileinput fileinput-new input-group" data-provides="fileinput">
						<div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"><?php echo lang('attachment')?></span></div>
						<span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text"><?php echo lang('notchoise')?></span> <span class="fileinput-exists btn-text">Change</span>
						<input type="file" name="userfile" id="chooseFile">
						</span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a> 
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						<label for="importsource" class="control-label mb-10 text-left">
							<?php echo lang('source');?>
						</label>
						<select required name="importsource" class="select2">
							<option>
								<?php echo lang('source');?>
							</option>
							<?php
                foreach ($leadssources as $source) {
                    $selected = ($source[ 'id' ] == $this->input->post('importsource')) ? ' selected="selected"' : null;
                    echo '<option value="' . $source[ 'id' ] . '" ' . $selected . '>' . $source[ 'name' ] . '</option>';
                }
               ?>
						</select>
					</div>
					<div class="col-md-6 md-pr-0">
						<label for="importstatus" class="control-label mb-10 text-left">
							<?php echo lang('status');?>
						</label>
						<select required name="importstatus" class="select2">
							<option>
								<?php echo lang('status');?>
							</option>
							<?php
                foreach ($leadsstatuses as $status) {
                    $selected = ($status[ 'id' ] == $this->input->post('importstatus')) ? ' selected="selected"' : null;
                    echo '<option value="' . $status[ 'id' ] . '" ' . $selected . '>' . $status[ 'name' ] . '</option>';
                }
               ?>
						</select>
					</div>
					</div>
				</div>
				<div class="form-group">
					<label for="importassigned" class="control-label mb-10 text-left">
						<?php echo lang('assignedstaff');?>
					</label>
					<select required name="importassigned" class="select2">
						<option value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>"><?php echo lang('me');?></option>
						<?php
              foreach ($all_staff as $staff) {
                  $selected = ($staff[ 'id' ] == $this->input->post('importassigned')) ? ' selected="selected"' : null;
                  echo '<option value="' . $staff[ 'id' ] . '" ' . $selected . '>' . $staff[ 'staffname' ] . '</option>';
              }
            ?>
					</select>
				
				</div>
				<div class="well well-sm card-view bg-blue">
					<h6><?php echo lang('importcustomerinfo'); ?></h6>
				</div>
			</div>
			<div class="modal-footer">
				<a href="<?php echo base_url('uploads/samples/leadimport.csv')?>" class="btn btn-success pull-left">
					<?php echo lang('downloadsample'); ?>
				</a>
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
					<?php echo lang('cancel'); ?>
				</button>
				<button type="submit" class="btn btn-success">
					<?php echo lang('save'); ?>
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="add_source" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('leads/add_source'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">
					<?php echo lang('addsource')?>
				</h5>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="control-label mb-10 text-left">
						<?php echo lang('name'); ?>
					</label>
					<div class="input-group"><span class="input-group-addon"><i class="icon-user"></i></span>
						<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" placeholder="name"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
					<?php echo lang('cancel'); ?>
				</button>
				<button type="submit" class="btn btn-success">
					<?php echo lang('save'); ?>
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="addlead" tabindex="-1" role="dialog" class="modal fade">
	<?php echo form_open('leads/add', array("class"=>"form-vertical")); ?>
	<div style="width: 70%;" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">
					<?php echo lang('newlead');?>
				</h5>
				<span>
					<?php echo lang('newleaddesc');?>
				</span>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div id="" class="form-group company">
							<label class="control-label mb-10 text-left">
								<?php echo lang('name')?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-user"></i></span>
								<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="title" class="control-label mb-10 text-left">
								<?php echo lang('title');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-info"></i></span>
								<input type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div id="" class="form-group company">
							<label for="company" class="control-label mb-10 text-left">
								<?php echo lang('company');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-briefcase"></i></span>
								<input type="text" name="company" value="<?php echo $this->input->post('company'); ?>" class="form-control" id="company"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="description" class="control-label mb-10 text-left">
								<?php echo lang('description');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class=" icon-options"></i></span>
								<input type="text" name="description" value="<?php echo $this->input->post('description'); ?>" class="form-control" id="description"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="assigned" class="control-label mb-10 text-left">
								<?php echo lang('assignedstaff');?>
							</label>
							<select required name="assigned" class="select2">
								<option value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>"><?php echo lang('me');?></option>
								<?php
                  foreach ($all_staff as $staff) {
                      $selected = ($staff[ 'id' ] == $this->input->post('assigned')) ? ' selected="selected"' : null;
                      echo '<option value="' . $staff[ 'id' ] . '" ' . $selected . '>' . $staff[ 'staffname' ] . '</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="phone" class="control-label mb-10 text-left">
								<?php echo lang('phone');?>
							</label>
							<div class="input-group "><span class="input-group-addon"><i class="icon-phone"></i></span>
								<input type="text" name="phone" value="<?php echo $this->input->post('phone'); ?>" class="form-control" id="phone"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
								<label for="source" class="control-label mb-10 text-left">
									<?php echo lang('source');?>
								</label>
								<select required name="source" class="select2 required">
									<option value="">
										<?php echo lang('source');?>
									</option>
									<?php
                    foreach ($leadssources as $source) {
                        $selected = ($source[ 'id' ] == $this->input->post('source')) ? ' selected="selected"' : null;
                        echo '<option value="' . $source[ 'id' ] . '" ' . $selected . '>' . $source[ 'name' ] . '</option>';
                    }
                   ?>
								</select>
							</div>
							<div class="col-md-6">
								<label for="status" class="control-label mb-10 text-left">
									<?php echo lang('status');?>
								</label>
								<select required name="status" class="select2 required">
									<option value="">
										<?php echo lang('status');?>
									</option>
									<?php
                    foreach ($leadsstatuses as $status) {
                        $selected = ($status[ 'id' ] == $this->input->post('status')) ? ' selected="selected"' : null;
                        echo '<option value="' . $status[ 'id' ] . '" ' . $selected . '>' . $status[ 'name' ] . '</option>';
                    }
                   ?>
								</select>
							</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="email" class="control-label mb-10 text-left">
								<?php echo lang('email');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-envelope-open"></i></span>
								<input required type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<div class="form-group">
							<label for="website" class="control-label mb-10 text-left">
								<?php echo lang('website');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-globe"></i></span>
								<input type="text" name="website" value="<?php echo $this->input->post('website'); ?>" class="form-control" id="website"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="address" class="control-label mb-10 text-left">
								<?php echo lang('address');?>
							</label>
							<textarea name="address" class="form-control">
								<?php echo $this->input->post('address'); ?>
							</textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label for="country" class="control-label mb-10 text-left">
								<?php echo lang('country');?>
							</label>
							<select required name="country" class="select2 required">
								<option value="">
									<?php echo lang('country');?>
								</option>
								<?php
                  foreach ($countries as $country) {
                      $selected = ($country[ 'id' ] == $this->input->post('country')) ? ' selected="selected"' : null;
                      echo '<option value="' . $country[ 'id' ] . '" ' . $selected . '>' . $country[ 'shortname' ] . '</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label for="state" class="control-label mb-10 text-left">
								<?php echo lang('state');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-location-pin"></i></span>
								<input type="text" name="state" value="<?php echo $this->input->post('state'); ?>" class="form-control" id="state"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label for="city" class="control-label mb-10 text-left">
								<?php echo lang('city');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-location-pin"></i></span>
								<input type="text" name="city" value="<?php echo $this->input->post('city'); ?>" class="form-control" id="city"/>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label for="zip" class="control-label mb-10 text-left">
								<?php echo lang('zip');?>
							</label>
							<div class="input-group"><span class="input-group-addon"><i class="icon-location-pin"></i></span>
								<input type="text" name="zip" value="<?php echo $this->input->post('zip'); ?>" class="form-control" id="zip"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
					<div class="form-group pull-left mt-20">
						<div class="checkbox checkbox-success checkbox-circle pull-left">
							<input name="public" class="success-check" id="publivs" type="checkbox" value="1">
							<label for="publivs" class="control-label mb-10 text-left">
								<?php echo lang('public');?>
							</label>
						</div>
						<div class="checkbox checkbox-primary checkbox-circle pull-left ml-20 mt-0">
							<input name="type" class="primary-check" id="type" type="checkbox" value="1">
							<label for="type" class="control-label mb-10 text-left">
								<?php echo lang('individuallead');?>
							</label>
						</div>
					</div>
				<div class="form-group pull-right">
					<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
						<?php echo lang('cancel');?>
					</button>
					<button type="submit" class="btn btn-success modal-close">
						<?php echo lang('add');?>
					</button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('form').each(function() {  // attach to all form elements on page
        $(this).validate({       // initialize plugin on each form
            // global options for plugin
        });
    });

    $('#datable_1').DataTable();
    $('#datable_2').DataTable({ "lengthChange": false});

});
	$( '#chooseFile' ).bind( 'change', function () {
		var filename = $( "#chooseFile" ).val();
		if ( /^\s*$/.test( filename ) ) {
			$( ".file-upload" ).removeClass( 'active' );
			$( "#noFile" ).text( "<?php echo lang('notassignedanystaff')?>" );
		} else {
			$( ".file-upload" ).addClass( 'active' );
			$( "#noFile" ).text( filename.replace( "C:\\fakepath\\", "" ) );
		}
	} );
	$( '.search-table-external' ).on( 'keyup click', function () {
		$( '#table2' ).DataTable().search(
			$( '.search-table-external' ).val()
		).draw();
	} );
</script>