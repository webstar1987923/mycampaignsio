<?php $page = "proposals"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>
<div class="row">
	<div class="col-xs-12 col-md-12 col-lg-12">
		<div class="panel card-view">
			<div class="panel-wrapper">
				<div class="panel-body">
					<div class="row border-right-grey">
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('draft')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $dpp; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $dpp; ?>%</b> <?php echo $dpc; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('sent')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $spp; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $spp; ?>%</b> <?php echo $spc; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('open')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $opp; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $opp; ?>%</b> <?php echo $opc; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('revised')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $rpp; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $rpp; ?>%</b> <?php echo $rpc; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('declined')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $pdp; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $pdp; ?>%</b> <?php echo $pdc; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-2">
							<h5 class="mb-15">
								<?php echo lang('accepted')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $pap; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $pap; ?>%</b> <?php echo $pac; ?>/<?php echo $tpc; ?></span>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<a href="<?php echo base_url('proposals/add');?>" type="button" class="btn btn-primary">
						<i class="fa fa-plus mr-5"></i> <?php echo lang('createproposal'); ?>
					</a>
				</div>
				<div class="pull-right">
					<div style="width: 200px" class="btn-group">
						<select id="" class="select2" onChange="window.location.href=this.value">
							<option value="?"><?php echo lang('filter') ?></option>
							<option value="?"><?php echo lang('all') ?></option>
							<option value="?filter-status=1"><?php echo lang('draft') ?></option>
							<option value="?filter-status=2"><?php echo lang('sent') ?></option>
							<option value="?filter-status=3"><?php echo lang('open') ?></option>
							<option value="?filter-status=4"><?php echo lang('revised') ?></option>
							<option value="?filter-status=5"><?php echo lang('declined') ?></option>
							<option value="?filter-status=6"><?php echo lang('accepted') ?></option>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="datable_1" class="table table-hover display  pb-30">
								<thead>
									<tr>
										<th><?php echo lang('id')?></th>
										<th><?php echo lang('name')?></th>
										<th><?php echo lang('to')?></th>
										<th><?php echo lang('total')?></th>
										<th><?php echo lang('date')?></th>
										<th><?php echo lang('opentill')?></th>
										<th><?php echo lang('assigned')?></th>
										<th><?php echo lang('status')?></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th><?php echo lang('id')?></th>
										<th><?php echo lang('name')?></th>
										<th><?php echo lang('to')?></th>
										<th><?php echo lang('total')?></th>
										<th><?php echo lang('date')?></th>
										<th><?php echo lang('opentill')?></th>
										<th><?php echo lang('assigned')?></th>
										<th><?php echo lang('status')?></th>
									</tr>
								</tfoot>
								<tbody>
									<?php foreach($proposals as $proposal){ ?>
										<tr data-filterable data-filter-status="<?php echo $proposal['statusid']?>" data-filter-staff="<?php echo $proposal['assigned']?>" class="ciuis-254325232344" onclick="window.location='<?php echo base_url('proposals/proposal/'.$proposal['id'].'')?>'">
											<td>
												<?php echo $proposal['id']; ?>
												
											</td>
											<td>
												<strong>
													<?php echo $proposal['subject']; ?><br>
													<a href="<?php echo base_url('invoices/invoice/'.$proposal['invoiceid'].'')?>"  class="label label-default" style="<?php if($proposal['invoiceid'] === NULL){echo 'display:none';}?>"><b><i class="ion-document-text"> </i><?php echo lang('invoiced') ?></b></a>
												</strong>
											</td>
											<td>
												<?php $pro = $this->Proposals_Model->get_proposals($proposal['id'],$proposal['relation_type'] );?>
												<?php if($pro['relation_type'] == 'customer'){if($pro['customer']===NULL){echo '<strong><a data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="'.lang('customer').'" href="'.base_url('customers/customer/'.$proposal['relation'].'').'">'.$pro['namesurname'].'</a></strong>' ;} else echo '<strong><a data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="'.lang('customer').'" href="'.base_url('customers/customer/'.$proposal['relation'].'').'">'.$pro['customer'].'</a></strong>';} ?>
												<?php if($pro['relation_type'] == 'lead'){echo '<strong><a data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="'.lang('lead').'" href="'.base_url('leads/lead/'.$proposal['relation'].'').'">'.$pro['leadname'].'</a></strong>';} ?>
											</td>
											<td>
												<strong class="text-muted"><span class="money-area"><?php echo $proposal['total']; ?></span></strong>
											</td>
											<td>
												<?php switch($settings['dateformat']){ 
													case 'yy.mm.dd': echo _rdate($proposal['date']);break; 
													case 'dd.mm.yy': echo _udate($proposal['date']); break;
													case 'yy-mm-dd': echo _mdate($proposal['date']); break;
													case 'dd-mm-yy': echo _cdate($proposal['date']); break;
													case 'yy/mm/dd': echo _zdate($proposal['date']); break;
													case 'dd/mm/yy': echo _kdate($proposal['date']); break;
													}?>
											</td>
											<td>
												<?php switch($settings['dateformat']){ 
													case 'yy.mm.dd': echo _rdate($proposal['opentill']);break; 
													case 'dd.mm.yy': echo _udate($proposal['opentill']); break;
													case 'yy-mm-dd': echo _mdate($proposal['opentill']); break;
													case 'dd-mm-yy': echo _cdate($proposal['opentill']); break;
													case 'yy/mm/dd': echo _zdate($proposal['opentill']); break;
													case 'dd/mm/yy': echo _kdate($proposal['opentill']); break;
													}?>
											</td>
											<td class="user-avatar md-pl-20"> <img src="<?php echo base_url('uploads/staffavatars/'.$proposal['staffavatar'].'')?>" alt="Avatar">
												<?php echo $proposal['staffmembername']; ?>
											</td>
											<td>
												<?php if($proposal['statusid'] == 1){echo '<span class="label label-success pull-right proposal-status-draft">'.lang('draft').'</span>';}  ?>
												<?php if($proposal['statusid'] == 2){echo '<span class="label label-success pull-right proposal-status-sent">'.lang('sent').'</span>';}  ?>
												<?php if($proposal['statusid'] == 3){echo '<span class="label label-success pull-right proposal-status-open">'.lang('open').'</span>';}  ?>
												<?php if($proposal['statusid'] == 4){echo '<span class="label label-success pull-right proposal-status-revised">'.lang('revised').'</span>';}  ?>
												<?php if($proposal['statusid'] == 5){echo '<span class="label label-success pull-right proposal-status-declined">'.lang('declined').'</span>';}  ?>
												<?php if($proposal['statusid'] == 6){echo '<span class="label label-success pull-right proposal-status-accepted">'.lang('accepted').'</span>';}  ?>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_graph.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_invoice.php';?>
	<script type="text/javascript">
		$( document ).ready( function () {
			"use strict";

			$('#datable_1').DataTable();
    	$('#datable_2').DataTable({ "lengthChange": false});
		} );
	</script>