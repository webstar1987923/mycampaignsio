<?php $page = 'report'; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4><?php echo lang('menu_reports');?></h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
					<div  class="tab-struct custom-tab-1">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<li class="active" role="presentation"><a id="tab_1" href="#financial_summary" aria-expanded="true"  data-toggle="tab" role="tab"><?php echo lang('financialsummary');?></a></li>
							<li role="presentation" class=""><a id="tab_2" href="#invoice_ist" data-toggle="tab" role="tab" aria-expanded="false"><?php echo lang('invoicessummary');?></a></li>
							<li role="presentation" class=""><a id="tab_3" href="#customer_ist" data-toggle="tab" role="tab" aria-expanded="false"><?php echo lang('customersummary');?></a></li>
						</ul>
						<div class="tab-content" id="myTabContent_7">
							<div id="financial_summary" class="tab-pane active cont">
								<div class="col-md-12">
									<div class="panel panel-default border-panel card-view">
										<div class="panel-heading">
											<div class="pull-left">
												<i style="font-size: 38px;color: #eaeaea;"" class="ion-stats-bars mr-15"></i>
											</div>
											<div class="pull-left mr-20">
												<h4><?php echo lang('salesgraphtitle');?></h4>
												<span class="panel-subheading"><?php echo lang('staffsalesgraphdescription');?></span>
											</div>
											<div class="pull-left">
												<h4 class="text-success counter-anim"><?php echo $ycr; ?></h4>
												<span class="panel-subheading"><?php echo lang('inthisyear');?></span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<div class="chart-wrapper" style="height:300px">
													<canvas id="m12gain" height="300px"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="panel panel-default border-panel card-view">
										<div class="panel-heading">
											<div class="pull-left">
												<i style="font-size: 38px;color: #eaeaea;"" class="ion-stats-bars mr-15"></i>
											</div>
											<div class="pull-left mr-20">
												<h4><?php echo lang('weeklysaleschart');?></h4>
												<span class="panel-subheading"><?php echo lang('currentweekstats');?></span>
											</div>
											<div class="pull-left">
												<h4 class="text-success counter-anim"><?php echo $bht; ?></h4>
												<span class="panel-subheading"><?php echo lang('inthisweek');?></span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<div class="chart-wrapper" style="height:300px">
													<canvas id="weeklygain_c" height="300px"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<div class="panel panel-default border-panel card-view">
										<div class="panel-heading">
											<div class="pull-left">
												<i style="font-size: 38px;color: #eaeaea;" class="ion-stats-bars mr-15"></i>
											</div>
											<div class="pull-left mr-20">
												<h4><?php echo lang('incomingsvsoutgoings');?></h4>
												<span class="panel-subheading"><?php echo lang('currentyearstats');?></span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<div id="incomingsvsoutgoins" class="morris-chart"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="invoice_ist" class="tab-pane cont">
								<div class="col-md-12">
									<div class="panel panel-default border-panel card-view">
										<div class="panel-heading">
											<div class="pull-left">
												<i style="font-size: 38px;color: #eaeaea;" class="ion-stats-bars mr-15"></i>
											</div>
											<div class="pull-left mr-20">
												<h4><?php echo lang('invoicesbystatuses');?></h4>
												<span class="panel-subheading"><?php echo lang('billsbystatus');?></span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<div class="chart-wrapper" style="height:auto">
													<canvas id="invoice_chart_by_status"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="customer_ist" class="tab-pane">
								<div class="col-md-12">
									<div class="panel panel-default card-view">
										<div class="panel-heading">
											<h5><?php echo lang('customermonthlyreporttitle');?></h5>
										</div>
										<div class="panel-wrapper">
											<div class="panel-body">
												<div class="row">
													<div class="col-md-3">
														<?php
														echo '<select name="m" class="form-control select2" data-none-selected-text="April">' . PHP_EOL;
														for ( $m = 1; $m <= 12; $m++ ) {
															$_selected = '';
															if ( $m == date( 'm' ) ) {
																$_selected = ' selected';
															}
															echo '  <option value="' . $m . '"' . $_selected . '>' . ( date( 'F', mktime( 0, 0, 0, $m, 1 ) ) ) . '</option>' . PHP_EOL;
														}
														echo '</select>' . PHP_EOL;
														?>
													</div>
												</div>
												<canvas class="customergraph_ciuis-xe chart mt-20" id="customergraph_ciuis-xe" height="100"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_graph.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_report.php';?>

<script>
	new Chart($('#invoice_chart_by_status'), {
             type: 'horizontalBar',
             data: <?php echo $invoice_chart_by_status; ?>,
			 options: {
				 legend: {
					display: false,
				 }
			 }
		 });
</script>
<script>

	"use strict";

/*****Ready function start*****/
$(document).ready(function(){

	var values = <?php echo $incomings_vs_outgoins; ?>;
	if($('#incomingsvsoutgoins').length > 0) {
		var data = [{
			period: values.labels[0],
			Incomings: values.datasets[0].data[0],
			Expenses: values.datasets[1].data[0],
		},
		{
			period: values.labels[1],
			Incomings: values.datasets[0].data[1],
			Expenses: values.datasets[1].data[1],
		},
		{
			period: values.labels[2],
			Incomings: values.datasets[0].data[2],
			Expenses: values.datasets[1].data[2],
		},
		{
			period: values.labels[3],
			Incomings: values.datasets[0].data[3],
			Expenses: values.datasets[1].data[3],
		},
		{
			period: values.labels[4],
			Incomings: values.datasets[0].data[4],
			Expenses: values.datasets[1].data[4],
		},
		{
			period: values.labels[5],
			Incomings: values.datasets[0].data[5],
			Expenses: values.datasets[1].data[5],
		},
		{
			period: values.labels[6],
			Incomings: values.datasets[0].data[6],
			Expenses: values.datasets[1].data[6],
		},
		{
			period: values.labels[7],
			Incomings: values.datasets[0].data[7],
			Expenses: values.datasets[1].data[7],
		},
		{
			period: values.labels[8],
			Incomings: values.datasets[0].data[8],
			Expenses: values.datasets[1].data[8],
		},
		{
			period: values.labels[9],
			Expenses: values.datasets[1].data[9],
			Expenses: values.datasets[1].data[9],
		},
		{
			period: values.labels[10],
			Incomings: values.datasets[0].data[10],
			Expenses: values.datasets[1].data[10],
		},
		{
			period: values.labels[11],
			Incomings: values.datasets[0].data[11],
			Expenses: values.datasets[1].data[11],
		},
		];
		var lineChart = Morris.Line({
		element: 'incomingsvsoutgoins',
		data: data ,
		xkey: 'period',
		ykeys: ['Incomings', 'Expenses'],
		pointSize: 2,
		fillOpacity: 0,
		lineWidth:2,
		pointStrokeColors:['#e69a2a', '#dc4666'],
		behaveLikeLine: true,
		gridLineColor: '#878787',
		hideHover: 'auto',
		lineColors: ['green', 'red'],
		resize: true,
		redraw: true,
		gridTextColor:'#878787',
		gridTextFamily:"Roboto",
		parseTime: false
		});
	}
})
</script>
<script>
	new Chart($('#expensesbycategories'), {
             type: 'bar',
             data:values.datasets[1].data[0]egories; ?>,
			 options: {
				 legend: {
					display: false,
				 }
			 }
		 });
</script>
<script>
new Chart($('#top_selling_staff_chart'), {
		 type: 'line',
		 data: <?php echo $top_selling_staff_chart; ?>,
		 options:{
			 legend: {
				display: false,
			 }
	     }
		 });
</script>
<script>
	$( function () {
		
		var data = {
			"labels": [ "<?php echo lang('january'); ?>", "<?php echo lang('february'); ?>", "<?php echo lang('march'); ?>", "<?php echo lang('april'); ?>", "<?php echo lang('may'); ?>", "<?php echo lang('june'); ?>", "<?php echo lang('july'); ?>", "<?php echo lang('august'); ?>", "<?php echo lang('september'); ?>", "<?php echo lang('october'); ?>", "<?php echo lang('november'); ?>", "<?php echo lang('december'); ?>" ],
			"datasets": [ {
					"type": "line",
					backgroundColor: 'white',
					"hoverBorderColor": "#f5f5f5",

					borderColor: '#ffbc00',
					borderWidth: 1,
					"data": <?php echo $monthly_sales_graph ?>,
				}

			]
		};
		var options = {
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				xAxes: [ {
					categoryPercentage: .2,
					barPercentage: 1,
					position: 'top',
					gridLines: {
						color: '#C7CBD5',
						zeroLineColor: '#C7CBD5',
						drawTicks: true,
						borderDash: [ 5, 5 ],
						offsetGridLines: false,
						tickMarkLength: 10,
						callback: function ( value ) {
							console.log( value )
								// return value.charAt(0) + value.charAt(1) + value.charAt(2);
						}
					},
					ticks: {
						callback: function ( value ) {
							return value.charAt( 0 ) + value.charAt( 1 ) + value.charAt( 2 );
						}
					}
				} ],
				yAxes: [ {
					display: true,
					gridLines: {
						drawBorder: true,
						drawOnChartArea: true,
						borderDash: [ 8, 5 ],
						offsetGridLines: true
					},
					ticks: {
						beginAtZero: true,
						// max: <?php echo $ycr ?>,
						maxTicksLimit: 12,
					}
				} ]
			},
			legend: {
				display: false
			}
		};
		var ctx = $( '#m12gain' );
		var mainChart = new Chart( ctx, {
			type: 'bar',
			data: data,
			options: options
		} );
	} );
</script>
<script>
	$( function () {
		//İstatistik
		var data = <?php echo $weekly_sales_chart_report ?>;
		var options = {
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				xAxes: [ {
					categoryPercentage: .2,
					barPercentage: 1,
					position: 'top',
					gridLines: {
						color: '#C7CBD5',
						zeroLineColor: '#C7CBD5',
						drawTicks: true,
						borderDash: [ 5, 5 ],
						offsetGridLines: false,
						tickMarkLength: 10,
						callback: function ( value ) {
							console.log( value )
								// return value.charAt(0) + value.charAt(1) + value.charAt(2);
						}
					},
					ticks: {
						callback: function ( value ) {
							return value.charAt( 0 ) + value.charAt( 1 ) + value.charAt( 2 );
						}
					}
				} ],
				yAxes: [ {
					display: true,
					gridLines: {
						drawBorder: true,
						drawOnChartArea: false,
						borderDash: [ 8, 5 ],
						offsetGridLines: true
					},
					ticks: {
						beginAtZero: true,
						// max: <?php echo $ycr ?>,
						maxTicksLimit: 12,
					}
				} ]
			},
			legend: {
				display: false
			}
		};
		var ctx = $( '#weeklygain_c' );
		var mainChart = new Chart( ctx, {
			type: 'bar',
			data: data,
			options: options,
		} );
	} );
</script>
<script>
	var ciuis_url = "<?php echo base_url();?>";
	var AylikCustomerGrafigi;
	$( function () {
		$.get( ciuis_url + 'report/customer_monthly_increase_chart/' + $( 'select[name="m"]' ).val(), function ( response ) {
			var ctx = $( '#customergraph_ciuis-xe' ).get( 0 ).getContext( '2d' );
			AylikCustomerGrafigi = new Chart( ctx, {
				'type': 'bar',
				data: response,
				options: {
					responsive: true
				},
			} );
		}, 'json' );
		$( 'select[name="m"]' ).on( 'change', function () {
			AylikCustomerGrafigi.destroy();
			$.get( ciuis_url + 'report/customer_monthly_increase_chart/' + $( 'select[name="m"]' ).val(), function ( response ) {
				var ctx = $( '#customergraph_ciuis-xe' ).get( 0 ).getContext( '2d' );
				AylikCustomerGrafigi = new Chart( ctx, {
					'type': 'bar',
					data: response,
					options: {
						responsive: true
					},
				} );
			}, 'json' );
		} );
	} );
</script> 
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_report.php' ;?>