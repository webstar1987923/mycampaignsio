<?php $page = "expenses"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default panel-table border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4><?php echo lang('expensestitle'); ?></h4>
					<span class="panel-subtitle"><?php echo lang('expensesdescription'); ?></span>
				</div>
				<div class="pull-right">
					<button type="button" data-target="#addexpense" data-toggle="modal" data-placement="left" title="" data-original-title="Add Expense" class="btn btn-success btn-lable-wrap left-label" id="buttons">
						<span class="btn-label">
							<i class="icon-plus txt-dark"></i>
						</span>
						<span class="btn-text"><?php echo lang('addexpense'); ?></span>
					</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<table id="table2" class="table table-striped table-hover table-fw-widget">
					<thead>
						<tr class="text-muted">
							<th width="100px"><?php echo lang('expense'); ?></th>
							<th><?php echo lang('detail'); ?></th>
							<th class="hidden-xs"><?php echo lang('category'); ?></th>
							<th class="hidden-xs"><?php echo lang('date'); ?></th>
							<th class="text-right"><?php echo lang('amount'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($expenses as $expense) { ?>
							<tr class="" onclick="window.location='<?php echo base_url('expenses/receipt/'.$expense['id'].'')?>'">
								<td width="130px">
									<a class="ciuis_expense_receipt_number" href="<?php echo base_url('expenses/receipt/'.$expense['id'].''); ?>"><span><?php echo lang('expenseprefix'),'-',str_pad($expense['id'], 6, '0', STR_PAD_LEFT) ?></span></a>
								</td>
								<td class="md-pt-10">
									<span><b><?php echo $expense['title']; ?></b></span>
									<p>
									<code class="pull-left md-mb-5"><?php echo lang('addedby'); ?> <a href="<?php echo base_url('staff/staffmember/'.$expense['staffid'].'')?>"><?php echo $expense['staff']; ?></a></code>
									</p>
								</td>
								<td class="hidden-xs"><?php echo $expense['category']; ?></td>
								<td class="md-pt-10 hidden-xs">
								<?php switch ($settings['dateformat']) {
		                              case 'yy.mm.dd': echo _pdate($expense['date']);break;
		                              case 'dd.mm.yy': echo _udate($expense['date']); break;
		                          } ?>
								<td class="md-pt-10 text-right">
									<b class="money-area"><?php echo $expense['amount']?></b><BR>
									<div style="<?php if ($expense['customerid'] == 0) {
		                              echo 'display:none';
		                          } ?>">
										<?php if ($expense[ 'invoiceid' ] == null) {
		                              $billstatus = lang('notbilled') and $color = 'warning';
		                          } else {
		                              $billstatus = lang('billed') and $color = 'success';
		                          } ?>
										<span class="label label-<?php echo $color ?>"><?php echo  $billstatus ?></span>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default panel-table border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4><?php echo lang('expensescategories'); ?></h4>
					<span class="panel-subtitle"><?php echo lang('expensescategoriessub'); ?>
				</div>
				<div class="pull-right">
					<button type="button" data-target="#addexpensecategory" data-toggle="modal" data-placement="left" title="" class="btn btn-primary btn-sm btn-rounded pl-10 pr-10 pull-right" data-original-title="Add Expense Category"><i class="icon-plus txt-dark"></i></button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
				  <table id="table" class="table table-striped table-hover">
						<thead>
							<tr>
								<th><?php echo lang('category')?></th>
								<th></th>
							</tr>
						</thead>
						<?php foreach ($expensecat as $e) { ?>
						<tbody>
							<tr>
								<td><?php echo $e['name']; ?></td>
								<td class="pull-right md-pt-5">
									<button type="button" data-target="#editexpensecategory<?php echo $e['id']; ?>" data-toggle="modal" data-placement="left" title="" class="btn btn-warning btn-md" data-original-title="Update Expense Category"><i class="icon-note"></i></button>
									<a href="<?php echo site_url('expenses/removecategory/'.$e['id']); ?>"><button class="btn btn-danger btn-md"><i class="icon-trash"></i></button></a>
								</td>
							</tr>
						</tbody>
						<div id="editexpensecategory<?php echo $e['id']; ?>" tabindex="-1" role="content" class="modal fade">
							<div class="modal-dialog">
							 	<?php echo form_open('expenses/editcategory/'.$e['id']); ?>
								<div class="modal-content">
								  <div class="modal-header">
								  	<div class="pull-left">
								  		<h4 class="modal-title"><?php echo lang('updateexpensecategory'); ?></h4>
								  	</div>
										<div class="pull-right">
											<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
										</div>
									</div>
									<div class="modal-body">
										<div class="form-group">
											<label for="name" class="control-label mb-5"><?php echo lang('name'); ?></label>
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-info"></i></span>
												<input type="text" name="name" value="<?php echo($this->input->post('name') ? $this->input->post('name') : $e['name']); ?>" class="form-control" id="name" placeholder="name"/>
											</div>
										</div>
										<div class="form-group">
											<label for="name" class="control-label mb-5"><?php echo lang('description'); ?></label>
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-doc"></i></span>
												<textarea name="description" class="form-control" id="description" placeholder="<?php echo lang('description'); ?>"><?php echo($this->input->post('description') ? $this->input->post('description') : $e['description']); ?></textarea>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
										<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
						<?php } ?>
				  </table>
				</div>
			</div>
    </div>
	</div>
	
</div>

<div id="addexpensecategory" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
	 	<?php echo form_open('expenses/addcategory'); ?>
	 	<div class="modal-content">
		  <div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
				<h3 class="modal-title"><?php echo lang('addexpensecategory')?></h3>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="control-label mb-5"><?php echo lang('name'); ?></label>
					<div class="input-group"><span class="input-group-addon"><i class="icon-info"></i></span>
						<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control required" id="name" placeholder="name" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="control-label mb-5"><?php echo lang('description'); ?></label>
					<div class="input-group"><span class="input-group-addon"><i class="icon-doc"></i></span>
						<textarea name="description" class="form-control" id="description" placeholder="<?php echo lang('description');?>"><?php echo $this->input->post('description'); ?></textarea>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
				<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div id="addexpense" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('expenses/add'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-left">
					<h3 class="modal-title"><?php echo lang('addexpense')?></h3>
				</div>
				<div class="pull-right">
					<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<label for="title" class="control-label mb-5"><?php echo lang('title'); ?></label>
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-info"></i></span>
							<input required type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title" placeholder="<?php echo lang('title'); ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="amount" class="control-label mb-5"><?php echo lang('amount'); ?></label>
						<div class="input-group">
							<span class="input-group-addon"><?php echo currency ;?></span>
							<input required type="text" name="amount" value="<?php echo $this->input->post('amount'); ?>" class="input-money-format form-control" id="amount" placeholder="0.00"/>
						</div>
					</div>
					<div class="form-group">
						<label for="date" class="control-label mb-5"><?php echo lang('date'); ?></label>
						<div id="datetimepicker1" data-min-view="2" data-date-format="dd.mm.yyyy" class="input-group date datetimepicker">
							<span class="input-group-addon"><i class="icon-calendar"></i></span>
							<input placeholder="<?php echo date(" d.m.Y "); ?>" required type='input' name="date" value="<?php echo $this->input->post('date'); ?>" class="form-control" id="date"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="expensecategoryid" class="control-label mb-5"><?php echo lang('category'); ?></label>
						<div class="add-on-edit">
							<select name="expensecategoryid" class="form-control select2 required" required>
								<?php
                  foreach ($expensecat as $expensecategory) {
                    $selected = ($expensecategory['id'] == $this->input->post('expensecategoryid')) ? ' selected="selected"' : "";
                    echo '<option value="'.$expensecategory['id'].'" '.$selected.'>'.$expensecategory['name'].'</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="accountid"><?php echo lang('choiseaccount'); ?></label>
						<div class="add-on-edit">
							<select name="accountid" class="form-control select2">
								<?php
                  foreach ($all_accounts as $account) {
                    $selected = ($account['id'] == $this->input->post('accountid')) ? ' selected="selected"' : null;
                    echo '<option value="'.$account['id'].'" '.$selected.'>'.$account['name'].'</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="customerid">Select Supplier</label>
						<div class="add-on-edit">
							<select name="customerid" class="form-control select2">
								<option value="">Select Supplier</option>
								<?php
                  foreach ($all_customers as $customers) {
                    $selected = ($customers[ 'id' ] == $this->input->post('customerid')) ? ' selected="selected"' : null;
                    if ($customers[ 'type' ] == 0) {
                        echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'companyname' ] . '</option>';
                    } else {
                        echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'namesurname' ] . '</option>';
                    }
                  }
                ?>
							</select>
						</div>
					</div>
				</div>
				</div>
				<input hidden="" type="text" name="dateadded" value="<?php echo date("Y-m-d H:i:s"); ?>"/>
				<input type="hidden" name="staffid" value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description"><?php echo lang('description'); ?></label>
							<div class="input-group xs-mb-15">
								<span class="input-group-addon"><i class="icon-doc"></i></span>
								<textarea name="description" class="form-control" id="description" placeholder="Description"><?php echo $this->input->post('description'); ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
				<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<table style="visibility: hidden" id="tableexpense" class="table table-striped table-hover table-fw-widget">
	<thead>
		<tr class="text-muted">
			<th><?php echo lang('id')?></th>
			<th><?php echo lang('title')?></th>
			<th><?php echo lang('description')?></th>
			<th><?php echo lang('staff')?></th>
			<th><?php echo lang('category')?></th>
			<th><?php echo lang('date')?></th>
			<th><?php echo lang('amount')?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($expenses as $expense) { ?>
			<tr>
				<td><?php echo $expense['id']; ?></td>
				<td><?php echo $expense['title']; ?></td>
				<td><?php echo $expense['description']; ?></td>
				<td><?php echo $expense['staff']; ?></td>
				<td><?php echo $expense['category']; ?></td>
				<td><?php echo $expense['date']; ?></td>
				<td><?php echo currency; ?> <?php echo number_format($expense['amount'], 2, ',', '.'); ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('form').each(function() {  // attach to all form elements on page
        $(this).validate({       // initialize plugin on each form
            // global options for plugin
        });
    });

});	
</script>
	
