<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default panel-table border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4><?php echo lang('expensestitle'); ?></h4>
					<span class="panel-subtitle"><?php echo lang('expensesdescription'); ?></span>
				</div>
				<div class="pull-right">
					<button type="button" data-target="#addexpense" data-toggle="modal" data-placement="left" title="" data-original-title="Add Expense" class="btn btn-primary btn-lable-wrap left-label" id="buttons">
					<span class="btn-label"><i class="icon-plus txt-dark"></i></span>
					<span class="btn-text"> <?php echo lang('addexpense'); ?></span>
				</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<table id="table2" class="table table-striped table-hover table-fw-widget">
					<thead>
						<tr class="text-muted">
							<th width="100px"><?php echo lang('expense'); ?></th>
							<th><?php echo lang('detail'); ?></th>
							<th><?php echo lang('category'); ?></th>
							<th><?php echo lang('date'); ?></th>
							<th class="text-right"><?php echo lang('amount'); ?></th>
						</tr>
					</thead>
					<?php foreach($expensesdata as $expense){ ?>
					<tr>
						<td width="130px" class="ciuis_expense_receipt">
							<a class="ciuis_expense_receipt_number huppur" href="<?php echo base_url('expenses/receipt/'.$expense['id'].'');?>"><span><?php echo lang('expenseprefix'),'-',str_pad($expense['id'], 6, '0', STR_PAD_LEFT) ?></span></a>
						</td>
						<td class="md-pt-10">
							<span><b><?php echo $expense['title']; ?></b></span>
							<p>
							<code class="pull-left md-mb-5"><?php echo lang('addedby'); ?> <a href="<?php echo base_url('staff/staffmember/'.$expense['staffid'].'')?>"><?php echo $expense['staff']; ?></a></code>
							</p>
						</td>
						<td><?php echo $expense['category']; ?></td>
						<td class="md-pt-10">
						<?php switch($settings['dateformat']){ 
							case 'yy.mm.dd': echo _pdate($expense['date']);break;
							case 'dd.mm.yy': echo _udate($expense['date']); break;
						}?>
						</td>
						<td class="md-pt-10 text-right">
							<b class="money-area"><?php echo $expense['amount']?></b><BR>
							<div style="<?php if($expense['customerid'] == 0){echo 'display:none';} ?>">
								<?php if ( $expense[ 'invoiceid' ] == NULL ) {$billstatus = lang('notbilled') and $color = 'warning';}else $billstatus = lang('billed') and $color = 'success'; ?>
								<span class="label label-<?php echo $color ?>"><?php echo  $billstatus ?></span>
							</div>
						</td>
					</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left mr-20">
					<h5><?php echo lang('receiptfor')?></h5>
					<span class="panel-titletext-success"><?php echo lang('expenseprefix'),'-',str_pad($expenses['id'], 6, '0', STR_PAD_LEFT) ?></span>
				</div>
				<div class="pull-left">
					<h5><?php echo $expenses['title'] ?></h5>
					<span class="panel-title"><?php echo $expenses['desc'] ?></span>
				</div>
				<div class="pull-right">
					<div class="btn-group btn-space pull-right">								
						<button type="button" data-target="#editexpense" data-toggle="modal" data-placement="left" title="" class="btn btn-sm btn-warning" data-original-title="Edit"><i class="icon-pencil font-12 txt-dark"></i></button>
						<button data-toggle="modal" data-target="#remove" class="btn btn-sm btn-danger"><i class="icon-trash font-12 txt-dark"></i></button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 mb-20 pb-20 border-bottom">
							<div class="pull-left">
								<i class="icon-wallet mr-5" aria-hidden="true"></i> 
								<span><?php echo lang('amount')?></span>
							</div>
							<div class="pull-right">
								<span class="money-area pull-right txt-dark font-24"><?php echo $expenses['amount']?></span><br>
								<span class="pull-right"><?php echo lang('paidvia')?> <?php echo $expenses['account'];?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-12 mb-20 pb-20 border-bottom">
							<div class="pull-left">
								<i class="icon-calendar mr-5" aria-hidden="true"></i> 
								<span><?php echo lang('date')?></span>
							</div>
							<div class="pull-right">
								<span class="money-area"><?php echo  _adate($expenses['date']); ?></span> 
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-12 mb-20 pb-20 border-bottom">
							<div class="pull-left">
								<i class="icon-star mr-5" aria-hidden="true"></i> 
								<span><?php echo lang('staff')?></span>
							</div>
							<div class="pull-right">
								<span class="money-area"><?php echo $expenses['staff']?></span> 
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-12 mb-20 pb-20 border-bottom">
							<div class="pull-left">
								<i class="icon-star mr-5" aria-hidden="true"></i> 
								<span><?php echo lang('category')?></span>
							</div>
							<div class="pull-right">
								<span class="money-area"><?php echo $expenses['category']?></span> 
							</div>
							<div class="clearfix"></div>
						</div>
						<div style="<?php if($expenses['customerid'] == 0){echo 'display:none';}?>" class="expenseconvert"> 
							<a style="<?php if($expenses['invoiceid'] != NULL){echo 'display:none';}?>" href="#" data-target="#convert" data-toggle="modal" class="expenseconvertbutton"><i class="icon-doc"></i><?php echo lang('convertinvoice')?></a>
							<a style="<?php if($expenses['invoiceid'] == NULL){echo 'display:none';}?>" href="<?php echo base_url('invoices/invoice/'.$expenses['invoiceid'].'')?>" class="expenseconvertbutton"><i class="icon-doc"> </i><?php echo lang('invoiceprefix'),'-',str_pad($expenses['invoiceid'], 6, '0', STR_PAD_LEFT) ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<table style="visibility: hidden" id="table" class="table table-striped table-hover table-fw-widget">
	<thead>
		<tr class="text-muted">
			<th><?php echo lang('id')?></th>
			<th><?php echo lang('title')?></th>
			<th><?php echo lang('description')?></th>
			<th><?php echo lang('staff')?></th>
			<th><?php echo lang('category')?></th>
			<th><?php echo lang('date')?></th>
			<th><?php echo lang('amount')?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($expensesdata as $expense){ ?>
		<tr>
			<td><?php echo $expense['id']; ?></td>
			<td><?php echo $expense['title']; ?></td>
			<td><?php echo $expense['description']; ?></td>
			<td><?php echo $expense['staff']; ?></td>
			<td><?php echo $expense['category']; ?></td>
			<td><?php echo $expense['date']; ?></td>
			<td><?php echo currency;?> <?php echo number_format($expense['amount'], 2, ',', '.'); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>


<div id="remove" tabindex="-1" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="text-danger"><span class="icon-close"></span></div>

					<h3>
						<?php echo lang('attention'); ?>
					</h3>

					<p>
						<?php echo lang('expensesatentiondetail'); ?>
					</p>

					<div class="mt-20">
						<a type="button" data-dismiss="modal" class="btn btn-warning">
							<?php echo lang('cancel'); ?>
						</a>
						<a href="<?php echo base_url('expenses/remove/'.$expenses['id'].'')?>" type="button" class="btn btn-danger">
							<?php echo lang('delete'); ?>
						</a>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>


<div id="convert" tabindex="-1" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
		<?php echo form_open('expenses/convertinvoice/'.$expenses['id'].'',array("class"=>"form-vertical")); ?>
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
			</div>
			<div class="modal-body">
			<input type="hidden" name="itemname" value="<?php echo $expenses['title']?>">
			<input type="hidden" name="total" value="<?php echo $expenses['amount']?>">
			<input type="hidden" name="description" value="<?php echo $expenses['description']?>">
				<div class="text-center">
					<div class="text-success"><span class="icon-info"></span>
					</div>
					<h3>
						<?php echo lang('information'); ?>
					</h3>

					<p>
						<?php echo lang('convertexpensedetail'); ?>
					</p>

					<div class="mt-20">
						<a type="button" data-dismiss="modal" class="btn btn-warning">
							<?php echo lang('cancel'); ?>
						</a>
						<button type="submit" class="btn btn-success">
							<?php echo lang('convert'); ?>
						</button>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		<?php echo form_close(); ?>
		</div>
	</div>
</div>

<div id="addexpense" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('expenses/add'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-left">
					<h3 class="modal-title"><?php echo lang('addexpense')?></h3>
				</div>
				<div class="pull-right">
					<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<label for="title" class="control-label mb-5"><?php echo lang('title'); ?></label>
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-info"></i></span>
							<input required type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title" placeholder="<?php echo lang('title'); ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="amount" class="control-label mb-5"><?php echo lang('amount'); ?></label>
						<div class="input-group">
							<span class="input-group-addon"><i class="ion-social-usd"></i></span>
							<input required type="text" name="amount" value="<?php echo $this->input->post('amount'); ?>" class="input-money-format form-control" id="amount" placeholder="0.00"/>
						</div>
					</div>
					<div class="form-group">
						<label for="date" class="control-label mb-5"><?php echo lang('date'); ?></label>
						<div id="datetimepicker1" data-min-view="2" data-date-format="dd.mm.yyyy" class="input-group date datetimepicker">
							<span class="input-group-addon"><i class="icon-calendar"></i></span>
							<input placeholder="<?php echo date(" d.m.Y "); ?>" required type='input' name="date" value="<?php echo $this->input->post('date'); ?>" class="form-control" id="date"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="expensecategoryid" class="control-label mb-5"><?php echo lang('category'); ?></label>
						<div class="add-on-edit">
							<select name="expensecategoryid" class="form-control select2 required" required>
								<?php
                  foreach ($expensecat as $expensecategory) {
                    $selected = ($expensecategory['id'] == $this->input->post('expensecategoryid')) ? ' selected="selected"' : "";
                    echo '<option value="'.$expensecategory['id'].'" '.$selected.'>'.$expensecategory['name'].'</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="accountid"><?php echo lang('choiseaccount'); ?></label>
						<div class="add-on-edit">
							<select name="accountid" class="form-control select2">
								<?php
                  foreach ($all_accounts as $account) {
                    $selected = ($account['id'] == $this->input->post('accountid')) ? ' selected="selected"' : null;
                    echo '<option value="'.$account['id'].'" '.$selected.'>'.$account['name'].'</option>';
                  }
                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="customerid"><?php echo lang('choisecustomer'); ?></label>
						<div class="add-on-edit">
							<select name="customerid" class="form-control select2">
								<option value=""><?php echo lang('choisecustomer'); ?></option>
								<?php
                  foreach ($all_customers as $customers) {
                    $selected = ($customers[ 'id' ] == $this->input->post('customerid')) ? ' selected="selected"' : null;
                    if ($customers[ 'type' ] == 0) {
                        echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'companyname' ] . '</option>';
                    } else {
                        echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'namesurname' ] . '</option>';
                    }
                  }
                ?>
							</select>
						</div>
					</div>
				</div>
				</div>
				<input hidden="" type="text" name="dateadded" value="<?php echo date("Y-m-d H:i:s"); ?>"/>
				<input type="hidden" name="staffid" value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description"><?php echo lang('description'); ?></label>
							<div class="input-group xs-mb-15">
								<span class="input-group-addon"><i class="icon-doc"></i></span>
								<textarea name="description" class="form-control" id="description" placeholder="Description"><?php echo $this->input->post('description'); ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
				<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>


<div id="editexpense" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('expenses/edit/'.$expenses['id'],array("class"=>"form-vertical")); ?>
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-left">
					<h3 class="modal-title"><?php echo lang('updateexpense')?> <?php echo $expenses['id']?></h3>
				</div>
				<div class="pull-right">
					<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close">x</button>
				</div>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="title" class="control-label mb-5"><?php echo lang('title'); ?></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="icon-info"></i></span>
								<input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $expenses['title']); ?>" class="form-control" id="title" placeholder="title"/>
							</div>
						</div>
						<div class="form-group">
							<label for="amount" class="control-label mb-5"><?php echo lang('amount'); ?></label>
							<div class="input-group">
								<span class="input-group-addon"><?php echo currency ;?></i></span>
								<input type="text" name="amount" value="<?php echo ($this->input->post('amount') ? $this->input->post('amount') : $expenses['amount']); ?>" class="form-control" id="amount" placeholder="amount"/>
							</div>
						</div>
						 <div class="form-group">
	           	<label for="date" class="control-label mb-5"><?php echo lang('date'); ?></label>
		          <div id="datetimepicker4" data-min-view="50" class="input-group"> 
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
	              <input data-date-format="YYYY.MM.DD h:mm A" placeholder="<?php echo date(" Y.m.d "); ?>" required name="date" type="text" value="<?php echo ($this->input->post('date') ? $this->input->post('date') : $expenses['date']); ?>" class="form-control datetimepicker">
	            </div>
	          </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="expensecategoryid" class="control-label mb-5"><?php echo lang('category'); ?></label>
							<div class="add-on-edit">
								<select name="expensecategoryid" class="form-control select2">
									<option value="<?php echo $expenses['expensecategoryid'];?>"><?php echo $expenses['category'];?></option>
									<?php
									foreach ( $expensecat as $expensecategory ) {
										$selected = ( $expensecategory[ 'id' ] == $this->input->post( 'expensecategoryid' ) ) ? ' selected="selected"' : null;
										echo '<option value="' . $expensecategory[ 'id' ] . '" ' . $selected . '>' . $expensecategory[ 'name' ] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="accountid" class="control-label mb-5"><?php echo lang('choiseaccount'); ?></label>
							<div class="add-on-edit">
								<select name="accountid" class="form-control select2">
									<option value="<?php echo $expenses['accountid'];?>"><?php echo $expenses['account'];?></option>
									<?php
									foreach ( $all_accounts as $account ) {
										$selected = ( $account[ 'id' ] == $this->input->post( 'accountid' ) ) ? ' selected="selected"' : null;
										echo '<option value="' . $account[ 'id' ] . '" ' . $selected . '>' . $account[ 'name' ] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="customerid" class="control-label mb-5"><?php echo lang('choisecustomer'); ?></label>
							<div class="add-on-edit">
								<select name="customerid" class="form-control select2">
								<option value="<?php echo $expenses['customerid'];?>"><?php if($expenses['type']==0) {echo $expenses['customer'];} else echo $expenses['individual'];?></option>
									<?php
									foreach ( $all_customers as $customers ) {
										$selected = ( $customers[ 'id' ] == $this->input->post( 'customerid' ) ) ? ' selected="selected"' : null;
										if ($customers[ 'type' ] ==0 ){
										echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'companyname' ] . '</option>';}
										else echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'namesurname' ] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description" class="control-label mb-5"><?php echo lang('description'); ?></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="icon-note"></i></span>
								<textarea name="description" class="form-control" id="description" placeholder="Description"><?php echo ($this->input->post('description') ? $this->input->post('description') : $expenses['desc']); ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

			<input hidden="" type="text" name="dateadded" value="<?php echo date("Y-m-d H:i:s"); ?>"/>
			<input type="hidden" name="staffid" value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>">

			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
				<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>



<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>

<script>
	"use strict";

  $('#datetimepicker4').datetimepicker({
      format: 'YYYY.MM.DD h:mm A',
      inline:false,
      sideBySide: true,
      icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
    });
</script>
