<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
  <?php echo form_open_multipart('staff/add/',array("class"=>"form-horizontal")); ?>
  <div class="col-sm-12 col-lg-6">
    <div class="panel panel-default border-panel card-view">
      <div class="panel-heading">
       <div class="pull-left">
         <h4><?php echo lang('staffinfotitle'); ?></h4>
         <span class="panel-subtitle"><?php echo lang('staffinfodescription'); ?></span>
       </div>
       <div class="clearfix"></div>
      </div>
      <div class="panel-wrapper">
        <div class="panel-body">
          <div class="col-md-12">
            <div class="form-group">
              <label for="staffname" class="control-label mb-5"><?php echo lang('staffnamesurname');?></label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icon-user"></i>
                </span>
                <input name="staffname" value="<?php $this->input->post('staffname') ?>" type="text" placeholder="Name Surname" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="birthday" class="control-label mb-5"><?php echo lang('staffbirthday');?></label>
              <div id="datetimepicker1" data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker"> 
                <span class="input-group-addon">
                  <i class="icon-calendar"></i>
                </span>
                <input name="birthday" required type='input' name="birthday" placeholder="14.03.2018" value="<?php $this->input->post('birthday') ?>" class="form-control" id="birthday" />
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="control-label mb-5"><?php echo lang('staffphone');?></label>
              <div class="input-group ">
                <span class="input-group-addon">
                  <i class="icon-phone"></i>
                </span>
                <input name="phone" type="text" value="<?php $this->input->post('phone') ?>" placeholder="+90 (532) 000 0000" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="address" class="control-label mb-5"><?php echo lang('staffaddress');?></label>
              <textarea name="address" class="form-control" placeholder="Address"><?php $this->input->post('address') ?></textarea>
            </div>
            <div class="form-group">
              <label for="departmentid" class="control-label mb-5"><?php echo lang('department');?></label>
              <select required name="departmentid" class="form-control select2">
                <?php
                foreach ( $departments as $department ) {
                  $selected = ( $department[ 'id' ] == $this->input->post( 'departmentid' ) ) ? ' selected="selected"' : null;
                  echo '<option value="' . $department[ 'id' ] . '" ' . $selected . '>' . $department[ 'name' ] . '</option>';
                }
                ?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-12 col-lg-6">
    <div class="panel panel-default border-panel card-view">
      <div class="panel-heading">
        <h4><?php echo lang('staffdetailstitle');?></h4>
        <span class="panel-subtitle"><?php echo lang('staffdetailsdescription');?></span>
      </div>
      <div class="panel-body">
        <div class="col-md-12">
          <div class="form-group">
            <label for="email" class="control-label mb-5"><?php echo lang('staffemail');?></label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icon-envelope"></i>
              </span>
              <input value="<?php $this->input->post('email') ?>" name="email" type="text" placeholder="johndoe@example.com" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="ad" class="control-label mb-5"><?php echo lang('password');?></label>
            <p class="xs-mb-5"><?php echo lang('loginpassword');?><a href="javascript:;" data-toggle="popover" data-trigger="hover" title="<?php echo lang('information')?>" data-content="<?php echo lang('staffhover')?>" data-placement="top"><b> ?</b></a>
            </p>
            <div class="input-group">
              <input name="password" type="text" class="form-control " rel="gp" data-size="9" id="nc" data-character-set="a-z,A-Z,0-9,#">
              <span class="input-group-btn"><button type="button" class="btn btn-warning getNewPass"><span class="icon-refresh"></span>
              </button>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="language" class="control-label mb-5"><?php echo lang('languages');?></label>
            <select required name="language" class="form-control select2">
              <?php
              foreach ( $languages as $language ) {
                $selected = ( $language[ 'name' ] == $this->input->post( 'language' ) ) ? ' selected="selected"' : null;
                echo '<option value="' . $language[ 'foldername' ] . '" ' . $selected . '>' . $language[ 'name' ] . '</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label mb-10 text-left"><?php echo lang('image');?></label>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
              <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
              <input type="file" name="staffavatar" id="chooseFile">
              </span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a> 
            </div>
          </div>
          <div class="row">
            <div class="col-xs-3">
              <div class="checkbox checkbox-success">
                <input name="admin" id="admin" type="checkbox" value="1">
                <label for="admin" class="control-label mb-5"><?php echo lang('staffadmin');?></label>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="checkbox checkbox-primary">
                <input name="staffmember" id="staffmember" type="checkbox" value="1">
                <label for="staffmember" class="control-label mb-5"><?php echo lang('staffisstaff');?></label>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="checkbox checkbox-danger">
                <input name="inactive" id="inactive" type="checkbox" value="1">
                <label for="inactive" class="control-label mb-5"><?php echo lang('staffinactive');?></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <button type="submit" class="btn btn-success pull-right mt-10 mb-10"><?php echo lang('save');?></button>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>



<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php' ;?>
<script type="text/javascript">
	// Generate a password string
	function randString( id ) {
		var dataSet = $( id ).attr( 'data-character-set' ).split( ',' );
		var possible = '';
		if ( $.inArray( 'a-z', dataSet ) >= 0 ) {
			possible += 'abcdefghijklmnopqrstuvwxyz';
		}
		if ( $.inArray( 'A-Z', dataSet ) >= 0 ) {
			possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
		if ( $.inArray( '0-9', dataSet ) >= 0 ) {
			possible += '0123456789';
		}
		if ( $.inArray( '#', dataSet ) >= 0 ) {
			possible += '![]{}()%&*$#^<>~@|';
		}
		var text = '';
		for ( var i = 0; i < $( id ).attr( 'data-size' ); i++ ) {
			text += possible.charAt( Math.floor( Math.random() * possible.length ) );
		}
		return text;
	}

	// Create a new password on page load
	$( 'input[rel="gp"]' ).each( function () {
		$( this ).val( randString( $( this ) ) );
	} );

	// Create a new password
	$( ".getNewPass" ).click( function () {
		var field = $( this ).closest( 'div' ).find( 'input[rel="gp"]' );
		field.val( randString( field ) );
	} );

	// Auto Select Pass On Focus
	$( 'input[rel="gp"]' ).on( "click", function () {
		$( this ).select();
	} );
</script>
<script type="text/javascript">
$('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }
});	
</script>