<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php';?>

<div class="row">
  <?php echo form_open_multipart('staff/edit/' . $staff['id'], array("class" => "form-horizontal")); ?>
  <div class="col-sm-12 col-lg-12">
    <div class="panel panel-default border-panel card-view">
      <div class="panel-heading">
       <div class="pull-left">
         <h4><?php echo lang('staffinfotitle'); ?></h4>
         <span class="panel-subtitle"><?php echo lang('staffinfodescription'); ?></span>
       </div>
       <div class="pull-right">
         <button type="button" data-toggle="modal" data-target="#changepassword" class="btn btn-warning pull-right">Change Password</button>
       </div>
       <div class="clearfix"></div>
      </div>
      <div class="panel-wrapper">
        <div class="panel-body row">
          <div class="col-sm-12 col-lg-6">
            <div class="form-group ma-0 mb-10">
              <label for="staffname" class="control-label mb-5"><?php echo lang('staffnamesurname'); ?></label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icon-user"></i>
                </span>
                <input name="staffname" value="<?php echo ($this->input->post('staffname') ? $this->input->post('staffname') : $staff['staffname']); ?>" type="text" placeholder="Name Surname" class="form-control">
              </div>
            </div>
            <div class="form-group ma-0 mb-10">
              <label for="birthday" class="control-label mb-5"><?php echo lang('staffbirthday'); ?></label>
              <div class='input-group date' id='datetimepicker1'>
                <span class="input-group-addon">
                  <span class="icon-calendar"></span>
                </span>
                <input id="birthday" type='text' required class="form-control" placeholder="<?php echo ($this->input->post('birthday') ? $this->input->post('birthday') : $staff['birthday']); ?>" value="<?php echo ($this->input->post('birthday') ? $this->input->post('birthday') : $staff['birthday']); ?>" />
              </div>
            </div>
            <div class="form-group ma-0 mb-10">
              <label for="phone" class="control-label mb-5"><?php echo lang('staffphone'); ?></label>
              <div class="input-group ">
                <span class="input-group-addon">
                  <i class="icon-phone"></i>
                </span>
                <input name="phone" type="text" value="<?php echo ($this->input->post('phone') ? $this->input->post('phone') : $staff['phone']); ?>" placeholder="+90 (532) 000 0000" class="form-control">
              </div>
            </div>
            <div class="form-group ma-0 mb-10">
              <label for="address" class="control-label mb-5"><?php echo lang('staffaddress'); ?></label>
              <textarea name="address" class="form-control" placeholder="Address"><?php echo ($this->input->post('address') ? $this->input->post('address') : $staff['address']); ?></textarea>
            </div>
          </div>

          <div class="col-sm-12 col-lg-6">
            <div class="form-group ma-0 mb-10">
              <label for="departmentid" class="control-label mb-5">Departments</label>
              <select required name="departmentid" class="form-control select2">
                <option value="<?php echo $staff['departmentid']; ?>">Active : <?php echo $staff['department']; ?></option>
                <?php
                  foreach ($departments as $department) {
                    $selected = ($department['id'] == $this->input->post('departmentid')) ? ' selected="selected"' : null;
                    echo '<option value="' . $department['id'] . '" ' . $selected . '>' . $department['name'] . '</option>';
                  }
                ?>
              </select>
            </div>
            <div class="form-group ma-0 mb-10">
              <label for="email" class="control-label mb-5"><?php echo lang('staffemail'); ?></label>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icon-envelope"></i>
                </span>
                <input value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $staff['email']); ?>" name="email" type="text" placeholder="johndoe@example.com" class="form-control">
              </div>
            </div>
            <div class="form-group ma-0 mb-10">
              <label for="language" class="control-label mb-5"><?php echo lang('languages') ?></label>
              <select required name="language" class="form-control select2">
                <option value="<?php echo $staff['foldername']; ?>">Active : <?php echo $staff['stafflanguage']; ?></option>
                <?php
                  foreach ($languages as $language) {
                    $selected = ($language['name'] == $this->input->post('language')) ? ' selected="selected"' : null;
                    echo '<option value="' . $language['foldername'] . '" ' . $selected . '>' . $language['name'] . '</option>';
                  }
                ?>
              </select>
            </div>
            <?php
            if ($staff['staffavatar'] == NULL || $staff['staffavatar'] === 'n-img.jpg') {
              echo '
                <div class="form-group ma-0 mb-10">
                  <label class="control-label mb-10 text-left">' . lang('image') .'</label>
                  <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                    <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
                    <input type="file" name="staffavatar" id="chooseFile">
                    </span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a> 
                  </div>
                </div>
              ';
            } else {
              echo '
                <div class="form-group ma-0 mb-10">
                  <label class="control-label mb-10 text-left">' . lang('image') .'</label>
                  <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">' . $staff['staffavatar'] . '</span></div>
                    <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
                    <input type="file" name="staffavatar" id="staffavatar" placeholder="staffavatar" value="' . $staff['staffavatar'] . '">
                    </span> <a href="' . base_url('staff/removestaffavatar/' . $staff['id'] . '') . '" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a> 
                  </div>
                </div>
              ';
            } 
            ?>

            <div class="form-group">
             <div <?php if (if_admin) {echo 'style="display:none"';}?>>
              <div class="col-xs-3">
                <div class="checkbox checkbox-success">
                  <input name="admin" id="evet" type="checkbox"  <?=$staff['admin'] == 1 ? 'checked value="1"' : 'value="1"'?>>
                  <label for="evet"><?php echo lang('staffadmin'); ?></label>
                </div>
              </div>
              <div class="col-xs-3">
                <div class="checkbox checkbox-success">
                  <input name="staffmember" id="staffmember" type="checkbox" <?=$staff['staffmember'] == 1 ? 'checked value="1"' : 'value="1"'?>>
                  <label for="staffmember"><?php echo lang('staffisstaff'); ?></label>
                </div>
              </div>
              <div class="col-xs-3">
                <div class="checkbox checkbox-success">
                  <input name="inactive" id="inactive" type="checkbox" <?=$staff['inactive'] == 1 ? 'checked value="1"' : 'value="1"'?>>
                  <label for="inactive"><?php echo lang('staffinactive'); ?></label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="pull-right mt-10 mb-10">
            <button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>

  <div id="changepassword" tabindex="-1" role="dialog" class="modal fade">
    <?php echo form_open('staff/changestaffpassword/' . $staff['id'] . ''); ?>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="pull-left">
            <h5><?php echo lang('changepassword'); ?></h5>
          </div>
          <div class="pull-right">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close pull-right"><span class="mdi mdi-close"></span></button>
          </div>
        </div>
        <div class="modal-body">
          <div class="form-group ma-0 mb-10">
            <label for="ad" class="control-label mb-5"><?php echo lang('password'); ?></label>
            <p class="mb-5">Login Password<a href="javascript:;" data-toggle="popover" data-trigger="hover" title="<?php echo lang('information') ?>" data-content="<?php echo lang('contactprimaryhover') ?>" data-placement="top"><b> ?</b></a>
            </p>
            <div class="input-group">
              <input name="staffnewpassword" type="text" class="form-control " rel="gp" data-size="9" id="nc" data-character-set="a-z,A-Z,0-9,#">
              <span class="input-group-btn"><button type="button" class="btn btn-warning getNewPass"><span class="icon-refresh"></span>
              </button>
              </span>
            </div>
          </div>
          <div class="form-group ma-0 mb-10 mt-20">
            <button type="submit" class="btn btn-success pull-right">
              <?php echo lang('changepassword'); ?>
            </button>
          </div>
        </div>
        <div class="modal-footer"></div>
      </div>
    </div>
    <?php echo form_close(); ?>
  </div>


<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php' ;?>


<script type="text/javascript">
  // Generate a password string
  function randString( id ) {
    var dataSet = $( id ).attr( 'data-character-set' ).split( ',' );
    var possible = '';
    if ( $.inArray( 'a-z', dataSet ) >= 0 ) {
      possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if ( $.inArray( 'A-Z', dataSet ) >= 0 ) {
      possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if ( $.inArray( '0-9', dataSet ) >= 0 ) {
      possible += '0123456789';
    }
    if ( $.inArray( '#', dataSet ) >= 0 ) {
      possible += '![]{}()%&*$#^<>~@|';
    }
    var text = '';
    for ( var i = 0; i < $( id ).attr( 'data-size' ); i++ ) {
      text += possible.charAt( Math.floor( Math.random() * possible.length ) );
    }
    return text;
  }

  // Create a new password on page load
  $( 'input[rel="gp"]' ).each( function () {
    $( this ).val( randString( $( this ) ) );
  } );

  // Create a new password
  $( ".getNewPass" ).click( function () {
    var field = $( this ).closest( 'div' ).find( 'input[rel="gp"]' );
    field.val( randString( field ) );
  } );

  // Auto Select Pass On Focus
  $( 'input[rel="gp"]' ).on( "click", function () {
    $( this ).select();
  } );
</script>
<script type="text/javascript">
$('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen...");
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
  }
});
</script>