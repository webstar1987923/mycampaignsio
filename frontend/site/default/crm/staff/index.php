<?php $page = "staff"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-sm-12 col-lg-8">
		<?php foreach($staff as $t){ ?>
		<div class="panel panel-default card-view  pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body  pa-0">
					<div class="profile-box">
						<div class="profile-cover-pic">
							<div class="profile-image-overlay" style="background-image: url('<?php echo base_url(); ?>assets/crm/img/staffmember_bg.png');"></div>
						</div>
						<div class="profile-info text-center">
							<div class="profile-img-wrap">
								<img class="inline-block mb-10" src="<?php echo base_url(); ?>uploads/staffavatars/<?php if($t['staffavatar'] != NULL){echo $t['staffavatar'];}else {echo 'n-img.jpg';}; ?>" alt="user"/>
							</div>	
							<h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-danger"><a href="<?php echo site_url('staff/staffmember/'.$t['id']); ?>"><?php echo $t['staffname']; ?></a></h5>
							<h6 class="block capitalize-font pb-20"><?php echo $t['department']; ?></h6>
						</div>
						<div class="social-info">
							<div class="btn-group btn-space pull-right mb-20"> 
								<a href="<?php echo site_url('staff/edit/'.$t['id']); ?>" type="button" class="btn btn-primary btn-outline btn-anim"><i class="icon mdi mdi-edit text-primary"></i><span class="btn-text text-primary">edit profile</span></a> 
								<button data-target="#remove<?php echo $t['id']?>" data-toggle="modal" type="button" class="btn btn-default btn-danger btn-outline btn-anim ml-10"><i class="icon mdi mdi-delete text-danger"></i><span class="btn-text text-danger">delete profile</span></button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <div id="remove<?php echo $t['id']?>" tabindex="-1" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
							</div>
							<h3>
								<?php echo lang('attention'); ?>
							</h3>
							<p>
								<?php echo lang('staffattentiondetail'); ?>
							</p>
							<div class="xs-mt-50">
								<a type="button" data-dismiss="modal" class="btn btn-space btn-default">
									<?php echo lang('cancel'); ?>
								</a>
								<a href="<?php echo base_url('staff/remove/'.$t['id'].'')?>" type="button" class="btn btn-space btn-danger">
									<?php echo lang('delete'); ?>
								</a>
							</div>
						</div>
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
    <?php } ?>
	</div>
	<div class="col-sm-12 col-lg-4">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				 <div class="pull-left">
          	<a href="<?php echo base_url('staff/add')?>" class="btn btn-success">
          		<?php echo lang('addstaff')?>
          	</a>
          </div>
          <div class="pull-right">
          	<button data-target="#adddepartment" data-toggle="modal" class="btn btn-primary">
          		<?php echo lang('adddepartment')?> 
          	</button>
          </div>
          <div class="clearfix"></div>
			</div>
			<div class="panel-body">
			  <table id="table" class="table table-striped table-hover table-fw-widget">
				<thead>
					<tr class="text-muted">
						<th><?php echo lang('departments')?></th>
						<th class="text-right"></th>
					</tr>
				</thead>
					<?php foreach($departments as $department){ ?>
					<tr>
						<td><b class="text-muted"><?php echo $department['name']; ?></b></td>
						<td class="pull-right md-pt-5">
							<button type="button" data-target="#updatedepartment<?php echo $department['id']; ?>" data-toggle="modal" data-placement="left" title="" class="btn btn-primary btn-sm" data-original-title="Update Expense Category"><i class="icon-pencil"></i></button>
							<a href="<?php echo site_url('staff/removedepartment/'.$department['id']); ?>"><button class="btn btn-danger btn-sm"><i class="icon-trash"></i></button></a>
						</td>
					</tr>
					<div id="updatedepartment<?php echo $department['id']; ?>" tabindex="-1" role="content" class="modal fade colored-header colored-header-dark">
						 <div class="modal-dialog">
						 <?php echo form_open('staff/updatedepartment/'.$department['id']); ?>
						 <div class="modal-content">
						  <div class="modal-header">
							<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
								<h5 class="modal-title"><?php echo lang('updatedepartment'); ?></h5>
								</div>
								<div class="modal-body">
								<div class="form-group">
									<label for="name" class="control-label mb-5"><?php echo lang('name'); ?></label>
									<div class="input-group"><span class="input-group-addon">
										<i class="icon-info"></i></span>
										<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $department['name']); ?>" class="form-control" id="name" placeholder="name"/>
									</div>
								</div>
								</div>
								<div class="modal-footer">
									<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
									<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
					<?php } ?>
			  </table>
			  <br>
			</div>
  	</div>
	</div>
</div>

<div id="adddepartment" tabindex="-1" role="content" class="modal fade colored-header colored-header-dark">
 	<div class="modal-dialog">
	<?php echo form_open('staff/adddepartment'); ?>
		<div class="modal-content">
		  <div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
				<h5 class="modal-title"><?php echo lang('adddepartment')?></h5>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="control-label mb-5"><?php echo lang('name'); ?></label>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="icon-info"></i>
						</span>
						<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" placeholder="name"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
				<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php' ;?>





