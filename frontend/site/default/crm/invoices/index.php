<?php $page = "invoices"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>
	<div class="row">
		<div class="invoice-summary"></div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left col-sm-6 mb-5">
						<a href="<?php echo base_url('invoices/add');?>" type="button" class="pull-left btn btn-success btn-xl  text-muted ion-plus-round">
						<?php echo lang('newinvoice'); ?>
						</a>
					</div>
					<div class="pull-right col-sm-6">
						<div class="btn-group btn-hspace pull-right">
							<select id="invoicefilterbystatus" class="form-control select2" onChange="window.location.href=this.value">
								<option value='?'><?php echo lang('invoicefilterstatus') ?></option>
								<option value='?'><?php echo lang('all') ?></option>
								<option value="?filter-status=1"><?php echo lang('draft') ?></option>
								<option value="?filter-status=2"><?php echo lang('paid') ?></option>
								<option value="?filter-status=3"><?php echo lang('notpaid') ?></option>
								<option value="?filter-status=4"><?php echo lang('cancelled') ?></option>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="datable_1" class="table table-hover display pb-30">
									<thead>
										<tr>
											<th><?php echo lang('invoiceidno'); ?></th>
											<th><?php echo lang('invoicetablecustomer'); ?></th>
											<th><?php echo lang('billeddate'); ?></th>
											<th><?php echo lang('invoiceduedate'); ?></th>
											<th><?php echo lang('status'); ?></th>
											<th><?php echo lang('invoiceamount'); ?></th>
											<th><?php echo lang('action'); ?></th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th><?php echo lang('invoiceidno'); ?></th>
											<th><?php echo lang('invoicetablecustomer'); ?></th>
											<th><?php echo lang('billeddate'); ?></th>
											<th><?php echo lang('invoiceduedate'); ?></th>
											<th><?php echo lang('status'); ?></th>
											<th><?php echo lang('invoiceamount'); ?></th>
											<th><?php echo lang('action'); ?></th>
										</tr>
									</tfoot>
									<tbody>
										<?php foreach($invoices as $fa){ ?>
											<tr data-filterable data-filter-status="<?php echo $fa['statusid']?>">
												<td><a href="<?php echo site_url('invoices/invoice/'.$fa['id']); ?>"><?php echo lang('invoiceprefix'),'-',str_pad($fa['id'], 6, '0', STR_PAD_LEFT); ?></a>
												</td>
												<td><span class="title uppercase"><a href="<?php echo site_url('customers/customer/'.$fa['customerid']); ?>"> <?php if($fa['customer']===NULL){echo $fa['individual'];} else echo $fa['customer']; ?></a></span>
												</td>
												<td>
													<?php switch($settings['dateformat']){ 
													case 'yy.mm.dd': echo _rdate($fa['invoicedatecreated']);break; 
													case 'dd.mm.yy': echo _udate($fa['invoicedatecreated']); break;
													case 'yy-mm-dd': echo _mdate($fa['invoicedatecreated']); break;
													case 'dd-mm-yy': echo _cdate($fa['invoicedatecreated']); break;
													case 'yy/mm/dd': echo _zdate($fa['invoicedatecreated']); break;
													case 'dd/mm/yy': echo _kdate($fa['invoicedatecreated']); break;
													}?>
													</b>
												</td>
												<td>
													<?php if($fa['invoiceduedate'] == 0000-00-00){echo '<span class="badge">No Due Date</span>';};?>
													<?php switch($settings['dateformat']){ 
													case 'yy.mm.dd': echo _rdate($fa['invoiceduedate']);break; 
													case 'dd.mm.yy': echo _udate($fa['invoiceduedate']); break;
													case 'yy-mm-dd': echo _mdate($fa['invoiceduedate']); break;
													case 'dd-mm-yy': echo _cdate($fa['invoiceduedate']); break;
													case 'yy/mm/dd': echo _zdate($fa['invoiceduedate']); break;
													case 'dd/mm/yy': echo _kdate($fa['invoiceduedate']); break;
													}?>
												</td>
												<td>
												<?php $totalx = $fa['total'];$this->db->select_sum('amount')->from('payments')->where('(invoiceid ='.$fa['id'].') ');$paytotal = $this->db->get();
													$balance = $totalx - $paytotal->row()->amount;
													if($balance > 0) {echo '';} else echo '<span class="txt-success">'.lang('paidinv').' <i class="icon ion-checkmark-circled"></i></span>';					
													if($paytotal->row()->amount < $fa['total'] && $paytotal->row()->amount > 0 && $fa['statusid'] == 3){echo '<span class="txt-pink">'.lang('partial').' <i class="icon ion-checkmark-circled"></i></span>';}else{
														if($paytotal->row()->amount < $fa['total'] && $paytotal->row()->amount > 0){echo '<span class="txt-pink">'.lang('partial').' <i class="icon ion-checkmark-circled"></i></span>';}
														if($fa['statusid'] == 3){echo '<span class="txt-danger">'.lang('unpaid').' <i class="icon ion-close-circled"></i></span>';}
													}
													if($fa['statusid'] == 1){echo '<span class="txt-primary">'.lang('draft').' <i class="icon ion-document"></i></span>';}
													if($fa['statusid'] == 4){echo '<span class="txt-warning">'.lang('cancelled').' <i class="icon ion-android-cancel"></i></span>';}
													?></td>
												<td>
													<?php $totalx = $fa['total'];$this->db->select_sum('amount')->from('payments')->where('(invoiceid ='.$fa['id'].') ');$paytotal = $this->db->get();?>
													<?php $balance = $totalx - $paytotal->row()->amount;?>
													<span class="money-area"><?php echo $fa['total'];?></span>
													<small class="txt-dark"><?php if($balance == 0){echo '';}else{
														echo ''.lang('balance').'  <span class="money-area">'.$balance.'</span>';
														}?></small>
												</td>
												<td>
													<div class="btn-group btn-hspace">
														<button type="button" data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle"><span class="ion-android-checkmark-circle"></span></button>
														<ul style="margin-right: 40px;margin-top: -30px;" role="menu" class="dropdown-menu dropdown-menu-right">
														<li><a href="<?php echo site_url('invoices/edit/'.$fa['id']); ?>" ><?php echo lang('updateinvoicetitle'); ?></a></li>
														<li><a href="<?php echo site_url('invoices/invoice/'.$fa['id']); ?>"><?php echo lang('viewinvoice'); ?></a></li>
														<li class="divider"></li>
														<li><a href="#" data-toggle="modal" data-target="#mod-danger<?=$fa['id']?>"><?php echo lang('delete'); ?></a></li>
													  	</ul>
													</div>
													<div id="mod-danger<?=$fa['id']?>" tabindex="-1" role="dialog" class="modal fade">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
																</div>
																<div class="modal-body">
																	<div class="text-center">
																		<div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
																		</div>
																		<h3><?php echo lang('attention'); ?></h3>
																		<p><?php echo lang('invoiceattentiondetail'); ?></p>
																		<div class="xs-mt-50"> <a type="button" data-dismiss="modal" class="btn btn-space btn-default"><?php echo lang('cancel'); ?></a> <a href="<?php echo site_url('invoices/remove/'.$fa['id']); ?>" type="button" class="btn btn-space btn-danger"><?php echo lang('delete'); ?></a> </div>
																	</div>
																</div>
																<div class="modal-footer"></div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_graph.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_invoice.php';?>
<script type="text/javascript">
(function umd(root, name, factory)
	{
	  'use strict';
	  if ('function' === typeof define && define.amd) {
		define(name, ['jquery'], factory);
	  } else {
		root[name] = factory();
	  }
	}
	(this, 'CiuisInvoiceStats', function UMDFactory()
		{
		  'use strict';
		  var ReportOverview = ReportOverviewConstructor;
		  reportCircleGraph();
		  return ReportOverview;
		  function ReportOverviewConstructor(options) {
			var factory = {
				init: init
			  },
			  _elements = {
				$element: options.element
			  };
			init();
			return factory;
			function init() {
			  _elements.$element.append($(getTemplateString()));

			  $('.invoice-percent').percentCircle({
				width: 130,
				trackColor: '#ececec',
				barColor: '#22c39e',
				barWeight: 3,
				endPercent: 0.<?php echo $ofx ?>,
				fps: 60
			  });
			  $('.invoice-percent-2').percentCircle({
				width: 130,
				trackColor: '#ececec',
				barColor: '#ee7a6b',
				barWeight: 3,
				endPercent: 0.<?php echo $ofy ?>,
				fps: 60
			  });

			  $('.invoice-percent-3').percentCircle({
				width: 130,
				trackColor: '#ececec',
				barColor: '#808281',
				barWeight: 3,
				endPercent: 0.<?php echo $vgy ?>,
				fps: 60
			  });

			  if( $('#pie_chart_4').length > 0 ){
			  	$('#pie_chart_4').easyPieChart({
			  		barColor : '#469408',
			  		lineWidth: 20,
			  		animate: 3000,
			  		size:	165,
			  		lineCap: 'square',
			  		trackColor: 'rgba(33,33,33,0.1)',
			  		scaleColor: false,
			  		onStep: function(from, to, percent) {
			  			$(this.el).find('.percent').text(Math.round(percent));
			  		}
			  	});
			  }
			  if( $('#pie_chart_5').length > 0 ){
			  	$('#pie_chart_5').easyPieChart({
			  		barColor : '#469408',
			  		lineWidth: 20,
			  		animate: 3000,
			  		size:	165,
			  		lineCap: 'square',
			  		trackColor: 'rgba(33,33,33,0.1)',
			  		scaleColor: false,
			  		onStep: function(from, to, percent) {
			  			$(this.el).find('.percent').text(Math.round(percent));
			  		}
			  	});
			  }
			  if( $('#pie_chart_6').length > 0 ){
			  	$('#pie_chart_6').easyPieChart({
			  		barColor : '#e69a2a',
			  		lineWidth: 20,
			  		animate: 3000,
			  		size:	165,
			  		lineCap: 'square',
			  		trackColor: 'rgba(33,33,33,0.1)',
			  		scaleColor: false,
			  		onStep: function(from, to, percent) {
			  			$(this.el).find('.percent').text(Math.round(percent));
			  		}
			  	});
			  }
			  if( $('#pie_chart_7').length > 0 ){
			  	$('#pie_chart_7').easyPieChart({
			  		barColor : '#ea6c41',
			  		lineWidth: 20,
			  		animate: 3000,
			  		size:	165,
			  		lineCap: 'square',
			  		trackColor: 'rgba(33,33,33,0.1)',
			  		scaleColor: false,
			  		onStep: function(from, to, percent) {
			  			$(this.el).find('.percent').text(Math.round(percent));
			  		}
			  	});
			  }
			  if( $('#chart_7').length > 0 ){
			  	var ctx7 = document.getElementById("chart_7").getContext("2d");
			  	var data7 = {
			  		 labels: [
			  		"Paid Invoices",
			  		"Unpaid Invoices",
			  		"Over Due Invoices",
			  	],
			  	datasets: [
			  		{
			  			data: [<?php echo $otf ?>,<?php echo $tef; ?>,<?php echo $vdf ?>],
			  			backgroundColor: [
			  				"rgba(70,148,8,1)",
			  				"rgba(230,154,42,1)",
			  				"rgba(234,108,65,1)"
			  			],
			  			hoverBackgroundColor: [
			  				"rgba(70,148,8,1)",
			  				"rgba(230,154,42,1)",
			  				"rgba(234,108,65,1)"
			  			]
			  		}]
			  	};
			  	
			  	var doughnutChart = new Chart(ctx7, {
			  		type: 'doughnut',
			  		data: data7,
			  		options: {
			  			animation: {
			  				duration:	3000
			  			},
			  			elements: {
			  				arc: {
			  					borderWidth: 0
			  				}
			  			},
			  			responsive: true,
			  			maintainAspectRatio:false,
			  			percentageInnerCutout: 50,
			  			legend: {
			  				display:false
			  			},
			  			tooltips: {
			  				backgroundColor:'rgba(33,33,33,1)',
			  				cornerRadius:0,
			  				footerFontFamily:"'Roboto'"
			  			},
			  			cutoutPercentage: 70,
			  			segmentShowStroke: false
			  		}
			  	});
			  }	
			}
			function getTemplateString()
			{
			  return [
			  '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">',
				  '<div class="panel panel-default card-view panel-refresh">',
						'<div class="panel-heading">',
							'<div class="pull-left">',
								'<h6 class="panel-title txt-dark"><?php echo lang('totalinvoice'); ?></h6>',
							'</div>',
							'<div class="pull-right">',
								'<span class="label label-info capitalize-font inline-block ml-10">{{totalinvoicesayisi}}</span>'.replace(/{{totalinvoicesayisi}}/, options.data.totalinvoicesayisi),
							'</div>',
							'<div class="clearfix"></div>',
						'</div>',
						'<div class="panel-wrapper collapse in">',
							'<div class="panel-body">',
								'<div class="col-xs-6 pa-0">',
									'<canvas id="chart_7" height="165"></canvas>',
								'</div>',
								'<div class="col-xs-6 pr-0 pt-10">',
									'<div class="label-chatrs pull-right">',
										'<div class="mb-5">',
											'<span class="clabels inline-block bg-green mr-5"></span>',
											'<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $otf ?> Paid</span>',
											'<span class="clearfix"></span>',
											'<span class="txt-dark font-16 ml-15"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($ofv, 2, ',', '.');break;case '.': echo number_format($ofv, 2, '.', ',');break;}?></span>',
										'</div>',	
										'<div class="mb-5">',
											'<span class="clabels inline-block bg-yellow mr-5"></span>',
											'<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $tef; ?> Unpaid</span>',
											'<span class="clearfix"></span>',
											'<span class="txt-dark font-16 ml-15"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($oft, 2, ',', '.');break;case '.': echo number_format($oft, 2, '.', ',');break;}?></span>',
										'</div>',	
										'<div class="">',
											'<span class="clabels inline-block bg-red mr-5"></span>',
											'<span class="clabels-text font-12 inline-block txt-dark capitalize-font"><?php echo $vdf ?> Over Due</span>',
											'<span class="clearfix"></span>',
											'<span class="txt-dark font-16 ml-15"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($vgf, 2, ',', '.');break;case '.': echo number_format($vgf, 2, '.', ',');break;}?></span></span></span>',
										'</div>',				
									'</div>',
								'</div>',								
							'</div>',
						'</div>',
					'</div>',
				'</div>',
				'<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">',
				'<div class="panel panel-default card-view sm-data-box-3">',
					'<div class="panel-heading">',
						'<div class="pull-left">',
							'<h6 class="panel-title txt-dark"><?php echo lang('paid'); ?></h6>',
						'</div>',
						'<div class="pull-right">',
						'<span class="label label-success capitalize-font inline-block ml-10"><?php echo $otf ?></span>',
						'</div>',
						'<div class="clearfix"></div>',
					'</div>',
					'<div class="panel-wrapper collapse in">',
						'<div class="panel-body">',
							'<div class="col-xs-6 pa-0">',
								'<span id="pie_chart_5" class="easypiechart" data-percent="<?php echo $ofx ?>">',
									'<span class="percent block txt-dark weight-500"></span>',
								'</span>',
							'</div>	',
							'<div class="col-xs-6 pr-0">',
								'<ul class="flex-stat mb-15 pull-right">',
									'<li class="text-left block no-float full-width mb-15">',
										'<span class="block"><?php echo lang('invoiceamount'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($ofv, 2, ',', '.');break;case '.': echo number_format($ofv, 2, '.', ',');break;}?></span></span>'.replace(/{{OdenmeyenInvoicesAmount}}/, options.data.OdenmeyenInvoicesAmount),
										'<div class="clearfix"></div>',
									'</li>',
									'<li class="text-left block no-float full-width">',
										'<span class="block"><?php echo lang('paid'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo $ofx ?></span>%</span>',
										'<div class="clearfix"></div>',
									'</li>',
								'</ul>',
							'</div>	',
						'</div>	',
					'</div>',
				'</div>',
				'</div>',
				'<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">',
				'<div class="panel panel-default card-view sm-data-box-3">',
					'<div class="panel-heading">',
						'<div class="pull-left">',
							'<h6 class="panel-title txt-dark"><?php echo lang('unpaidinvoice'); ?></h6>',
						'</div>',
						'<div class="pull-right">',
						'<span class="label label-warning capitalize-font inline-block ml-10"><?php echo $tef; ?></span>',
						'</div>',
						'<div class="clearfix"></div>',
					'</div>',
					'<div class="panel-wrapper collapse in">',
						'<div class="panel-body">',
							'<div class="col-xs-6 pa-0">',
								'<span id="pie_chart_6" class="easypiechart" data-percent="<?php echo $ofy ?>">',
									'<span class="percent block txt-dark weight-500"></span>',
								'</span>',
							'</div>	',
							'<div class="col-xs-6 pr-0">',
								'<ul class="flex-stat mb-15 pull-right">',
									'<li class="text-left block no-float full-width mb-15">',
										'<span class="block"><?php echo lang('invoiceamount'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($oft, 2, ',', '.');break;case '.': echo number_format($oft, 2, '.', ',');break;}?></span></span>'.replace(/{{OdenmeyenInvoicesAmount}}/, options.data.OdenmeyenInvoicesAmount),
										'<div class="clearfix"></div>',
									'</li>',
									'<li class="text-left block no-float full-width">',
										'<span class="block"><?php echo lang('unpaidinvoice'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo $ofy ?></span>%</span>',
										'<div class="clearfix"></div>',
									'</li>',
								'</ul>',
							'</div>	',
						'</div>	',
					'</div>',
				'</div>',
				'</div>',
				'<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">',
				'<div class="panel panel-default card-view sm-data-box-3">',
					'<div class="panel-heading">',
						'<div class="pull-left">',
							'<h6 class="panel-title txt-dark"><?php echo lang('overdue'); ?></h6>',
						'</div>',
						'<div class="pull-right">',
						'<span class="label label-danger capitalize-font inline-block ml-10"><?php echo $vdf ?></span>',
						'</div>',
						'<div class="clearfix"></div>',
					'</div>',
					'<div class="panel-wrapper collapse in">',
						'<div class="panel-body">',
							'<div class="col-xs-6 pa-0">',
								'<span id="pie_chart_7" class="easypiechart" data-percent="<?php echo $vgy ?>">',
									'<span class="percent block txt-dark weight-500"></span>',
								'</span>',
							'</div>	',
							'<div class="col-xs-6 pr-0">',
								'<ul class="flex-stat mb-15 pull-right">',
									'<li class="text-left block no-float full-width mb-15">',
										'<span class="block"><?php echo lang('invoiceamount'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($vgf, 2, ',', '.');break;case '.': echo number_format($vgf, 2, '.', ',');break;}?></span></span>'.replace(/{{OdenmeyenInvoicesAmount}}/, options.data.OdenmeyenInvoicesAmount),
										'<div class="clearfix"></div>',
									'</li>',
									'<li class="text-left block no-float full-width">',
										'<span class="block"><?php echo lang('overdue'); ?></span>',
										'<span class="block txt-dark weight-500  font-18"><span class="counter-anim"><?php echo $vgy ?></span>%</span>',
										'<div class="clearfix"></div>',
									'</li>',
								'</ul>',
							'</div>	',
						'</div>	',
					'</div>',
				'</div>',
				'</div>'
			  ].join('');
			}
		  }
  		function reportCircleGraph() {
			$.fn.percentCircle = function pie(options) {
				var settings = $.extend({
					width: 130,
					trackColor: '#fff',
					barColor: '#fff',
					barWeight: 3,
					startPercent: 0,
					endPercent: 1,
					fps: 60
				}, options);
				this.css({
					width: settings.width,
					height: settings.width
				});
				var _this = this,
					canvasWidth = settings.width,
					canvasHeight = canvasWidth,
					id = $('canvas').length,
					canvasElement = $('<canvas id="' + id + '" width="' + canvasWidth + '" height="' + canvasHeight + '"></canvas>'),
					canvas = canvasElement.get(0).getContext('2d'),
					centerX = canvasWidth / 2,
					centerY = canvasHeight / 2,
					radius = settings.width / 2 - settings.barWeight / 2,
					counterClockwise = false,
					fps = 500 / settings.fps,
					update = 0.01;
				this.angle = settings.startPercent;
				this.drawInnerArc = function (startAngle, percentFilled, color) {
					var drawingArc = true;
					canvas.beginPath();
					canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
					canvas.strokeStyle = color;
					canvas.lineWidth = settings.barWeight - 2;
					canvas.stroke();
					drawingArc = false;
				};
				this.drawOuterArc = function (startAngle, percentFilled, color) {
					var drawingArc = true;
					canvas.beginPath();
					canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
					canvas.strokeStyle = color;
					canvas.lineWidth = settings.barWeight;
					canvas.lineCap = 'round';
					canvas.stroke();
					drawingArc = false;
				};
				this.fillChart = function (stop) {
					var loop = setInterval(function () {
						canvas.clearRect(0, 0, canvasWidth, canvasHeight);
						_this.drawInnerArc(0, 360, settings.trackColor);
						_this.drawOuterArc(settings.startPercent, _this.angle, settings.barColor);
						_this.angle += update;
						if (_this.angle > stop) {
							clearInterval(loop);
						}
					}, fps);
				};
				this.fillChart(settings.endPercent);
				this.append(canvasElement);
				return this;
			};
		}
		function getMockData() {
			return {
				totalinvoicesayisi: <?php echo $tfa ?>,
			};
		}
	}));
(function activateCiuisInvoiceStats($) {
	'use strict';
	var $el = $('.invoice-summary');
	return new CiuisInvoiceStats({
		element: $el,
		data: {
			totalinvoicesayisi: <?php echo $tfa ?>,
		}
	});
}(jQuery));
</script>
<script>
$('.search-table-external').on( 'keyup click', function () {
   $('#table2').DataTable().search(
	   $('.search-table-external').val()
   ).draw();
} );	
</script>
