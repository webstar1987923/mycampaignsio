<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <?php echo form_open('invoices/add',array("class"=>"form-horizontal invoice-form")); ?>
    <input type="hidden" name="id" value="">
    <input type="hidden" name="type" value="purchase_invoice">
    <input type="hidden" name="related_to" value="">
    <div class="panel panel-inverse card-view panel-form">
      <div class="panel-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="pull-left">
              <div class="form-inline invoice-extra button-properties-list">
                <div class="form-wrap property ml-15" data-name="series" data-title="<?php echo lang('invoicenumber')?>" data-status="passive">
                  <div class="form-group mr-30">
                    <label for="in_series" class="mr-20"><?php echo lang('serie')?></label>
                    <input type="text" name="series" id="in_series" class="form-control" value="<?php echo $this->input->post('series'); ?>">
                  </div>
                  <div class="form-group mr-30">
                    <label for="in_no" class="mr-20"><?php echo lang('invno')?></label>
                    <input type="text" name="no" id="in_no" class="form-control" value="<?php echo $this->input->post('no'); ?>">
                  </div>
                  <div class="form-group mr-30" data-title="<?php echo lang('invoicenumber')?>" data-status="passive">
                    <a href="#" class="btn btn-sm btn-danger delete"><i class="icon-trash"></i></a>
                  </div>
                </div>
                <div class="form-wrap button-properties" style="visibility: visible;">
                  <div class="form-group mr-10">
                <button class="btn btn-primary" data-name="series"><i class="icon-plus"></i> <?php echo lang('addinvoicenumber'); ?></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="pull-right">
              <div class="btn-group">
                <a href="<?php echo site_url('invoices/'); ?>" class="btn btn-warning"><?php echo lang('cancel'); ?></a>
                <button type="submit" class="btn btn-space btn-success save-invoice"><?php echo lang('save'); ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-wrapper">
        <div class="panel-body">
            <div class="col-md-6">
              <div class="form-wrap">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label for="in_account_name" class="control-label"><?php echo lang('invoicetablecustomer'); ?></label>
                    </div>

                    <div class="col-md-7">
                      <select required name="customerid" class="form-control select2">
                        <option value=""><?php echo lang('choisecustomer'); ?></option>
                        <?php
                        foreach ( $all_customers as $customers ) {
                          $selected = ( $customers[ 'id' ] == $this->input->post( 'customerid' ) ) ? ' selected="selected"' : null;
                          if ( $customers[ 'type' ] == 0 ) {
                            echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'companyname' ] . '</option>';
                          } else echo '<option value="' . $customers[ 'id' ] . '" ' . $selected . '>' . $customers[ 'namesurname' ] . '</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label for="in_date_issue" class="control-label"><?php echo lang('dateofissuance'); ?></label>
                    </div>

                    <div class="col-md-7">
                      <div id="datetimepicker1" class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input placeholder="<?php echo date(" d.m.Y "); ?>" required type='input' name="datecreated" value="<?php echo $this->input->post('datecreated'); ?>" class="form-control" id="datecreated"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
            </div>
            <div class="col-md-6">
              <div class="form-wrap">
                <div id="toggle-invoice-status" class="form-group">
                    <div class="col-md-5">
                      <label class="control-label"><?php echo lang('invoicestatus'); ?></label>
                    </div>

                    <div class="col-md-7">
                      <div class="btn-group pull-right" data-toggle="buttons">
                        <label class="btn btn-primary btn-reverse active">
                        <input name="statusid" type="radio" autocomplete="off" checked value="3">
                        <?php echo lang('willbepaid'); ?></label>
                              <label class="btn btn-primary btn-reverse">
                        <input name="statusid" type="radio" autocomplete="off" value="2">
                        <?php echo lang('paid'); ?></label>
                        <label class="btn btn-primary btn-reverse">
                        <input name="statusid" type="radio"  autocomplete="off"  value="1">
                        <?php echo lang('draft'); ?></label>
                      </div>
                    </div>
                </div>

                <div id="toggle-account-info">
                  <div class="form-group toggle-cash" style="display:none;">
                      <div class="col-md-5">
                        <label for="vade" class="control-label"><?php echo lang('paidcashornank'); ?></label>
                      </div>

                      <div class="col-md-7">
                        <select required name="accountid" class="form-control select2">
                          <option value=""><?php echo lang('choiseaccount'); ?></option>
                          <?php 
                          foreach($all_accounts as $account)
                          {
                            $selected = ($account['id'] == $this->input->post('accountid')) ? ' selected="selected"' : null;

                            echo '<option value="'.$account['id'].'" '.$selected.'>'.$account['name'].'</option>';
                          } 
                          ?>
                        </select>
                      </div>
                  </div>

                  <div class="form-group toggle-payment" style="display:none;">
                      <div class="col-md-5">
                        <label for="vade" class="control-label"><span><?php echo lang('datepaid'); ?></span></label>
                      </div>

                      <div class="col-md-7">
                        <div id="datetimepicker2" class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input required placeholder="<?php echo date(" d.m.Y "); ?>" type='input' name="date_payment" value="<?php echo $this->input->post('date_payment'); ?>" class="form-control" id="date_payment"/>
                        </div>
                      </div>
                  </div>

                  <div class="form-group toggle-due" style="display:none;">
                      <div class="col-md-5">
                        <label for="vade" class="control-label"><?php echo lang('duedate'); ?></label>
                      </div>

                      <div class="col-md-7">
                        <div id="datetimepicker4" class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input required placeholder="<?php echo date(" d.m.Y "); ?>" type='input' name="duedate" value="<?php echo $this->input->post('date_payment'); ?>" class="form-control" id="duedate"/>
                        </div>
                      </div>
                  </div>

                  <div class="form-group" style="display: none">
                    <div class="col-md-offset-3 col-md-5">
                      <button class="btn btn-save btn-lg" data-post-form="#form" tabindex="-1" data-loading-text="Kaydet"><?php echo lang('save'); ?></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>

		<div class="panel-body table-responsive">
			<table class="table table-hovered table-invoice">
				<thead>
					<tr>
					<th width="22.5%"><?php echo lang('productcode'); ?></th>
					<th width="22.5%"><?php echo lang('productservice'); ?></th>
					<th width="10%"><?php echo lang('quantity'); ?></th>
					<th width="15%"><?php echo lang('price'); ?></th>
					<th width="10%"><?php echo lang('tax'); ?></th>
					<th width="15%"><?php echo lang('total'); ?></th>
					<!-- <th width="5%"></th> -->
					<th width="5%">
          <!-- <a class="btn btn-success btn-reverse lg-mt-30" id="add-line"><?php echo lang('addnewline'); ?></a> -->
          <a class="btn btn-success btn-sm delete-line" id="add-line"><i class="icon ion-plus-round"></i></a>
          </th>
					</tr>
				</thead>
				<tbody>
					<tr class="select-properties-list sample-line" style="display:none">
					<td class="add-on-edit">
						<div class="add-on-edit">
						<input type="hidden" name="in[code][]" class="form-control input-code">
						<select class="form-control product_code">
							<option value="0"></option>
							<?php
							foreach ( $products as $product ) {
								echo '<option value='.$product["id"].'>'.$product["code"].'</option>';
							}
							?>
						</select>
						<input class="input-productcode" type="hidden" name="in[product_id][]">
						<div class="property mt-10" data-name="description" data-title="<?php echo lang('description')?>" data-status="passive">
						<textarea type="text" name="in[description][]" class="form-control input-item-description" placeholder="<?php echo lang('description')?>"></textarea>
						<div class="property-delete mt-10">
							<a class="btn btn-danger delete"><i class="icon icon-left mdi mdi mdi-delete"></i></a>
						</div>
						</div>
					</td>
					<td class="add-on-edit">
						<div class="add-on-edit">
						<input required type="text" name="in[name][]" class="form-control input-product autocomplate-product ui-autocomplete-input " autocomplete="on" placeholder="Product Name">
						</div>
					</td>
					<td>
						<div class="input-group ">
						<input type="text" name="in[amount][]" class="form-control input-amount filter-money" value="1,00">
						<!-- <div class="input-group text-muted">
							<input type="hidden" name="in[unit][]" value="<?php echo lang('unit'); ?>" class="input-unit input-xs">
						</div> -->
						</div>
					</td>
					<td>
						<div class="input-group-icon ">
						<input required type="text" name="in[price][]" class="form-control input-price filter-money">
						<input type="hidden" name="in[pricepost][]" class="price-post">
						<input type="hidden" name="in[price_discounted][]" class="form-control input-price-discounted" value="0">
						</div>
						<div class="property" data-name="indirim" data-title="<?php echo lang('discount'); ?>" data-status="passive">
						<div class="input-group mt-10">
						<input type="text" name="in[discount_rate][]" class="form-control input-discount-rate delete-on-delete" value="0">
						<input name="in[discount_type][]" type="hidden" value="rate" class="input-discount-type">
						<input type="hidden" name="in[discount_rate_status][]" class="form-control input-status" value="0">
						<div class="input-group-addon text-muted">%</div>
						</div>
						</div>
					</td>
					<td>
						<div class="input-group ">
						<input type="text" name="in[vat][]" class="form-control input-vat input-vat-vat filter-number" value="0,00">
						<input type="hidden" name="in[total_vat][]" class="input-vat-vat-total" value="0">
						</div>
						
					</td>
					<td>
						<div class="input-group ">
						<input type="text" class="form-control input-total filter-money on-tab-add-line" value="0,00">
						<input type="hidden" name="in[total][]" class="input-total-real">
						<input name="in[currency][]" type="hidden" value="USD" class="input-currency">
						<input name="rate_in[currency][]" type="hidden" value="1" class="input-rate">
						</div>
					</td>
					<!-- <td>
						<div class="select-properties"></div>
					</td> -->
					<td><a class="btn btn-danger btn-sm delete-line"><i class="icon icon-left mdi mdi mdi-delete"></i></a>
					</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
					<td colspan="3">
					</td>
					<td colspan="4" rowspan="2" style="padding:0px;">
						<table class="table-total pull-right select-properties-list">
						<tbody>
							<tr class="sub-totals">
							<th width="280"><?php echo lang('subtotal'); ?></th>
							<th class="text-right" width="170">
								<div class="sub-total"><span class="money-format"><span class="money-main">0</span><span class="money-float">,00</span></span> £
								</div>
							</th>
							<th width="50">
								<div class="select-properties dropdown" style="visibility: visible;"><a class="dropdown-toggle btn btn-sm btn-success " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span class="vals"><i class="icon ion-plus"></i></span> </a>
								<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">
									<li><a href="#" data-name="subtotaldiscount"><?php echo lang('subtotaldiscount'); ?></a>
									</li>
								</ul>
								</div>
							</th>
							</tr>
							<tr class="no-border line-discount" style="display: none;">
							<th width="200"><?php echo lang('linediscount');?></th>
							<th class="text-right"><span class="money-format"><span class="money-main">0</span><span class="money-float">,00</span></span> £
							</th>
							<th></th>
							</tr>
							<tr class="no-border gross-total" style="display: none;">
							<th width="200"><?php echo lang('grosstotal')?></th>
							<th class="text-right"><span class="money-format"><span class="money-main">0</span><span class="money-float">,00</span></span> £
							</th>
							<th></th>
							</tr>
						</tbody>
						<tbody>
							<tr>
							<th><?php echo lang('tax')?></th>
							<th class="text-right"><span class="vat-total money-format"><span class="money-main">0</span><span class="money-float">,00</span></span> £
							</th>
							<th></th>
							</tr>
						</tbody>
						<tbody class="grandtotal">
							<tr class="money-bold">
							<th><?php echo lang('grandtotal'); ?></th>
							<th class="text-right"><span class="grant-total money-format"><span class="money-main">0</span><span class="money-float">,00</span></span> £
							</th>
							<th></th>
							</tr>
						</tbody>
						</table>
						<input type="hidden" class="input-sub-total" name="total_sub" value="0">
						<input type="hidden" class="input-line-discount" name="total_discount" value="0">
						<input type="hidden" class="input-vat-total" name="total_vat" value="0">
						<input type="hidden" class="input-grant-total" name="total" value="0">
						<input type="hidden" name="staffid" value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>">
					</td>
					</tr>
				</tfoot>
			</table>
		</div>
    </div>
    </form>
  </div>
<!-- 
  <div class="col-lg-3">
    <?php include_once dirname(dirname(__FILE__)) . '/inc/sidebar.php' ;?>
  </div> -->
</div>


<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_invoice.php';?>

  <script src="<?php echo base_url('assets/crm/lib/jquery-ui/jquery-ui.js')?>"></script>
    
  <script>
  $(function () {
    "use strict";
    var id;
    id = new Invoice_Create({currency : 'USD', edit : 0,type : 'sale', payment_count : 1,copy:0});
  });
  </script>

  <script>
  
	$( ".save-invoice" ).click( function (){
		var invoicetotal = $('.input-grant-total').val();
	if (invoicetotal > 0) {
		$( ".invoice-form" ).submit();
	}else{
		$.gritter.add( {
			title: '<b><?php echo lang('notification')?></b>',
			text: '<?php echo lang('pleaseenteritem')?>',
			position: 'bottom',
			class_name: 'color danger',
		} );
		speak(emptyinvoice);
	}
	});

  </script>