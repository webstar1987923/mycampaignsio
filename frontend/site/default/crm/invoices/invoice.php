<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h3><?php echo lang('invoiceprefix'),'-',str_pad($invoices['id'], 6, '0', STR_PAD_LEFT); ?></h3>
					<span class="panel-subheading">
						<?php switch($settings['dateformat']){ 
							case 'yy.mm.dd': echo _rdate($invoices['datecreated']);break; 
							case 'dd.mm.yy': echo _udate($invoices['datecreated']); break;
							case 'yy-mm-dd': echo _mdate($invoices['datecreated']); break;
							case 'dd-mm-yy': echo _cdate($invoices['datecreated']); break;
							case 'yy/mm/dd': echo _zdate($invoices['datecreated']); break;
							case 'dd/mm/yy': echo _kdate($invoices['datecreated']); break;
						} ?>
					</span>
				</div>
				<div class="pull-right" style="<?php if($invoices['statusid'] == 4){echo'display:interit';}else echo 'display:none'?>">
          <div role="alert" class="alert alert-danger">
      			<i class="icon-close pull-left pr-15"></i> 
      			<p class="pull-left"><?php echo lang('cancelledinvoice');?></p>
      		</div>
				</div>
				<div class="pull-right">
					<a href="<?php echo site_url('invoices/edit/'.$invoices['id']); ?>" class="btn btn-warning pull-right"><?php echo lang('updateinvoicetitle'); ?></a>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6">
							<address class="mb-15">
								<h4 class="address-head mb-5"><?php echo $settings['company'] ?></h4>
								<span><?php echo $settings['address'] ?></span> <br>
								<span><?php echo $settings['postcode'] ?>/<?php echo $settings['town'] ?>/<?php echo $settings['city'] ?>,<?php echo $settings['countryid'] ?></span> <br>
								<span><?php echo lang('phone')?>: <?php echo $settings['phone'] ?></span> <br>
								<span><?php echo lang('fax')?>: <?php echo $settings['fax'] ?></span> <br>
								<span><?php echo lang('contactemail')?>: <?php echo $settings['email'] ?></span> <br>
								<span><?php echo $settings['taxoffice'] ?></span> <br>
								<span><?php echo lang('vatnumber')?>: <?php echo $settings['vatnumber'] ?></span>
							</address>
							
						</div>
						<div class="col-xs-6 text-right">
							<span class="txt-dark head-font inline-block capitalize-font mb-5">TO</span>
							<address class="mb-15">
								<h4 class="address-head mb-5"><?php if($invoices['customer']===NULL){echo $invoices['namesurname'];} else echo $invoices['customer']; ?></h4>
								<span><?php echo $invoices['companyemail']; ?></span><br>
								<span><?php echo $invoices['customeraddress']; ?></span><br>
								<span><?php echo $invoices['companyphone'] ?></span><br>
								<span><?php echo $invoices['taxoffice'] ?></span><br>
								<span><?php echo lang('vatnumber')?>: <?php echo $invoices['taxnumber'] ?></span>
							</address>
						</div>
					</div>
					
					<div class="seprator-block"></div>
					
					<div class="invoice-bill-table">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width:50%"><?php echo lang('invoiceitemdescription')?></th>
										<th style="width:10%" class="amount"><?php echo lang('quantity')?></th>
										<th style="width:10%" class="amount"><?php echo lang('price')?></th>
										<th style="width:10%" class="amount"><?php echo lang('discount')?></th>
										<th style="width:10%" class="amount"><?php echo lang('vat')?></th>
										<th style="width:10%" class="amount"><?php echo lang('total')?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($invoiceitems as $fu) {?>
									<tr>
										<td class="description"><?php if($fu['in[product_id]'] = NULL){echo $fu['name'];}else {echo $fu['in[name]'];}?><br>(<?php echo $fu['in[description]'];?>)</td>
										<td class="amount">
										<span class="money-area"><?php echo $fu['in[amount]'];?></span>
										</td>
										<td class="amount">
										<span class="money-area"><?php echo $fu['in[price]']?></span>
										</td>
										<td class="amount">
											<?php echo $fu['in[discount_rate]'];?>
										</td>
										<td class="amount">
											<?php echo $fu['in[vat]'] ?>
										</td>
										<td class="amount">
										<span class="money-area"><?php echo $fu['in[total]']?></span>
										</td>
									</tr>
									<?php }?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3"></td>
										<td colspan="3">
											<table class="table-total select-properties-list invoice-details pull-right">
												<tr>
													<td width="280" class="summary"><?php echo lang('subtotal')?></td>
													<td width="100" class="amount">
														<span class="money-area weight-600">£ <?php echo $invoices['total_sub']?></span>
													</td>
												</tr>
												<tr>
													<td class="summary"><?php echo lang('linediscount')?></td>
													<td class="amount">
													<span class="money-area weight-600">£ <?php echo $invoices['total_discount']?></span>
													</td>
												</tr>
												<tr>
													<td class="summary"><?php echo lang('grosstotal')?></td>
													<td class="amount">
													<?php $grosstotal = ($invoices['total_sub'] - $invoices['total_discount']);?>
													<span class="money-area weight-600">£ <?php echo $grosstotal?></span>
													</td>
												</tr>
												<tr>
													<td class="summary"><?php echo lang('tax')?></td>
													<td class="amount">
													<span class="money-area weight-600">£ <?php echo $invoices['total_vat']?></span>
												</tr>
												
												<tr>
													<td class="summary total"><?php echo lang('total')?></td>
													<td class="amount total-value">
													<span class="money-area weight-600">£ <?php echo $invoices['total']?></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</tfoot>
							</table>

							
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer pa-20">
				<div class="row">
					<div class="col-md-12 invoice-message">
						<span class="title"><?php echo $invoices['notetitle']; ?></span>
						<p><?php echo $invoices['not']; ?></p>
					</div>
				</div>
				<div class="row invoice-company-info">
					<div class="col-sm-6 col-md-2 invoice-logo" style="background-image: url(<?php echo base_url('uploads/ciuis_settings/'.$settings['logo'].'') ?>)"></div>
					<div class="col-sm-6 col-md-4 summary">
						<span class="title"><?php echo $settings['company'] ?></span>
						<p><?php echo $settings['address']; ?></p>
					</div>
					<div class="col-sm-6 col-md-3 phone">
						<ul class="list-unstyled">
							<li><?php echo $settings['phone']; ?></li>
							<li> <?php echo $settings['fax']; ?></li>
						</ul>
					</div>
					<div class="col-sm-6 col-md-2 email">
						<ul class="list-unstyled">
							<li><?php echo $settings['email']; ?></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<h6>Invoice Note</h6>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
					<p>Some note goes here.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<?php include_once dirname(dirname(__FILE__)) . '/inc/invoice_sidebar.php'; ?>
	</div>
</div>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_invoice.php' ;?>