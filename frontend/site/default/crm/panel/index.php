<?php $page = 'dashboard'; ?>

<?php include_once dirname( dirname(__FILE__) ) . '/inc/header.php'; ?>
<?php
	$currentyear = date('Y');
	$currentMonth = date('m');
	$lastyear = $currentyear - 1;
	$this->db->select_sum('total');
	$this->db->from('invoices');
	$this->db->where('company_id', $_SESSION['company_id'])->where('MONTH(duedate)', $currentMonth)->where('YEAR(duedate)', $currentyear);
	$cma = $this->db->get()->row()->total;
	
	$this->db->select_sum('total');
	$this->db->from('invoices');
	$this->db->where('company_id', $_SESSION['company_id'])->where('MONTH(duedate)', $currentMonth)->where('YEAR(duedate)', $lastyear);
	$lym = $this->db->get()->row()->total;

	$this->db->select_sum('amount');
	$this->db->from('expenses');
	$this->db->where('company_id', $_SESSION['company_id'])->where('YEAR(date)', $lastyear);
	$lye = $this->db->get()->row()->amount;

	$settings = $this->Settings_Model->get_settings_ciuis();
;?>
	<!-- Row -->
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view panel-refresh">
				<div class="refresh-container">
					<div class="la-anim-1"></div>
				</div>

				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Invoices Overdue / Current</h6>
					</div>
					<div class="pull-right">
						<a href="#" class="pull-right inline-block refresh mr-15">
							<i class="zmdi zmdi-replay"></i>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<center><div id="ct_chart_3" class="ct-chart ct-perfect-fourth" style="width: 183px;"></div></center>
					</div>	
				</div>
			</div>

			<div class="panel panel-default card-view panel-refresh">
				<div class="refresh-container">
					<div class="la-anim-1"></div>
				</div>
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Expenses</h6>
					</div>
					<div class="pull-right">
						<a href="#" class="pull-right inline-block refresh mr-15">
							<i class="zmdi zmdi-replay"></i>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper">
					<div class="panel-body">
						<center><div id="ct_chart_4" class="ct-chart ct-perfect-fourth" style="width: 195px;"></div></center>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body sm-data-box-1">
						<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Profit Month To Date</span>	
						<div class="cus-sat-stat weight-500 txt-success text-center mt-5">
							<span class="counter-anim"><?php echo ($akt - $mex); ?></span>
						</div>
						<div class="progress-anim mt-20">
							<div class="progress">
								<div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
							<center>
								<span class="block">Profit Year to Date</span>
								<span class="block txt-dark weight-500 font-15"><?php echo ($ycr - $lye); ?></span>
							</center>
					</div>
				</div>
			</div>
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">invoices stats</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div>
							<span class="pull-left inline-block capitalize-font txt-dark">
								paid
							</span>
							<span class="label label-warning pull-right"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($ofv, 2, ',', '.');break;case '.': echo number_format($ofv, 2, '.', ',');break;}?></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10"/>
							<span class="pull-left inline-block capitalize-font txt-dark">
								unpaid
							</span>
							<span class="label label-danger pull-right"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($oft, 2, ',', '.');break;case '.': echo number_format($oft, 2, '.', ',');break;}?></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10"/>
							<span class="pull-left inline-block capitalize-font txt-dark">
								overdue
							</span>
							<span class="label label-success pull-right"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format($vgf, 2, ',', '.');break;case '.': echo number_format($vgf, 2, '.', ',');break;}?></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10"/>
							<span class="pull-left inline-block capitalize-font txt-dark">
								due
							</span>
							<span class="label label-primary pull-right"><?php echo currency;?> <?php switch($settings['unitseparator']){case ',': echo number_format(($oft + $vgf), 2, ',', '.');break;case '.': echo number_format(($oft + $vgf), 2, '.', ',');break;}?></span>
							<div class="clearfix"></div>
						</div>
					</div>	
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">This year to date - Income</h6>
					</div>
					<div class="pull-right">
						<span class="no-margin-switcher">
							<input type="checkbox" id="morris_switch"  class="js-switch" data-color="#ea6c41" data-secondary-color="#177ec1" data-size="small"/>	
						</span>	
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div id="morris_extra_line_chart" class="morris-chart" style="height:289px;"></div>
						<ul class="flex-stat mt-40">
							<li>
								<span class="block">Month To Date</span>
								<span class="block txt-dark weight-500 font-18"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($cma, 2, ',', '.');break;case '.': echo number_format($cma, 2, '.', ',');break;}?></span></span>
							</li>
							<li>
								<span class="block">Year To Date</span>
								<span class="block txt-dark weight-500 font-18"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($ycr, 2, ',', '.');break;case '.': echo number_format($ycr, 2, '.', ',');break;}?></span></span></span>
							</li>
							<li>
								<span class="block">Last year same period</span>
								<span class="block txt-dark weight-500 font-18"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($lym, 2, ',', '.');break;case '.': echo number_format($lym, 2, '.', ',');break;}?></span></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
				
	<!-- Row -->
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view pa-0">
				<div class="panel-wrapper collapse in">
					<div class="panel-body pa-0">
						<div class="sm-data-box bg-red">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left wrap-center">
										<span class="txt-light block counter"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($akt, 2, ',', '.');break;case '.': echo number_format($akt, 2, '.', ',');break;}?></span></span>
										<span class="weight-500 uppercase-font txt-light block font-13">Monthly Income</span>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view pa-0">
				<div class="panel-wrapper collapse in">
					<div class="panel-body pa-0">
						<div class="sm-data-box bg-yellow">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
										<span class="txt-light block counter"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($mex, 2, ',', '.');break;case '.': echo number_format($mex, 2, '.', ',');break;}?></span></span>
										<span class="weight-500 uppercase-font txt-light block font-13">Monthly Expenses</span>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view pa-0">
				<div class="panel-wrapper collapse in">
					<div class="panel-body pa-0">
						<div class="sm-data-box bg-green">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left wrap-center">
										<span class="txt-light block counter"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($ycr, 2, ',', '.');break;case '.': echo number_format($ycr, 2, '.', ',');break;}?></span></span>
										<span class="weight-500 uppercase-font txt-light block font-13">Yearly Total Income</span>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-default card-view pa-0">
				<div class="panel-wrapper collapse in">
					<div class="panel-body pa-0">
						<div class="sm-data-box bg-blue">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left wrap-center">
										<span class="txt-light block counter"><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($bbt, 2, ',', '.');break;case '.': echo number_format($bbt, 2, '.', ',');break;}?></span></span>
										<span class="weight-500 uppercase-font txt-light block">Bank Balance Total</span>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view panel-refresh">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Invoice Summary</h6>
					</div>
					<div class="pull-right">
						<!-- <span class="label label-info capitalize-font inline-block ml-10">{{totalinvoicesayisi}}</span>.replace(/{{totalinvoicesayisi}}/, options.data.totalinvoicesayisi) -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-sm-5 pa-0">
							<canvas id="chart_7" height="120"></canvas>
						</div>
						<div class="col-sm-5 pr-0 pt-0">
							<table class="table">
								<tr class="success">
									<td>
										<span class="clabels inline-block bg-green mr-5"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Invoiced</span>
									</td>
									<td>
										<span class="txt-dark font-12 ml-15"><?php echo $settings['currencyname'];?> <?php switch($settings['unitseparator']){case ',': echo number_format($fam, 2, ',', '.');break;case '.': echo number_format($fam, 2, '.', ',');break;}?></span>
									</td>
								</tr>
								<tr class="success">
									<td>
										<span class="clabels inline-block bg-yellow mr-5"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Received</span>
									</td>
									<td>
									<span class="txt-dark font-12 ml-15"><?php echo $settings['currencyname'];?> <?php switch($settings['unitseparator']){case ',': echo number_format($ofv, 2, ',', '.');break;case '.': echo number_format($ofv, 2, '.', ',');break;}?></span>
									</td>
								</tr>
								<tr class="success">
									<td>
										<span class="clabels inline-block bg-red mr-5"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Outstanding</span>										
									</td>
									<td>
										<span class="txt-dark font-12 ml-15"><?php echo $settings['currencyname'];?> <?php switch($settings['unitseparator']){case ',': echo number_format($vgf, 2, ',', '.');break;case '.': echo number_format($vgf, 2, '.', ',');break;}?></span></span></span>
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>					
					</div>
				</div>
			</div>
		</div>	

		<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view panel-refresh">
				<div class="refresh-container">
					<div class="la-anim-1"></div>
				</div>
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">ALL BANKS</h6>
					</div>
					<div class="pull-right">
						<a href="#" class="pull-left inline-block refresh mr-15">
							<i class="zmdi zmdi-replay"></i>
						</a>
						<div class="pull-left inline-block dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
							<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>option 1</a></li>
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>option 2</a></li>
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>option 3</a></li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
								<table class="table mb-0">
									<thead>
									<tr>
										<th>Name</th>
										<th>Balance</th>
									</tr>
									</thead>
									<tbody>
									<?php
									$accounts = $this->db->where('company_id', $_SESSION['company_id'])->from('accounts')->get()->result_array();
									foreach($accounts as $row):
									?>
										<tr class="success">
											<td><?php echo $row['name'];?></td>
											<td><?php echo currency;echo $row['total'];?></td>
										</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
	</div>	
	<!-- Row -->

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view panel-refresh">
				<div class="refresh-container">
					<div class="la-anim-1"></div>
				</div>
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Forecast vs Actual</h6>
					</div>
					<div class="pull-right">
						<a href="#" class="pull-left inline-block refresh mr-15">
							<i class="zmdi zmdi-replay"></i>
						</a>
						<a href="#" class="pull-left inline-block full-screen mr-15">
							<i class="zmdi zmdi-fullscreen"></i>
						</a>
						<div class="pull-left inline-block dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
							<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Edit</a></li>
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>Delete</a></li>
								<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>New</a></li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div id="morris_extra_bar_chart" class="morris-chart" style="height:250px;"></div>
				</div>
			</div>
		</div>
	</div>	
	<!-- Row -->

<!-- Dashboard Data init -->

<?php

	$currentMonth = date('m');
	for ($i=0; $i < 12; $i++) { 
		$this->db->select_sum('total');
		$this->db->from('invoices');
		$this->db->where('company_id', $_SESSION['company_id'])->where('MONTH(duedate)', $currentMonth + $i);
		$amount = $this->db->get()->row()->total;
		if ($amount > 0) {
			$projection[$i] = $amount;
		} else {
			$projection[$i] = '0.00';
		}
	}

	$currentyear = date('Y');
	for ($i=1; $i <= 12; $i++) { 
		$this->db->select_sum('total');
		$this->db->from('invoices');
		$this->db->where('company_id', $_SESSION['company_id'])->where('MONTH(duedate)', $i)->where('YEAR(duedate)', $currentyear);
		$amount = $this->db->get()->row()->total;
		if ($amount > 0) {
			$income[$i] = $amount;
		} else {
			$income[$i] = '0.00';
		}
	}

	$lastyear = $currentyear - 1;
	for ($i=1; $i <= 12; $i++) { 
		$this->db->select_sum('total');
		$this->db->from('invoices');
		$this->db->where('company_id', $_SESSION['company_id'])->where('statusid', 2)->where('MONTH(duedate)', $i)->where('YEAR(duedate)', $currentyear);
		$amount = $this->db->get()->row()->total;
		if ($amount > 0) {
			$paid_income[$i] = $amount;
		} else {
			$paid_income[$i] = 0.00;
		}
	}

	for ($i=1; $i <= 12; $i++) { 
		$trend_income[$i] = ($paid_income[$i] + $income[$i])/2;
	}
	
	$accounts = $this->db->from('accounts')->get()->result_array();
	$key = 0;
	foreach($accounts as $row):
		$account_name[$key] = $row['name'];
		$account_total[$key] = $row['total'];
		$key++;
	endforeach;
?>

<script>
	/*Dashboard Init*/
	
	
	"use strict";

	/*****Ready function start*****/
	$(document).ready(function(){
		$('#statement').DataTable({
			"bFilter": false,
			"bLengthChange": false,
			"bPaginate": false,
			"bInfo": false,
		});

		if( $('#chart_7').length > 0 ){
			var ctx7 = document.getElementById("chart_7").getContext("2d");
			var data7 = {
					labels: [
				"Total Received",
				"Total Unpaid",
			],
			datasets: [
				{
					data: [<?php echo $ofv ?>,<?php echo $oft; ?>],
					backgroundColor: [
						"rgba(70,148,8,1)",
						"rgba(230,154,42,1)",
						"rgba(234,108,65,1)"
					],
					hoverBackgroundColor: [
						"rgba(70,148,8,1)",
						"rgba(230,154,42,1)",
						"rgba(234,108,65,1)"
					]
				}]
			};
			
			var doughnutChart = new Chart(ctx7, {
				type: 'doughnut',
				data: data7,
				options: {
					animation: {
						duration:	3000
					},
					elements: {
						arc: {
							borderWidth: 0
						}
					},
					responsive: true,
					maintainAspectRatio:false,
					percentageInnerCutout: 50,
					legend: {
						display:false
					},
					tooltips: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Roboto'"
					},
					cutoutPercentage: 70,
					segmentShowStroke: false
				}
			});
		}	
	
		if( $('#chart_8').length > 0 ){
			var ctx2 = document.getElementById("chart_8").getContext("2d");
			var labels = <?php echo json_encode($account_name); ?>;
			var data = <?php echo json_encode($account_total); ?>;
			var data2 = {
				labels: labels,
				datasets: [
					{
						label: "My First dataset",
						backgroundColor: "rgba(230,154,42,1)",
						borderColor: "rgba(230,154,42,1)",
						data: data
					}
				]
			};
			
			var hBar = new Chart(ctx2, {
				type:"horizontalBar",
				data:data2,
				
				options: {
					tooltips: {
						mode: 'none',
					},
					scales: {
						yAxes: [{
							stacked: true,
							gridLines: {
								color: "#878787",
							},
							ticks: {
								fontFamily: "Roboto",
								fontColor:"#878787"
							}
						}],
						xAxes: [{
							stacked: true,
							gridLines: {
								color: "#878787",
							},
							ticks: {
								fontFamily: "Roboto",
								fontColor:"#878787"
							}
						}],
						
					},
					elements:{
						point: {
							hitRadius:40
						}
					},
					animation: {
						duration:	3000
					},
					responsive: true,
					maintainAspectRatio:false,
					legend: {
						display: false,
					},
				}
			});
		}
		if( $('#ct_chart_3').length > 0 ){
			var data = {
				series: ['<?php echo $vgf + 0 ;?>','<?php echo $vgf + $oft;?>'],
				labels: ['Overdue <?php echo currency;?><?php echo $vgf + 0 ;?>', 'Current <?php echo currency;?><?php echo $vgf + $oft;?>'],
			};

			var options = {
				labelInterpolationFnc: function(value) {
				return value
				},
			};

			new Chartist.Pie('#ct_chart_3', data, options);
		}
		if( $('#ct_chart_4').length > 0 ){
			var data = {
				series: ['<?php echo $tem + 0; ?>'],
				labels: ['<?php echo currency;echo $tem + 0; ?>'],
			};

			var options = {
				labelInterpolationFnc: function(value) {
				return value
				},
				labelOffset: -30,
			};

			new Chartist.Pie('#ct_chart_4', data, options);
		}
		if($('#morris_extra_bar_chart').length > 0) {
		// Morris bar chart
			var data = [{
					period: 'Jan',
					Income: '<?php echo $income[1];?>',
					paid_Income: '<?php echo $paid_income[1];?>',
					Trend: '<?php echo $trend_income[1];?>',
				}, 
				{
					period: 'Feb',
					Income: '<?php echo $income[2];?>',
					paid_Income: '<?php echo $paid_income[2];?>',
					Trend: '<?php echo $trend_income[2];?>',
				},
				{
					period: 'March',
					Income: '<?php echo $income[3];?>',
					paid_Income: '<?php echo $paid_income[3];?>',
					Trend: '<?php echo $trend_income[3];?>',
				},
				{
					period: 'April',
					Income: '<?php echo $income[4];?>',
					paid_Income: '<?php echo $paid_income[4];?>',
					Trend: '<?php echo $trend_income[4];?>',
				},
				{
					period: 'May',
					Income: '<?php echo $income[5];?>',
					paid_Income: '<?php echo $paid_income[5];?>',
					Trend: '<?php echo $trend_income[5];?>',
				},
				{
					period: 'June',
					Income: '<?php echo $income[6];?>',
					paid_Income: '<?php echo $paid_income[6];?>',
					Trend: '<?php echo $trend_income[6];?>',
				},
				{
					period: 'July',
					Income: '<?php echo $income[7];?>',
					paid_Income: '<?php echo $paid_income[7];?>',
					Trend: '<?php echo $trend_income[7];?>',
				},
				{
					period: 'Aug',
					Income: '<?php echo $income[8];?>',
					paid_Income: '<?php echo $paid_income[8];?>',
					Trend: '<?php echo $trend_income[8];?>',
				},
				{
					period: 'Sep',
					Income: '<?php echo $income[9];?>',
					paid_Income: '<?php echo $paid_income[9];?>',
					Trend: '<?php echo $trend_income[9];?>',
				},
				{
					period: 'Oct',
					Income: '<?php echo $income[10];?>',
					paid_Income: '<?php echo $paid_income[10];?>',
					Trend: '<?php echo $trend_income[10];?>',
				},
				{
					period: 'Nov',
					Income: '<?php echo $income[11];?>',
					paid_Income: '<?php echo $paid_income[11];?>',
					Trend: '<?php echo $trend_income[11];?>',
				},
				{
					period: 'Dec',
					Income: '<?php echo $income[12];?>',
					paid_Income: '<?php echo $paid_income[12];?>',
					Trend: '<?php echo $trend_income[12];?>',
				}
				];
			var barchart = Morris.Bar({
				element: 'morris_extra_bar_chart',
				data: data,
				xkey: 'period',
				ykeys: ['Income', 'paid_Income'],
				barColors:['#177ec1', '#dc4666', '#e69a2a'],
				hideHover: 'auto',
				gridLineColor: '#878787',
				resize: true,
				gridTextColor:'#878787',
				gridTextFamily:"Roboto"
			});
		}

		if($('#morris_extra_line_chart').length > 0) {
			var data = [{
				period: 'Jan',
				Income: '<?php echo $income[1];?>',
				paid_Income: '<?php echo $paid_income[1];?>',
				Trend: '<?php echo $trend_income[1];?>',
			}, 
			{
				period: 'Feb',
				Income: '<?php echo $income[2];?>',
				paid_Income: '<?php echo $paid_income[2];?>',
				Trend: '<?php echo $trend_income[2];?>',
			},
			{
				period: 'March',
				Income: '<?php echo $income[3];?>',
				paid_Income: '<?php echo $paid_income[3];?>',
				Trend: '<?php echo $trend_income[3];?>',
			},
			{
				period: 'April',
				Income: '<?php echo $income[4];?>',
				paid_Income: '<?php echo $paid_income[4];?>',
				Trend: '<?php echo $trend_income[4];?>',
			},
			{
				period: 'May',
				Income: '<?php echo $income[5];?>',
				paid_Income: '<?php echo $paid_income[5];?>',
				Trend: '<?php echo $trend_income[5];?>',
			},
			{
				period: 'June',
				Income: '<?php echo $income[6];?>',
				paid_Income: '<?php echo $paid_income[6];?>',
				Trend: '<?php echo $trend_income[6];?>',
			},
			{
				period: 'July',
				Income: '<?php echo $income[7];?>',
				paid_Income: '<?php echo $paid_income[7];?>',
				Trend: '<?php echo $trend_income[7];?>',
			},
			{
				period: 'Aug',
				Income: '<?php echo $income[8];?>',
				paid_Income: '<?php echo $paid_income[8];?>',
				Trend: '<?php echo $trend_income[8];?>',
			},
			{
				period: 'Sep',
				Income: '<?php echo $income[9];?>',
				paid_Income: '<?php echo $paid_income[9];?>',
				Trend: '<?php echo $trend_income[9];?>',
			},
			{
				period: 'Oct',
				Income: '<?php echo $income[10];?>',
				paid_Income: '<?php echo $paid_income[10];?>',
				Trend: '<?php echo $trend_income[10];?>',
			},
			{
				period: 'Nov',
				Income: '<?php echo $income[11];?>',
				paid_Income: '<?php echo $paid_income[11];?>',
				Trend: '<?php echo $trend_income[11];?>',
			},
			{
				period: 'Dec',
				Income: '<?php echo $income[12];?>',
				paid_Income: '<?php echo $paid_income[12];?>',
				Trend: '<?php echo $trend_income[12];?>',
			}
			];
			var dataNew = [{
				period: 'Jan',
				Income: '<?php echo $income[1];?>',
			}, 
			{
				period: 'Feb',
				Income: '<?php echo $income[2];?>',
			},
			{
				period: 'March',
				Income: '<?php echo $income[3];?>',
			},
			{
				period: 'April',
				Income: '<?php echo $income[4];?>',
			},
			{
				period: 'May',
				Income: '<?php echo $income[5];?>',
			},
			{
				period: 'June',
				Income: '<?php echo $income[6];?>',
			},
			{
				period: 'July',
				Income: '<?php echo $income[7];?>',
			},
			{
				period: 'Aug',
				Income: '<?php echo $income[8];?>',
			},
			{
				period: 'Sep',
				Income: '<?php echo $income[9];?>',
			},
			{
				period: 'Oct',
				Income: '<?php echo $income[10];?>',
			},
			{
				period: 'Nov',
				Income: '<?php echo $income[11];?>',
			},
			{
				period: 'Dec',
				Income: '<?php echo $income[12];?>',
			}
			];
			var lineChart = Morris.Line({
			element: 'morris_extra_line_chart',
			data: data ,
			xkey: 'period',
			ykeys: ['Income', 'paid_Income', 'Trend'],
			pointSize: 2,
			fillOpacity: 0,
			lineWidth:2,
			pointStrokeColors:['#e69a2a', '#dc4666', '#177ec1'],
			behaveLikeLine: true,
			gridLineColor: '#878787',
			hideHover: 'auto',
			lineColors: ['#e69a2a', '#dc4666', '#177ec1'],
			resize: true,
			redraw: true,
			gridTextColor:'#878787',
			gridTextFamily:"Roboto",
			parseTime: false
		});

		}
		/* Switchery Init*/
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		$('#morris_switch').each(function() {
			new Switchery($(this)[0], $(this).data());
		});
		var swichMorris = function() {
			if($("#morris_switch").is(":checked")) {
				lineChart.setData(data);
				lineChart.redraw();
			} else {
				lineChart.setData(dataNew);
				lineChart.redraw();
			}
		}
		swichMorris();	
		$(document).on('change', '#morris_switch', function () {
			swichMorris();
		});
		
	});

	var sparklineLogin = function() { 
		if( $('#sparkline_1').length > 0 ){
			$("#sparkline_1").sparkline([2,4,4,6,8,5,6,4,8,6,6,2 ], {
				type: 'line',
				width: '100%',
				height: '35',
				lineColor: '#177ec1',
				fillColor: 'rgba(23,126,193,.2)',
				maxSpotColor: '#177ec1',
				highlightLineColor: 'rgba(0, 0, 0, 0.2)',
				highlightSpotColor: '#177ec1'
			});
		}	
		if( $('#sparkline_2').length > 0 ){
			$("#sparkline_2").sparkline([0,2,8,6,8], {
				type: 'line',
				width: '100%',
				height: '35',
				lineColor: '#177ec1',
				fillColor: 'rgba(23,126,193,.2)',
				maxSpotColor: '#177ec1',
				highlightLineColor: 'rgba(0, 0, 0, 0.2)',
				highlightSpotColor: '#177ec1'
			});
		}	
		if( $('#sparkline_3').length > 0 ){
			$("#sparkline_3").sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40, 45, 56, 7, 10], {
				type: 'line',
				width: '100%',
				height: '35',
				lineColor: '#177ec1',
				fillColor: 'rgba(23,126,193,.2)',
				maxSpotColor: '#177ec1',
				highlightLineColor: 'rgba(0, 0, 0, 0.2)',
				highlightSpotColor: '#177ec1'
			});
		}
		if( $('#sparkline_4').length > 0 ){
			$("#sparkline_4").sparkline([0,2,8,6,8,5,6,4,8,6,6,2 ], {
				type: 'bar',
				width: '100%',
				height: '50',
				barWidth: '5',
				resize: true,
				barSpacing: '5',
				barColor: '#e69a2a',
				highlightSpotColor: '#e69a2a'
			});
		}	
	}
	var sparkResize;
		$(window).resize(function(e) {
			clearTimeout(sparkResize);
			sparkResize = setTimeout(sparklineLogin, 200);
		});
	sparklineLogin();
</script>


<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_graph.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>

<script>
	console.clear();
	$( function () {
		Highcharts.setOptions( {
			colors: [ '#ffbc00', 'rgb(239, 89, 80)' ]
		} );

		Highcharts.chart( 'monthlyexpenses', {
			title: {
				text: '',
			},
			credits: {
				enabled: false
			},
			chart: {
				backgroundColor: 'transparent',
				marginBottom: 0,
				marginLeft: -10,
				marginRight: -10,
				marginTop: 0,

				type: 'area',
			},
			exporting: {
				enabled: false
			},
			plotOptions: {
				series: {
					fillOpacity: 0.1
				},
				area: {
					lineWidth: 1,
					marker: {
						lineWidth: 2,
						symbol: 'circle',
						fillColor: 'black',
						radius: 3,
					},
					legend: {
						radius: 2,
					}
				}
			},
			xAxis: {
				categories: [ "<?php echo lang('january'); ?>", "<?php echo lang('february'); ?>", "<?php echo lang('march'); ?>", "<?php echo lang('april'); ?>", "<?php echo lang('may'); ?>", "<?php echo lang('june'); ?>", "<?php echo lang('july'); ?>", "<?php echo lang('august'); ?>", "<?php echo lang('september'); ?>", "<?php echo lang('october'); ?>", "<?php echo lang('november'); ?>", "<?php echo lang('december'); ?>" ],
				visible: true,
			},
			yAxis: {
				title: {
					enabled: false
				},
				visible: false
			},
			tooltip: {
				shadow: false,
				useHTML: true,
				padding: 0,
				formatter: function () {
					return '<div class="bis-tooltip" style="background-color: ' + this.color + '">' + this.x + ' <span>' + this.y + '$' + '</span></div>'
				}
			},
			legend: {
				align: 'right',
				enabled: false,
				verticalAlign: 'top',
				layout: 'vertical',
				x: -15,
				y: 100,
				itemMarginBottom: 20,
				useHTML: true,
				labelFormatter: function () {
					console.log( this )
					return '<span style="color:' + this.color + '">' + this.name + '</span>'
				},
				symbolPadding: 0,
				symbolWidth: 0,
				symbolRadius: 0
			},
			series: [ {"data":<?php echo $monthly_expense_graph ?>}]
		}, function ( chart ) {
			var series = chart.series;
			series.forEach( function ( serie ) {
				console.log( serie )
				if ( serie.legendSymbol ) {
					console.log( serie.legendSymbol.destroy )

					serie.legendSymbol.destroy();
				}
				if ( serie.legendLine ) {
					serie.legendLine.destroy();
				}
			} );
		} );
	} );
</script>
<style>
	.highcharts-tooltip-box {
		fill: transparent;
		stroke-width: 0;
	}
	.highcharts-legend-item {
		text-transform: uppercase;
	}
	.bis-tooltip {
		background-color:#fff;
		padding: 8px;
		border-radius: 30px;
		color: #FFF;
		box-shadow: 0px 5px 6px 3px rgba(0, 0, 0, 0.2);
	}
	.bis-tooltip span {
		font-weight: bold;
	}
</style>
<script>
	$( function () {
		var data = <?php echo $weekly_sales_chart ?>;
		var options = {
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				xAxes: [ {
					categoryPercentage: .2,
					barPercentage: 1,
					position: 'top',
					gridLines: {
						color: '#C7CBD5',
						zeroLineColor: '#C7CBD5',
						drawTicks: true,
						borderDash: [ 8, 5 ],
						offsetGridLines: false,
						tickMarkLength: 10,
						callback: function ( value ) {
							console.log( value )
								// return value.charAt(0) + value.charAt(1) + value.charAt(2);
						}
					},
					ticks: {
						callback: function ( value ) {
							return value.charAt( 0 ) + value.charAt( 1 ) + value.charAt( 2 );
						}
					}
				} ],
				yAxes: [ {
					display: false,
					gridLines: {
						drawBorder: false,
						drawOnChartArea: false,
						borderDash: [ 8, 5 ],
						offsetGridLines: false
					},
					ticks: {
						beginAtZero: true,
						maxTicksLimit: 5,
					}
				} ]
			},
			legend: {
				display: false
			}
		};
		var ctx = $( '#main-chart' );
		var mainChart = new Chart( ctx, {
			type: 'bar',
			data: data,
			borderRadius: 10,
			options: options
		} );
	} );
</script>
<script type="text/javascript">
	$( ".show-advanced-summary" ).click( function () {
		$( '.advanced-summary' ).show();
		$( '.advanced-summary' ).addClass( 'animated fadeIn' );
		$( '.hide-advanced-summary' ).show();
		$( '.show-advanced-summary' ).hide();
		speak(advancedsummaryshow);
	} );
	$( ".hide-advanced-summary" ).click( function () {
		$( '.advanced-summary' ).hide();
		$( '.hide-advanced-summary' ).hide();
		$( '.show-advanced-summary' ).show();
	} );
</script>