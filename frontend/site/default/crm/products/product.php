<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4><?php echo $product['productname']; ?></h4>
					<span class="panel-subtitle">
						<?php echo $product['description']; ?>
					</span>
				</div>
				<div class="pull-right">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<?php echo lang('action')?> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a data-toggle="modal" data-target="#edit">
									<?php echo lang('edit')?>
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a data-toggle="modal" data-target="#remove">
									<?php echo lang('delete')?>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body row row-eq-height">
					<div class="col-sm-12 col-lg-6">
						<div class="ml-10 mr-10">
							<span class="pull-left inline-block capitalize-font txt-dark"><?php echo lang('purchaseprice')?></span>
							<span class="label label-warning pull-right"><?php echo $product['purchase_price']?></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10">
							<span class="pull-left inline-block capitalize-font txt-dark"><?php echo lang('salesprice')?></span>
							<span class="label label-danger pull-right"><?php echo $product['sale_price']?></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10">
							<span class="pull-left inline-block capitalize-font txt-dark"><?php echo lang('vat')?></span>
							<span class="label label-success pull-right"><?php echo $product['vat']?>  %</span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10">
							<span class="pull-left inline-block capitalize-font txt-dark"><?php echo lang('instock')?></span>
							<span class="label label-primary pull-right"><?php echo $product['stock']?> <i class="ion-arrow-right-c"></i> <?php echo lang('remainingstock')?> : <b><?php echo $remaining = $product['stock'] - $tsp ?></b></span>
							<div class="clearfix"></div>
							<hr class="light-grey-hr row mt-10 mb-10">
							<span class="pull-left inline-block capitalize-font txt-dark"><?php echo lang('productcode')?></span>
							<span class="label label-success pull-right"><?php echo $product['code']?></span>
						</div>
					</div>
					<div class="col-sm-12 col-lg-3">
						<div class="sm-data-box-1">
							<span class="uppercase-font weight-500 font-24 block text-center txt-dark"><?php echo lang('totalsales')?></span>
							<div class="cus-sat-stat weight-500 txt-success text-center mt-5">
								<span class="anin-counter">
									<?php echo $tsp ?>
								</span>
							</div>
							<sapn class="font-14 block text-center"><?php echo lang('netearnings') ?></sapn>
						</div>	
					</div>
					<div class="col-sm-12 col-lg-3">
						<div class="sm-data-box-1">
							<span class="uppercase-font weight-500 font-24 block text-center txt-dark"><?php echo lang('netearnings')?></span>
							<sapn class="font-14 block text-center"><?php echo lang('netearningssub') ?></sapn>
							<?php
								$pricepurchase = $tsp *  $product['purchase_price'];
								$pricesales = $tsp * $product['sale_price'];

								$netearnings = $pricesales - $pricepurchase;
							?>
							<div class="cus-sat-stat weight-500 txt-success text-center mt-5">
								<span class="anin-counter">
									<?php echo $netearnings ?></span>
								</span>
							</div>
							<sapn class="font-14 block text-center"><?php echo lang('productnetearnings') ?></sapn>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="remove" tabindex="-1" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
					</div>
					<h3>
						<?php echo lang('attention'); ?>
					</h3>
					<p>
						<?php echo lang('productattentiondetail'); ?>
					</p>
					<div class="mt-50">
						<a type="button" data-dismiss="modal" class="btn btn-warning">
							<?php echo lang('cancel'); ?>
						</a>
						<a href="<?php echo base_url('products/remove/'.$product['id'].'')?>" type="button" class="btn btn-danger">
							<?php echo lang('delete'); ?>
						</a>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div id="edit" tabindex="-1" role="content" class="modal fade colored-header colored-header-warning">
	<div class="modal-dialog">
		<?php echo form_open('products/edit/'.$product['id'],array("class"=>"form-vertical")); ?>
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-left">
					<h5 class="modal-title">
						<?php echo lang('updateproduct')?>
					</h5>
				</div>
				<div class="pull-right">
					<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
				</div>
			</div>
			<div class="modal-body row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="productname" class="control-label mb-5">
							<?php echo lang('productname'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-pricetags"></i></span>
							<input type="text" name="productname" value="<?php echo ($this->input->post('productname') ? $this->input->post('productname') : $product['productname']); ?>" class="form-control" id="productname" placeholder="productname"/>
						</div>
					</div>
					<div class="form-group">
						<label for="sale_price" class="control-label mb-5">
							<?php echo lang('purchaseprice'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-social-usd"></i></span>
							<input type="text" name="purchase_price" value="<?php echo ($this->input->post('purchase_price') ? $this->input->post('purchase_price') : $product['purchase_price']); ?>" class="form-control" id="purchase_price" placeholder="purchase_price"/>
						</div>
					</div>
					<div class="form-group">
						<label for="sale_price" class="control-label mb-5">
							<?php echo lang('salesprice'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-social-usd"></i></span>
							<input type="text" name="sale_price" value="<?php echo ($this->input->post('sale_price') ? $this->input->post('sale_price') : $product['sale_price']); ?>" class="form-control" id="sale_price" placeholder="sale_price"/>
						</div>
					</div>
				</div>
				<div class="col-md-6 md-pl-0">
					<div class="form-group">
						<label for="code" class="control-label mb-5">
							<?php echo lang('productcode'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-barcode"></i></span>
							<input type="text" name="code" value="<?php echo ($this->input->post('code') ? $this->input->post('code') : $product['code']); ?>" class="form-control" id="code" placeholder="Product Code"/>
						</div>
					</div>
					<div class="form-group">
						<label for="vat" class="control-label mb-5">
							<?php echo lang('vat'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-medical"></i></span>
							<input type="text" name="vat" value="<?php echo ($this->input->post('vat') ? $this->input->post('vat') : $product['vat']); ?>" class="form-control" id="vat" placeholder="Vat Rate %"/>
						</div>
					</div>
					<div class="form-group">
						<label for="stock" class="control-label mb-5">
							<?php echo lang('instock'); ?>
						</label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-cube"></i></span>
							<input type="text" name="stock" value="<?php echo ($this->input->post('stock') ? $this->input->post('stock') : $product['stock']); ?>" class="form-control" id="stock" placeholder="In Stock"/>
						</div>
					</div>
				</div>
				<div class="col-md-12 md-pt-0">
					<div class="form-group">
						<label for="description" class="control-label mb-5">
							<?php echo lang('description'); ?>
						</label>
						<div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-compose-outline"></i></span>
							<textarea name="description" class="form-control" id="description" placeholder="Description"><?php echo ($this->input->post('description') ? $this->input->post('description') : $product['description']); ?></textarea>
						</div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-6 pull-left text-left md-pl-0">
					<div class="checkbox checkbox-success">
						<input name="stocktracking" id="yes" type="checkbox" value="1">
						<label for="yes" class="control-label">
							<?php echo lang('stocktracking')?>
						</label>
					</div>
				</div>
				<div class="btn-group">
					<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
						<?php echo lang('cancel'); ?>
					</button>
					<button type="submit" class="btn btn-success">
						<?php echo lang('save'); ?>
					</button>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>



<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php' ;?>
