<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h3><?php echo lang('products'); ?></h3>
						<span class="panel-subtitle">
						<?php echo lang('productsdescription'); ?>
					</span>
				</div>
				
				<div class="pull-right" id="buttons">
					<button type="button" data-target="#add" data-toggle="modal" data-original-title="Add Product" class="btn btn-primary btn-lable-wrap left-label"> 
						<span class="btn-label">
							<i class="icon-plus txt-dark"></i> 
						</span>
						<span class="btn-text"><?php echo lang('addnewproduct'); ?></span>
					</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="table2" class="table table-striped table-hover">
							<thead>
								<tr>
									<th><?php echo lang('productcode')?></th>
									<th><?php echo lang('productname')?></th>
									<th class="hidden-xs"><?php echo lang('purchaseprice')?></th>
									<th><?php echo lang('salesprice')?></th>
									<th class="text-right"><?php echo lang('productactions')?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($products as $u){ ?>
								<tr class="ciuis-254325232344" onclick="window.location='<?php echo base_url('products/product/'.$u['id'].'')?>'" style="cursor: pointer">
									<td><?php echo $u['code']; ?></td>
									<td><?php echo $u['productname']; ?></td>
									<td class="hidden-xs"><b class="money-area"><?php echo $u['purchase_price'] ?></b></td>
									<td><b class="money-area"><?php echo $u['sale_price']; ?></b></td>
									<td class="text-right">
									<div class="btn-group">
									<a href="<?php echo site_url('products/remove/'.$u['id']); ?>" class="btn btn-danger"><i class="icon-trash"></i></a>
									</div>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="add" tabindex="-1" role="content" class="modal fade">
	<div class="modal-dialog">
		<?php echo form_open('products/add'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
				<h4 class="modal-title"><?php echo lang('addproduct')?></h4>
			</div>

			<div class="modal-body row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="productname" class="control-label mb-5"><?php echo lang('productname'); ?></label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-pricetags"></i></span>
							<input type="text" name="productname" value="<?php echo $this->input->post('productname'); ?>" class="form-control" id="productname" placeholder="<?php echo lang('productname')?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="sale_price" class="control-label mb-5"><?php echo lang('purchaseprice'); ?></label>
						<div class="input-group"><span class="input-group-addon"><?php echo currency;?></span>
							<input type="text" name="purchase_price" value="<?php echo $this->input->post('purchase_price'); ?>" class="form-control" id="purchase_price" placeholder="0,00"/>
						</div>
					</div>
					<div class="form-group">
						<label for="sale_price" class="control-label mb-5"><?php echo lang('salesprice'); ?></label>
						<div class="input-group"><span class="input-group-addon"><?php echo currency;?></i></span>
							<input type="text" name="sale_price" value="<?php echo $this->input->post('sale_price'); ?>" class="form-control" id="sale_price" placeholder="0,00"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="code" class="control-label mb-5"><?php echo lang('productcode'); ?></label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-barcode"></i></span>
							<input type="text" name="code" value="<?php echo $this->input->post('code'); ?>" class="form-control" id="code" placeholder="<?php echo lang('productcode')?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="vat" class="control-label mb-5"><?php echo lang('vat'); ?></label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-ios-medical"></i></span>
							<input type="text" name="vat" value="<?php echo $this->input->post('vat'); ?>" class="form-control" id="vat" placeholder="Vat Rate %"/>
						</div>
					</div>
					<div class="form-group">
						<label for="stock" class="control-label mb-5"><?php echo lang('instock'); ?></label>
						<div class="input-group"><span class="input-group-addon"><i class="ion-cube"></i></span>
							<input type="text" name="stock" value="<?php echo $this->input->post('stock'); ?>" class="form-control" id="stock" placeholder="<?php echo lang('instock')?>"/>
						</div>
					</div>
				</div>
				<div class="col-md-12 md-pt-0">
					<div class="form-group">
						<label for="description" class="control-label mb-5"><?php echo lang('description'); ?></label>
						<div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-compose-outline"></i></span>
							<textarea name="description" class="form-control" id="description" placeholder="Description"><?php echo $this->input->post('description'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<div class="pull-left">
          <div class="checkbox checkbox-success">
            <input name="stocktracking" id="yes" type="checkbox" value="1">
            <label for="yes" class="control-label mb-5"><?php echo lang('stocktracking')?></label>
          </div>
        </div>
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel'); ?></button>
						<button type="submit" class="btn btn-success"><?php echo lang('save'); ?></button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
	

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php' ;?>
