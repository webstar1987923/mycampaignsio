<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<!-- Row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="calendar-wrap mt-40">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>	
	</div>	
</div>	
<!-- /Row -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             <div class="modal-body">
             
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
             </div>
        </div>
    </div>
</div>

<?php
foreach ($fit as $key => $val) {
    $this->db->select('SUM(invoices.total) as total, customers.companyname as company_name');
    $this->db->where('invoices.company_id', $_SESSION['company_id']);
    $this->db->where('duedate', $val['start']);
    $this->db->group_by('invoices.customerid');
    $this->db->join('customers', 'customers.id = invoices.customerid');
    $company = $this->db->get('invoices')->result_array();
    $fit[$key]['companies'] = $company;
}

?>

<style>
.modal-dialog {
    width: 300px;
    margin-top: 200px;
}
.table {
    margin-top: 20px;
    margin-bottom: 0px;
}
</style>
	
	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>

<script>
$(document).ready(function() {
	'use strict';
	
    var drag =  function() {
        $('.calendar-event').each(function() {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 1111999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
    };
    
    var removeEvent =  function() {
		$(document).on('click','.remove-calendar-event',function(e) {
			$(this).closest('.calendar-event').fadeOut();
        return false;
    });
    };
    
    $(".add-event").keypress(function (e) {
        if ((e.which == 13)&&(!$(this).val().length == 0)) {
            $('<div class="btn btn-success calendar-event">' + $(this).val() + '<a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>').insertBefore(".add-event");
            $(this).val('');
        } else if(e.which == 13) {
            alert('Please enter event name');
        }
        drag();
        removeEvent();
    });
    
    
    drag();
    removeEvent();
    
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
	var year = date.getFullYear();
	
    var incomes = <?php echo json_encode($fit)?>;
    for (var i = 0; i < incomes.length; i++) {
        var des = incomes[i]['companies'];
        incomes[i]['description'] = '<table class="table"><tbody>';
        for (var j = 0; j < des.length; j++) {
            incomes[i]['description'] += '<tr class="success"><td>' + des[j]['company_name'] + '</td><td>' + '<?php echo currency; ?>' + des[j]['total'] + '</td></tr>';
        }
        if (des.length != 1) {
            incomes[i]['description'] += '<tr class="success"><td>Total</td><td>' + '<?php echo currency; ?>' + incomes[i]['title'] + '</td></tr>';
        }
        incomes[i]['description'] += '</tbody></table>';
    }
    // console.log(incomes);
   
    var date_last_clicked = null;
    
    $('#calendar').fullCalendar({
       
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            eventLimit: true, // allow "more" link when too many events
        	
        eventMouseover: function (data, event, view) {
			var tooltip = '<div class="tooltiptopicevent tooltip tooltip-inner" style="width:auto;height:auto;position:absolute;z-index:10001;">10:00 AM ' + data.title + '</div>';
			// $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').remove();
        },
        dayClick: function () {
            // tooltip.hide();
        },
        eventResizeStart: function () {
            tooltip.hide()
        },
        eventDragStart: function () {
            tooltip.hide()
        },
        viewDisplay: function () {
            tooltip.hide()
        },
            events: incomes,
        eventClick: function(event) {
            date_last_clicked = $(this);
            $('.modal-body').html(event.description);
            $('#addModal').modal();
        }
		});
});
</script>