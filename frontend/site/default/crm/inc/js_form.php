<!-- Moment JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/moment/min/moment-with-locales.min.js"></script>

<!-- Bootstrap Colorpicker JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

<!-- Switchery JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/switchery/dist/switchery.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/select2/dist/js/select2.full.min.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Bootstrap Tagsinput JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- Multiselect JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/multiselect/js/jquery.multi-select.js"></script>
 
<!-- Bootstrap Switch JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<!-- Bootstrap Datetimepicker JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
