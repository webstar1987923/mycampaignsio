<script src="<?php echo base_url(); ?>assets/doodle/js/autoNumeric.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/jquery.alphanumeric.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/jquery.tools.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/bootstrap-editable.min.js"></script>
<script src='<?php echo base_url('frontend/site/default/js/custom/campaigns-io-script.js'); ?>'></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/init.js"></script>
<!-- Calender JavaScripts -->
<script src="<?php echo base_url(); ?>assets/doodle/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/doodle/js/fullcalendar.min.js"></script>
