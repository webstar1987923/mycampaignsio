
<!-- Bootstrap-table JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap-table/dist/bootstrap-table.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/datatables/media/js/jquery.dataTables.min.js"></script>

<script>
  $(function () {
    "use strict";

    $('#datetimepicker1').datetimepicker({
      format: 'DD.MM.YYYY',
      inline:false,
      sideBySide: true,
      icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
    });
    $('#datetimepicker2').datetimepicker({
      format: 'DD.MM.YYYY',
      inline:false,
      sideBySide: true,
      icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
    });
    $('#datetimepicker4').datetimepicker({
      format: 'DD.MM.YYYY',
      inline:false,
      sideBySide: true,
      icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
    });
    $('#datetimepicker3').datetimepicker({
      format: 'DD.MM.YYYY h:mm A',
      inline:false,
      sideBySide: true,
      icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
    });
  });
  </script>