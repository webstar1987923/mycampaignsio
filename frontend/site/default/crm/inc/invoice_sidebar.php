<?$inv = $invoices['id'] ?>
<?$total = $invoices['total'];?>

	<div class="panel panel-default border-panel card-view">
		<div class="panel-heading">
			<div class="pull-left">
				<h5><?php echo lang('balance')?> : <span class="money-area"><?php $kalan = $total - $tadtu->row()->amount;  echo $kalan ?></span></h5>
			</div>

			<div class="pull-right">
				<?php if($kalan > 0) {echo '';}else echo '<h5 class="text-success">'.lang('paidinv').' <i class="icon-check text-success"></i></h5>'; ?>
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="panel-wrapper">
			<div class="panel-body">
				<div class="tab-container">
					<div class="tab-content ">
						<div id="detail" class="tab-pane cont active">
							<div class="alert alert-success alert-dismissable alert-style-1">
								<i class="icon-bell"></i> 
								<?php
									$today = time();
									$duedate = strtotime( $invoices[ 'duedate' ] ); // or your date as well
									$datecreated = strtotime( $invoices[ 'datecreated' ] );
									$paymentday = $duedate - $datecreated; // Bunun sonucu 14 gün olcak
									$paymentx = $today - $datecreated;
									$paymentdatenet = $paymentday - $paymentx;
									if($invoices['duedate'] == 0000-00-00){
											echo '<span class="badge">No Due Date</span>';
										}else{
											if ( $paymentdatenet < 0 ) {
											echo '<span class="text-danger mdi mdi-timer-off"></span> <span class="text-danger"><b>'.lang('overdue').'</b> </span>';
											echo '<b>', floor( $paymentdatenet / ( 60 * 60 * 24 ) ), '</b> days';;

										} else {
											echo lang('payableafter') . floor( $paymentdatenet / ( 60 * 60 * 24 ) ) . ' '.lang('day').'';

										}
									}
								?>
							</div>

							<div class="alert alert-success alert-dismissable alert-style-1">
								<i class="icon-envelope-open"></i> 
								<?php if($invoices['datesend'] == '0000-00-00 00:00:00'){echo lang('notyetbeensent');}else echo _adate($invoices['datesend']);?>
							</div>

							<div class="alert alert-success alert-dismissable alert-style-1">
								<i class="icon-user"></i> 
								<?php echo $invoices['staffmembername'];?>
							</div>

							<div class="form-group text-center">
									
								<div class="btn-group">
									<a href="<?php echo base_url('invoices/share/'.$invoices['id'].''); ?>" class="btn btn-success"><i class="icon-send"></i> <?php echo lang('send'); ?></a>
									<?php echo form_close(); ?>
									<a target="new" href="<?php echo base_url('invoices/print_/'.$invoices['id'].''); ?>" type="submit" class="btn btn-success" data-original-title="<?php echo lang('print')?>" data-placement="bottom" data-toggle="tooltip"><i class="icon-printer"></i></a>
									<a href="<?php echo base_url('invoices/download/'.$invoices['id'].''); ?>" type="submit" class="btn btn-space btn-success" data-original-title="<?php echo lang('download')?>" data-placement="bottom" data-toggle="tooltip"><i class="icon-arrow-down-circle"></i></a>
								</div>
							</div>
							<div class="form-group text-center">
								<a type="button" <?php if($kalan> 0) {echo '';}else echo 'disabled'; ?> href="#paymentadd" data-toggle="tab" class="btn btn-warning"><?php echo lang('recordpayment'); ?></a>
							</div>
						</div>
						<div id="paymentadd" class="tab-pane cont">
							<?php echo form_open('payments/add/',array("class"=>"form-vertical")); ?>
								<div class="form-group mb-10">
									<label for="" class="control-label mb-5">
										<?php echo lang('datepayment')?>
									</label>
									<div data-min-view="2" data-date-format="dd.mm.yyyy" class="input-group date datetimepicker">
										<span class="input-group-addon"><i class="ti-calendar"></i></span>
										<input required type='input' name="date" value="" placeholder="<?php echo date('d.m.Y');?>" class=" form-control" id=""/>
									</div>
								</div>
								<div class="form-group mb-10">
									<label for="" class="control-label mb-5">
										<?php echo lang('amount')?>
									</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="ti-money"></i></span>
										<input type="text" name="amount" value="" class="input-money-format form-control" placeholder="0.00">
										<input type="hidden" name="invoicetotal" value="<?php echo $total; ?>">
										<input type="hidden" name="customerid" value="<?php echo $invoices['customerid']; ?>">
									</div>
								</div>
								<div class="form-group mb-10">
									<label for="" class="control-label mb-5">
										<?php echo lang('description')?>
									</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="ti-info"></i></span>
										<input type="text" name="not" value="" class=" form-control" id="" placeholder="Description for this payment">
									</div>
								</div>
								<div class="form-group mb-10">
									<label for="" class="control-label mb-5">Account</label>
									<select id="accountid" name="accountid" class="form-control select2">
										<?php
										foreach ( $accounts as $account ) {
											$selected = ( $account[ 'id' ] == $this->input->post( 'accountid' ) ) ? ' selected="selected"' : null;
											echo '<option value="' . $account[ 'id' ] . '" ' . $selected . '>' . $account[ 'name' ] . '</option>';
										}
										?>
									</select>
								</div>
								<input type="hidden" name="invoiceid" value="<?php echo $invoices['id'];?>">
								<input type="hidden" name="staffid" value="<?php echo $this->session->userdata('logged_in_staff_id'); ?>">
								<div class="form-group">
									<div class="pull-right">
										<div class="btn-group">
											<a href="#detail" data-toggle="tab" aria-expanded="true" class="btn btn-warning">
												<?php echo lang('cancel')?>
											</a>
											<button type="submit" class="btn btn-success">
												<?php echo lang('save')?>
											</button>
										</div>
									</div>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default card-view borderten">
		<div class="panel-heading">
			<h5><?php echo lang('payments')?></h5>
			<span class="panel-subtitle"><?php echo lang('paymentsside')?></span>
		</div>
		<div class="panel-body <?php if ($tadtu->row()->amount == 0){echo 'text-center';}?>">
		<?php if ($tadtu->row()->amount == 0){echo '<img width="80%" src="'.base_url('assets/images/payments.png').'">';}?>
		
			<table class="table <?php if ($tadtu->row()->amount == 0){echo 'hide';}?>">
				<thead>
					<tr>
						<th style="width:30%;"><?php echo lang('date')?></th>
						<th class="text-left"><?php echo lang('account')?></th>
						<th class="text-right"><?php echo lang('amount')?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($payments as $payment){?>
					<tr>
						<td>
							<small class="text-muted">
								<?php echo _udate($payment['date'])?>
							</small>
						</td>
						<td class="text-left">
							<small>
								<?php echo $payment['accountname'];?>
							</small>
						</td>
						<td class="text-right">
							<span class="money-area"><?php echo $payment['amount']?></span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
			<br>
		</div>
	</div>