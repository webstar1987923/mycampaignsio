<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/doodle/js/jquery.slimscroll.js"></script>

<!-- Fancy Dropdown JS -->
<script src="<?php echo base_url(); ?>assets/doodle/js/dropdown-bootstrap-extended.js"></script>

<!-- Jasny-bootstrap JS -->
<script src="<?php echo base_url(); ?>assets/lib/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<!--Nestable js -->
<script src="<?php echo base_url(); ?>assets/lib/nestable2/jquery.nestable.js"></script>

<!-- Owl JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Ion.RangeSlider -->
<script src="<?php echo base_url(); ?>assets/lib/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>