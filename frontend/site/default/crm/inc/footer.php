<?php
$crm_assets_path = base_url('assets/crm') . '/';
?>
        </div>
    </div>			
    <script type="text/javascript">var site_url = '<?php echo base_url();?>';</script>
    
    <!-- Bootstrap Core JavaScript -->
    <!-- <script src="<?php echo base_url(); ?>assets/doodle/js/bootstrap.min.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/dropdown-bootstrap-extended.js"></script> 
    <script src="<?php echo base_url(); ?>assets/doodle/js/moment-with-locales.min.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/doodle/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.bootstrap-touchspin.min.js"></script>



    <!-- Data table JavaScript -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.dataTables.min.js"></script>
    <!-- Slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.slimscroll.js"></script>
    <!-- Progressbar Animation JavaScript -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.counterup.min.js"></script>
    <!-- Fancy Dropdown JS -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.multi-select.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/select2.full.min.js"></script>

    
    <script src="<?php echo base_url('frontend/site/default/js/custom/init.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('frontend/site/default/js/custom/dashboard-data.js'); ?>" type="text/javascript"></script>

    <!-- ChartJS JavaScript -->
	<script src="<?php echo base_url(); ?>assets/doodle/js/Chart.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/doodle/js/chartlist.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url(); ?>assets/doodle/js/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/doodle/js/morris.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/doodle/js/jquery.toast.min.js"></script> -->

    <!-- Switchery JavaScript -->
	<script src="<?php echo base_url(); ?>assets/doodle/js/switchery.min.js"></script>

    <!-- Init canvas data -->


    <?php include_once dirname(dirname(__FILE__)) . '/inc/initjs.php';?>

    <script type="text/javascript">
        $(document).ready(function(){
            App.init();
            App.formElements();
        });
    </script>

    <!-- Notifications-->
    <?php if ($this->session->flashdata('login_notification')) {

    	if ($this->session->userdata('admin')) { ?>
    		<script type="text/javascript">
                $.gritter.add({
    				title: 'Vuuv! <?php echo $this->session->userdata('staffname'); ?>',
    				text: '<?php echo $this->session->userdata('admin_notification'); ?>',
    				image: App.conf.assetsPath + '/' +  App.conf.imgPath + '/root_avatar.png',
    				class_name: 'clean img-rounded',
    				time: ''
                });
    		</script>
        <?php } ?>

        <script type="text/javascript">
            $.gritter.add({
                title: '<?php echo lang('hello'); ?> <?php echo $this->session->userdata('staffname'); ?>',
                text: '<?php echo $this->session->flashdata('login_notification'); ?>',
                image: '<?php echo base_url(); ?>uploads/staffavatars/<?php echo $this->session->userdata('staffavatar'); ?>',
                class_name: 'clean img-rounded',
                time: ''
            });
        </script>

        <script type="text/javascript">
            $.gritter.add({
                title: '<?php echo lang('crmwelcome'); ?>',
                text: "<?php echo lang('welcomemessage'); ?>",
                image: '<?php echo base_url(); ?>uploads/staffavatars/<?php echo $this->session->userdata('staffavatar'); ?>',
                time: '',
                class_name: 'img-rounded'
            });
        </script>

        <script>
            var staffname = "<?php echo $message = sprintf(lang('welcome_once_message'), $this->session->userdata('staffname')) ?> ";
            <?php echo 'speak(staffname);'; ?>
            <?php if ($newreminder > 0) {echo 'speak(reminder);';}?>
            <?php if ($openticket > 0) {echo 'speak(oepnticket);';}?>
        </script>

    <?php } ?>

    <script src='<?php echo base_url('frontend/site/default/js/custom/campaigns-io-script.js'); ?>'></script>
</body>
</html>