<?php
	$user_session = $this->session->get_userdata();
	$user_session['email'] = isset($user_session['email']) ? $user_session['email'] : '';
	/* $profile = isset($profile) ? $profile : user_profile_data( $this->db, $user_session['user_id'] );
	$profile->email = $user_session['email'];*/
	$user_fullname =  $user_session['staffname'];
	$user_email = $user_session['email'];
	/*debug($user_session);
	die();*/

	$newnotification = $this->Notifications_Model->newnotification();
	$readnotification = $this->Notifications_Model->readnotification();
	$settings = $this->Settings_Model->get_settings_ciuis();
	$todos = $this->Trivia_Model->get_todos();
	$donetodo = $this->Trivia_Model->get_done_todos();
	$notifications = $this->Notifications_Model->get_all_notifications();

	$current_page = '';

	$user_profile_data = user_profile_data( $this->db, $user_session['user_id'] );
	$user_fullname = format_user_fullname( $user_profile_data->first_name, $user_profile_data->last_name );
	$user_gravatar = gravatar_thumb( $user_profile_data->email, 112);

	if( isset( $_COOKIE['campaigns-io']['collapse-sidebar'] ) ){
        $collapsed_sidebar = 1 === (int) $_COOKIE['campaigns-io']['collapse-sidebar'];
    }
    if( isset( $_COOKIE['campaigns-io']['collapse-author-nav'] ) ){
		// NOTE: Replaced to keep users navigation menu collapsed on pages load.
		// $collapsed_author_nav = 1 === (int) $_COOKIE['campaigns-io']['collapse-author-nav'];

		$collapsed_author_nav = 1;
    }

    $crm_assets_path = base_url('assets/crm') . '/';	
	$assets_url = base_url('assets');
?>
<!DOCTYPE html>

<html lang="<?php echo lang('language_datetime'); ?>">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo $crm_assets_path; ?>img/logo-fav.png">
	<title><?php echo $title; ?></title>

	<link href="<?php echo $assets_url; ?>/doodle/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>


	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/material-design-icons/css/material-design-iconic-font.min.css"/>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/> -->
	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/jquery.gritter/css/jquery.gritter.css"/>
	
	<!-- link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>css/ciuis.css" type="text/css"/-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/material/angular-material.min.css" type="text/css"/> -->
	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>css/animate.css" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $crm_assets_path; ?>lib/bootstrap-slider/css/bootstrap-slider.css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
	
	
	<!-- bootstrap-tagsinput CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
	
	<!-- bootstrap-touchspin CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- multi-select CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/multi-select.css" rel="stylesheet" type="text/css"/>
	
	<!-- Bootstrap Switches CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

	<!-- Bootstrap Colorpicker CSS -->
	<link href="<?php echo $assets_url; ?>/lib/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Bootstrap Datetimepicker CSS -->
	<link href="<?php echo $assets_url; ?>/lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Bootstrap Daterangepicker CSS -->
	<link href="<?php echo $assets_url; ?>/lib/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
	
	
	<link href="<?php echo $assets_url; ?>/doodle/css/morris.css" rel="stylesheet" type="text/css"/>	
	<link href="<?php echo $assets_url; ?>/doodle/css/chartlist.min.css" rel="stylesheet" type="text/css"/>	
	<link href="<?php echo $assets_url; ?>/doodle/css/switchery.min.css" rel="stylesheet" type="text/css"/>	
	<!-- <link href="<?php echo $assets_url; ?>/doodle/css/jquery.toast.min.css" rel="stylesheet" type="text/css">		 -->
	<link href="<?php echo $assets_url; ?>/doodle/css/select2.min.css" rel="stylesheet" type="text/css">	
	<link href="<?php echo $assets_url; ?>/doodle/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">	

	<!-- Data table CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

	<link href="<?php echo base_url(); ?>assets/crm/css/bootstrap-editable.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url('assets/crm/lib/jquery-ui/jquery-ui.css')?>">

	<!-- Jasny-bootstrap CSS -->
	<link href="<?php echo $assets_url; ?>/lib/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

	<!-- Calendar CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/fullcalendar.css" rel="stylesheet" type="text/css"/>

	<!-- Custom CSS -->
	<link href="<?php echo $assets_url; ?>/doodle/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $assets_url; ?>/doodle/css/custom-style.css" rel="stylesheet" type="text/css">

	<script src="<?php echo $assets_url; ?>/lib/jquery/dist/jquery.js"></script>    
</head>
<body>
	<div class="wrapper theme-4-active pimary-color-red box-layout">
		<!-- Top Menu Items -->       
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="<?php echo base_url(); ?>panel">
							<img class="brand-text" src="<?php echo base_url('frontend/site/default/images/campaigns-io-logo.png'); ?>" alt="brand" style="height:30px;"/>							
						</a>
					</div>
				</div>                    
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>


				<a id="open_right_sidebar" class="mobile-only-view toggle_right_menu" href="javascript:void(0);"><i class="icon-layers"></i></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>                    
				
				<div class=" tab-struct custom-tab-1 top-nav-search collapse pull-left" style="padding-top:0px;">
					<ul class="nav nav-tabs" id="myTabs_7">
						<li class="<?php echo $page=='dashboard'?'active':''; ?>" role="presentation"><a href="<?php echo base_url('panel'); ?>">DASHBOARD</a></li>									
						<li class="<?php echo $page=='customers'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('customers'); ?>" >CUSTOMERS</a></li>
						<li class="<?php echo $page=='leads'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('leads'); ?>" >LEADS</a></li>
						<li class="<?php echo $page=='accounts'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('accounts'); ?>" >ACCOUNTS</a></li>
						<li class="<?php echo $page=='invoices'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('invoices'); ?>" >INVOICES</a></li>
						<li class="<?php echo $page=='proposals'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('proposals'); ?>" >PROPOSALS</a></li>
						<li class="<?php echo $page=='expenses'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('expenses'); ?>" >EXPENSES</a></li>
						<li class="<?php echo $page=='tickets'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('tickets'); ?>" >TICKETS</a></li>

						<div class="dropdown pull-right mt-10">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> More <span class="caret"></span></button>
							<ul role="menu" class="dropdown-menu">
								<li class="<?php echo $page=='staff'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('staff'); ?>" >STAFF</a></li>
								<li class="<?php echo $page=='products'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('products'); ?>" >PRODUCTS</a></li>
								<li class="<?php echo $page=='calendar'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('calendar'); ?>" >CALENDAR</a></li>
								<!-- <li class="<?php echo $page=='reports'?'active':''; ?>" role="presentation"><a  href="<?php echo base_url('reports'); ?>" >REPORTS</a></li>   -->
							</ul>
						</div>
						
					</ul>
				</div>
			</div>  
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
					<!-- <li>
						<a id="open_right_sidebar" href="#"><i class="zmdi zmdi-settings top-nav-icon"></i></a>
					</li> -->
					<li class="dropdown app-drp">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps top-nav-icon"></i></a>
						<ul class="dropdown-menu app-dropdown" data-dropdown-in="slideInRight" data-dropdown-out="flipOutX">
							<li>
								<div class="app-nicescroll-bar">
									<ul class="app-icon-wrap pa-10">
										<li>
											<a href="<?php echo base_url( 'reports' ); ?>" class="connection-item">
											<i class="txt-info"><?php echo currency;?></i>
											<span class="block">Financial</span>
											</a>
										</li>
										<li>
											<a href="inbox.html" class="connection-item">
											<i class="zmdi zmdi-trending-up txt-success"></i>
											<span class="block">Traffic</span>
											</a>
										</li>
										<li>
											<a href="calendar.html" class="connection-item">
											<i class="zmdi zmdi-phone txt-primary"></i>
											<span class="block">Telephony</span>
											</a>
										</li>
										<li>
											<a href="vector-map.html" class="connection-item">
											<i class="zmdi zmdi-balance-wallet txt-danger"></i>
											<span class="block">Money</span>
											</a>
										</li>
										<li>
											<a href="chats.html" class="connection-item">
											<i class="zmdi zmdi-comment-outline txt-warning"></i>
											<span class="block">Leads</span>
											</a>
										</li>
										<li>
											<a href="contact-card.html" class="connection-item">
											<i class="zmdi zmdi-graphic-eq"></i>
											<span class="block">Forecasts</span>
											</a>
										</li>
									</ul>
								</div>	
							</li>
						</ul>
					</li>
					<li class="dropdown alert-drp">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-notifications top-nav-icon"></i><span class="top-nav-icon-badge">5</span></a>
						<ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
							<li>
								<div class="notification-box-head-wrap">
									<span class="notification-box-head pull-left inline-block">notifications</span>
									<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>
									<div class="clearfix"></div>
									<hr class="light-grey-hr ma-0"/>
								</div>
							</li>
							<li>
								<div class="streamline message-nicescroll-bar">
									<div class="sl-item">
										<a href="javascript:void(0)">
											<div class="icon bg-green">
												<i class="zmdi zmdi-flag"></i>
											</div>
											<div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications">
												New subscription created</span>
												<span class="inline-block font-11  pull-right notifications-time">2pm</span>
												<div class="clearfix"></div>
												<p class="truncate">Your customer subscribed for the basic plan. The customer will pay $25 per month.</p>
											</div>
										</a>	
									</div>
									<hr class="light-grey-hr ma-0"/>
									<div class="sl-item">
										<a href="javascript:void(0)">
											<div class="icon bg-yellow">
												<i class="zmdi zmdi-trending-down"></i>
											</div>
											<div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications txt-warning">Server #2 not responding</span>
												<span class="inline-block font-11 pull-right notifications-time">1pm</span>
												<div class="clearfix"></div>
												<p class="truncate">Some technical error occurred needs to be resolved.</p>
											</div>
										</a>	
									</div>
									<hr class="light-grey-hr ma-0"/>
									<div class="sl-item">
										<a href="javascript:void(0)">
											<div class="icon bg-blue">
												<i class="zmdi zmdi-email"></i>
											</div>
											<div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications">2 new messages</span>
												<span class="inline-block font-11  pull-right notifications-time">4pm</span>
												<div class="clearfix"></div>
												<p class="truncate"> The last payment for your G Suite Basic subscription failed.</p>
											</div>
										</a>	
									</div>
									<hr class="light-grey-hr ma-0"/>
									<div class="sl-item">
										<a href="javascript:void(0)">
											<div class="sl-avatar">
												<img class="img-responsive" src="<?php echo $assets_url; ?>/doodle/images/avatar.jpg" alt="avatar"/>
											</div>
											<div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications">Sandy Doe</span>
												<span class="inline-block font-11  pull-right notifications-time">1pm</span>
												<div class="clearfix"></div>
												<p class="truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
											</div>
										</a>	
									</div>
									<hr class="light-grey-hr ma-0"/>
									<div class="sl-item">
										<a href="javascript:void(0)">
											<div class="icon bg-red">
												<i class="zmdi zmdi-storage"></i>
											</div>
											<div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications txt-danger">99% server space occupied.</span>
												<span class="inline-block font-11  pull-right notifications-time">1pm</span>
												<div class="clearfix"></div>
												<p class="truncate">consectetur, adipisci velit.</p>
											</div>
										</a>	
									</div>
								</div>
							</li>
							<li>
								<div class="notification-box-bottom-wrap">
									<hr class="light-grey-hr ma-0"/>
									<a class="block text-center read-all" href="javascript:void(0)"> read all </a>
									<div class="clearfix"></div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown auth-drp">
						<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="<?php echo $user_gravatar; ?>" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
							<?php user_navigation( $current_page, $user['parent_id'] ); ?>    
						</ul>
					</li>
				</ul>                    
			</div>                
		</nav>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar pt-10">                    
				<?php main_navigation( $current_page, $domain['id'] ); ?>
			</ul>
		</div>

		<!-- Right Sidebar Menu -->
		<div class="fixed-sidebar-right">
			<ul class="right-sidebar">
				<li>
					<div  class="tab-struct custom-tab-1">
						<div class="todo-box-wrap">
							<div class="set-height-wrap">
								<!-- Todo-List -->
								<ul class="todo-list nicescroll-bar">
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='dashboard'?'active':''; ?>"><a href="<?php echo base_url('panel'); ?>">DASHBOARD</a></li>		
									<li><hr class="light-grey-hr"/>							
									<li class="todo-item <?php echo $page=='customers'?'active':''; ?>"><a  href="<?php echo base_url('customers'); ?>" >CUSTOMERS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='leads'?'active':''; ?>"><a  href="<?php echo base_url('leads'); ?>" >LEADS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='accounts'?'active':''; ?>"><a  href="<?php echo base_url('accounts'); ?>" >ACCOUNTS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='invoices'?'active':''; ?>"><a  href="<?php echo base_url('invoices'); ?>" >INVOICES</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='proposals'?'active':''; ?>"><a  href="<?php echo base_url('proposals'); ?>" >PROPOSALS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='expenses'?'active':''; ?>"><a  href="<?php echo base_url('expenses'); ?>" >EXPENSES</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='tickets'?'active':''; ?>"><a  href="<?php echo base_url('tickets'); ?>" >TICKETS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='staff'?'active':''; ?>"><a  href="<?php echo base_url('staff'); ?>" >STAFF</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='products'?'active':''; ?>"><a  href="<?php echo base_url('products'); ?>" >PRODUCTS</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='calendar'?'active':''; ?>"><a  href="<?php echo base_url('calendar'); ?>" >CALENDAR</a></li>
									<li><hr class="light-grey-hr"/>
									<li class="todo-item <?php echo $page=='reports'?'active':''; ?>"><a  href="<?php echo base_url('reports'); ?>" >REPORTS</a></li> 
									<li><hr class="light-grey-hr"/></li>
								</ul>
							</div>
						</div>
					</div>
				</li>
			</ul>
			
		</div>
		<!-- /Right Sidebar Menu -->

		<div class="page-wrapper">
			<div class="container-fluid pt-25">
