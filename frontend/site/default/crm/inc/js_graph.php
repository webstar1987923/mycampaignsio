<!-- Sparkline JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/morris.js/morris.min.js"></script>
  
<!-- Chartist JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/chartist/dist/chartist.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/chart.js/Chart.min.js"></script>

<!-- Easy Pie Chart JavaScript -->
<script src="<?php echo base_url(); ?>assets/lib/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>