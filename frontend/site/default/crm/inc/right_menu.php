<div class="fixed-sidebar-right">
  <ul class="right-sidebar">
    <li>
      <div  class="tab-struct custom-tab-1">
        <div class="todo-box-wrap">
          <div class="set-height-wrap">
            <!-- Todo-List -->
            <ul class="todo-list nicescroll-bar">
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='dashboard'?'active':''; ?>"><a href="<?php echo base_url('panel'); ?>">DASHBOARD</a></li>   
              <li><hr class="light-grey-hr"/>             
              <li class="todo-item <?php echo $page=='customers'?'active':''; ?>"><a  href="<?php echo base_url('customers'); ?>" >CUSTOMERS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='leads'?'active':''; ?>"><a  href="<?php echo base_url('leads'); ?>" >LEADS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='accounts'?'active':''; ?>"><a  href="<?php echo base_url('accounts'); ?>" >ACCOUNTS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='invoices'?'active':''; ?>"><a  href="<?php echo base_url('invoices'); ?>" >INVOICES</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='proposals'?'active':''; ?>"><a  href="<?php echo base_url('proposals'); ?>" >PROPOSALS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='expenses'?'active':''; ?>"><a  href="<?php echo base_url('expenses'); ?>" >EXPENSES</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='tickets'?'active':''; ?>"><a  href="<?php echo base_url('tickets'); ?>" >TICKETS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='staff'?'active':''; ?>"><a  href="<?php echo base_url('staff'); ?>" >STAFF</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='products'?'active':''; ?>"><a  href="<?php echo base_url('products'); ?>" >PRODUCTS</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='calendar'?'active':''; ?>"><a  href="<?php echo base_url('calendar'); ?>" >CALENDAR</a></li>
              <li><hr class="light-grey-hr"/>
              <li class="todo-item <?php echo $page=='reports'?'active':''; ?>"><a  href="<?php echo base_url('reports'); ?>" >REPORTS</a></li> 
              <li><hr class="light-grey-hr"/></li>
            </ul>
          </div>
        </div>
      </div>
    </li>
  </ul>
</div>