<?php $page = "customers"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php';?>
<?php
	$this->db->from('customers');
	$this->db->where('company_id', $_SESSION['company_id']);
	$company = $this->db->get()->result_array();
?>
<div class="row">
	<div class="col-xs-6 col-md-6 col-lg-2">
		<div class="panel panel-default card-view bg-blue pt-0">
      <div class="panel-wrapper collapse in">
        <div class="panel-body pa-0">
          <div class="sm-data-box bg-white">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 text-left pl-0 pr-0 data-wrap-left">
                  <span class="txt-dark block counter">£0</span>
                  <span class="block txt-dark font-13">0 Estimate</span>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-2">
		<div class="panel panel-default card-view bg-pink pt-0">
      <div class="panel-wrapper collapse in">
        <div class="panel-body pa-0">
          <div class="sm-data-box bg-white">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 text-left pl-0 pr-0 data-wrap-left">
                  <span class="txt-dark block counter">£0</span>
                  <span class="block txt-dark font-13">0 Unbilled Activity</span>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-2">
		<div class="panel panel-default card-view bg-red pt-0">
      <div class="panel-wrapper collapse in">
        <div class="panel-body pa-0">
          <div class="sm-data-box bg-white">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 text-left pl-0 pr-0 data-wrap-left">
                  <span class="txt-dark block counter"><?php echo currency;?><?php switch($settings['unitseparator']){case ',': echo number_format($vgf, 2, ',', '.');break;case '.': echo number_format($vgf, 2, '.', ',');break;}?></span>
                  <span class="block txt-dark font-13"><?php echo $vdf;?> Overdue</span>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	<div class="col-xs-6 col-md-6 col-lg-2">
		<div class="panel panel-default card-view bg-yellow pt-0">
      <div class="panel-wrapper collapse in">
        <div class="panel-body pa-0">
          <div class="sm-data-box bg-white">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 text-left pl-0 pr-0 data-wrap-left">
                  <span class="txt-dark block counter"><?php echo currency;?><?php switch($settings['unitseparator']){case ',': echo number_format($oft, 2, ',', '.');break;case '.': echo number_format($oft, 2, '.', ',');break;}?></span>
                  <span class="block txt-dark font-13"><?php echo $tef;?> Open Invoices</span>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	<div class="col-xs-12 col-md-4 col-lg-4">
		<div class="panel panel-default card-view bg-green pt-0">
      <div class="panel-wrapper collapse in">
        <div class="panel-body pa-0">
          <div class="sm-data-box bg-white">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 text-left pl-0 pr-0 data-wrap-left">
                  <span class="txt-dark block counter"><?php echo currency;?><?php switch($settings['unitseparator']){case ',': echo number_format($olt, 2, ',', '.');break;case '.': echo number_format($olt, 2, '.', ',');break;}?></span>
                  <span class="block txt-dark font-13"><?php echo $oln;?> Paid Last 30 Days</span>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 col-lg-8">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">INCOME</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div id="morris_line_chart" class="morris-chart" style="height: 78px"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-lg-4">
		<div class="panel panel-default card-view">
			<div class="panel-heading pb-40">
				<button data-target="#addcustomer" data-toggle="modal" type="button" class="pull-left btn btn-success full-width">Add <?php echo lang('newcustomer'); ?></button>
			</div>
			<div class="panel-body mt-40">
				<div class="searchcustomer-container">
					<div class="searchcustomer-box">
						<input name="q" value="" x-webkit-speech='x-webkit-speech' class="form-control" id="searchcustomer" type="text" placeholder="<?php echo lang('searchcustomer'); ?>"/>
						<ul class="select2-results__options" id="results"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<!-- <div class="panel-heading">
				<div class="pull-left">
					<button data-target="#addcustomer" data-toggle="modal" style="margin-right:10px;" type="button" class="pull-left btn btn-success">Add <?php echo lang('newcustomer'); ?></button>
				</div>
				<div class="pull-right">
					<div class="searchcustomer-container">
						<div class="searchcustomer-box">
							<input name="q" value="" x-webkit-speech='x-webkit-speech' class="form-control" id="searchcustomer" type="text" placeholder="<?php echo lang('searchcustomer'); ?>"/>
							<ul class="select2-results__options" id="results"></ul>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div> -->
			<div class="panel-body collapse in">
				<div class="table-wrap mt-0">
					<div class="table-responsive">
						<table class="table mb-0" id="table2">
							<thead>
							  <tr>
									<th>Company</th>
									<th>Name</th>
									<th>Balance</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Status</th>
							  </tr>
							</thead>
							<tbody>
								<?php foreach ($company as $f) { ?>
							  <tr>
									<td><a style="color: #383838;font-weight: 700;" href="<?php echo site_url('customers/customer/' . $f['id']); ?>"><?php echo $f['companyname'];?></a></td>
									<td><?php echo $f['namesurname'];?></td>							
									<td>
										<?php
											$this->db->select_sum('total');
											$this->db->from('invoices');
											$this->db->where('company_id', $_SESSION['company_id'])->where('customerid', $f['id'])->where('statusid = 3 AND CURDATE() <= duedate');
											$balance = $this->db->get()->row()->total;
											if($balance != null) {echo currency;echo $balance;}
										?>
									</td>
									<td><?php echo $f['companyphone'];?></td>
									<td><?php echo $f['companyemail'];?></td>
									<td><?php echo $f['status'];?></td>
							  </tr>
							  <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<div id="addcustomer"  role="dialog" class="modal fade bs-example-modal-lg in">
		<?php echo form_open('customers/add', array("class" => "form-vertical")); ?>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="pull-left">
						<h4 class="modal-title">
							<?php echo lang('newcustomertitle'); ?>
						</h4>
						<span class="panel-subheading">
						<?php echo lang('newcustomerdescription'); ?>
					</span>
					</div>
					<div class="pull-right">
						<button type="button" data-dismiss="modal" aria-hidden="true" class="ml-20 close modal-close"><span class="mdi mdi-close"></span></button>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-xl btn-warning btn-reverse active">
							<input name="type" type="radio" checked value="0"><?php echo lang('company') ?>
							</label>
							<label class="btn btn-xl btn-warning btn-reverse">
							<input name="type" type="radio" value="1"><?php echo lang('individual') ?>
							</label>
						</div>
					</div>
					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div id="" class="form-group company">
								<label class="control-label mb-5"><?php echo lang('updatecustomercompanyname') ?></label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-user"></i>
									</span>
									<input type="text" name="companyname" value="<?php echo $this->input->post('companyname'); ?>" class="form-control" id="taxnumber"/>
								</div>
							</div>
							<div style="display: none" id="" class="form-group individual">
								<label  class="control-label mb-5"><?php echo lang('updatecustomerindividualname') ?></label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-user"></i>
									</span>
									<input type="text" name="namesurname" value="<?php echo $this->input->post('namesurname'); ?>" class="form-control" id="namesurname"/ placeholder="Customer Name Surname">
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="taxoffice" class="control-label mb-5">
									<?php echo lang('taxofficeedit'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-briefcase"></i>
									</span>
									<input type="text" name="taxoffice" value="<?php echo $this->input->post('taxoffice'); ?>" class="form-control" id="taxoffice"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div id="" class="form-group company">
								<label for="taxnumber" class="control-label mb-5">
									<?php echo lang('taxnumberedit'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-tag"></i>
									</span>
									<input type="number" name="taxnumber" value="<?php echo $this->input->post('taxnumber'); ?>" class="form-control" id="taxnumber">
								</div>
							</div>
							<div style="display: none" id="" class="form-group individual">
								<label for="socialsecuritynumber" class="control-label mb-5">
									<?php echo lang('socialsecuritynumberedit'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="pe-7s-id"></i>
									</span>
									<input type="text" name="socialsecuritynumber" value="<?php echo $this->input->post('socialsecuritynumber'); ?>" class="form-control" id="socialsecuritynumber"/ placeholder="<?php echo lang('socialsecuritynumberedit'); ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="companyexecutive" class="control-label mb-5">
									<?php echo lang('companyexecutiveupdate'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-info"></i>
									</span>
									<input type="text" name="companyexecutive" value="<?php echo $this->input->post('companyexecutive'); ?>" class="form-control" id="companyexecutive"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="zipcode" class="control-label mb-5">
									<?php echo lang('zipcode'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-home"></i>
									</span>
									<input type="text" name="zipcode" value="<?php echo $this->input->post('zipcode'); ?>" class="form-control" id="zipcode"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="companyphone" class="control-label mb-5">
									<?php echo lang('customerphoneupdate'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-phone"></i>
									</span>
									<input type="text" name="companyphone" value="<?php echo $this->input->post('companyphone'); ?>" class="form-control" id="companyphone"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="companyfax" class="control-label mb-5">
									<?php echo lang('customerfaxupdate'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-printer"></i>
									</span>
									<input type="text" name="companyfax" value="<?php echo $this->input->post('companyfax'); ?>" class="form-control" id="companyfax"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="companyemail" class="control-label mb-5">
									<?php echo lang('emailedit'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-envelope"></i>
									</span>
									<input required type="text" name="companyemail" value="<?php echo $this->input->post('companyemail'); ?>" class="form-control" id="companyemail"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="companyweb" class="control-label mb-5">
									<?php echo lang('customerwebupdate'); ?>
								</label>
								<div class="input-group xs-pt-10">
									<span class="input-group-addon">
										<i class="icon-globe"></i>
									</span>
									<input type="text" name="companyweb" value="<?php echo $this->input->post('companyweb'); ?>" class="form-control" id="companyweb"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="companyaddress" class="control-label mb-5">
									<?php echo lang('customeraddressupdate'); ?>
								</label>
								<textarea name="companyaddress" class="form-control"><?php echo $this->input->post('companyaddress'); ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-lg-3">
							<div class="form-group">
								<label for="companyemail" class="control-label mb-5">
									<?php echo lang('customercountryupdate'); ?>
								</label>
								<select required name="countryid" class="form-control select2">
									<option value=""><?php echo lang('selectcountry'); ?></option>
									<?php
										foreach ($countries as $country) {
							        $selected = ($country['id'] == $this->input->post('countryid')) ? ' selected="selected"' : null;
							        echo '<option value="' . $country['id'] . '" ' . $selected . '>' . $country['shortname'] . '</option>';
								    }
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
								<label for="customerstate" class="control-label mb-5">
									<?php echo lang('customerstateupdate'); ?>
								</label>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icon-map"></i>
									</span>
									<input type="text" name="customerstate" value="<?php echo $this->input->post('customerstate'); ?>" class="form-control" id="customerstate"/>
								</div>
							</div></div>
						<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
								<label for="customercity" class="control-label mb-5">
									<?php echo lang('customercityupdate'); ?>
								</label>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icon-map"></i>
									</span>
									<input type="text" name="customercity" value="<?php echo $this->input->post('customercity'); ?>" class="form-control" id="customercity"/>
								</div>
							</div></div>
					<div class="col-xs-12 col-md-6 col-lg-3">
						<div class="form-group">
								<label for="customertown" class="control-label mb-5">
									<?php echo lang('customertownupdate'); ?>
								</label>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icon-map"></i>
									</span>
									<input type="text" name="customertown" value="<?php echo $this->input->post('customertown'); ?>" class="form-control" id="customertown"/>
								</div>
							</div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-warning modal-close">
						<?php echo lang('cancel'); ?>
					</button>
					<button type="submit" class="btn btn-success modal-close">
						<?php echo lang('add'); ?>
					</button>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>

<?php
$currentyear = date('Y');
for ($i=1; $i <= 12; $i++) { 
	$this->db->select_sum('total');
	$this->db->from('invoices');
	$this->db->where('company_id', $_SESSION['company_id'])->where('MONTH(duedate)', $i)->where('YEAR(duedate)', $currentyear);
	$amount = $this->db->get()->row()->total;
	if ($amount > 0) {
		$income[$i] = $amount;
	} else {
		$income[$i] = '0.00';
	}
}
?>

	<?php //include_once dirname(dirname(__FILE__)) . '/inc/sidebar.php';?>

<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/js_graph.php';?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_invoice.php';?>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('form').each(function() {  // attach to all form elements on page
        $(this).validate({       // initialize plugin on each form
            // global options for plugin
        });
    });

});
$(document).ready(function () {
"use strict";
$('input[name=type]').change(function () {
	if (!$(this).is(':checked')) {
			return;
		}
	if ($(this).val() === '0') {
		$('.company').show();
		$('.individual').hide();
	} else if ($(this).val() === '1') {
		$('.company').hide();
		$('.individual').show();
	}
});
});
</script>
<script>
	( function () {
		var displayResults, findAll, maxResults, names, resultsOutput, searchcustomerInput;
		names = [
			<?php foreach ($customers as $f) {
    ?>
			"<a href='<?php echo base_url('customers/customer/' . $f['id'] . ''); ?>'><?php if ($f['type'] == 0) {
        echo $f['companyname'];
			    } else {
			        echo $f['namesurname'];
			    } ?></a>",
						<?php
			}?>
			""
		];
		findAll = ( function ( _this ) {
			return function ( wordList, collection ) {
				return collection.filter( function ( word ) {
					word = word.toLowerCase();
					return wordList.some( function ( name ) {
						return ~word.indexOf( name );
					} );
				} );
			};
		} )( this );
		displayResults = function ( resultsEl, wordList ) {
			return resultsEl.innerHTML = ( wordList.map( function ( name ) {
				return '<li>' + name + '</li>';
			} ) ).join( '' );
		};
		searchcustomerInput = document.getElementById( 'searchcustomer' );
		resultsOutput = document.getElementById( 'results' );
		maxResults = 20;
		searchcustomerInput.addEventListener( 'keyup', ( function ( _this ) {
			return function ( e ) {
				var suggested, value;
				value = searchcustomerInput.value.toLowerCase().split( ' ' );
				suggested = ( value[ 0 ].length ? findAll( value, names ) : [] );
				return displayResults( resultsOutput, suggested );
			};
		} )( this ) );
	} ).call( this );
	function startDictation() {
		if ( window.hasOwnProperty( 'webkitSpeechRecognition' ) ) {
			var recognition = new webkitSpeechRecognition();
			recognition.continuous = false;
			recognition.interimResults = false;
			recognition.lang = "<?php echo lang('language_datetime') ?>";
			recognition.start();
			recognition.onresult = function ( e ) {
				document.getElementById( 'searchcustomer' ).value = e.results[ 0 ][ 0 ].transcript;
				recognition.stop();
				$('.searchcustomer-input').value = e.results[ 0 ][ 0 ].transcript;
				$('.searchcustomer-input').focus();
				$('.searchcustomer-input').keyup();

			};
			recognition.onerror = function ( e ) {
				recognition.stop();
			}
		}
	}
</script>

<script>
			if($('#morris_line_chart').length > 0)
			var data = [{
				period: 'Jan',
				Income: '<?php echo $income[1];?>',
			}, 
			{
				period: 'Feb',
				Income: '<?php echo $income[2];?>',
			},
			{
				period: 'March',
				Income: '<?php echo $income[3];?>',
			},
			{
				period: 'April',
				Income: '<?php echo $income[4];?>',
			},
			{
				period: 'May',
				Income: '<?php echo $income[5];?>',
			},
			{
				period: 'June',
				Income: '<?php echo $income[6];?>',
			},
			{
				period: 'July',
				Income: '<?php echo $income[7];?>',
			},
			{
				period: 'Aug',
				Income: '<?php echo $income[8];?>',
			},
			{
				period: 'Sep',
				Income: '<?php echo $income[9];?>',
			},
			{
				period: 'Oct',
				Income: '<?php echo $income[10];?>',
			},
			{
				period: 'Nov',
				Income: '<?php echo $income[11];?>',
			},
			{
				period: 'Dec',
				Income: '<?php echo $income[12];?>',
			}
			];
			console.log(data);
			Morris.Line({
				element: 'morris_line_chart',
				data: data ,
				xkey: 'period',
				labels: ['Income'],
				axes:"x",
				ykeys: ['Income'],
				pointSize: 1,
				grid: false,
				lineWidth:2,
				pointStrokeColors:['#177ec1'],
				behaveLikeLine: true,
				gridLineColor: '#878787',
				hideHover: 'auto',
				lineColors: ['#177ec1'],
				resize: true,
				redraw: true,
				smooth: false,
				gridTextColor:'#878787',
				gridTextFamily:"Roboto",
				parseTime: false,
				gridTextFamily:"Roboto"
			});
</script>