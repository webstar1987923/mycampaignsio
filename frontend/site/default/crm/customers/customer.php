<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>
  
<input type="hidden" id='billing_address' value='<?php echo $customers['companyaddress'].', '.$customers['customercity'].', '.$customers['customerstate'].', '.$customers['customercountry'];?>'>
    <!-- Row -->
    <div class="row">
      <div class="col-xs-12 col-md-5 col-lg-5">
        <div class="panel panel-default card-view  pa-0">
          <div class="panel-wrapper collapse in">
            <div class="panel-body  pa-0">
              <div class="profile-box">
                <div class="profile-cover-pic">
                  <div class="profile-image-overlay">
                    <div id="map_canvas" style="height:200px;"></div>
                  </div>
                </div>
                
                  <div class="col-xs-12 col-lg-6 pa-15">
                    <div class="profile-info"> 
                      <h5 class="block mb-5 weight-500 capitalize-font txt-danger">
                        <?php
                          if ($customers['type']==0) {
                              echo $customers['companyname'];
                          } else {
                              echo $customers['namesurname'];
                          } 
                        ?>
                      </h5>
                      <h6 class="block capitalize-font pb-20"><?php echo $customers['customercity']; ?></h6>
                    </div>
                  </div>
                  <div class="col-xs-12 col-lg-6 pa-15">
                    <h4 class="mb-15"><?php echo lang('riskstatus');?><small class="block clear pl-0"><?php echo lang('customerrisksubtext');?></small></h4>
                    
                    <?php
                      $this->db->select('risk');
                      $this->db->from('customers');
                      $this->db->where('(id = '.$customers['id'].') ');
                      $riskstatus = $this->db->get();

                      if ($riskstatus->row()->risk<1) {
                            echo '<div class="stat"><span><i class="text-success mdi mdi-shield-check"></i> '.lang('norisk').'</span></div>' ;
                      } else {
                          if ($riskstatus->row()->risk>50) {
                                echo '<div class="stat"><span>%'.$riskstatus->row()->risk.'</span></div><div class="progress"><div style="width: '.$riskstatus->row()->risk.'%" class="progress-bar progress-bar-danger"></div></div>' ;
                          } else {
                                echo '<div class="stat"><span>%'.$riskstatus->row()->risk.'</span></div><div class="progress"><div style="width: '.$riskstatus->row()->risk.'%" class="progress-bar progress-bar-primary"></div></div>' ;
                          }
                      }
                    ?>
                  </div>
                <div class="social-info">
                  <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6">
                      <div class="panel card-view">
                        <div class="panel-wrapper collapse in">
                          <div class="panel-body row">
                            <div class="user-others-details pl-15 pr-15">
                              <div class="mb-15">
                                <i class="icon-envelope-open inline-block mr-10"></i>
                                <span class="inline-block txt-dark"><?php echo $customers['companyemail']; ?></span>
                              </div>
                              <div class="mb-15">
                                <i class="icon-globe inline-block mr-10"></i>
                                <span class="inline-block txt-dark"><?php echo $customers['companyweb']; ?></span>
                              </div>
                              <div class="mb-0">
                                <i class="icon-location-pin inline-block mr-10"></i>
                                <span class="inline-block txt-dark">Billing Addres</span>
                              </div>
                              <div class="md-15">
                                <span class="inline-block txt-grey ml-25">
                                  <?php echo $customers['companyaddress']; ?><br>
                                  <?php echo $customers['customercity']; ?><br>
                                  <?php echo $customers['customerstate']; ?><br>
                                  <?php echo $customers['customercountry']; ?>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6">
                      <div class="panel card-view">
                        <div class="panel-wrapper collapse in">
                          <div class="panel-body row">
                            <div class="user-others-details pl-15 pr-15">
                              <div class="mb-15">
                                <i class="icon-people inline-block mr-10"></i>
                                <span class="inline-block txt-dark">Customer</span>
                              </div>
                              <div class="mb-15">
                                <i class="icon-phone inline-block mr-10"></i>
                                <span class="inline-block txt-dark"><?php echo $customers['companyphone']; ?></span>
                              </div>
                              <div class="mb-15">
                                <i class="icon-doc inline-block mr-10"></i>
                                <span class="inline-block txt-dark"><?php echo $customers['companyfax']; ?></span>
                              </div>
                              <div class="mb-0">
                                <i class="icon-location-pin inline-block mr-10"></i>
                                <span class="inline-block txt-dark">Shipping Addres</span>
                              </div>
                              <div class="md-15">
                                <span class="inline-block txt-grey ml-25">
                                  <?php echo $customers['companyaddress']; ?><br>
                                  <?php echo $customers['customercity']; ?><br>
                                  <?php echo $customers['customerstate']; ?><br>
                                  <?php echo $customers['customercountry']; ?>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12 col-md-8 col-lg-8 mt-15">
                      <button class="btn btn-sm btn-success btn-outline btn-anim" data-toggle="modal" data-target="#contactadd" ><i class="icon mdi mdi-account-add"></i><span class="btn-text"><?php echo lang('addcontact') ?></span></button>
                      <button type="button" data-target="#edit" data-toggle="modal" data-placement="left" title="" class="btn btn-sm btn-warning btn-outline btn-anim" data-original-title="<?php echo lang('edit')?>"><i class="icon mdi mdi-edit"> </i><span class="btn-text"><?php echo lang('editcustomer') ?></span></button>
                      <button type="button" data-target="#remove" data-toggle="modal" data-placement="left" title="" class="btn btn-sm btn-danger btn-outline btn-anim" data-original-title="<?php echo lang('delete')?>"><i class="icon mdi mdi-delete"></i><span class="btn-text">Delete</span></button>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4 mt-15">
                      <button class="btn btn-sm btn-warning btn-block btn-anim" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>
                    </div>
                  </div>
                  
                  <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          <h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
                        </div>
                        <div class="modal-body">
                          <!-- Row -->
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="">
                                <div class="panel-wrapper collapse in">
                                  <div class="panel-body pa-0">
                                    <div class="col-sm-12 col-xs-12">
                                      <div class="form-wrap">
                                        <form action="#">
                                          <div class="form-body overflow-hide">
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputuname_1">Name</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                <input type="text" class="form-control" id="exampleInputuname_1" placeholder="willard bryant">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputEmail_1">Email address</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                <input type="email" class="form-control" id="exampleInputEmail_1" placeholder="<?php echo $customers['companyemail']; ?>">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputWeb_1">Website</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-globe"></i></div>
                                                <input type="email" class="form-control" id="exampleInputWeb_1" placeholder="<?php echo $customers['companyweb']; ?>">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputContact_1">Contact number</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                <input type="email" class="form-control" id="exampleInputContact_1" placeholder="<?php echo $customers['companyphone']; ?>">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputFax_1">Fax number</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-doc"></i></div>
                                                <input type="email" class="form-control" id="exampleInputFax_1" placeholder="<?php echo $customers['companyfax']; ?>">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-xs-12 col-md-6 col-lg-6">
                                                <h6 class="panel-title txt-dark">Billing Address</h6>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputAddress_1">Address</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputAddress_1" placeholder="<?php echo $customers['companyaddress']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputCity_1">City</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputCity_1" placeholder="<?php echo $customers['customercity']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputState_1">State</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputState_1" placeholder="<?php echo $customers['customerstate']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10">Country</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                      <option value="Category 1">USA</option>
                                                      <option value="Category 2">Austrailia</option>
                                                      <option value="Category 3">India</option>
                                                      <option value="Category 4">UK</option>
                                                    </select>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="col-xs-12 col-md-6 col-lg-6">
                                                <h6 class="panel-title txt-dark">Shipping Address</h6>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputAddress_2">Address</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputAddress_2" placeholder="<?php echo $customers['companyaddress']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputCity_2">City</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputCity_2" placeholder="<?php echo $customers['customercity']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10" for="exampleInputState_2">State</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputState_2" placeholder="<?php echo $customers['customerstate']; ?>">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label mb-10">Country</label>
                                                  <div class="input-group">
                                                    <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                      <option value="Category 1">USA</option>
                                                      <option value="Category 2">Austrailia</option>
                                                      <option value="Category 3">India</option>
                                                      <option value="Category 4">UK</option>
                                                    </select>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10" for="exampleInputpwd_1">Password</label>
                                              <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-lock"></i></div>
                                                <input type="password" class="form-control" id="exampleInputpwd_1" placeholder="Enter pwd" value="password">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label mb-10">Gender</label>
                                              <div>
                                                <div class="radio">
                                                  <input type="radio" name="radio1" id="radio_1" value="option1" checked="">
                                                  <label for="radio_1">
                                                  M
                                                  </label>
                                                </div>
                                                <div class="radio">
                                                  <input type="radio" name="radio1" id="radio_2" value="option2">
                                                  <label for="radio_2">
                                                  F
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          <div class="form-actions mt-10">      
                                            <button type="submit" class="btn btn-sm btn-success mr-10 mb-30">Update profile</button>
                                          </div>        
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-sm btn-success waves-effect" data-dismiss="modal">Save</button>
                          <button type="button" class="btn btn-sm btn-default waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      <div class="col-xs-12 col-md-7 col-lg-7">
        <div class="row">
          <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-default card-view bg-yellow pt-0">
              <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                  <div class="sm-data-box bg-white">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-xs-6 text-left pl-0 pr-0 pt-10 pb-10">
                          <span class="txt-dark block counter">Total Spent</span>
                        </div>
                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-10 pb-10 pull-right">
                          <span class="txt-dark block counter clear pull-right"><?php echo currency;?><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($fam, 2, ',', '.');break;case '.': echo number_format($fam, 2, '.', ',');break;}?></span></span>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default card-view bg-green pt-0">
              <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                  <div class="sm-data-box bg-white">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-xs-6 text-left pl-0 pr-0 pt-10 pb-10">
                          <span class="txt-dark block counter">This Year</span>
                        </div>
                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-10 pb-10 pull-right">
                          <span class="txt-dark block counter clear pull-right"><?php echo currency;?><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($ycr, 2, ',', '.');break;case '.': echo number_format($ycr, 2, '.', ',');break;}?></span></span>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-default card-view bg-blue pt-0">
              <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                  <div class="sm-data-box bg-white">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-xs-6 text-left pl-0 pr-0 pt-10 pb-10">
                          <span class="txt-dark block counter">Avg Inv</span>
                        </div>
                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-10 pb-10 pull-right">
                          <span class="txt-dark block counter clear pull-right"><?php echo currency;?><span class="counter-anim"><?php $ave = $fam/$tfa; switch($settings['unitseparator']){case ',': echo number_format($ave, 2, ',', '.');break;case '.': echo number_format($ave, 2, '.', ',');break;}?></span></span>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default card-view bg-pink pt-0">
              <div class="panel-wrapper collapse in">
                <div class="panel-body pa-0">
                  <div class="sm-data-box bg-white">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-xs-6 text-left pl-0 pt-10 pb-10">
                          <span class="txt-dark block counter">Unpaid</span>
                        </div>
                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-10 pb-10 pull-right">
                          <span class="txt-dark block counter clear pull-right"><?php echo currency;?><span class="counter-anim"><?php switch($settings['unitseparator']){case ',': echo number_format($oft, 2, ',', '.');break;case '.': echo number_format($oft, 2, '.', ',');break;}?></span></span>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-default card-view">
              <div class="panel-body">
                <div  class="tab-struct custom-tab-1">
                  <ul role="tablist" class="nav nav-tabs" id="myTabs_15">
                    <li role="presentation" class="active"><a  data-toggle="tab" id="profile_tab_15" role="tab" href="#invoices" aria-expanded="false"><?php echo lang('customerinvoices');?></a></li>
                    <li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_15" role="tab" href="#proposals" aria-expanded="false"><?php echo lang('proposals');?></a></li>
                    <li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_15" role="tab" href="#payments" aria-expanded="false"><?php echo lang('payments');?></a></li>
                    <li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_15" role="tab" href="#tickets" aria-expanded="false"><?php echo lang('tickets');?></a></li>
                    <li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_15" role="tab" href="#reminders" aria-expanded="false"><?php echo lang('reminders');?></a></li>
                  </ul>
                  <div class="tab-content" id="myTabContent_15">
                    <div  id="invoices" class="tab-pane fade active in" role="tabpanel">
                      <table id="table2" class="table table-striped table-hover table-fw-widget">
                        <thead>
                          <tr>
                            <th><?php echo lang('id')?></th>
                            <th><?php echo lang('dateofissuance')?></th>
                            <th class="text-right"><?php echo lang('total')?></th>
                          </tr>
                        </thead>
                        <?php foreach ($invoices as $mf) {
                                                        ?>
                        <tr>
                          <td>
                            <a href="<?php echo base_url('invoices/invoice/'.$mf['id'].'')?>"><i class="ion-document"> </i><?php echo lang('invoiceprefix'),'-',str_pad($mf['id'], 6, '0', STR_PAD_LEFT); ?></a>
                          </td>
                          <td>
                            <?php echo _adate($mf['datecreated']); ?>
                          </td>
                          <td class="text-right">
                          <span class="money-area"><?php echo $mf['total']?></span>
                          </td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div>
                    <div id="proposals" class="tab-pane fade" role="tabpanel">
                      <table id="table2" class="table table-striped table-hover table-fw-widget">
                        <thead>
                          <tr>
                            <th><?php echo lang('id')?></th>
                            <th><?php echo lang('subject')?></th>
                            <th><?php echo lang('dateofissuance')?></th>
                            <th><?php echo lang('opentill')?></th>
                            <th class="text-right"><?php echo lang('total')?></th>
                          </tr>
                        </thead>
                        <?php foreach ($proposals as $proposal) {
                                                        ?>
                        <tr>

                          <td>
                            <a class="label label-default" href="<?php echo base_url('proposals/proposal/'.$proposal['id'].'')?>"><i class="ion-document"> </i><?php echo lang('proposalprefix'),'-',str_pad($proposal['id'], 6, '0', STR_PAD_LEFT); ?></a>
                          </td>
                          <td><?php echo $proposal['subject']?></td>
                          <td>
                            <?php echo _adate($proposal['date']); ?>
                          </td>
                          <td>
                            <?php echo _adate($proposal['opentill']); ?>
                          </td>
                          <td class="text-right">
                          <span class="money-area"><?php echo $proposal['total']?></span>
                          </td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div>
                    <div id="payments" class="tab-pane fade" role="tabpanel">
                      <table id="table2" class="table table-striped table-hover table-fw-widget">
                        <thead>
                          <tr>
                            <th><?php echo lang('id')?></th>
                            <th><?php echo lang('invoice')?></th>
                            <th><?php echo lang('date')?></th>
                            <th class="text-right"><?php echo lang('amount')?></th>
                          </tr>
                        </thead>
                        <?php foreach ($payments as $payment) {
                                                        ?>
                        <tr>
                          <td>
                            <a class="label label-default" href=""><i class="ion-document"> </i><?php echo $payment['id']; ?></a>
                          </td>
                          <td>
                            <b><a class="label label-default" href="<?php echo base_url('invoices/invoice/'.$payment['invoiceid'].'')?>"><?php echo lang('invoiceprefix'),'-',$payment['invoiceid']; ?></a></b>

                          </td>
                          <td>
                            <?php echo _adate($payment['date']); ?>
                          </td>
                          <td class="text-right">
                            <span class="money-area"><?php echo $payment['amount']?></span>
                          </td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div>
                    <div id="tickets" class="tab-pane fade" role="tabpanel">
                      <table id="table2" class="table table-striped table-hover table-fw-widget">
                        <thead>
                          <tr>
                            <th><?php echo lang('id')?></th>
                            <th><?php echo lang('subject')?></th>
                            <th><?php echo lang('date')?></th>
                            <th class="text-right"><?php echo lang('status')?></th>
                          </tr>
                        </thead>
                        <?php foreach ($tickets as $tickets) {
                                                        ?>
                        <tr>
                          <td>
                            <a class="label label-default" href="<?php echo base_url('tickets/ticket/'.$tickets['id'].'')?>"><i class="ion-document"> </i><?php echo $tickets['id']; ?></a>
                          </td>
                          <td>
                            <?php echo $tickets['subject'] ?>
                          </td>
                          <td><?php echo _adate($tickets['date']); ?></td>
                          <td class="text-right">
                            <?php echo $tickets['statusid'] ?>
                          </td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div>

                    <div id="reminders" class="tab-pane fade" role="tabpanel">
                      <table class="table table-striped table-hover reminder-table">
                        <thead>
                          <tr>
                            <th style="width:30%;">
                                <?php echo lang('description') ?>
                            </th>
                            <th style="width:20%;">
                                <?php echo lang('remind') ?>
                            </th>
                            <th style="width:10%;">
                                <?php echo lang('date') ?>
                            </th>
                            <th style="width:10%; text-align: right;">
                              <button type="button" data-toggle="dropdown" class="add-reminder btn btn-sm btn-success dropdown-toggle ion-android-alarm-clock">
                                <?php echo lang('addreminder') ?>
                              </button>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($reminders as $reminder) {
                                                        ?>
                          <tr class="reminder-<?php echo $reminder['id']; ?>">
                            <td class="cell-detail">
                              <span class="cell-detail-description">
                                <?php echo $reminder['description']; ?>
                              </span>
                            </td>
                            <td class="user-avatar cell-detail user-info"><img src="<?php echo base_url('uploads/staffavatars/'.$reminder['staffpicture'].'')  ?>" alt="Avatar">
                              <span>
                                <?php echo $reminder['reminderstaff']; ?>
                              </span>
                            </td>
                            <td class="cell-detail">
                              <span>
                                <?php echo _adate($reminder['date']); ?>
                              </span>
                            </td>
                            <td class="text-right"><button data-reminder="<?php echo $reminder['id']; ?>" type="button" class="btn btn-default ion-android-delete delete-reminder"></button>
                            </td>
                          </tr>
                            <?php } ?>
                        </tbody>
                      </table>
                      <div class="row">
                        <div class="reminder-form col-md-12" style="display: none">
                          <?php echo form_open_multipart('customers/addreminder', array("class"=>"form-horizontal col-md-12")); ?>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label mb-10 text-left"><?php echo lang('datetobenotified'); ?></label>
                                <div class='input-group date mr-10' id='datetimepicker1'>
                                  <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                  </span>
                                  <input type='text' class="form-control" value="<?php $this->input->post('date')?>" placeholder="<?php echo date(" d.m.Y "); ?>" />
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="staff" class="control-label mb-10 ml-10 text-left">
                                  <?php echo lang('setreminderto');?>
                                </label>
                                <select required name="staff" class="form-control ml-10">
                                  <?php
                                  foreach ($all_staff as $staff) {
                                      $selected = ($staff[ 'id' ] == $this->input->post('staff')) ? ' selected="selected"' : null;
                                      echo '<option value="' . $staff[ 'id' ] . '" ' . $selected . '>' . $staff[ 'staffname' ] . '</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="assignedstaff" class="control-label mb-10 text-left">
                              <?php echo lang('description');?>
                            </label>
                            <textarea name="description" class="form-control" rows="5"><?php $this->input->post('description')?></textarea>
                            <input hidden="" type="text" name="relation" value="<?php echo $customers['id']; ?>">
                          </div>
                          <div class="form-group pull-right">
                            <button type="button" class="btn btn-sm btn-warning btn-space reminder-cancel"><i class="icon s7-mail"></i> <?php echo lang('cancel')?></button>
                            <button type="submit" class="btn btn-sm btn-success btn-space"><i class="icon s7-close"></i> <?php echo lang('add')?></button>
                          </div>
                          <?php echo form_close(); ?>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-12col-lg-12">
            <div class="panel panel-default card-view">
              <div class="panel-wrapper">
                <div class="panel-body">
                  <h3 class="mb-10 font-20">Notes</h3>
                  <div class="form-wrap">
                    <form>
                      <div class="form-group">
                        <textarea class="form-control" rows="5"></textarea>
                      </div>
                      <div class="form-group mb-0">
                        <button type="button" class="btn btn-sm btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>

    <!--<div class="ciuis-body-content">
        <div class="main-content col-xs-12 col-md-12 col-lg-9">
            <div class="col-md-4">
                <div id="ciuis-customer-detail-contacts">
                    <div id="ciuis-customer-detail-contacts-list">
                        <div id="ciuis-customer-contacts-menu">
                            <h4><?php echo lang('customercontacts');?></h4>
                        </div>
                        <div id="ciuis-customer-contact-detail">
                            <?php foreach ($contacts as $tem) {
                                  ?>
                            <div class="ciuis-customer-contacts">
                                <div data-toggle="modal" data-target="#contactmodal<?php echo $tem['id']; ?>">
                                    <img class="ciuis-contact-avatar" width="40" height="40" avatar="<?php echo $tem['name']; ?> <?php echo $tem['surname']; ?>">
                                    <div style="padding: 16px;position: initial;">
                                      <strong>
                                        <?php echo $tem['name']; ?>
                                        <?php echo $tem['surname']; ?>
                                      </strong>
                                      <br>
                                      <span>
                                        <?php echo $tem['email']; ?>
                                      </span>
                                    </div>
                                    <div class="status 
                                      <?php if ($tem['primary']==1) {
                                          echo 'available';
                                      } else {
                                          echo 'inactive';
                                        } ?>">
                                    </div>
                                </div>
                            </div>
                            <div id="contactmodal<?php echo $tem['id']; ?>" tabindex="-1" role="dialog" class="modal fade contact-detail-modal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="user-info-list panel panel-default">
                                                <div class="panel-heading panel-heading-divider">
                                                    <b>
                                                        <?php echo $tem['name']; ?>
                                                        <?php echo $tem['surname']; ?>
                                                    </b>
                                                    <h4 style="margin: 0px;" class="pull-right">
                                                      <?php if ($tem['primary']==1) {
                                                          echo '<span class="text-success"><i class="ion-person"></i> <b>'.lang('primarycontact').'</b></span>';
                                                        }
                                                      ?>
                                                    </h4>
                                                    <span class="panel-subtitle"><b><?php echo $tem['email']; ?></b></span>
                                                </div>
                                                <div class="panel-body">
                                                    <table class="no-border no-strip skills">
                                                        <tbody class="no-border-x no-border-y">
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi mdi-case"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactposition')?><span class="icon s7-portfolio"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['position']; ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi mdi-phone"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactphone')?><span class="icon s7-phone"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['phone']; ?> -
                                                                    <?php echo $tem['intercom']; ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi ion-iphone"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactmobile')?><span class="icon s7-phone"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['mobile']; ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi mdi-pin"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactaddress')?><span class="icon s7-map-marker"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['address']; ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi mdi-skype"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactskype')?><span class="icon s7-gift"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['skype']; ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: 1px solid rgb(239, 239, 239);">
                                                                <td class="icon"><span class="mdi mdi-linkedin"></span>
                                                                </td>
                                                                <td class="item">
                                                                    <?php echo lang('contactlinkedin')?><span class="icon s7-gift"></span>
                                                                </td>
                                                                <td>
                                                                    <?php echo $tem['linkedin']; ?>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-toggle="modal" data-target="#changepassword<?=$tem['id']?>" class="btn btn-danger modal-close pull-left contact-change-button"><i class="ion-refresh"> </i>
                                                <?php echo lang('changepassword')?>
                                            </button>
                                            <div class="btn-group">
                                                <button data-toggle="modal" data-target="#mod-danger<?=$tem['id']?>" class="btn btn-default modal-close contact-delete-button">
                                                    <?php echo lang('delete')?>
                                                </button>
                                                <button data-toggle="modal" data-target="#updatecontact<?=$tem['id']?>" type="submit" class="btn btn-default modal-close contact-edit-button">
                                                    <?php echo lang('edit')?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="changepassword<?php echo $tem['id']?>" tabindex="-1" role="dialog" class="modal fade">
                                <?php echo form_open('customers/changecontactpassword/'.$tem['id'].''); ?>
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="pull-left">
                                              <?php echo lang('changepassword'); ?>
                                            </h3>
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-md-12 nopadding">
                                                <div class="form-group">
                                                    <label for="ad">
                                                        <b><?php echo lang('password'); ?></b>
                                                    </label>
                                                    <p class="xs-mb-5">Customer Area Login Password<a href="javascript:;" data-toggle="popover" data-trigger="hover" title="<?php echo lang('information')?>" data-content="<?php echo lang('contactprimaryhover')?>" data-placement="top"><b> ?</b></a>
                                                    </p>
                                                    <div class="input-group ">
                                                        <input name="contactnewpassword" type="text" class="form-control " rel="gp" data-size="9" id="nc" data-character-set="a-z,A-Z,0-9,#">
                                                        <span class="input-group-btn"><button type="button" class="btn btn-default getNewPass"><span class="ion-refresh"></span>
                                                        </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-default pull-right">
                                                    <?php echo lang('changepassword'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div id="mod-danger<?=$tem['id']?>" tabindex="-1" role="dialog" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
                                                </div>
                                                <h3>
                                                  <?php echo lang('attention'); ?>
                                                </h3>
                                                <p>
                                                    <?php echo lang('contactattentiondetail'); ?>
                                                </p>
                                                <div class="xs-mt-50">
                                                    <a type="button" data-dismiss="modal" class="btn btn-space btn-warning">
                                                        <?php echo lang('cancel'); ?>
                                                    </a>
                                                    <a href="<?php echo site_url('contacts/remove/'.$tem['id']); ?>" type="button" class="btn btn-space btn-danger">
                                                        <?php echo lang('delete'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="updatecontact<?php echo $tem['id']?>" tabindex="-1" role="dialog" class="modal fade">
                                <?php echo form_open('customers/updatecontact/'.$tem['id'], array("class"=>"form-vertical")); ?>
                                <div style="width: 65%;" class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="pull-left">
                                                <?php echo lang('newcontacttitle'); ?>
                                            </h3>
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-md-12 nopadding">
                                                <div style="padding: 0px;" class="form-group col-md-4">
                                                    <label for="ad"><b><?php echo lang('contactname'); ?></b></label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-person"></i></span>
                                                        <input type="text" name="name" value="<?php echo($this->input->post('name') ? $this->input->post('name') : $tem['name']); ?>" class="form-control ci-contact-name" id="name" placeholder="<?php echo lang('contactname'); ?>" />
                                                        <input type="hidden" name="customerid" value="<?php echo $customers['id']; ?>" />
                                                    </div>
                                                </div>
                                                <div style="padding-right: 0px;" class="form-group col-md-4">
                                                    <label for="surname">
                                                        <b><?php echo lang('contactsurname'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-person-outline"></i></span>
                                                        <input type="text" name="surname" value="<?php echo($this->input->post('surname') ? $this->input->post('surname') : $tem['surname']); ?>" class="form-control ci-contact-surname" id="surname" placeholder="<?php echo lang('contactsurname'); ?>" />
                                                    </div>
                                                </div>
                                                <div style="padding-right: 0px;" class="form-group col-md-4">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactposition'); ?></b>
                                                    </label>
                                                    <input type="text" name="position" value="<?php echo($this->input->post('position') ? $this->input->post('position') : $tem['position']); ?>" class="form-control ci-contact-position" id="position" placeholder="<?php echo lang('contactposition'); ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-12 nopadding">
                                                <div style="padding: 0px;" class="form-group col-md-4">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactphone'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-telephone"></i></span>
                                                        <input type="text" name="phone" value="<?php echo($this->input->post('phone') ? $this->input->post('phone') : $tem['phone']); ?>" class="form-control ci-contact-phone" id="phone" placeholder="<?php echo lang('contactphone'); ?>" />
                                                    </div>
                                                </div>
                                                <div style="padding-right: 0px;" class="form-group col-md-4">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactintercom'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-grid-view-outline"></i></span>
                                                        <input type="text" name="intercom" value="<?php echo($this->input->post('intercom') ? $this->input->post('intercom') : $tem['intercom']); ?>" class="form-control ci-contact-intercom" id="intercom" placeholder="<?php echo lang('contactintercom'); ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4 pr-0">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactmobile'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-iphone"></i></span>
                                                        <input type="text" name="mobile" value="<?php echo($this->input->post('mobile') ? $this->input->post('mobile') : $tem['mobile']); ?>" class="form-control ci-contact-mobile" id="mobile" placeholder="<?php echo lang('contactmobile'); ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 nopadding">
                                                <div class="form-group col-md-4 md-p-0 sm-p-0 lg-p-0">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactemail'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-at"></i></span>
                                                        <input type="text" name="email" value="<?php echo($this->input->post('email') ? $this->input->post('email') : $tem['email']); ?>" class="form-control ci-contact-email" id="email" placeholder="<?php echo lang('contactemail'); ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4 pr-0">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactskype'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-social-skype"></i></span>
                                                        <input type="text" name="skype" value="<?php echo($this->input->post('skype') ? $this->input->post('skype') : $tem['skype']); ?>" class="form-control ci-contact-skype" id="skype" placeholder="Skype" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4 pr-0 ">
                                                    <label for="ad">
                                                        <b><?php echo lang('contactlinkedin'); ?></b>
                                                    </label>
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-social-linkedin"></i></span>
                                                        <input type="text" name="linkedin" value="<?php echo($this->input->post('linkedin') ? $this->input->post('linkedin') : $tem['linkedin']); ?>" class="form-control ci-contact-linkedin" id="linkedin" placeholder="Linkedin" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 nopadding">
                                                <div class="form-group">
                                                    <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-location"></i></span>
                                                        <textarea name="address" class="form-control ci-contact-address" id="address" placeholder="<?php echo lang('contactaddress'); ?>">
                                                            <?php echo($this->input->post('address') ? $this->input->post('address') : $tem['address']); ?>
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-ciuis pull-right ci-update-contact-button">
                                                    <?php echo lang('update'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

    <div id="remove" tabindex="-1" role="dialog" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
              </div>
              <h3>
                <?php echo lang('attention'); ?>
              </h3>
              <p>
                <?php echo lang('customerattentiondetail'); ?>
              </p>
              <div class="xs-mt-50">
                <a type="button" data-dismiss="modal" class="btn btn-space btn-warning"><?php echo lang('cancel'); ?></a>
                <a href="<?php echo base_url('customers/remove/'.$customers['id'].'')?>" type="button" class="btn btn-space btn-danger"><?php echo lang('delete'); ?></a>
              </div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>
    <div id="contactadd" tabindex="-1" role="dialog" class="modal fade">
      <?php echo form_open('customers/contactadd', array("class"=>"form-vertical")); ?>
      <div style="width: 65%;" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="pull-left">
                <?php echo lang('newcontacttitle');?>
            </h3>
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
          </div>
          <div class="modal-body">
            <div class="col-md-12 nopadding">
              <div class="form-group col-md-4 pr-0 pl-0">
                <label for="name">
                  <b><?php echo lang('contactname');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-person"></i></span>
                  <input required type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" placeholder="<?php echo lang('contactname');?>"/>
                  <input type="hidden" name="customerid" value="<?php echo $customers['id']; ?>"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0">
                <label for="surname">
                  <b><?php echo lang('contactsurname');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-person-outline"></i></span>
                  <input required type="text" name="surname" value="<?php echo $this->input->post('surname'); ?>" class="form-control" id="surname" placeholder="<?php echo lang('contactsurname');?>"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0">
                <label for="position">
                  <b><?php echo lang('contactposition');?></b>
                </label>

                <input type="text" name="position" value="<?php echo $this->input->post('position'); ?>" class="form-control" id="position" placeholder="<?php echo lang('contactposition');?>"/>
              </div>
            </div>
            <div class="col-md-12 nopadding">
              <div class="form-group col-md-4 pr-0 pl-0">
                <label for="ad">
                  <b><?php echo lang('contactphone');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-telephone"></i></span>
                  <input type="text" name="phone" value="<?php echo $this->input->post('phone'); ?>" class="form-control" id="phone" placeholder="<?php echo lang('contactphone');?>"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0">
                <label for="intercom">
                  <b><?php echo lang('contactintercom');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-ios-grid-view-outline"></i></span>
                  <input type="text" name="intercom" value="<?php echo $this->input->post('intercom'); ?>" class="form-control" id="intercom" placeholder="<?php echo lang('contactintercom');?>"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0">
                <label for="ad">
                  <b><?php echo lang('contactmobile');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-iphone"></i></span>
                  <input type="text" name="mobile" value="<?php echo $this->input->post('mobile'); ?>" class="form-control" id="mobile" placeholder="<?php echo lang('contactmobile');?>"/>
                </div>
              </div>
            </div>
            <div class="col-md-12 nopadding">
              <div class="form-group col-md-4 pl-0 pr-0">
                <label for="email">
                  <b><?php echo lang('contactemail');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-at"></i></span>
                  <input required type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" placeholder="<?php echo lang('contactemail');?>"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0">
                <label for="skype">
                  <b><?php echo lang('contactskype');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-social-skype"></i></span>
                  <input type="text" name="skype" value="<?php echo $this->input->post('skype'); ?>" class="form-control" id="skype" placeholder="Skype"/>
                </div>
              </div>
              <div class="form-group col-md-4 pr-0 ">
                <label for="linkedin">
                  <b><?php echo lang('contactlinkedin');?></b>
                </label>

                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-social-linkedin"></i></span>
                  <input type="text" name="linkedin" value="<?php echo $this->input->post('linkedin'); ?>" class="form-control" id="linkedin" placeholder="Linkedin"/>
                </div>
              </div>
              <div style="display: none" class="form-group col-md-4 password-input md-pl-0 sm-pl-0 lg-pl-0 pr-0">
                <label for="password">
                  <b><?php echo lang('password');?></b>
                </label>
                <p class="xs-mb-5">Customer Area Login Password<a href="javascript:;" data-toggle="popover" data-trigger="hover" title="<?php echo lang('information')?>" data-content="<?php echo lang('contactprimaryhover')?>" data-placement="top"><b> ?</b></a>
                </p>
                <div class="input-group ">
                  <input name="password" type="text" class="form-control " rel="gp" data-size="9" id="nc" data-character-set="a-z,A-Z,0-9,#">
                  <span class="input-group-btn"><button type="button" class="btn btn-default getNewPass"><span class="ion-refresh"></span>
                  </button>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-12 nopadding">
              <div class="form-group">
                <div class="input-group xs-mb-15"><span class="input-group-addon"><i class="ion-location"></i></span>
                  <textarea name="address" class="form-control" id="address" placeholder="<?php echo lang('contactaddress');?>"><?php echo $this->input->post('address'); ?></textarea>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="ciuis-body-checkbox has-success col-md-5">
                <input name="primary" class="primary-check" id="primary" type="checkbox" value="1" onchange="valueChanged()">
                <label for="primary"><?php echo lang('primarycontact') ?></label>
              </div>
              <button type="submit" class="btn btn-ciuis pull-right">
                <?php echo lang('contactaddbuton');?>
              </button>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
        <?php echo form_close(); ?>
    </div>
    <div id="edit" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-warning">
      <?php echo form_open('customers/customer/'.$customers['id'], array("class"=>"form-vertical")); ?>
      <div style="width: 70%;" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title">
                <?php echo lang('updatecustomertitle');?>
            </h3>
            <span>
                <?php echo lang('updatecustomerdescription');?>
            </span>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <?php if ($customers['type']==0) {
                        echo'<label for="companyname">'.lang('updatecustomercompanyname').'</label><div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-case"></i></span><input type="text" name="companyname" value="'.($this->input->post('companyname') ? $this->input->post('companyname') : $customers['companyname']).'" class="form-control" id="companyname"/></div>';
                            } else {
                        echo'<label for="namesurname">'.lang('updatecustomerindividualname').'</label><div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-case"></i></span><input type="text" name="namesurname" value="'.($this->input->post('namesurname') ? $this->input->post('namesurname') : $customers['namesurname']).'" class="form-control" id="namesurname"/></div>';
                      }
                    ?>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="taxoffice">
                    <?php echo lang('taxofficeedit');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-local-atm"></i></span>
                    <input type="text" name="taxoffice" value="<?php echo($this->input->post('taxoffice') ? $this->input->post('taxoffice') : $customers['taxoffice']); ?>" class="form-control" id="taxoffice"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <?php if ($customers['type']==0) {
                            echo'<label for="taxnumber">'.lang('taxnumberedit').'</label><div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-case"></i></span><input type="number" name="taxnumber" value="'.($this->input->post('taxnumber') ? $this->input->post('taxnumber') : $customers['taxnumber']).'" class="form-control required" id="taxnumber" required/></div>';
                          } else {
                            echo'<label for="socialsecuritynumber">'.lang('socialsecuritynumberedit').'</label><div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-case"></i></span><input type="text" name="socialsecuritynumber" value="'.($this->input->post('socialsecuritynumber') ? $this->input->post('socialsecuritynumber') : $customers['socialsecuritynumber']).'" class="form-control" id="socialsecuritynumber"/></div>';
                          }
                    ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="companyexecutive">
                    <?php echo lang('companyexecutiveupdate');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                    <input type="text" name="companyexecutive" value="<?php echo($this->input->post('companyexecutive') ? $this->input->post('companyexecutive') : $customers['companyexecutive']); ?>" class="form-control" id="companyexecutive"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="zipcode">
                    <?php echo lang('zipcode');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="ion-paper-airplane"></i></span>
                    <input type="text" name="zipcode" value="<?php echo($this->input->post('zipcode') ? $this->input->post('zipcode') : $customers['zipcode']); ?>" class="form-control" id="zipcode"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="companyphone">
                    <?php echo lang('customerphoneupdate');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-phone"></i></span>
                    <input type="text" name="companyphone" value="<?php echo($this->input->post('companyphone') ? $this->input->post('companyphone') : $customers['companyphone']); ?>" class="form-control" id="companyphone"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="companyfax">
                    <?php echo lang('customerfaxupdate');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-scanner"></i></span>
                    <input type="text" name="companyfax" value="<?php echo($this->input->post('companyfax') ? $this->input->post('companyfax') : $customers['companyfax']); ?>" class="form-control" id="companyfax"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="companyemail">
                    <?php echo lang('emailedit');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon">@</span>
                    <input type="text" name="companyemail" value="<?php echo($this->input->post('companyemail') ? $this->input->post('companyemail') : $customers['companyemail']); ?>" class="form-control" id="companyemail"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="companyweb">
                    <?php echo lang('customerwebupdate');?>
                  </label>
                  <div class="input-group xs-pt-10"><span class="input-group-addon"><i class="mdi mdi-http"></i></span>
                    <input type="text" name="companyweb" value="<?php echo($this->input->post('companyweb') ? $this->input->post('companyweb') : $customers['companyweb']); ?>" class="form-control" id="companyweb"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="companyemail">
                    <?php echo lang('customeraddressupdate');?>
                  </label>
                  <textarea name="companyaddress" class="form-control"><?php echo($this->input->post('companyaddress') ? $this->input->post('companyaddress') : $customers['companyaddress']); ?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="form-group">
                  <label for="companyemail">
                    <?php echo lang('customercountryupdate');?>
                  </label>
                  <select required name="countryid" class="form-control select2 required">
                    <option value="<?php echo $customers['countryid'];?>"><?php echo $customers['country'];?></option>
                    <?php
                    foreach ($countries as $country) {
                        $selected = ($country[ 'id' ] == $this->input->post('countryid')) ? ' selected="selected"' : null;
                        echo '<option value="' . $country[ 'id' ] . '" ' . $selected . '>' . $country[ 'shortname' ] . '</option>';
                    }
                    ?>
                  </select>

                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="form-group">
                  <label for="customerstate">
                    <?php echo lang('customerstateupdate');?>
                  </label>
                  <div class="input-group"><span class="input-group-addon"><i class="mdi mdi-http"></i></span>
                    <input type="text" name="customerstate" value="<?php echo($this->input->post('customerstate') ? $this->input->post('customerstate') : $customers['customerstate']); ?>" class="form-control" id="customerstate"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="form-group">
                  <label for="customercity">
                    <?php echo lang('customercityupdate');?>
                  </label>
                  <div class="input-group"><span class="input-group-addon"><i class="mdi mdi-http"></i></span>
                    <input type="text" name="customercity" value="<?php echo($this->input->post('customercity') ? $this->input->post('customercity') : $customers['customercity']); ?>" class="form-control" id="customercity"/>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-3">
                <div class="form-group">
                  <label for="customertown">
                    <?php echo lang('customertownupdate');?>
                  </label>
                  <div class="input-group"><span class="input-group-addon"><i class="mdi mdi-http"></i></span>
                    <input type="text" name="customertown" value="<?php echo($this->input->post('customertown') ? $this->input->post('customertown') : $customers['customertown']); ?>" class="form-control" id="customertown"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="form-group text-left">
              <div class="col-sm-3">
                <label for="risk">
                    <?php echo lang('riskstatus');?>
                </label><br>
                <input data="value:'<?php echo $customers['risk'];?>'" name="risk" type="text" data-slider-max="100" value="<?php echo($this->input->post('risk') ? $this->input->post('risk') : $customers['risk']); ?>" class="bslider form-control">
              </div>
            </div>
            <input hidden="" type="text" name="type" value="<?php echo $customers['type'];?>">
            <button type="button" data-dismiss="modal" class="btn btn-warning modal-close"><?php echo lang('cancel');?></button>
            <button type="submit" class="btn btn-success modal-close"><?php echo lang('update');?></button>
          </div>
        </div>
      </div>
        <?php echo form_close(); ?>
    </div>

<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_post.php' ;?>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('form').each(function() {  // attach to all form elements on page
        $(this).validate({       // initialize plugin on each form
            // global options for plugin
        });
    });

}); // Generate a password string
  function randString( id ) {
    var dataSet = $( id ).attr( 'data-character-set' ).split( ',' );
    var possible = '';
    if ( $.inArray( 'a-z', dataSet ) >= 0 ) {
      possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if ( $.inArray( 'A-Z', dataSet ) >= 0 ) {
      possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if ( $.inArray( '0-9', dataSet ) >= 0 ) {
      possible += '0123456789';
    }
    if ( $.inArray( '#', dataSet ) >= 0 ) {
      possible += '![]{}()%&*$#^<>~@|';
    }
    var text = '';
    for ( var i = 0; i < $( id ).attr( 'data-size' ); i++ ) {
      text += possible.charAt( Math.floor( Math.random() * possible.length ) );
    }
    return text;
  }

  // Create a new password on page load
  $( 'input[rel="gp"]' ).each( function () {
    $( this ).val( randString( $( this ) ) );
  } );

  // Create a new password
  $( ".getNewPass" ).click( function () {
    var field = $( this ).closest( 'div' ).find( 'input[rel="gp"]' );
    field.val( randString( field ) );
  } );

  // Auto Select Pass On Focus
  $( 'input[rel="gp"]' ).on( "click", function () {
    $( this ).select();
  } );
</script>
<script type="text/javascript">
  function valueChanged() {
    if ( $( '.primary-check' ).is( ":checked" ) )
      $( ".password-input" ).show();
    else
      $( ".password-input" ).hide();
  }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js" type="text/javascript"></script>
<script>
  $( '.bslider' ).bootstrapSlider( {
    value: <?php echo $customers['risk']; ?>,
  } );
  $( ".contact-edit-button" ).click( function () {
    $('.contact-detail-modal').modal('hide');
  });
  $( ".contact-delete-button" ).click( function () {
    $('.contact-detail-modal').modal('hide');
  });
  $('.contact-change-button').click( function(){
    $('.contact-detail-modal').modal('hide');
  });
</script>
<script>
  $( function () {
    var data = {
      "labels": [ "<?php echo lang('january');?>", "<?php echo lang('february');?>", "<?php echo lang('march');?>", "<?php echo lang('april');?>", "<?php echo lang('may');?>", "<?php echo lang('june');?>", "<?php echo lang('july');?>", "<?php echo lang('august');?>", "<?php echo lang('september');?>", "<?php echo lang('october');?>", "<?php echo lang('november');?>", "<?php echo lang('december');?>" ],
      "datasets": [ {
        "type": "line",
        backgroundColor: 'rgba(57, 57, 57, 0.69)',
        "hoverBorderColor": "#f5f5f5",

        borderColor: '#ffbc00',
        borderWidth: 1,
        "data": <?php echo $customer_annual_sales_chart ?>,
      } ]
    };
    var options = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [ {
          categoryPercentage: .2,
          barPercentage: 1,
          position: 'top',
          gridLines: {
            color: '#C7CBD5',
            zeroLineColor: '#C7CBD5',
            drawTicks: true,
            borderDash: [ 5, 5 ],
            offsetGridLines: false,
            tickMarkLength: 10,
            callback: function ( value ) {
              console.log( value )
                // return value.charAt(0) + value.charAt(1) + value.charAt(2);
            }
          },
          ticks: {
            callback: function ( value ) {
              return value.charAt( 0 ) + value.charAt( 1 ) + value.charAt( 2 );
            }
          }
        } ],
        yAxes: [ {
          display: false,
          gridLines: {
            drawBorder: true,
            drawOnChartArea: true,
            borderDash: [ 8, 5 ],
            offsetGridLines: true
          },
          ticks: {
            beginAtZero: true,
            max: "<?php echo $ycr; ?>",
            maxTicksLimit: 12,
          }
        } ]
      },
      legend: {
        display: false
      }
    };
    var ctx = $( '#customerthisyearsalesgraph' );
    var mainChart = new Chart( ctx, {
      type: 'bar',
      data: data,
      options: options
    } );
  } );
</script>
<script>
  $( document ).ready( function () {
    //Update Contact
    var contactid = $('#cx-ci-form').attr('data-contactid');
    $( "#ajaxpost-contact-update" ).click( function () {
      $.ajax( {
        type: "POST",
        url:  "<?php echo base_url(); ?>customers/updatecontact/" + contactid,
        data: {
          name: $( ".ci-contact-name" ).val(),
          surname: $( ".ci-contact-surname" ).val(),
          phone: $( ".ci-contact-phone" ).val(),
          intercom: $( ".ci-contact-intercom" ).val(),
          mobile: $( ".ci-contact-mobile" ).val(),
          email: $( ".ci-contact-email" ).val(),
          address: $( ".ci-contact-address" ).val(),
          skype: $( ".ci-contact-skype" ).val(),
          linkedin: $( ".ci-contact-linkedin" ).val(),
          position: $( ".ci-contact-position" ).val(),
          primary: $( ".ci-contact-primary" ).val(),
          password: $( ".ci-contact-password" ).val()
        },
        dataType: "text",
        cache: false,
        success: function ( data ) {
          $.gritter.add( {
            title: '<b><?php echo lang('notification')?></b>',
            text: '<?php echo lang('contactupdated')?>',
            class_name: 'color success',
          } );
        }
      } );
      return false;
    } );
} );
</script>
<script type="text/javascript">
  var base_url = '<?php echo base_url(); ?>';
    $( ".add-note-button" ).click( function () {
        $.ajax( {
        type: "POST",
        url:  base_url + "trivia/addnote",
        data: {
          description: $( ".note-description" ).val(),
          relation: $( ".note-customer-id" ).val(),
          relation_type: 'customer'
        },
        dataType: "text",
        cache: false,
        success: function ( data ) {
          $.gritter.add( {
            title: '<b><?php echo lang('notification')?></b>',
            text: '<?php echo lang('noteadded')?>',
            position: 'bottom',
            class_name: 'color success',
          } );
          var noteid = data.insert_id;
          $( '.all-notes' ).append( '<div style="padding: 20px;border: 2px dashed #b7d4cd;border-radius: 10px;margin-bottom: 10px" class="ticket-data note-data" data-id="10"><li data-id="'+noteid+'" class="one-note"><a style="cursor: pointer;" class="mdi mdi-close pull-right delete-note"></a> <p>'+ $( '.note-description' ).val() +'</p> <code class="pull-left">Added by <a href="http://localhost:8888/ciuis/staff/staffmember/<?php echo $this->session->userdata('logged_in_staff_id'); ?>"><?php echo $this->session->userdata('staffname'); ?></a></code> <code class="pull-left">Date Added <span class="text-muted"><?php echo date('Y.m.d')  ?></span></code><br></li></div>');
          $( '.note-description' ).val( '' );
        }
      } );
      return false;
    } );

  $( ".delete-note" ).click( function () {
      var base_url = '<?php echo base_url(); ?>';
      var noteid = $( this ).parent().data( 'id' );
      var $div = $( this ).closest( 'div.note-data' );
      $.ajax( {
        type: "POST",
        url: base_url + "trivia/removenote",
        data: {
          notes: noteid
        },
        dataType: "text",
        cache: false,
        success: function ( data ) {
          $.gritter.add( {
            title: '<b><?php echo lang('notification')?></b>',
            text: '<?php echo lang('notedeleted')?>',
            position: 'bottom',
            class_name: 'color warning',
          } );
          $div.find( 'li' ).fadeOut( 1000, function () {
            $div.remove();
          } );
        }
      } );
      return false;
    } );
  $( ".delete-reminder" ).click( function () {
    var base_url = '<?php echo base_url(); ?>';
    var reminder = $( this ).data( 'reminder' );
    $.ajax( {
      type: "POST",
      url: base_url + "trivia/removereminder",
      data: {
        reminder: reminder
      },
      dataType: "text",
      cache: false,
      success: function ( data ) {
        $.gritter.add( {
          title: '<b><?php echo lang('notification')?></b>',
          text: '<?php echo lang('reminderdeleted')?>',
          position: 'bottom',
          class_name: 'color warning',
        } );
        $( '.reminder-'+reminder+'').remove();
      }
    } );
    return false;
  } );
</script>
<script type="text/javascript">
    $( ".add-reminder" ).click( function () {
      $( '.reminder-table' ).hide();
      $( '.reminder-form' ).show();
    } );
    $( ".reminder-cancel" ).click( function () {
      $( '.reminder-form' ).hide();
      $( '.reminder-table' ).show();
    } );
    $( '#chooseFile' ).bind( 'change', function () {
      var filename = $( "#chooseFile" ).val();
      if ( /^\s*$/.test( filename ) ) {
        $( ".file-upload" ).removeClass( 'active' );
        $( "#noFile" ).text( "<?php echo lang('notassignedanystaff')?>" );
      } else {
        $( ".file-upload" ).addClass( 'active' );
        $( "#noFile" ).text( filename.replace( "C:\\fakepath\\", "" ) );
      }
    } );
    $( '.search-table-external' ).on( 'keyup click', function () {
      $( '#table2' ).DataTable().search(
        $( '.search-table-external' ).val()
      ).draw();
    } );
  </script>