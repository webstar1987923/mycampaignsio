<?php $page = "tickets"; ?>
<?php include_once dirname(dirname(__FILE__)) . '/inc/header.php'; ?>

<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<form>
							<select id="select" class="filterbyword select2" width="150px">
								<option value="all"><?php echo lang('all')?></option>
								<option value="2"><?php echo lang('high')?></option>
								<option value="1"><?php echo lang('medium')?></option>
								<option value="0"><?php echo lang('low')?></option>
							</select>
						</form>
					</div>
				</div>
			</div>
			<div class="panel-wrapper">
				<div class="panel-body">
					<!-- <div class="row">
						<div class="col-lg-4">
							<ul class="">
								<?php foreach($tickets as $ticket){ ?>
								<li id="dana" class="<?php echo $ticket['priority']?>">
									<a href="<?php echo base_url('tickets/ticket/'.$ticket['id'].'');?>">
										<?php switch($ticket['priority']){ 
										case '0': echo '<span class="ticket-priority label label-primary">'.lang('low').'</span>';break; 
										case '1': echo '<span class="ticket-priority label label-warning">'.lang('medium').'</span>'; break;
										case '2': echo '<span class="ticket-priority label label-danger">'.lang('high').'</span>'; break;
										}?>
										<h3><?php echo lang('ticket')?>: <?php echo $ticket['id']; ?></h3>
										<h4>
											<?php echo $ticket['subject']; ?>
										</h4>
										<p>
											<?php echo $ticket['message']; ?>
										</p>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div> -->
					<div class="row border-right-grey">
						<div class="col-sm-6 col-md-3 col-lg-3" onclick="window.location='<?php echo base_url('tickets/open/')?>'">
							<h5 class="mb-15">
								<?php echo lang('open'); ?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $ysy; ?>%"> </div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $ysy ?>% </b> <?php $this->db->where('statusid',1);$this->db->from('tickets'); echo $this->db->count_all_results();?> /
											<?php $this->db->from('tickets'); echo $this->db->count_all_results();?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3" onclick="window.location='<?php echo base_url('tickets/inprogress/')?>'">
							<h5 class="mb-15">
								<?php echo lang('inprogress'); ?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $bsy; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $bsy; ?>%</b> <?php $this->db->where('statusid',2);$this->db->from('tickets'); echo $this->db->count_all_results();?> /
											<?php $this->db->from('tickets'); echo $this->db->count_all_results();?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3" onclick="window.location='<?php echo base_url('tickets/answered/')?>'">
							<h5 class="mb-15">
								<?php echo lang('answered')?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $twy; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $twy; ?>%</b> <?php $this->db->where('statusid',3);$this->db->from('tickets'); echo $this->db->count_all_results();?> /
											<?php $this->db->from('tickets'); echo $this->db->count_all_results();?></span>
							  </div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3" onclick="window.location='<?php echo base_url('tickets/closed/')?>'">
							<h5 class="mb-15">
								<?php echo lang('closed'); ?>
							</h5>
							<div class="row">
								<div class="col-sm-8">
										<div class="progress progress-xs mb-0 mt-5">
											<div class="progress-bar progress-bar-danger" style="width: <?php echo $iey; ?>%"></div>
									  </div>
								</div>
							  <div class="col-sm-4 txt-dark">
							  	<span class="pull-right"><b class="mr-5"><?php echo $iey; ?>%</b> <?php $this->db->where('statusid',4);$this->db->from('tickets'); echo $this->db->count_all_results();?> /
											<?php $this->db->from('tickets'); echo $this->db->count_all_results();?></span>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_bootstrap.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_table.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/js_form.php';?>
	<?php include_once dirname(dirname(__FILE__)) . '/inc/footer_bottom.php';?>

	<script>
		// short foreach
		function Each( el, callback ) {
			// get all el selectors = li
			const allDivs = document.querySelectorAll( el );
			const // new array
				alltoArr = Array.prototype.slice.call( allDivs );
			// foreach new array
			Array.prototype.forEach.call( alltoArr, ( selector, index ) => {
				// callback selector
				if ( callback ) return callback( selector );
			} );
		}
		// search filter
		function textFilter( input, selectorOutput ) {
			// get selector
			searchInput = document.querySelector( input );
			// listen on keyup
			searchInput.addEventListener( 'keyup', e => {
				// each function
				Each( selectorOutput, obj => {
					// get text to lower case
					const text = obj.textContent.toLowerCase();
					const // get value to lower case
						val = searchInput.value.toLowerCase();
					// hide or show 
					obj.style.display = !text.includes( val ) ? 'none' : '';
				} );
			} );
		}
		// select filter
		function selectFilter( input, selector ) {
			// get select class
			const search = document.querySelector( input );
			const // get li class
				div = document.querySelector( selector );
			// listen select on change
			search.addEventListener( 'change', e => {
				// if value all show all 
				if ( search.value === 'all' ) {
					// each function
					Each( selector, obj => {
						// empty display to show
						obj.style.display = '';
					} );
				} else {
					// each function
					Each( selector, obj => {
						// if contains class show if not hide
						if ( obj.classList.contains( search.value ) ) {
							// empty display to show
							obj.style.display = '';
						} else {
							// none display to hide
							obj.style.display = 'none';
						}
					} );
				}
			} );
		}
		// text filter
		textFilter( '.textfilter', '#dana' );
		// select filter
		selectFilter( '.filterbyword', '#dana' );
	</script>
	
	<script type="text/javascript">
		$( document ).ready( function () {
			//initialize the javascript
			App.init();
			App.textEditors();
		} );
	</script>