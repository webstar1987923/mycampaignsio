<?php
global $current_page;

$current_page = "login";

$theme_base_url = base_url() . 'frontend/site/default/';

$links = array(
    'register' => base_url() . 'auth/register',
    'forgot_pass' => base_url() . 'auth/forgotpassword',
);

$attrs = array(
    'form' => array('class' => 'login-page-form relative dib center white')
);

$enabled_login = array(
    'facebook' => 1 === (int) $this->config->item('enable_facebook'),
    'twitter' => 1 === (int) $this->config->item('enable_twitter'),
    'gplus' => 1 === (int) $this->config->item('enable_gplus'),
);

require 'parts/html-top.php';
?>

<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="index.html">
                <img class="brand-img mr-10" src="<?php echo $theme_base_url; ?>images/campaigns-io-logo.png"" alt="campaingsio"/>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <span class="inline-block pr-10">Don't have an account?</span>
            <a class="inline-block btn btn-info btn-rounded btn-outline" href="<?php echo $links['register']; ?>">Sign Up</a>
        </div>
        <div class="clearfix"></div>
    </header>
    
    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Sign in to Campaingsio</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                </div>  
                                <div class="form-wrap">
                                    <?php echo form_open($this->uri->uri_string(), $attrs['form']); ?>
                                        <?php login_form_messages(
                                            isset($errors) ? ( is_array($errors) ? $errors : array($errors) ) : array(),
                                            isset($success) ? $success : '',
                                            isset($message) ? $message : ''
                                        ); ?>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                            <?php echo form_input( $login ); ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
                                            <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="<?php echo $links['forgot_pass']; ?>">forgot password ?</a>
                                            <div class="clearfix"></div>
                                            <?php echo form_password( $password ); ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary pr-10 pull-left">
                                                <?php echo form_checkbox( $remember ); ?>
                                                <label for="checkbox_2"> Remember me</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center">
                                            <?php if ( $show_captcha ) {
                                                login_form_captcha($use_recaptcha, $this->config, $captcha_html, $captcha );
                                            } ?>
                                            <button type="submit" value="true" class="btn btn-info btn-rounded">sign in</button>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <?php if( $enabled_login['facebook'] || $enabled_login['twitter'] || $enabled_login['gplus'] ){ ?>
                        <div class="social-login-wrap">
                            <?php if ( $enabled_login['facebook'] ) { ?>
                            <a href="<?php echo site_url('auth/oauth2/facebook');?>" class="social-login facebook">
                                <span><img src="<?php echo $theme_base_url; ?>images/svg/facebook.svg" alt="" /></span>
                                <span>Login with Facebook</span>
                            </a>
                            <?php } ?>
                            <?php if ( $enabled_login['twitter'] ) { ?>
                            <a href="<?php echo site_url('auth/oauth/twitter');?>" class="social-login twitter">
                                <span><img src="<?php echo $theme_base_url; ?>images/svg/twitter.svg" alt="" /></span>
                                <span>Login with Twitter</span>
                            </a>
                            <?php } ?>
                            <?php if ( $enabled_login['gplus'] ) { ?>
                            <a href="<?php echo site_url('auth/oauth2/google');?>" class="social-login google-plus">
                                <span><img src="<?php echo $theme_base_url; ?>images/svg/googleplus.svg" alt="" /></span>
                                <span>Login with Google+</span>
                            </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /Row -->   
        </div>
        
    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<?php require 'parts/html-bottom.php'; ?>
