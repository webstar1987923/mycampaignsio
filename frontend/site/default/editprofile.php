<?php 
global $active_main_nav_item;
$active_main_nav_item = 'profile';

$form_attr = array(
    'id' => 'editUser',
    'class' => 'edit-profile-form cf'
);
?>

<?php require 'parts/top.php'; ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default border-panel card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h4>EDIT PROFILE</h4>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php login_form_messages(
                isset($errors) ? ( is_array($errors) ? $errors : array($errors) ) : array(),
                isset($success) ? $success : '',
                isset($message) ? $message : '',
                false
            ); ?>

            <div class="panel-wrapper">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <img src="<?php echo gravatar_thumb($profile->email, 340); ?>" width="100%" alt="">
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <?php echo form_open($this->uri->uri_string(), $form_attr); ?>

                                <div class="form-group">
                                    <label for="" class="control-label mb-5">First name:</label>
                                    <?php echo form_input($first_name); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Last name:</label>
                                    <?php echo form_input($last_name); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Email address:</label>
                                    <?php echo form_input($email); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Username:</label>
                                    <?php echo form_input($username); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Password:</label>
                                    <?php echo form_password($password); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Confirm password:</label>
                                    <?php echo form_password($confirm_password); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Phone:</label>
                                    <?php echo form_input($phone); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Company:</label>
                                    <?php echo form_input($company); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Country:</label>
                                    <?php
                                    $selected_country = ( ! isset( $profile->country ) || '' === $profile->country ) ? '' : $profile->country;
                                    echo country_dropdown(
                                        'country',
                                        'country',
                                        'form-control select-full',
                                        $selected_country,
                                        array(),
                                        '',
                                        $selection = NULL,
                                        $show_all = true,
                                        $country_atts
                                    );
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Website URL:</label>
                                    <?php echo form_input($website); ?>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Pushbullet API key:</label>
                                    <?php echo form_input($pushbulletapi); ?>
                                    <span>Get from <a href="https://www.pushbullet.com/#settings/account" target="_blank" class="txt-dark">https://www.pushbullet.com/#settings/account</a></span>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label mb-5">Address:</label>
                                    <?php echo form_textarea($address); ?>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-md">UPDATE PROFILE</button>
                                    <a href="<?php echo site_url(); ?>auth/profile" title="" class="btn btn-warning btn-md">CANCEL</a>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'parts/bottom.php'; ?>