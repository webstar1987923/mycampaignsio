<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view(get_template_directory() . 'header');

?>

<div class="page-container"> 
    <!-- Content -->
    <div class="page-content">
        <div class="page-content-inner"> 
            <!-- Page header -->
            <div class="page-header">
                <div class="page-title profile-page-title">
                    <h2>Google Analytics</h2>

                </div>
                <div class="row">
<!--Custom-grid-->
<div class="cg-site-content">
            <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="cg-panel">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-globe" aria-hidden="true"></i> Total Visits</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 ">
                    <div class="cg-panel">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Grraph</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Page Per Visit</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Unique Visitor</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Site Visitor Stat</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Visits</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>

                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Top Countries (Visits)</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>

                </div>
                    <div class="col-md-6 col-sm-6">
                    <div class="cg-panel cg-panel-style-1">
                        <div class="cg-panel-head">
                        <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Top Sources/Medium(Visits)</h6>
                        </div>
                        <div class="cg-panel-content">
                            
                        </div>
                    </div>

                </div>
            </div>  

<!--Custom-grid-->

                
    </script>
    <?php $this->load->view(get_template_directory() . 'footer'); ?>