<?php
$assets_url = base_url('assets');

if( ! function_exists('enqueue_html_stylesheets') ){
    require_once FCPATH . 'app/helpers/campaigns-io/functions_helper.php';
}

if( ! isset( $current_page ) ){
    global $current_page;
}

$stylesheets = array(
    array( "rel" => "stylesheet", "href" => $assets_url."/doodle/css/jquery.dataTables.min.css" ),
    array( "rel" => "stylesheet", "href" => $assets_url."/doodle/css/jquery.toast.min.css" ),
    array( "rel" => "stylesheet", "href" => $assets_url."/doodle/css/select2.min.css" ),
    array( "rel" => "stylesheet", "href" => $assets_url."/doodle/css/style.css" ),
    array( "rel" => "stylesheet", "href" => $assets_url."/doodle/css/custom-style.css" )
);

if( in_array( $current_page, array('login', 'register', 'forgot-password'), true ) ){
    $stylesheets[] = array( "href" => 'https://www.google.com/recaptcha/api.js' );
}

$head_top_scripts = array(
    array( "type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.min.js" ),
);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="http://my.campaigns.io/themes/site/default/images/favicon.png" type="image/png">
    <title><?php echo isset( $document_data ) && isset( $document_data['title'] ) ? $document_data['title'] : 'Campaigns.io' ?></title>
    <?php enqueue_html_stylesheets( $stylesheets ); ?>
    <?php enqueue_html_scripts( $head_top_scripts ); ?>
</head>

<body>
    <div class="wrapper theme-4-active pimary-color-red box-layout">