<?php
$theme_base_url = base_url() . 'frontend/site/default/';

$app_wrapper_classname = 'f6 before-init';

$user_session = $this->session->get_userdata();

// Getting the domains of user.
$query = "SELECT * FROM user_domain ud JOIN domains d ON d.id=ud.domain_id WHERE ud.user_id='" . $user_session['user_id'] . "' ORDER BY d.id DESC";
$query = $this->db->query( $query );
$domains = $query->result_array();

if( isset( $_COOKIE['campaigns-io'] ) ){	
	// NOTE: Replaced to keep users navigation menu collapsed on pages load.
	// $app_wrapper_classname .= isset( $_COOKIE['campaigns-io']['collapse-author-nav'] ) && 1 === (int) $_COOKIE['campaigns-io']['collapse-author-nav'] ? ' collapse-author-nav' : '';
	$app_wrapper_classname .= ' collapse-author-nav';
	
	$app_wrapper_classname .= isset( $_COOKIE['campaigns-io']['collapse-sidebar'] ) && 1 === (int) $_COOKIE['campaigns-io']['collapse-sidebar'] ? ' collapse-sidebar' : '';
}

?>

<?php require 'html-top.php'; ?>

<?php

if( ! isset( $profile ) || ! $user ){

    $user_session = $this->session->get_userdata();
    $user_session['email'] = isset($user_session['email']) ? $user_session['email'] : '';

    if( ! isset( $profile ) ){
		$profile = isset($profile) ? $profile : user_profile_data( $this->db, $user_session['user_id'] );		
        $profile->email = $user_session['email'];
    }

    unset($user_session);
}
?>    
	<!-- Top Menu Items -->            
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="mobile-only-brand pull-left">
			<div class="nav-header pull-left">
				<div class="logo-wrap">
					<a href="<?php echo base_url(); ?>auth/home">
						<img class="brand-text" src="<?php echo base_url('frontend/site/default/images/campaigns-io-logo.png'); ?>" alt="brand" style="height:30px;"/>                              
					</a>
				</div>
			</div>                    
			<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
			<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
			<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>                    
			
			<!-- Select tag -->
			<div class="top-nav-search collapse pull-left">
				<div class="dropdown pull-left btn-success" style="width:320px;">                            
					<select class="form-control select2"  onchange="window.location.href = '<?php echo base_url(); ?>auth/dashboard/' + $(this).val()">
						<option value="">Select Domain</option>
						<?php if( $domains ){
							foreach( $domains as $domain ) { ?>                                        
								<option value="<?php echo $domain['id'] ?>"><?php echo $domain['domain_name']; ?></option>                                                                                
						<?php }; 
							} else { ?>                                    
							<option>No domain available</option>
						<?php } ?>                                
					</select>
				</div>				
			</div>
		</div>  
		<div id="mobile_only_nav" class="mobile-only-nav pull-right">
			<ul class="nav navbar-right top-nav pull-right">
				<li class="dropdown app-drp">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps top-nav-icon"></i></a>
					<ul class="dropdown-menu app-dropdown" data-dropdown-in="slideInRight" data-dropdown-out="flipOutX">
						<li>
							<div class="app-nicescroll-bar">
								<ul class="app-icon-wrap pa-10">
									<li>
										<a href="weather.html" class="connection-item">
										<i class="zmdi zmdi-money txt-info"></i>
										<span class="block">Financial</span>
										</a>
									</li>
									<li>
										<a href="inbox.html" class="connection-item">
										<i class="zmdi zmdi-trending-up txt-success"></i>
										<span class="block">Traffic</span>
										</a>
									</li>
									<li>
										<a href="calendar.html" class="connection-item">
										<i class="zmdi zmdi-phone txt-primary"></i>
										<span class="block">Telephony</span>
										</a>
									</li>
									<li>
										<a href="vector-map.html" class="connection-item">
										<i class="zmdi zmdi-balance-wallet txt-danger"></i>
										<span class="block">Money</span>
										</a>
									</li>
									<li>
										<a href="chats.html" class="connection-item">
										<i class="zmdi zmdi-comment-outline txt-warning"></i>
										<span class="block">Leads</span>
										</a>
									</li>
									<li>
										<a href="contact-card.html" class="connection-item">
										<i class="zmdi zmdi-graphic-eq"></i>
										<span class="block">Forecasts</span>
										</a>
									</li>
								</ul>
							</div>	
						</li>
					</ul>
				</li>
				<li class="dropdown alert-drp">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-notifications top-nav-icon"></i><span class="top-nav-icon-badge">5</span></a>
					<ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
						<li>
							<div class="notification-box-head-wrap">
								<span class="notification-box-head pull-left inline-block">notifications</span>
								<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>
								<div class="clearfix"></div>
								<hr class="light-grey-hr ma-0"/>
							</div>
						</li>
						<li>
							<div class="streamline message-nicescroll-bar">
								<div class="sl-item">
									<a href="javascript:void(0)">
										<div class="icon bg-green">
											<i class="zmdi zmdi-flag"></i>
										</div>
										<div class="sl-content">
											<span class="inline-block capitalize-font  pull-left truncate head-notifications">
											New subscription created</span>
											<span class="inline-block font-11  pull-right notifications-time">2pm</span>
											<div class="clearfix"></div>
											<p class="truncate">Your customer subscribed for the basic plan. The customer will pay $25 per month.</p>
										</div>
									</a>	
								</div>
								<hr class="light-grey-hr ma-0"/>
								<div class="sl-item">
									<a href="javascript:void(0)">
										<div class="icon bg-yellow">
											<i class="zmdi zmdi-trending-down"></i>
										</div>
										<div class="sl-content">
											<span class="inline-block capitalize-font  pull-left truncate head-notifications txt-warning">Server #2 not responding</span>
											<span class="inline-block font-11 pull-right notifications-time">1pm</span>
											<div class="clearfix"></div>
											<p class="truncate">Some technical error occurred needs to be resolved.</p>
										</div>
									</a>	
								</div>
								<hr class="light-grey-hr ma-0"/>
								<div class="sl-item">
									<a href="javascript:void(0)">
										<div class="icon bg-blue">
											<i class="zmdi zmdi-email"></i>
										</div>
										<div class="sl-content">
											<span class="inline-block capitalize-font  pull-left truncate head-notifications">2 new messages</span>
											<span class="inline-block font-11  pull-right notifications-time">4pm</span>
											<div class="clearfix"></div>
											<p class="truncate"> The last payment for your G Suite Basic subscription failed.</p>
										</div>
									</a>	
								</div>
								<hr class="light-grey-hr ma-0"/>
								<div class="sl-item">
									<a href="javascript:void(0)">
										<div class="sl-avatar">
											<img class="img-responsive" src="<?php echo $assets_url; ?>/doodle/images/avatar.jpg" alt="avatar"/>
										</div>
										<div class="sl-content">
											<span class="inline-block capitalize-font  pull-left truncate head-notifications">Sandy Doe</span>
											<span class="inline-block font-11  pull-right notifications-time">1pm</span>
											<div class="clearfix"></div>
											<p class="truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</a>	
								</div>
								<hr class="light-grey-hr ma-0"/>
								<div class="sl-item">
									<a href="javascript:void(0)">
										<div class="icon bg-red">
											<i class="zmdi zmdi-storage"></i>
										</div>
										<div class="sl-content">
											<span class="inline-block capitalize-font  pull-left truncate head-notifications txt-danger">99% server space occupied.</span>
											<span class="inline-block font-11  pull-right notifications-time">1pm</span>
											<div class="clearfix"></div>
											<p class="truncate">consectetur, adipisci velit.</p>
										</div>
									</a>	
								</div>
							</div>
						</li>
						<li>
							<div class="notification-box-bottom-wrap">
								<hr class="light-grey-hr ma-0"/>
								<a class="block text-center read-all" href="javascript:void(0)"> read all </a>
								<div class="clearfix"></div>
							</div>
						</li>
					</ul>
				</li>
				<li class="dropdown auth-drp">
					<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="<?php echo gravatar_thumb( $profile->email, 112); ?>" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
					<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
						<?php user_navigation( $current_page, $user['parent_id'] ); ?>    
					</ul>
				</li>
			</ul>                    
		</div>                
	</nav>
	
	<?php require 'sidebar.php'; ?>

	<div class="page-wrapper">
        <div class="container-fluid pt-25">