<?php
$assets_url = base_url('assets');

if( ! isset( $current_page ) ){
    global $current_page;
}

$body_bottom_scripts = array(
	array( "language" => "JavaScript", "type" => "text/javascript", "src" => "http://www.geoplugin.net/javascript.gp" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/bootstrap.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.dataTables.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.slimscroll.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.waypoints.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.counterup.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/dropdown-bootstrap-extended.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/jquery.sparkline.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/owl.carousel.min.js" ),
	array("type" => "text/javascript", "src" => $assets_url."/doodle/js/select2.full.min.js" ),
	array("type" => "text/javascript", "src" => base_url('frontend/site/default/js/custom/init.js') ),	
	array("type" => "text/javascript", "src" => "//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js" ),	// Used in 'Business 
	array("type" => "text/javascript", "src" => "//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js" ),	// Used in 'Business 
);


if( 'business-overview' === $current_page ){
	$body_bottom_scripts[] = array( "type" => "text/javascript", "src" => base_url('frontend/site/default/js/custom/business-overview.js') );
}

if( 'view-serps' === $current_page ){
	$body_bottom_scripts[] = array( "type" => "text/javascript", "src" => base_url('frontend/site/default/js/custom/view-serp.js') );
}

if( 'seoreporting-project' === $current_page || 'seoreporting-task' === $current_page){
	$body_bottom_scripts[] = array( "type" => "text/javascript", "src" => base_url('frontend/site/default/js/custom/SeoReporting.js') );
}

// General Campaigns.io scripts file.
 $body_bottom_scripts[] = array("type" => "text/javascript", "src" => base_url('frontend/site/default/js/custom/campaigns-io-script.js') );
?>

<?php echo form_hidden( 'current_campaigns_page', $current_page ); ?>

<?php enqueue_html_scripts( $body_bottom_scripts ); ?>

</body>

</html>
