<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
global $active_main_nav_item;
$active_main_nav_item = 'list-sub-users';
global $include_filterable_table;
$include_filterable_table = true;
?>

<?php require 'parts/top.php'; ?>

<div class="content-row">
    <div class="content-column w-100 w-two-thirds-l">
        <div class="content-column-main">
            <div class="panel-heading">
                <div class="left-pos">
                    <h6 class="panel-title txt-dark">SUBUSER LIST</h6>
                </div>
                <div class="right-pos"><a href="<?php echo base_url(); ?>addsubuser" title=""><button class="btn  btn-primary">ADD NEW SUBUSER</button></a>
                </div>
            </div>
            <div class="content-column-inner">
                <div class="panel-wrapper">
                <div class="panel-body row pa-0">
                <div class="table-wrap">
                <div class="table-responsive pb-15">
                    <table id="datable_3" class="table table-hover display pb-12">
                        <thead>
                            <tr>
                                <th data-sortable="false"></th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>ACCOUNT PLAN</th>
                                <th>DOMAINS</th>
                                <th data-sortable="false">ACTION</th>
                            </tr>
                        </thead>
                        <tbody> <?php
                            if( is_array($userlist) && ! empty($userlist) ){
                                $counter = 0;
                                foreach($userlist as $user){
                                    $counter++;
                                    ?>
                                    <tr>
                                        <td><?php echo $counter; ?></td>
                                        <td class="txt-dark" ><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
                                        <td class="txt-dark"><?php echo $user['email']; ?></td>
                                        <td class="txt-dark">Demo Plan</td>
                                        <td class="txt-dark"><?php echo $user['total_domains']; ?></td>
                                        <td>
                                        	<a href="<?php echo site_url(); ?>editsubuser/<?php echo $user['id']; ?>" title="" class="dib mv1 mh1 f7 btn-color no-underline pv1 pr2 pl1-l br1" data-original-title="Edit User" data-toggle="tooltip" data-placement="top"><i class="icon-pencil" ></i></a>
                                        	<a href="<?php echo site_url(); ?>deletesubuser/<?php echo $user['id']; ?>" title="" class="dib mv1 mh1 f7 btn-lines btn-dark-br0 no-underline pv1 pr2 pl1-l br1" 
                                                data-original-title="Delete User" data-toggle="tooltip" data-placement="top"
                                                onclick="return on_click_domain_remove('Are you sure you want to delete this user?')"><i class="icon-trash"></i></a>
                                        </td>
                                    </tr> <?php
                                }
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="4">No entries found</td>
                                </tr> <?php
                            } ?>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datable_3').DataTable();    
    })
    
</script>

<?php require 'parts/bottom.php'; ?>