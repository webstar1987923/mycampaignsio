<?php 
global $active_main_nav_item;
$active_main_nav_item = 'profile';

$display_fields = array(
    'fullname' => ( $profile->first_name ? $profile->first_name : '' ) . ' ' . ( $profile->last_name ? $profile->last_name : '' ),
    'email' => $profile->email ? $profile->email : '--',
    'username' => $profile->username ? $profile->username : '--',
    'phone' => $profile->phone ? $profile->phone : '--',
    'company' => $profile->company ? $profile->company : '--',
    'country' => $profile->country ? $profile->country : '--',
    'website' => $profile->website ? $profile->website : '--',
    'address' => $profile->address ? $profile->address : '--'
);

$display_fullname = '' !== $display_fields['fullname'] ? $display_fields['fullname'] : '--';
?>

<?php require 'parts/top.php'; ?>

<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default border-panel card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h5>PROFILE</h5>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url('auth/profile/editprofile');?>" title="" class="btn btn-primary btn-lable-wrap left-label">
                        <span class="btn-label">
                            <i class="icon-pencil txt-dark"></i>
                        </span>
                        <span class="btn-text">EDIT PROFILE</span>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-5 col-md-5 mb-30">
                            <img src="<?php echo gravatar_thumb($profile->email, 340); ?>" width="100%" alt="">
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-user"></i><?php echo $display_fullname; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-envelope-open"></i><?php echo $display_fields['email']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-user-follow"></i><?php echo $display_fields['username']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-phone"></i><?php echo $display_fields['phone']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-briefcase"></i><?php echo $display_fields['company']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-map"></i><?php echo $display_fields['country']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-globe"></i><?php echo $display_fields['website']; ?>
                            </div>
                            <div class="alert alert-success alert-style-1">
                                <i class="icon-location-pin"></i><?php echo $display_fields['address']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default border-panel card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h5>API KEYS</h5>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper">
                <div class="panel-body">
                    <h4></h4>
                    <div class="form-group">
                        <label class="control-label mb-5">Pushbullet API:</label>
                        <input type="text" readonly="" value="<?php echo $profile->pushbullet_api ? $profile->pushbullet_api : 'Not added'; ?>" class="form-control text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'parts/bottom.php'; ?>