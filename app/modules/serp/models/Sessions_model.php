<?php
/**
 * CIMembership
 * 
 * @package		Modules
 * @author		1stcoder Team
 * @copyright   Copyright (c) 2015 1stcoder. (http://www.1stcoder.com)
 * @license		http://opensource.org/licenses/MIT	MIT License
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sessions_model extends CI_Model 
{

	function __construct()
	{
		parent::__construct();
		$this->sessions_table = 'sessions';
		$this->salt = 'X9WX^YvmY!5]\LnD';
	}

	function store($user_id, $domain_id){
		$this->db->from($this->sessions_table);
	    $this->db->where('user_id', $user_id);
	    $this->db->where('domain_id', $domain_id);
	    $this->db->where('social_account', 'facebook');
		$query = $this->db->get();
		$params = array(
			'user_id' => $user_id,
			'domain_id' => $domain_id,
			'social_account' => 'facebook',
			'value' => true
		);
		if (isset($query->row()->value)) {
			$this->db->where('user_id', $user_id);
			$this->db->where('domain_id', $domain_id);
			$response = $this->db->update( $this->sessions_table, $params );
		} else {
			$response = $this->db->insert( $this->sessions_table, $params );
		}
		return $response;
	}

	function get($user_id, $domain_id) {
	    $this->db->from($this->sessions_table);
	    $this->db->where('user_id', $user_id);
	    $this->db->where('domain_id', $domain_id);
	    $this->db->where('social_account', 'facebook');
	    $query = $this->db->get();
		if (isset($query->row()->value)) {
			if ($query->row()->value) {
				$this->session->set_userdata('fb_oauth_login' . $domain_id, true);
			} else {
				$this->session->set_userdata('fb_oauth_login' . $domain_id, false);
			}
		}
	}
	
}
?>
