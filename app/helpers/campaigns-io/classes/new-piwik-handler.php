<?php
// Based on controller 'analytics/piwik'.
class New_Piwik_Handler {

	private $ci;
	private $analyze_model;

	function __construct( $ci, $analyze_model, $db ){
		$this->config = $ci;
		$this->analyze_model = $analyze_model;
		$this->db = $db;
	}

	private function getPiwikResults( $urlParam, $limit = '' ) {
        $apiUrl = $this->config->config['piwik']['api_url'];
        $token_auth = $this->config->config['piwik']['auth_token'];
        $url = $apiUrl . $urlParam . "&token_auth=" . $token_auth . "&format=json&filter_limit=" . ( $limit=='' ? 40 : (int) $limit );

        $arrContextOptions = array( "ssl" => array( "verify_peer" => false, "verify_peer_name" => false, ) );
        $fetched = file_get_contents( $url, false, stream_context_create( $arrContextOptions ) );
        $result = json_decode( $fetched, true );
        return $result;
        // echo json_encode($url);die; 
    }

	public function getsearchengineclicks( $domainId ) {

		$return = array( 'status' => 'error', 'clicks' => null );

		$domain = $this->analyze_model->getDomain( $domainId );

		if ( 0 < $domain[0]->piwik_site_id ) {
			
			$totalClicks = 0;

			// Getting total visitors graph.
			$url = "?module=API&method=Referrers.getSearchEngines&idSite=";
			$url .= $domain[0]->piwik_site_id;
			$url .= "&period=month&date=today&showColumns=label,nb_visits";

			$response = $this->getPiwikResults( $url );

			if ( $response && isset( $response['result'] ) && 'error' !== $response['result'] ){
				$return['status'] = 'success';
				foreach($response as $res) {
					$totalClicks += $res['nb_visits'];
				}
			}
			else{
				$return['msg'] = $response;
			}

			$return['clicks'] = $totalClicks;
		}
		else {
			$return['msg'] = 'Piwik data not available';
		}
		return $return;
    }

    public function ecommerce()
    {
        if (!$this->ci_auth->is_logged_in()) {
            redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
              $date = $this->input->post('date');
              $_SESSION['ecom_data_date'] = $date;
                $this->load->view(get_template_directory() . '/ecommerce', $data);
            } else {
                redirect(site_url('/admin/login'));
            }
        }
    }

    public function getvisits($domainId, $days=15)
    {
                // $domainId = $this->input->post('domainId');
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $userDetails   = $this->analyze_model->getusersinfo(1);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
        //    $url .= "&userLogin=" . $userDetails[0]->username;
            $result = $this->getPiwikResults($url);
            $return = array();
            foreach($result as $key=>$res) {
                $return[$key] = $res['nb_visits'];
            }
            $ret['payload'] = $return;
            $ret['status'] = 'success';
            // return $ret;
        echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }
                
   
    }

    public function getactions($domainId, $days=15)
    {
                // $domainId = $this->input->post('domainId');
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $userDetails   = $this->analyze_model->getusersinfo(1);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
        //    $url .= "&userLogin=" . $userDetails[0]->username;
            $result = $this->getPiwikResults($url);
            $return = array();
            foreach($result as $key=>$res) {
                $return[$key] = $res['nb_actions'];
            }
            $ret['payload'] = $return;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }
                
   
    }

    public function getvisitsdaysgraph($domainId, $days=15)
    {
      $domainDetails = $this->analyze_model->getDomain($domainId);
      $currentDate = date('Y-m-'.'01');
      if ($domainDetails[0]->piwik_site_id > 0) {
         $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
         // $url .= "&userLogin=" . $userDetails[0]->username;
         $result = $this->getPiwikResults($url);
         $return = array();
         foreach($result as $key=>$res) {
            $return[$key] = $res['nb_visits'];
         }
         $ret['payload'] = $return;
         $ret['status'] = 'success';
         return $ret;
      } else {
          //no piwik added
        $ret['status'] = 'error';
        $ret['payload'] = '';
        return $ret;  
      }
    }

    public function bouncerate($domainId, $days=15)
    {
      $domainDetails = $this->analyze_model->getDomain($domainId);
      $currentDate = date('Y-m-'.'01');
      if ($domainDetails[0]->piwik_site_id > 0) {
         $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
         // $url .= "&userLogin=" . $userDetails[0]->username;
         $result = $this->getPiwikResults($url);
         $return = array();
         $entryday = 0;
         foreach($result as $key=>$res) {
             if (empty($res)) {
                $entryday++;
             }
            $return[$key] = $res['bounce_rate'];
         }
         $ret['payload'] = $return;
         $ret['status'] = 'success';
         $ret['entryday'] = $entryday;
         return $ret;
        //  echo json_encode($ret);die;
      } else {
          //no piwik added
        $ret['status'] = 'error';
        $ret['payload'] = '';
        return $ret;  
      }
    }

    public function totalvisits($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        // $userDetails   = $this->analyze_model->getusersinfo(1);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.getVisits&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
        //    $url .= "&userLogin=" . $userDetails[0]->username;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getrangeData($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=API.get&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getPageUrls($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Actions.getPageUrls&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getDeviceType($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=DevicesDetection.getType&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getReferrers($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Referrers.getReferrerType&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getHeatmaps($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=HeatmapSessionRecording.getHeatmaps&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $return = [];
            foreach ($result as $key => $value) {
                $return['idsitehsr'] = $value['idsitehsr'];
                $return['name'] = $value['name'];
                $return['sample_rate'] = $value['sample_rate'];
                $return['sample_limit'] = $value['sample_limit'];
                $return['sample_limit'] = $value['sample_limit'];
                $return['sample_limit'] = $value['sample_limit'];
                $return['sample_limit'] = $value['sample_limit'];
            }
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getWebsites($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Referrers.getWebsites&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function addGoals($domainId, $data)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
            // $url .= "?module=API&method=Goals.addGoal&idSite=".$domainDetails[0]->piwik_site_id."&name=".$data['name']."&matchAttribute=".$data['matchAttribute']."&pattern=".$data['pattern']."&patternType=".$data['patternType']."&caseSensitive=".$data['caseSensitive']."&revenue=".$data['revenue']."&allowMultipleConversionsPerVisit=".$data['allowMultipleConversionsPerVisit']."&description=".$data['description'];
            $url .= "?module=API&method=Goals.addGoal&idSite=".$domainDetails[0]->piwik_site_id."&name=".$data['name']."&matchAttribute=".$data['matchAttribute']."&pattern=".$data['pattern']."&patternType=".$data['patternType']."&caseSensitive=".$data['caseSensitive']."&revenue=".$data['revenue']."&allowMultipleConversionsPerVisit=".$data['allowMultipleConversionsPerVisit'];
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function getGoals($domainId, $period, $days)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Goals.get&idSite=".$domainDetails[0]->piwik_site_id."&period=".$period."&date=last".$days;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        }  
    }

    public function pagepervisit($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
           // $url .= "&userLogin=" . $userDetails[0]->username;
           $result = $this->getPiwikResults($url);
           $return = array();
           foreach($result as $key=>$res) {
              $return[$key] = $res['nb_actions_per_visit'];
           }
           $ret['payload'] = $return;
           $ret['status'] = 'success';
           return $ret;
        } else {
            //no piwik added
          $ret['status'] = 'error';
          $ret['payload'] = '';
          return $ret;  
        }
    }

    public function avgtime($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
           // $url .= "&userLogin=" . $userDetails[0]->username;
           $result = $this->getPiwikResults($url);
           $return = array();
           foreach($result as $key=>$res) {
              $return[$key] = $res['avg_time_on_site'];
           }
           $ret['payload'] = $return;
           $ret['status'] = 'success';
           return $ret;
        } else {
            //no piwik added
          $ret['status'] = 'error';
          $ret['payload'] = '';
          return $ret;  
        }
    }

    public function pageview($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Actions.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
           // $url .= "&userLogin=" . $userDetails[0]->username;
           $result = $this->getPiwikResults($url);
           $return = array();
           foreach($result as $key=>$res) {
              $return[$key] = $res['nb_pageviews'];
           }
           $ret['payload'] = $return;
           $ret['status'] = 'success';
           return $ret;
        } else {
            //no piwik added
          $ret['status'] = 'error';
          $ret['payload'] = '';
          return $ret;  
        }
    }

    public function uniqpageview($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=Actions.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
           // $url .= "&userLogin=" . $userDetails[0]->username;
           $result = $this->getPiwikResults($url);
           $return = array();
           foreach($result as $key=>$res) {
              $return[$key] = $res['nb_uniq_pageviews'];
           }
           $ret['payload'] = $return;
           $ret['status'] = 'success';
           return $ret;
        } else {
            //no piwik added
          $ret['status'] = 'error';
          $ret['payload'] = '';
          return $ret;  
        }
    }

    public function conversionrate($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=API.get&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
           // $url .= "&userLogin=" . $userDetails[0]->username;
           $result = $this->getPiwikResults($url);
           $return = array();
           foreach($result as $key=>$res) {
              $return[$key] = $res['conversion_rate'];
           }
           $ret['payload'] = $return;
           $ret['status'] = 'success';
           return $ret;
        } else {
            //no piwik added
          $ret['status'] = 'error';
          $ret['payload'] = '';
          return $ret;  
        }
    }

    public function uniquevisitors($domainId, $period, $days)
    {

        if ($period == 'year' && $days == 1) {
            $range = date('Y-m-d',strtotime('first day of january')) . ',' . date('Y-m-d',strtotime('this day'));
        } else if ($period == 'month' && $days == 1) {
            $range = date('Y-m-d',strtotime('first day of this month')) . ',' . date('Y-m-d',strtotime('this day'));
        } else if ($period == 'week' && $days == 1) {
            $range = date('Y-m-d',strtotime('last monday')) . ',' . date('Y-m-d',strtotime('this day'));
        } else if ($period == 'day' && $days == 1) {
            $range = date('Y-m-d',strtotime('this day'));
        } else if ($period == 'month' && $days == 2) {
            $range = date('Y-m-d',strtotime('first day of last month')) . ',' . date('Y-m-d',strtotime('last day of last month'));
        } 

        $domainDetails = $this->analyze_model->getDomain($domainId);
        // $userDetails   = $this->analyze_model->getusersinfo(1);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.getUniqueVisitors&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=".$range;
        //    $url .= "&userLogin=" . $userDetails[0]->username;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        } 
    }

    public function bouncecount($domainId, $days=15)
    {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        // $userDetails   = $this->analyze_model->getusersinfo(1);
        $currentDate = date('Y-m-'.'01');
        if ($domainDetails[0]->piwik_site_id > 0) {
           $url .= "?module=API&method=VisitsSummary.getBounceCount&idSite=".$domainDetails[0]->piwik_site_id."&period=day&date=last".$days;
        //    $url .= "&userLogin=" . $userDetails[0]->username;
            $result = $this->getPiwikResults($url);
            $ret['payload'] = $result;
            $ret['status'] = 'success';
            return $ret;
        // echo json_encode($ret);die; 
        } else {
            //no piwik added
            $return['status'] = 'error';
            $return['payload'] = '';
            $return['msg'] = 'No piwik data available';
            return $return;
        } 
    }

    public function referrervisits($domainId, $days=15)
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                // $domainId = $this->input->post('domainId');
                $domainDetails = $this->analyze_model->getDomain($domainId);
                $userDetails   = $this->analyze_model->getusersinfo($userId);
                $currentDate = date('Y-m-'.'01');
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Referrers.getReferrerType&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$currentDate."&showColumns=label,nb_visits";
                //    $url .= "&userLogin=" . $userDetails[0]->username;
                   $result = $this->getPiwikResults($url);
                   $referrervisit = $result;
                   $return['payload']['referrervisit'] =  $referrervisit;


                   $url .= "?module=API&method=Referrers.getReferrerType&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=last12&showColumns=label,nb_visits";
                //    $url .= "&userLogin=" . $userDetails[0]->username;
                   $result = $this->getPiwikResults($url);
                   $referrervisitgraph = $result;
                   $return['payload']['referrervisitgraph'] =  $referrervisitgraph;

                   $return['status'] = 'success';
                   echo json_encode($return);
                } else {
                    //no piwik added
                    $return['payload']['referrervisit'] =  null;
                    $return['status'] = 'error';
                    $return['msg'] = 'No piwik data available';
                    echo json_encode($return);

                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
        }
    }


    

    public function visittrends()
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                $domainId = $this->input->post('domainId');
                $domainDetails = $this->analyze_model->getDomain($domainId);
                $userDetails   = $this->analyze_model->getusersinfo($userId);
                $currentDate = date('Y-m-'.'01');
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Referrers.getReferrerType&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=last12&showColumns=label,nb_visits,segment";
                   $url .= "&userLogin=" . $userDetails[0]->username;
                   $result = $this->getPiwikResults($url);
                   $visitSourcesGraph = $result;
                } else {
                    //no piwik added
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
        }
    }

    
    public function visitsources($domainId, $days=15)
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                // $domainId = $this->input->post('domainId');
                $domainDetails = $this->analyze_model->getDomain($domainId);
                // $userDetails   = $this->analyze_model->getusersinfo($userId);
                $currentDate = date('Y-m-'.'01');
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Referrers.getWebsites&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$currentDate."&showColumns=label,nb_visits,segment";
                //    $url .= "&userLogin=" . $userDetails[0]->username;
                   $result = $this->getPiwikResults($url,7);
                   $visitSourcesGraph = $result;
                   $return['status'] = 'success';
                   $return['payload']['sites'] = $visitSourcesGraph;
                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = 'error';
                    $return['payload']['sites'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;  
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
        }
    }

    public function topcountry($domainId, $days=15)
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                // $domainId = $this->input->post('domainId');
                $domainDetails = $this->analyze_model->getDomain($domainId);
                // $userDetails   = $this->analyze_model->getusersinfo($userId);
                $currentDate = date('Y-m-'.'01');
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=UserCountry.getCountry&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$currentDate."&showColumns=label,nb_visits";
                //    $url .= "&userLogin=" . $userDetails[0]->username;
                   $result = $this->getPiwikResults($url,7);
                   $topCountryGraph = $result;
                   $return['status'] = 'success';
                   $return['payload']['topcountries'] = $topCountryGraph;
                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = false;
                    $return['payload'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
        }
    }



    public function newvsreturning()
    {

    }

    private function getPiwikResults1($urlParam,$limit='')
    {
        $apiUrl        = $this->ci->config->config['piwik']['api_url'];
        $token_auth = $this->ci->config->config['piwik']['auth_token'];
        // $url        = $apiUrl;
        $url=$urlParam;
        $url .= "&token_auth=$token_auth";
        if ($limit == '' ) {
          $limit=40;  
        }
        $url .= "&format=JSON&filter_limit=".$limit;

        require_once '/var/www/stats.campaigns.io/public_html' . "/papi.php";

        require_once '/var/www/stats.campaigns.io/public_html' . "/index.php";
        require_once '/var/www/stats.campaigns.io/public_html' . "/core/API/Request.php";

        $environment = new \Piwik\Application\Environment(null);
        $environment->init();

    }

    public function ecommercesummarystats()
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                $domainId = $this->input->post('domainId');
                if(!$domainId) {
                  $domainId =$this->session->userdata('domainId');
                }
                if ($_SESSION['ecom_data_date'] && $_SESSION['ecom_data_date'] != '') {
                  $date = $_SESSION['ecom_data_date'];
                }else{
                  $date = 'today';
                }

                $domainDetails = $this->analyze_model->getDomain($domainId);
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url = "?module=API&method=Goals.get&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$date."&idGoal=ecommerceOrder";
                   $result = $this->getPiwikResults($url);

                   $url = "?module=API&method=Goals.get&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$date."&idGoal=ecommerceAbandonedCart";
                   $result1 = $this->getPiwikResults($url);


                   $url = "?module=API&method=SitesManager.getSiteFromId&idSite=".$domainDetails[0]->piwik_site_id."";
                   $siteResult = $this->getPiwikResults($url);


                   $return['status'] = 'success';
                   $return['payload']['ecommercestats'] = $result;
                   $return['payload']['siteResult'] = $siteResult;
                   $return['payload']['abandonedcartstats'] = $result1;


                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = false;
                    $return['payload']['ecommercestats'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
         }   

    }    

    public function ecommerceproductdata()
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                $domainId = $this->input->post('domainId');
                if(!$domainId) {
                  $domainId =$this->session->userdata('domainId');
                }
                if ($_SESSION['ecom_data_date'] && $_SESSION['ecom_data_date'] != '') {
                  $date = $_SESSION['ecom_data_date'];
                }else{
                  $date = 'today';
                }
                $domainDetails = $this->analyze_model->getDomain($domainId);
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Goals.getItemsName&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$date;
                   $result = $this->getPiwikResults($url);

                   $url = "?module=API&method=SitesManager.getSiteFromId&idSite=".$domainDetails[0]->piwik_site_id."";
                   $siteResult = $this->getPiwikResults($url);

                   $return['status'] = 'success';
                   $return['payload']['productdata'] = $result;
                   $return['payload']['siteResult'] = $siteResult;
               
                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = false;
                    $return['payload']['productdata'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
         }   

    }

    public function ecommercereferrertype()
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                $domainId = $this->input->post('domainId');
                if(!$domainId) {
                  $domainId =$this->session->userdata('domainId');
                }

                $domainDetails = $this->analyze_model->getDomain($domainId);
                if ($_SESSION['ecom_data_date'] && $_SESSION['ecom_data_date'] != '') {
                  $date = $_SESSION['ecom_data_date'];
                }else{
                  $date = 'today';
                }
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Referrers.getReferrerType&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$date;
                   $result = $this->getPiwikResults($url);

                   $url = "?module=API&method=SitesManager.getSiteFromId&idSite=".$domainDetails[0]->piwik_site_id."";
                   $siteResult = $this->getPiwikResults($url);

                   $return['status'] = 'success';
                   $return['payload']['referrer'] = $result;
                   $return['payload']['siteResult'] = $siteResult;
               
                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = false;
                    $return['payload']['referrer'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
         }   

    }


    public function ecommercekeywords()
    {
        if (!$this->ci_auth->is_logged_in()) {
            // redirect(site_url('auth/login'));
        } elseif ($this->ci_auth->is_logged_in(false)) {
            // redirect('/auth/sendactivation/');
        } else {
            if ($this->ci_auth->canDo('login_to_frontend')) {
                $domainId = $this->input->post('domainId');
                if(!$domainId) {
                  $domainId =$this->session->userdata('domainId');
                }
                $domainDetails = $this->analyze_model->getDomain($domainId);
                if ($_SESSION['ecom_data_date'] && $_SESSION['ecom_data_date'] != '') {
                  $date = $_SESSION['ecom_data_date'];
                }else{
                  $date = 'today';
                }
                if ($domainDetails[0]->piwik_site_id > 0) {
                   //getting total visitors graph
                   $url .= "?module=API&method=Referrers.getKeywords&idSite=".$domainDetails[0]->piwik_site_id."&period=month&date=".$date;
                   $result = $this->getPiwikResults($url);

                   $url = "?module=API&method=SitesManager.getSiteFromId&idSite=".$domainDetails[0]->piwik_site_id."";
                   $siteResult = $this->getPiwikResults($url);

                   $return['status'] = 'success';
                   $return['payload']['keywords'] = $result;
                   $return['payload']['siteResult'] = $siteResult;
               
                   echo json_encode($return);die;  
                } else {
                    //no piwik added
                    $return['status'] = false;
                    $return['payload']['keywords'] = null;
                    $return['msg'] = 'Piwik data not available';
                    echo json_encode($return);die;
                }
                
            } else {
                // redirect(site_url('/admin/login'));
            }
         }   

    }


    public function code()
    {
        $id            = $this->uri->segment(3);
        $domainDetails = $this->analyze_model->getDomain($id);
        if (!$domainDetails || !$domainDetails[0]->piwik_site_id) {
            echo 'No code available for this domain';
            die;
        }
        $sitename      = strtr($domainDetails[0]->domain_name, array('http://' => '', 'https://' => '', 'www.' => '', '/' => ''));

        $html = "";
        $html .= '<!-- Piwik -->';
        $html.="\n";
        $html .= '<script type="text/javascript">';
        $html.="\n";
        $html .= 'var _paq = _paq || [];';
        $html.="\n";
        $html .= '// tracker methods like "setCustomDimension" should be called before "trackPageView"';
        $html.="\n";
        $html .= '_paq.push(["setDocumentTitle", document.domain + "/" + document.title]);';
        $html.="\n";
        $html .= '_paq.push(["setCookieDomain", "*.' . $sitename . '"]);';
        $html.="\n";
        $html .= '_paq.push(["setDomains", ["*.' . $sitename . '"]]);';
        $html.="\n";
        $html .= "_paq.push(['trackPageView']);";
        $html.="\n";
        $html .= "_paq.push(['enableLinkTracking']);";
        $html.="\n";
        $html .= '(function() {';
        $html.="\n";
        $html .= 'var u="//stats.campaigns.io/";';
        $html.="\n";
        $html .= "_paq.push(['setTrackerUrl', u+'piwik.php']);";
        $html.="\n";
        $html .= "_paq.push(['setSiteId', '" . $domainDetails[0]->piwik_site_id . "']);";
        $html.="\n";
        $html .= "var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];";
        $html.="\n";
        $html .= "g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js';";
        $html .= "s.parentNode.insertBefore(g,s);";
        $html.="\n";
        $html .= '})()';
        $html.="\n";
        $html .= '</script>';
        $html.="\n";
        $html .= '<noscript><p><img src="//stats.campaigns.io/piwik.php?idsite=4&rec=1" style="border:0;" alt=""';
        $html .= ' /></p></noscript>';
        $html.="\n";
        $html .= '<!-- End Piwik Code -->';
        $text = htmlentities($html);
        echo '<code>';
        echo nl2br($text);
        echo '</code>';
    }


    public function addPiwik($domainId, $userId, $isEcommerce = false) {
        $domainDetails = $this->analyze_model->getDomain($domainId);
        $userDetails = $this->analyze_model->getusersinfo($userId);
        $apiUrl = $this->config->config['piwik']['api_url'];
  
        $token_auth = $this->config->config['piwik']['auth_token'];
        $url = $apiUrl;
        $url .= "?module=API&method=UsersManager.userExists";
        $url .= "&userLogin=" . $userDetails[0]->username;
        $url .= "&format=php";
        $url .= "&token_auth=$token_auth";
        $fetched = file_get_contents($url);
        $userExists = unserialize($fetched);
        $siteName = strtr($domainDetails[0]->domain_name, array('www.' => '', 'http://' => '', 'https://' => '', '/' => ''));
        if ($userExists == 1) {
  
          //getting the site detail
          $url = $apiUrl;
          $url .= "?module=API&method=SitesManager.getSitesIdFromSiteUrl";
          $url .= "&url=" . urlencode($domainDetails[0]->domain_name);
          $url .= "&format=php";
          $url .= "&token_auth=$token_auth";
          $fetched = file_get_contents($url);
          $siteDetail = unserialize($fetched);
  
          if (!$siteDetail) {
            //add site
            $url = $apiUrl;
            $url .= "?module=API&method=SitesManager.addSite";
            if ($isEcommerce == true) {
              $url .= "&siteName=" . $siteName . "&urls=" . urlencode($domainDetails[0]->domain_name) . "&ecommerce=1&siteSearch=1";
            } else {
              $url .= "&siteName=" . $siteName . "&urls=" . urlencode($domainDetails[0]->domain_name) . "&ecommerce=0&siteSearch=1";
            }
  
            $url .= "&format=php";
            $url .= "&token_auth=$token_auth";
  
            $fetched = file_get_contents($url);
            $siteId = unserialize($fetched);
          } else {
            $siteId = $siteDetail[0]['idsite'];
          }
          //updating the piwik site id to domain table
          $this->db->flush_cache();
          $data = array();
          $data['piwik_site_id'] = $siteId;
          $this->db->where('id', $domainDetails[0]->id);
          $this->db->update('domains', $data);
  
          //give access to user
          $url = "https://stats.campaigns.io";
          $url .= "?module=API&method=UsersManager.setUserAccess";
          $url .= "&userLogin=" . $userDetails[0]->username . "&access=admin&idSites=" . $siteId;
          $url .= "&format=php";
          $url .= "&token_auth=$token_auth";
  
          $fetched = file_get_contents($url);
          $addSite = unserialize($fetched);
        } else {
          //add user
          $url = $apiUrl;
          $url .= "?module=API&method=UsersManager.addUser";
          $url .= "&userLogin=" . $userDetails[0]->username . "&password=" . urlencode($userDetails[0]->password) . "&email=" . $userDetails[0]->email;
          $url .= "&format=php";
          $url .= "&token_auth=$token_auth";
          $fetched = file_get_contents($url);
          $userAdded = unserialize($fetched);
          if ($userAdded['result'] == 'success') {
  
            //getting the site detail
            $url = $apiUrl;
            $url .= "?module=API&method=SitesManager.getSitesIdFromSiteUrl";
            $url .= "&url=" . urlencode($domainDetails[0]->domain_name);
            $url .= "&format=php";
            $url .= "&token_auth=$token_auth";
            $fetched = file_get_contents($url);
            $siteDetail = unserialize($fetched);
  
            if (!$siteDetail) {
              //add site
              $url = "https://stats.campaigns.io";
              $url .= "?module=API&method=SitesManager.addSite";
              if ($isEcommerce == true) {
                $url .= "&siteName=" . $siteName . "&urls=" . urlencode($domainDetails[0]->domain_name) . "&ecommerce=1&siteSearch=1";
              } else {
                $url .= "&siteName=" . $siteName . "&urls=" . urlencode($domainDetails[0]->domain_name) . "&ecommerce=0&siteSearch=1";
              }
  
              $url .= "&format=php";
              $url .= "&token_auth=$token_auth";
              $fetched = file_get_contents($url);
              $siteId = unserialize($fetched);
  
            } else {
              $siteId = $siteDetail[0]['idsite'];
            }
            //updating the piwik site id to domain table
            $this->db->flush_cache();
            $data = array();
            $data['piwik_site_id'] = $siteId;
            $this->db->where('id', $domainDetails[0]->id);
            $this->db->update('domains', $data);
  
            $url = $apiUrl;
            $url .= "?module=API&method=UsersManager.setUserAccess";
            $url .= "&userLogin=" . $userDetails[0]->username . "&access=admin&idSites=" . $siteId;
            $url .= "&format=php";
            $url .= "&token_auth=$token_auth";
            $fetched = file_get_contents($url);
            unserialize($fetched);
          }
  
        }
        return true;
      }

}