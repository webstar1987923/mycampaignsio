<?php
function user_navigation( $current_page = '', $user_parent_id = null ){

  if( ! $user_parent_id ){
    $user_parent_id = isset( $_SESSION['parent_id'] ) ? $_SESSION['parent_id'] : false;
  }

  $items = array(
      array(
          'id' => 'profile',
          'title' => 'My profile',
          'icon' => 'user',
          'link' => base_url('auth/profile')
      )
  );

  if( ! $user_parent_id ){

    $items[] = array(
          'id' => 'domains-assign',
          'title' => 'Assign domains',
          'icon' => 'check',
          'link' => base_url('assigndomain')
      );

    $items[] = array(
          'id' => 'sub-users',
          'title' => 'List Subuser',
          'icon' => 'people',
          'link' => base_url('listsubuser')
      );

    $items[] = array(
          'id' => 'groups',
          'title' => 'List Group',
          'icon' => 'list',
          'link' => base_url('listgroups')
      );
  }

  $items[] = array(
        'id' => 'domains',
        'title' => 'My domains',
        'icon' => 'briefcase',
        'link' => base_url('domains')
    );

  $items[] = array(
        'id' => 'business-settings',
        'title' => 'Business Settings',
        'icon' => 'wrench',
        'link' => base_url('settings/edit/ciuis')
    );

    $items[] = array(
        'id' => 'logout',
        'title' => 'Logout',
        'icon' => 'logout',
        'link' => base_url('auth/logout')
    );
    
  foreach ($items as $k => $v) {
    $classAttr = $current_page === $v['id'] ? ' class="active"': '';
    ?>    
    <li>
      <a href="<?php echo $v['link']; ?>" title="<?php echo $v['title']; ?>" <?php echo $classAttr; ?>>
        <div class="pull-left">         
          <i class='icon-<?php echo $v['icon']; ?> mr-20'></i>
          <span class="right-nav-text"><?php echo $v['title']; ?></span>
        </div>
        <div class="clearfix"></div>
      </a>    
    </li>
    <?php
  }
  
}

function main_navigation( $current_page = '' ){
  $items = array(
    array(
      'id' => 'business-overview',
      'title' => 'Business Overview',
      'icon' => 'book-open',
      'link' => base_url('panel'),
    ),
    /*array(
      'id' => 'listproducts',
      'title' => 'List Products',
      'icon' => '&#xE871;',
      'link' => base_url('listproduct'),
    ),*/
    /*array(
      'id' => 'crm',
      'title' => 'CRM',
      'icon' => '&#xE871;',
      'link' => base_url('panel'),
    ),*/
    /*array(
      'id' => 'websites',
      'title' => 'Websites',
      'icon' => '&#xE051;',
      'link' => '#',
    ),*/
    // array(
    //  'id' => 'domains',
    //  'title' => 'Domains',
    //  'icon' => 'briefcase',
    //  'link' => base_url('domains'),
    // ),
    array(
      'id' => 'domains',
      'title' => 'Domain Dashboard',
      'icon' => 'briefcase',
      'link' => base_url('auth/home'),
    ),
    array(
      'id' => 'uptime-report',
      'title' => 'Uptime',
      'icon' => 'graph',
      'link' => base_url('uptime/report'),
    ),
    array(
      'id' => 'backups',
      'title' => 'Backups',
      'icon' => 'cloud-download',
      'link' => base_url('auth/backups'),
    ),
    array(
      'id' => 'security',
      'title' => 'Security',
      'icon' => 'shield',
      'link' => '#',
    ),
    array(
      'id' => 'seo_manager',
      'title' => 'SEO Manager',
      'icon' => 'eyeglass',
      'link' => base_url('listlinkdomain'),
    ),
    array(
      'id' => 'seo_tasks',
      'title' => 'SEO Tasks',
      'icon' => 'feed',
      'link' => base_url('seoreporting/getprojects'),
    ),
    array(
      'id' => 'social',
      'title' => 'Social',
      'icon' => 'share',
      'link' => '#',
    )
  );
  
  foreach ($items as $k => $v) {
    $classAttr = $current_page === $v['id'] ? ' class="active"' : '';
    ?>    
    <li>
      <a href="<?php echo $v['link']; ?>" title="<?php echo $v['title']; ?>" <?php echo $classAttr; ?>>
        <div class="pull-left">         
          <i class='icon-<?php echo $v['icon']; ?> mr-20'></i>
          <span class="right-nav-text"><?php echo $v['title']; ?></span>
        </div>
        <div class="clearfix"></div>
      </a>    
    </li>   
    <?php
  } 
}

function domain_navigation( $current_page = '', $domain_id = 0, $user_domains = array()){

  if( 0 === $domain_id ){
      return;
  }
    if( ! empty( $user_domains ) ){ ?>
      <div class="select-domain mt1 mb3">
          <select class="pv2 ph1 search-and-select">
          <?php
          foreach( $user_domains as $k => $v ){
              switch($current_page){
                  case 'single-domain':
                      $op_val = base_url( 'domains/' . $v->id );
                      break;
                  case 'single-domain-analytics':
                      $op_val = base_url( 'domains/' . $v->id . '/analytics' );
                      break;
                  case 'single-domain-wordpress':
                      $op_val = base_url( 'domains/' . $v->id . '/wordpress' );
                      break;
                  case 'single-domain-serps':
                  // case 'view-serps':
                    // $op_val = base_url( 'auth/viewSerp/' . $v->id );
                      $op_val = base_url( 'domains/' . $v->id . '/serps' );
                      break;
                  case 'single-domain-heatmaps':
                      $op_val = base_url( 'domains/' . $v->id . '/heatmaps' );
                      break;
                  case 'single-domain-research':
                      $op_val = base_url( 'domains/' . $v->id . '/research' );
                      break;
                  case 'single-domain-e-commerce':
                      $op_val = base_url( 'domains/' . $v->id . '/e-commerce' );
                      break;
              }
              $op_sel = $domain_id === (int) $v->id ? ' selected="selected"': "";
              $op_name = $v->domain_name;
              ?><option value="<?php echo html_escape($op_val); ?>" <?php echo $op_sel; ?>><?php echo $op_name; ?></option><?php
          } ?>
          </select>
      </div>
      <?php
  }


}

function user_domain_navigation($current_page = '', $domain_id = 0 )
{
  $items = array(
      array(
          'id' => 'single-domain',
          'title' => 'Overview',
          'link' => base_url( 'domains/' . $domain_id )
      ),
      array(
          'id' => 'single-domain-serps',
          'title' => 'SERPS',
          'link' => base_url( 'domains/' . $domain_id . '/serps' )
      ),
      // TODO: Remove 'view-serps'.
      /*array(
          'id' => 'view-serps',
          'title' => 'SERPS',
          'link' => base_url( 'auth/viewSerp/' . $domain_id )
      ),*/
      array(
          'id' => 'single-domain-research',
          'title' => 'Research',
          'link' => base_url( 'domains/' . $domain_id . '/research' )
      ),
      array(
          'id' => 'single-domain-analytics',
          'title' => 'Analytics',
          'link' => base_url( 'domains/' . $domain_id . '/analytics' )
      ),
      array(
          'id' => 'single-domain-e-commerce',
          'title' => 'E-Commerce',
          'link' => base_url( 'domains/' . $domain_id . '/e-commerce' )
      ),
      array(
          'id' => 'single-domain-heatmaps',
          'title' => 'Heatmaps',
          'link' => base_url( 'domains/' . $domain_id . '/heatmaps' )
      )
  );


  if( ! empty( $user_domains ) ){ ?>
      <div class="select-domain mt1 mb3">
          <select class="pv2 ph1 search-and-select">
          <?php
          foreach( $user_domains as $k => $v ){
              switch($current_page){
                  case 'single-domain':
                      $op_val = base_url( 'domains/' . $v->id );
                      break;
                  case 'single-domain-analytics':
                      $op_val = base_url( 'domains/' . $v->id . '/analytics' );
                      break;
                  case 'single-domain-wordpress':
                      $op_val = base_url( 'domains/' . $v->id . '/wordpress' );
                      break;
                  case 'single-domain-serps':
                  // case 'view-serps':
                    // $op_val = base_url( 'auth/viewSerp/' . $v->id );
                      $op_val = base_url( 'domains/' . $v->id . '/serps' );
                      break;
                  case 'single-domain-heatmaps':
                      $op_val = base_url( 'domains/' . $v->id . '/heatmaps' );
                      break;
                  case 'single-domain-research':
                      $op_val = base_url( 'domains/' . $v->id . '/research' );
                      break;
                  case 'single-domain-e-commerce':
                      $op_val = base_url( 'domains/' . $v->id . '/e-commerce' );
                      break;
              }
              $op_sel = $domain_id === (int) $v->id ? ' selected="selected"': "";
              $op_name = $v->domain_name;
              ?><option value="<?php echo html_escape($op_val); ?>" <?php echo $op_sel; ?>><?php echo $op_name; ?></option><?php
          } ?>
          </select>
      </div>
      <?php
  }
  ?>
  <nav><?php
      foreach ($items as $k => $v) {
        $classAttr = $current_page === $v['id'] ? ' class="active"' : ''; ?>
          <a href="<?php echo $v['link']; ?>" title="<?php echo $v['title']; ?>" <?php echo $classAttr; ?>><?php echo $v['title']; ?></a><?php
      } ?>
  </nav>
<?php
}

function edit_domain_form_component( $domain_id, $user_id, $data, $available_values, $google_client_id, $google_oauth_redirect_uri ){
    ?>

    <?php if( isset( $data['submission_failed_html'] ) ){ echo $data['submission_failed_html']; } ?>

    <?php echo form_open( base_url( "domains/save" ), array( "class" => "edit-profile-form edit-site-form cf mt3" , "id"=>'domain_add_edit') ); ?>

        
    <?php echo form_hidden( 'domain_id', $domain_id ); ?>
          <div class="panel panel-default card-view">
              <div class="panel-wrapper collapse in">
                  <div class="panel-body">
                    <div id="example-basic" class="wizard clearfix">
                      <h3><span class="head-font capitalize-font">Domain info</span></h3>
                      <section>
                        <div class="row">
                          <div class="col-sm-12 col-md-6">
                            <div class="form-wrap">
                              <div class="form-group">
                                <label for="domain_name">Domain name <small class="fw4 o-90 campaignsio-admin-orange">(required)</small></label>
                                <?php echo form_input( array( "name" => "domain_name", "class" => "form-control required", "value" => $data['domain_name'], "placeholder" => "Domain name" ) ); ?>
                              </div>
                              <div class="form-group">
                                <div class="pull-left">
                                  <span>Is your site ecommerce site?</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('is_ecommerce', 1 === $data['is_ecommerce']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              <div class="form-group woo_api_fields" style="display: <?php echo 1 === $data['is_ecommerce'] ? 'block' : 'none' ;?>">
                                <label for="woo_consumer_key">Woocommerce Consumer key</label>
                                <input type="text" class="form-control" name="woo_consumer_key" value="<?php echo $data['wooapi_consumer_key']; ?>">
                              </div>
                              <div class="form-group woo_api_fields" style="display: <?php echo 1 === $data['is_ecommerce'] ? 'block' : 'none' ;?>">
                                <label for="woo_consumer_secret">Woocommerce Consumer Secret</label>
                                <input type="text" class="form-control" name="woo_consumer_secret" value="<?php echo $data['wooapi_consumer_secret']; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-6">
                            <div class="form-wrap">
                              <div class="form-group">
                                <label for="subusers">Assign subusers to domain</label> 
                                <?php echo form_dropdown( 'subusers[]', $available_values['subusers_values'], $data['subusers'], array( 'multiple' => 'multiple', 'placeholder' => 'Choose subusers', 'class' => 'select2 select2-multiple' ) ); ?>
                              </div>
                              <div class="form-group">
                                <label for="groups">Choose group to assign domain</label>
                                <?php echo form_dropdown( 'groups[]', $available_values['group_values'], $data['groups'], array( 'multiple' => 'multiple', 'placeholder' => 'Choose groups', 'class' => 'select2 select2-multiple' ) ); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row mt-30">
                          <div class="col-lg-4">
                            <div class="from-wrap">
                              <div class="form-group">
                                <div class="pull-left">
                                  <span>Would you like to Monitor website uptime?</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('monitor_website_uptime', 1 === $data['monitor_website_uptime'] ); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="form-wrap monitor_website_uptime" style="display: <?php echo 1 === $data['monitor_website_uptime'] ? 'block' : 'none' ;?>">
                              <div class="form-group">
                                <label for="frequency">Frequency</label>
                                <?php echo form_dropdown( 'frequency', $available_values['frequencies'], $data['frequency'], array( 'placeholder' => "Choose groups", 'class' => "selectpicker", 'data-style' => 'form-control btn-default btn-outline' ) ); ?>
                              </div>
                              <div class="form-group">
                                <label for="page_header">Page Header</label>
                                <?php echo form_input( array( "name" => "page_header", 'class' => "form-control", "value" => $data['page_header'], "placeholder" => "Page Header to Search" ) ); ?>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="form-wrap monitor_website_uptime" style="display: <?php echo 1 === $data['monitor_website_uptime'] ? 'block' : 'none' ;?>">
                              <div class="form-group">
                                <label for="page_body">Page Body</label>
                                <?php echo form_input( array( "name" => "page_body", 'class' => "form-control", "value" => $data['page_body'], "placeholder" => "Page Body to Search" ) ); ?>
                              </div>
                              <div class="form-group">
                                 <label for="page_footer">Page Footer</label>
                                  <?php echo form_input( array( "name" => "page_footer", 'class' => "form-control", "value" => $data['page_footer'], "placeholder" => "Page Footer to Search" ) ); ?>
                            </div>
                          </div>
                        </div>
                      </section>
                      <h3><span class="head-font capitalize-font">Connections</span></h3>
                      <section>
                        <div class="row">
                          <div class="col-sm-12 col-md-6">
                            <div class="form-wrap">
                              <div class="form-group">
                                  <label for="adminURL">Admin Login URL <small class="fw4 o-90 campaignsio-admin-orange">(required)</small></label>
                                  <?php echo form_input( array( "name" => "adminURL", 'class' => "form-control", "value" => $data['adminURL'], "placeholder" => "http://" ) ); ?>
                              </div>

                              <div class="form-group">
                                  <label for="adminUsername">Admin Username <small class="fw4 o-90 campaignsio-admin-orange">(required)</small></label>
                                  <?php echo form_input( array( "name" => "adminUsername", 'class' => "form-control", "value" => $data['adminUsername'], "placeholder" => "Admin Username" ) ); ?>
                              </div>

                              <div class="form-group">
                                <div class="pull-left">
                                  <span>Monitor Malware</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('monitor_malware', 1 === $data['monitor_malware']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                              <div class="form-group">
                                <div class="pull-left">
                                  <span>Connect to Google</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('connect_to_google', 1 === $data['connect_to_google']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>

                        <div class="form-group connect_to_google"  style="display: <?php echo 1 === $data['connect_to_google'] ? 'block' : 'none' ;?>">
                          <label for="ga_account">Choose your google webmaster account</label>
                          <?php echo form_dropdown( 'ga_account', $available_values['google_acounts'], $data['ga_account'], array( 'placeholder' => 'Select Account', 'class' => "form-control select2", 'data-style' => 'form-control btn-default btn-outline' ) ); ?>

                          <?php if( 1 === $user_id ){ ?>

                            <button class="add-new-google-account btn btn-warning mt-20" style="cursor:pointer;">ADD NEW GOOGLE ACCOUNT</button>
                            <select name="" id="client_domains" class="form-control mt-20" style="display: none;"></select>
                          <?php } ?>
                        </div>

                        <div class="form-group connect_to_google"  style="display: <?php echo 1 === $data['connect_to_google'] ? 'block' : 'none' ;?>">
                                <div class="pull-left">
                                  <span>Would you like to get Crawl Errors from Google Webmaster Tools?</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('crawl_error_webmaster', 1 === $data['crawl_error_webmaster']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>

                              <div class="form-group connect_to_google"  style="display: <?php echo 1 === $data['connect_to_google'] ? 'block' : 'none' ;?>">
                                <div class="pull-left">
                                  <span>Would you like to track your Search Queries from Google Webmaster Tools?</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('search_query_webmaster', 1 === $data['search_query_webmaster']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <h3><span class="head-font capitalize-font">Monitor Keyword SERP</span></h3>
                      <section>
                        <div class="row">
                          <div class="col-sm-12 col-md-6">
                            <div class="form-wrap">
                              <div class="form-group">
                                <label for="engines">Choose search engine</label>
                                <?php
                                  echo form_dropdown( 'engines[]', $available_values['search_engines'], $data['engines'], array( 'multiple' => 'multiple', 'placeholder' => 'Choose your Search Engine', 'class' => 'select2 select2-multiple' )  );
                                  ?>
                              </div>
                              <div class="form-group">
                                <div class="pull-left">
                                  <span>Include Mobile Search</span>
                                </div>
                                <div class="pull-right">
                                  <span class="no-margin-switcher">
                                    <?php checkbox_component('include_mobile_search', 1 === $data['include_mobile_search']); ?>
                                  </span> 
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-6">
                            <div class="form-wrap">
                              <div class="form-group">
                                  <textarea name="keywords" class="form-control" rows="20" placeholder="Enter your keywords, one per line, that you would like to monitor"><?php echo $data['keywords']; ?></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>

                    </div>
                  </div>
              </div>
          </div>
          <?php 
              echo form_hidden( 'base_url', base_url() );
            echo form_hidden( 'google_account_client_id', $google_client_id );
            echo form_hidden( 'google_auth_redirect_uri', $google_oauth_redirect_uri );
           ?>
      </form>
    <?php 

    
}

// @note: Same function in 'app/libraies/Ci_auth.php', but in global scope.
function is_user_logged_in( $activated = true ) {
  if( ! isset( $_SESSION['status'] ) ){
    return false;
  }
  return $_SESSION['status'] === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
}

// @note: Same function in 'app/modules/auth.controllers/CampaignIo_Pages_Authorized.php', but in global scope.
function validate_user_access(){
  if ( ! is_user_logged_in() ) {
        redirect( base_url('auth/login') );
        exit;
    }
    else if( is_user_logged_in( false ) ) {
        redirect( base_url( 'auth/sendactivation' ) );
        exit;
    }
}

function serp_report( $db, $analyze_model, $domain, $user_id, $search_engine = '', $return_html = false ){

  $domain_id = $domain['id'];
  $domain_url = $domain['url'];

  $serp_data = $analyze_model->getSerpResult( $domain_id, $search_engine );

  $db->flush_cache();
  $db->select( 'max(updated_date) as updated_date' );
  $db->from( 'serp' );
  $db->where( 'domain_id=', $domain_id );
  $query = $db->get();
  $date_result = $query->row_array();
  $show_date = $date_result['updated_date'] ? date('d F Y', strtotime( $date_result['updated_date'] ) ) : '-';

  $stat_keys = array( 'top1_latest', 'top3_latest','top5_latest', 'top10_latest', 'top20_latest', 'top30_latest' ,'ranked_latest', 'notranked_latest' );
  $stats = $analyze_model->getKeywordPositionComparitiveStats( $user_id, $domain_id, $search_engine );

  foreach ( $stat_keys as $key => $val) {
    $stats[ $val ] = isset( $stats[ $val ] ) ? $stats[ $val ] : 'n/a';
  }

  if( $return_html ){
    ob_start();
  }

  ?>
  <div class="cf">

    <?php /* ?>
    <div class="fl w-100 w-30-l">
            <div class="content-column-inner">
                <span class="f7">DOMAIN</span>
                <p><a href="<?php echo $domain_url; ?>" title="" target="_blank" class="no-underline underline-hover white f5 fw5"><?php echo $domain['url']; ?></a></p>
            </div>

            <hr class="dn-l" style="margin-top:0; margin-bottom:0;" />
        </div>
        <?php */ ?>

        <div class="fl w-100 tc">

          <div class="fl w-100 w-25-ns">
              <div class="content-column-inner">
                <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-green"><?php echo $stats['top1_latest']; ?></span>
            <span class="stat-label">FIRST PLACE</span>
          </div>
          <hr>
          <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-green"><?php echo $stats['top3_latest']; ?></span>
            <span class="stat-label">IN TOP 3</span>
          </div>
              </div>

              <hr class="dn-ns" style="margin-top:0; margin-bottom:0;"/>
          </div>

          <div class="fl w-100 w-25-ns">
              <div class="content-column-inner">
                <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-yellow"><?php echo $stats['top5_latest']; ?></span>
            <span class="stat-label">IN TOP 5</span>
          </div>
          <hr>
          <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-yellow"><?php echo $stats['top10_latest']; ?></span>
            <span class="stat-label">IN TOP 10</span>
          </div>
              </div>

              <hr class="dn-ns" style="margin-top:0; margin-bottom:0;"/>
          </div>

          <div class="fl w-100 w-25-ns">
              <div class="content-column-inner">
                <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-orange"><?php echo $stats['top20_latest']; ?></span>
            <span class="stat-label">IN TOP 20</span>
          </div>
          <hr>
          <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-orange"><?php echo $stats['top30_latest']; ?></span>
            <span class="stat-label">IN TOP 30</span>
          </div>

              </div>

              <hr class="dn-ns" style="margin-top:0; margin-bottom:0;"/>
          </div>

          <div class="fl w-100 w-25-ns">
              <div class="content-column-inner">
                <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-action-color"><?php echo $stats['ranked_latest']; ?></span>
            <span class="stat-label">RANKED</span>
          </div>
          <hr>
          <div class="bold-stat-num">
            <span class="stat-num campaignsio-admin-action-color"><?php echo $stats['notranked_latest']; ?></span>
            <span class="stat-label">NOT RANKED</span>
          </div>
              </div>
          </div>

        </div>

    </div>

  <hr/>

  <div class="list-table-wrap">
    <table data-table-id="serp-info" data-rows-per-page="100" class="serps-info-table filter-table table table-hover display pb-12">
        <thead>
          <tr>
            <th class="tl">KEYWORDS AS OF <i><?php echo $show_date; ?></i></th>
            <th>POSITION</th>
            <th>GOOGLE VOLUME</th>
            <th>COST</th>
            <th>COMPETITION IN PPC</th>
            <th>RESULTS</th>
            <th>GWT</th>
          </tr>
        </thead>
        <tbody><?php
          if( ! empty( $serp_data ) ){
            foreach ( $serp_data as $key => $val ){ ?>
              <tr>
                <td class="tl"><?php echo $val['keyword']; ?></td>
                <td><?php echo $val['position'] ? $val['position'] : "-"; ?></td>
                <td><?php echo $val['volume_google'] ? $val['volume_google'] : "-"; ?></td>
                <td><?php echo $val['cost'] ? $val['cost'] : "-"; ?></td>
                <td><?php echo $val['competition_in_ppc'] ? $val['competition_in_ppc'] : "-"; ?></td>
                <td><?php echo $val['results'] ? $val['results'] : "-"; ?></td>
                <td><?php
                                if( 'GWT' === $val['type'] ){ ?><i class="material-icons campaignsio-admin-green">&#xE5CA;</i><?php }
                                else{ ?><i class="material-icons campaignsio-admin-orange">&#xE5CD;</i><?php } ?>
                            </td>
              </tr> <?php
            }
          }
          else{ ?>
            <tr>
              <td colspan="7">No keywords found</td>
            </tr>
            <?php
          }?>
        </tbody>
      </table>
    </div><?php

    if( $return_html ){
      $ret = ob_get_contents();
      ob_end_clean();
  }
  else{
    $ret = true;
  }

    return $ret;
}

function serp_edit_keywords_table( $edit_keywords = array(), $return_html = false ){

  if( $return_html ){
    ob_start();
  }
  ?>
  <table data-table-id="serp-edit-keywords" data-rows-per-page="100" class="table table-hover display pb-12 edit-keywords-table filter-table">
    <thead>
      <tr>
        <th>KEYWORDS</th>
        <th>ACTIONS</th>
      </tr>
    </thead>
    <tbody>
      <?php
        if( ! empty( $edit_keywords ) ){
          foreach( $edit_keywords as $val ) { ?>
            <tr>
              <td ><?php echo $val['name']; ?></td>
              <td><a href="#" title="" data-keyword="<?php echo $val['name']; ?>" class="serp-keyword-remove dib mv1 mh1 f7 btn-lines btn-dark-br0 no-underline pv1 pr2 pl1-l br1"><i class="material-icons">&#xE872;</i><small class="fw7">REMOVE</small></a></td>
            </tr>
            <?php
          }
        }
        else{ ?>
              <tr>
                <td colspan="2">No keywords found</td>
              </tr>
              <?php
            }
        ?>
    </tbody>
  </table> 

  <?php

  if( $return_html ){
      $ret = ob_get_contents();
      ob_end_clean();
  }
  else{
    $ret = true;
  }

    return $ret;
}

function serp_keywords_table_html( $data = array(), $type = 'query' ){

  ob_start();

  $keywords_table = false;
  switch( $type ){
    case 'page':
      $value_title = 'URL ADDRESS';
      break;
    case 'country':
      $value_title = 'COUNTRY';
      break;
    case 'device':
      $value_title = 'DEVICE';
      break;
    case 'query':
    default:
      $value_title = 'KEYWORD';
      $keywords_table = true;
  }

  ?>
  <div class="table-wrap">
    <div class="table-responsive">
      <table id="datable_1" data-table-id="serp-webmaster-tools" data-rows-per-page="100" class="webmaster-tools-table table table-hover display  pb-30" >
        <thead>
          <tr>
            <?php if( $keywords_table ){ ?>
            <th class="nowrap" data-sortable="false"><!-- <label class="pointer"><input type="checkbox" /> CHECK ALL</label> --></th><?php
            } ?>
            <th class="tl"><?php echo $value_title; ?></th>
            <th>CLICKS</th>
            <th>CTR</th>
            <th>IMPRESSION</th>
            <th>POSITION</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <?php if( $keywords_table ){ ?>
              <th class="nowrap" data-sortable="false"><!-- <label class="pointer"><input type="checkbox" /> CHECK ALL</label> --></th>
            <?php } ?>
            <th><?php echo $value_title; ?></th>
            <th>CLICKS</th>
            <th>CTR</th>
            <th>IMPRESSION</th>
            <th>POSITION</th>
          </tr>
        </tfoot>
        <tbody>
          <tr>
            <?php
              if( ! empty( $data ) ){
                foreach( $data as $val ) { ?>
                  <tr>
                    <?php if( $keywords_table ){ ?>
                      <td><input type="checkbox" name="add_webmaster_kwewords[]" value="<?php echo $val['keyword']; ?>" /></td>
                      <?php } ?>
                    <td><?php echo $val['keyword']; ?></td>
                    <td><?php echo $val['clicks']; ?></td>
                    <td><?php echo $val['ctr']; ?>%</td>
                    <td><?php echo $val['impressions']; ?></td>
                    <td><?php echo $val['position']; ?></td>
                  </tr>
                      <?php
                    }
              }
              else{ ?>
                      <tr class="no-records">
                          <td colspan="6">No keywords found</td>
                      </tr> <?php
                  } ?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <?php
  $ret = ob_get_contents();
  ob_end_clean();
  return $ret;
}

function single_domain_gtmetrix_content( $url, $gtmetrix ){
  $screenshot_src = '';
  if( isset( $gtmetrix['metrix']['report_url'] ) && '' !== trim( $gtmetrix['metrix']['report_url'] ) ){
    $screenshot_src = ' src="'.$gtmetrix['metrix']['report_url'] . '/screenshot.jpg"';
  }
  ?>
  <?php
    $loadTimeValueHtml = '<small><span class="time-val"></span><small class="time-unit">n/a</small></small>';
    $pageSizeValueHtml = '<small><span class="size-val"></span><small class="size-unit">n/a</small></small>';
    $requestsValueHtml = '<small class="requests-val">n/a</small>';
    $pageSpeedValueHtml = '<small><span class="speed-grade"></span> <small>(<span class="speed-val">n/a</span>)</small></small>';
    $ySlowScoreValueHtml = '<small><span class="score-grade"></span> <small>(<span class="score-val">n/a</span>)</small></small>';

    if( isset( $gtmetrix['metrix']['page_load_time'] ) ){
      $loadTimeValueHtml = round( $gtmetrix['metrix']['page_load_time'] / 1000 * 100 ) / 100 . 's';
    }

    if( isset( $gtmetrix['metrix']['page_bytes'] ) ){
      $formated_bytes = format_bytes( $gtmetrix['metrix']['page_bytes'] );
      $pageSizeValueHtml = $formated_bytes['val'] . ' ' . $formated_bytes['unit'];
    }

    if( isset( $gtmetrix['metrix']['page_elements'] ) ){
      $requestsValueHtml = $gtmetrix['metrix']['page_elements'];
    }

    if( isset( $gtmetrix['metrix']['pagespeed_score'] ) ){

      if ($gtmetrix['metrix']['pagespeed_score'] < 60) {
        $pagespeed_score_classname = ' campaignsio-admin-orange';
        }
        else if ($gtmetrix['metrix']['pagespeed_score'] >= 60 && $gtmetrix['metrix']['pagespeed_score'] < 80) {
          $pagespeed_score_classname = ' campaignsio-admin-yellow';
        }
        else {
          $pagespeed_score_classname = ' campaignsio-admin-green';
        }

      $pageSpeedValueHtml = get_grade( $gtmetrix['metrix']['pagespeed_score'] ) . ' (' . $gtmetrix['metrix']['pagespeed_score'] . '%)';
    }

    if( isset( $gtmetrix['metrix']['yslow_score'] ) ){

      if ($gtmetrix['metrix']['yslow_score'] < 60) {
        $yslow_score_classname = ' campaignsio-admin-orange';
        }
        else if ($gtmetrix['metrix']['yslow_score'] >= 60 && $gtmetrix['metrix']['yslow_score'] < 80) {
          $yslow_score_classname = ' campaignsio-admin-yellow';
        }
        else {
          $yslow_score_classname = ' campaignsio-admin-green';
        }

      $ySlowScoreValueHtml = get_grade( $gtmetrix['metrix']['yslow_score'] ) .' (' . $gtmetrix['metrix']['yslow_score'] . '%)';
    }

  ?>
  <div class="panel-heading">
    <?php if( isset( $page_data_last_update ) && $page_data_last_update ){ ?>
      <span>
        <?php echo $page_data_last_update; ?>&nbsp;&nbsp;
        <?php if( isset( $page_data_update_now_link ) && $page_data_update_now_link ){ ?>
        <a href="<?php echo $page_data_update_now_link; ?>"><button class="btn btn-xs btn-primary">UPDATE STATS</button></a>
      </span>
      <?php } ?>
    <?php } ?>

    <?php
      if ($gtmetrix['avg_time']) {
        echo '<span class="block text-center"><span class="btn btn-xs btn-success btn-rounded font-12 text-center mb-5 pb-0">' . number_format($gtmetrix['avg_time'], 2, '.', ',') . ' seconds Real World Load Time ' . '</span></span>';
      }
    ?>
  <span class="block font-12 text-center mb-5 pb-0"><?php echo $gtmetrix['date']; ?></span>
  
    <div class="pull-left" style="max-width: 50%;">
      <img <?php echo $screenshot_src; ?> width="180px" style="display: block; margin-top: 5px;">
    </div>
    <div class="pull-right">
      <h6 class="panel-title txt-dark text-right">GT METRIX</h6>
      <span class="btn btn-primary btn-xs pull-right">
        <span>Load time</span>
        <span> <strong><?php echo $loadTimeValueHtml; ?></strong></span>
      </span>
    </div>
  </div>
  <div  class="panel-wrapper collapse in display-inline">
    <div class="panel-body pb-0">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-sm-6 col-lg-6">
              <span class="btn btn-danger btn-xs btn-block">
                <span>Page size</span>
                <span> <strong><?php echo $pageSizeValueHtml; ?></strong></span>
              </span>
              <span class="btn btn-danger btn-xs btn-block">
                <span>Requests</span>
                <span> <strong><?php echo $requestsValueHtml; ?></strong></span>
              </span>
            </div>
            <div class="col-sm-6 col-lg-6">
              <span class="btn <?php if ( get_grade( $gtmetrix['metrix']['pagespeed_score'] ) == 'A') {
                    echo "btn-success";
                  } elseif ( get_grade( $gtmetrix['metrix']['pagespeed_score'] ) == 'B') {
                    echo "btn-success";
                  } elseif ( get_grade( $gtmetrix['metrix']['pagespeed_score'] ) == 'C') {
                    echo "btn-warning";
                  } elseif ( get_grade( $gtmetrix['metrix']['pagespeed_score'] ) == 'D') {
                    echo "btn-warning";
                  } else {
                    echo "btn-danger";
                  }?> btn-xs btn-block">
                <span>Page speed</span>
                <span> <strong><?php echo $pageSpeedValueHtml; ?></strong></span>
              </span>
              <span class="btn <?php if ( get_grade( $gtmetrix['metrix']['yslow_score'] ) == 'A') {
                    echo "btn-success";
                  } elseif ( get_grade( $gtmetrix['metrix']['yslow_score'] ) == 'B') {
                    echo "btn-success";
                  } elseif ( get_grade( $gtmetrix['metrix']['yslow_score'] ) == 'C') {
                    echo "btn-warning";
                  } elseif ( get_grade( $gtmetrix['metrix']['yslow_score'] ) == 'D') {
                    echo "btn-warning";
                  } else {
                    echo "btn-danger";
                  }?> btn-xs btn-block">
                <span>Y Slow score</span>
                <span> <strong><?php echo $ySlowScoreValueHtml; ?></strong></span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
}

function heatmap_form( $domain_id, $heatmap_id = 0, $heatmap_data = array() ){

  if( 0 === $heatmap_id ){  // @note: Add new heatmap.
    $form_action = base_url('domains/' . $domain_id . '/heatmaps/add');
    $name_value = '';
    $embed_url_value = '';
    $save_label = "SAVE HEATMAP";
    $cancel_link = base_url('domains/' . $domain_id . '/heatmaps');
  }
  else{ // @note: Edit heatmap.
    $form_action = base_url('domains/' . $domain_id . '/heatmaps/' . $heatmap_id . '/edit');
    $name_value = $heatmap_data['page_name'];
    $embed_url_value = $heatmap_data['embed_url'];
    $save_label = "UPDATE HEATMAP";
    $cancel_link = base_url('domains/' . $domain_id . '/heatmaps/list');
  }

  ?>
  <div class="content-row">
      <div class="content-column w-100">
          <div class="content-column-main">
              <div class="title">
                  <div class="left-pos">
                      <h3>EDIT HEATMAP</h3>
                  </div>
              </div>
              <div class="content-column-inner">
                  <form action="<?php echo $form_action; ?>" method="post" class="edit-profile-form cf" style="width: 100%;">
                      <div class="dib w-100 cf">
                          <div class="field">
                              <label for="hotjar_page_name">Page name:</label>
                              <input type="text" name="hotjar_page_name" placeholder="Page name" value="<?php echo $name_value; ?>" />
                          </div>
                      </div>
                      <div class="dib w-100 cf">
                          <div class="field">
                              <label for="hotjar_url">Hotjar URL:</label>
                              <input type="text" name="hotjar_url" value="<?php echo $embed_url_value; ?>" placeholder="Hotjar URL">
                          </div>
                      </div>
                      <hr class="w-100">
                      <button type="submit" class="dib btn-color no-underline pointer ba f7 fw5 lh-solid pv3 ph4 br1"><span class="white"><?php echo $save_label; ?></span>
                      </button>

                      <a href="<?php echo $cancel_link; ?>" class="fr dib btn-lines no-underline pointer ba f7 fw5 lh-solid pv3 ph4 br1"><span class="white">CANCEL</span></a>

                      <br>
                  </form>
              </div>
          </div>
      </div>
  </div>
  <?php
}


function seoreporting_navigation($current_page)
{
  $items = array();
  $items[] = array(
        'id' => 'seoreporting-project',
        'title' => 'Projects',
        'icon' => '&#xE85D',
        'link' => base_url('seoreporting/getprojects')
    );
    $items[] = array(
        'id' => 'seoreporting-task',
        'title' => 'Tasks',
        'icon' => '&#xE85D',
        'link' => base_url('seoreporting/listjobs')
    );
    ?>
    <nav ><?php
      foreach ($items as $k => $v) {
        $classAttr = $current_page === $v['id'] ? ' class="active"' : '';
        ?>
            <a href="<?php echo $v['link']; ?>" title="<?php echo $v['title']; ?>" <?php echo $classAttr; ?>>
                <?php echo $v['title']; ?>
            </a><?php
      } ?>

    </nav>
  <?php
}
function domains_competition_results( $data ){
  ?><div class="content-column-inner" style="padding-top:0.5rem; padding-bottom:0;">
    <hr/>
        <div class="list-table-wrap">

          <table class="list-table mt3 collapse tc">
                <thead>
                  <tr>
                    <th>DOMAINS</th>
          <th>UNIQUE</th>
          <th>TOTAL NUMBER</th>
                  </tr>
                </thead>
                <tbody>
            <tr>
              <td><?php echo $data['domain1']; ?></td>
                <td><?php echo $data['domain1_unique']; ?></td>
                <td><?php echo $data['domain1_total']; ?></td>
            </tr>
            <tr>
              <td><?php echo $data['domain2']; ?></td>
              <td><?php echo $data['domain2_unique']; ?></td>
              <td><?php echo $data['domain2_total']; ?></td>
            </tr>
            <tr>
              <td><?php echo $data['domain1'] . ' , ' . $data['domain2']; ?> <small>(COMMON KEYWORDS)</small></td>
                <td></td>
                <td><?php echo $data['total_common_keyword']; ?></td>
            </tr>
        </tbody>
            </table>
      </div>
      <hr/>
      <div class="list-table-wrap">

          <table class="list-table mt3 collapse tc">
                <thead>
                  <tr>
          <th>#</th>
                <th>KEYWORDS</th>
          <th>POS. <?php echo $domain1; ?></th>
          <th>POS. <?php echo $domain2; ?></th>
          <th>GOOGLE VOLUME</th>
          <th>CPC ($)</th>
          <th>COMPETITION IN PPC</th>
                  </tr>
                </thead>
                <tbody> <?php
                  if( empty( $data['result'] ) ){ ?>
                    <tr>
                      <td colspan="7">No available data</td>
                    </tr><?php
                  }
                  else{
                    foreach( $data['result'] as $key => $val ){ ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td><?php echo $val['keyword']; ?></td>
                      <td><?php echo $val['position1']; ?></td>
                      <td><?php echo $val['position2']; ?></td>
                      <td><?php echo $val['concurrency']; ?></td>
                      <td><?php echo $val['cost']; ?></td>
                      <td><?php echo $val['region_queries_count']; ?></td>
                    </tr> <?php
              }
             }?>
        </tbody>
            </table>
      </div>
    </div><?php

}
