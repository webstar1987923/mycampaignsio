<?php
if( ! function_exists('checkbox_component') ){
	function checkbox_component($name='', $checked=false){ 
	    $id = '' !== $name ? 'id-' . $name : 'tmp-id-' . substr(uniqid(), -4);
	    ?>
	        <input id="<?php echo $id; ?>" class="js-switch js-switch-1" data-color="#469408" data-secondary-color="#ea6c41" data-size="small" type="checkbox" name="<?php echo $name; ?>" <?php echo $checked ? 'checked' : ''; ?> value="1"/>
	        <label for="<?php echo $id; ?>" class=""></label> <?php
	}
}