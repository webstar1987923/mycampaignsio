<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );

if( ! class_exists('CIUIS_Controller') ){
	require_once 'CIUIS_Controller.php';
}

use Facebook\Facebook as FB;
use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FacebookBatchResponse;
use Facebook\Helpers\FacebookCanvasHelper;
use Facebook\Helpers\FacebookJavaScriptHelper;
use Facebook\Helpers\FacebookPageTabHelper;
use Facebook\Helpers\FacebookRedirectLoginHelper;

class Facebook extends CIUIS_Controller {

	public function __construct() {
		parent::__construct();
		// Load form helper library
		$this->load->helper('form');
		$this->load->model('serp/sessions_model');
		// Load session library
		$this->load->library('session');
	}

	function index($id) {

		$this->session->set_userdata('domainID', $id);

		$fb = new FB([
		'app_id' => '212976822440813',
		'app_secret' => 'b5667f1dea5bbf41b1756d896c508249',
		'default_graph_version' => 'v2.2',
		]);
		  
		$helper = $fb->getRedirectLoginHelper();

		$permissions = ['pages_show_list', 'manage_pages', 'read_insights'];

		if( preg_match('/localhost/i', $_SERVER['HTTP_HOST']) || preg_match('/campaignsio/i', $_SERVER['HTTP_HOST']) ){
			$url = 'http://localhost/mycampaignsio/facebook/callback';
		}else{
			$url = 'http://my.campaigns.io/facebook/callback';
		}

		$loginUrl = $helper->getLoginUrl($url, $permissions);

		// echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
		echo $loginUrl;
		redirect($loginUrl);
	}

	function callback() {
		$fb = new FB([
			'app_id' => '212976822440813', // Replace {app-id} with your app id
			'app_secret' => 'b5667f1dea5bbf41b1756d896c508249',
			'default_graph_version' => 'v2.2',
		]);
		
		$helper = $fb->getRedirectLoginHelper();
		
		try {
			$accessToken = $helper->getAccessToken();
		} catch(FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		if (! isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo "Error: " . $helper->getError() . "\n";
				echo "Error Code: " . $helper->getErrorCode() . "\n";
				echo "Error Reason: " . $helper->getErrorReason() . "\n";
				echo "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}
		
		// Logged in
		echo '<h3>Access Token</h3>';
		var_dump($accessToken->getValue());	
		
		try {
			$response = $fb->get('/me/accounts', $accessToken);
		} catch(FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
		exit;
		} catch(FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		echo '<h3>Page Id</h3>';
		$body = $response->getBody();
		$data = json_decode($body, true);
		$pages = $data['data'];
		$pageData = [];
		$pageData['pageData'] = [];
		$pageData['accessToken'] = $accessToken;
		foreach ($pages as $key => $page) {
			$pageData['pageData'][$page['id']] = $page['name'];
		}
		// var_dump($pageID);	
		var_dump(count($pages));
		if (count($pages) == 0) {
			redirect('https://www.facebook.com/');
		}
		$this->session->set_userdata($pageData);
		
		if (isset($_SESSION['domainID'])) {
			$id = $_SESSION['domainID'];
			unset($_SESSION['domainID']);
			$this->session->set_userdata('fb_oauth_login' . $id, true);
			$this->sessions_model->store($_SESSION['user_id'], $id);
			redirect('domains/' . $id . '/analytics');
		}
	}

	function test() {
		if (isset($_SESSION['accessToken'])) {
			var_dump($_SESSION['accessToken']);
		}

		if (isset($_SESSION['domainID'])) {
			var_dump($_SESSION['domainID']);
		}
	}

	function getData($id) {
		if (isset($_POST) && count($_POST) > 0) {
			$pageID = $this->input->post('pageID');
			$fb = new FB([
				'app_id' => '212976822440813', // Replace {app-id} with your app id
				'app_secret' => 'b5667f1dea5bbf41b1756d896c508249',
				'default_graph_version' => 'v2.2',
			]);

			if (isset($_SESSION['accessToken'])) {
				$accessToken = $_SESSION['accessToken'];
			}

			$this->session->set_userdata('facebook_tab' . $id, true);

			try {
				$response = $fb->get('/' . $pageID, $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				// redirect('https://www.facebook.com/' . $pageID);
				redirect($_SERVER['HTTP_REFERER']);
				exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				// redirect('https://www.facebook.com/' . $pageID);
				redirect($_SERVER['HTTP_REFERER']);
				exit;
			}

			$accessToken = $response->getAccessToken();

			try {
				$response = $fb->get('/' . $pageID . '/insights?pretty=0&since=1520755200&until=1520924400&metric=page_views_total', $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				// redirect('https://www.facebook.com/' . $pageID);
				redirect($_SERVER['HTTP_REFERER']);
				exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				// redirect('https://www.facebook.com/' . $pageID);
				redirect($_SERVER['HTTP_REFERER']);
				exit;
			}

			// Logged in
			echo '<h3>Page Views</h3>';
			$graphNode = $response->getGraphEdge();
			var_dump($response->getBody());

			try {
				$response = $fb->get('/' . $pageID . '/insights?pretty=0&since=1520755200&until=1520924400&metric=page_views_logged_in_total', $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
			exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			// Logged in
			echo "<h3>The number of times a Page's profile has been viewed by people logged in to Facebook.</h3>";
			$graphNode = $response->getGraphEdge();
			var_dump($response->getBody());

			try {
				$response = $fb->get('/' . $pageID . '/insights?pretty=0&since=1520755200&until=1520924400&metric=page_total_actions', $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
			exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			// Logged in
			echo "<h3>The number of clicks on your Page's contact info and call-to-action button.</h3>";
			$graphNode = $response->getGraphEdge();
			var_dump($response->getBody());

			try {
				$response = $fb->get('/' . $pageID . '/insights?pretty=0&since=1520755200&until=1520924400&metric=page_fans_gender_age', $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
			exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			// Logged in
			echo "<h3>Aggregated demographic data about the people who like your Page based on the age and gender information they provide in their user profiles.</h3>";
			$graphNode = $response->getGraphEdge();
			var_dump($response->getBody());

			try {
				$response = $fb->get('/' . $pageID . '/insights?pretty=0&since=1520755200&until=1520924400&metric=page_fans_country', $accessToken);
			} catch(FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
			exit;
			} catch(FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			// Logged in
			echo "<h3>The number of people, aggregated per country, that like your Page. Only the 45 countries with the most people that like your Page are included.</h3>";
			$graphNode = $response->getGraphEdge();
			var_dump($response->getBody());
		
		}
	}

}