<?php return array (
  'lifetime' => 1525873728,
  'data' => 
  array (
    'goals' => 
    array (
    ),
    'urls' => 
    array (
      0 => 'https://www.mangobikes.com',
    ),
    'hosts' => 
    array (
      0 => 'www.mangobikes.com',
    ),
    'exclude_unknown_urls' => '0',
    'excluded_ips' => 
    array (
    ),
    'excluded_parameters' => 
    array (
    ),
    'excluded_user_agents' => 
    array (
    ),
    'keep_url_fragment' => false,
    'sitesearch' => '1',
    'sitesearch_keyword_parameters' => 
    array (
      0 => 'q',
      1 => 'query',
      2 => 's',
      3 => 'search',
      4 => 'searchword',
      5 => 'k',
      6 => 'keyword',
    ),
    'sitesearch_category_parameters' => 
    array (
      0 => '',
    ),
    'timezone' => 'Europe/London',
    'ts_created' => '2018-05-08 00:00:00',
    'hsr' => 
    array (
    ),
  ),
);