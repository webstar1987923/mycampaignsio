<?php return array (
  'lifetime' => 1525916629,
  'data' => 
  array (
    'PluginDBStatsMetadata' => 
    array (
      'description' => 'DBStats_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleCommandMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a console command.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleCommand',
    ),
    'PluginExampleReportMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to define and display a data report.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleReport',
    ),
    'PluginExampleSettingsPluginMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to define and how to access plugin settings.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleSettingsPlugin',
    ),
    'PluginExampleThemeMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: example of how to create a simple Theme.',
      'homepage' => '',
      'authors' => 
      array (
        0 => 
        array (
          'name' => '',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => true,
      'require' => 
      array (
        'piwik' => '>=3.0.0-b1,<4.0.0-b1',
      ),
      'name' => 'ExampleTheme',
      'stylesheet' => 'stylesheets/theme.less',
      'keywords' => 
      array (
      ),
      'support' => 
      array (
        'email' => '',
        'issues' => '',
        'forum' => '',
        'irc' => '',
        'source' => '',
        'docs' => '',
        'wiki' => '',
        'rss' => '',
      ),
    ),
    'PluginExampleTrackerMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to track additional custom data creating new database table columns.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleTracker',
    ),
    'PluginExampleUIMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to display data tables, graphs, and the UI framework.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'http://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0.1',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.3.0',
      ),
      'name' => 'ExampleUI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'framework',
        2 => 'platform',
        3 => 'ui',
        4 => 'visualization',
      ),
    ),
    'PluginExampleVisualizationMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a new custom data visualization.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'The Matomo Team',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleVisualization',
      'keywords' => 
      array (
        0 => 'SimpleTable',
      ),
    ),
    'PluginFeedbackMetadata' => 
    array (
      'description' => 'Feedback_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMobileAppMeasurableMetadata' => 
    array (
      'description' => 'Analytics for Mobile: lets you measure and analyze Mobile Apps with an optimized perspective of your mobile data.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'MobileAppMeasurable',
    ),
    'PluginProviderMetadata' => 
    array (
      'description' => 'Provider_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCorePluginsAdminMetadata' => 
    array (
      'description' => 'CorePluginsAdmin_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreAdminHomeMetadata' => 
    array (
      'description' => 'CoreAdminHome_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreHomeMetadata' => 
    array (
      'description' => 'CoreHome_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginWebsiteMeasurableMetadata' => 
    array (
      'description' => 'Analytics for the web: lets you measure and analyze Websites.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'WebsiteMeasurable',
    ),
    'PluginDiagnosticsMetadata' => 
    array (
      'description' => 'Performs diagnostics to check that Matomo is installed and runs correctly.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreVisualizationsMetadata' => 
    array (
      'description' => 'CoreVisualizations_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginProxyMetadata' => 
    array (
      'description' => 'Proxy services',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAPIMetadata' => 
    array (
      'description' => 'API_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExamplePluginMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create widgets, menus, scheduled tasks, a custom archiver, plugin tests, and an AngularJS component.',
      'homepage' => '',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.0.0-b1,<4.0.0-b1',
      ),
      'name' => 'ExamplePlugin',
      'support' => 
      array (
        'email' => '',
        'issues' => '',
        'forum' => '',
        'irc' => '',
        'wiki' => '',
        'source' => '',
        'docs' => '',
        'rss' => '',
      ),
      'keywords' => 
      array (
      ),
    ),
    'PluginWidgetizeMetadata' => 
    array (
      'description' => 'Widgetize_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTransitionsMetadata' => 
    array (
      'description' => 'Transitions_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLanguagesManagerMetadata' => 
    array (
      'description' => 'LanguagesManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginActionsMetadata' => 
    array (
      'description' => 'Actions_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDashboardMetadata' => 
    array (
      'description' => 'Dashboard_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMultiSitesMetadata' => 
    array (
      'description' => 'MultiSites_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginReferrersMetadata' => 
    array (
      'description' => 'Referrers_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserLanguageMetadata' => 
    array (
      'description' => 'UserLanguage_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicesDetectionMetadata' => 
    array (
      'description' => 'DevicesDetection_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginGoalsMetadata' => 
    array (
      'description' => 'Goals_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEcommerceMetadata' => 
    array (
      'description' => 'Ecommerce_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'Ecommerce',
    ),
    'PluginSEOMetadata' => 
    array (
      'description' => 'SEO_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEventsMetadata' => 
    array (
      'description' => 'Events_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMetadata' => 
    array (
      'description' => 'UserCountry_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginGeoIp2Metadata' => 
    array (
      'description' => 'GeoIp2_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitsSummaryMetadata' => 
    array (
      'description' => 'VisitsSummary_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitFrequencyMetadata' => 
    array (
      'description' => 'VisitFrequency_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitTimeMetadata' => 
    array (
      'description' => 'VisitTime_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitorInterestMetadata' => 
    array (
      'description' => 'VisitorInterest_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleAPIMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create an API for your plugin to let your users export your data in multiple formats.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleAPI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'api',
      ),
    ),
    'PluginRssWidgetMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a new widget that displays a user submitted RSS feed.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'RssWidget',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'feed',
        2 => 'widget',
      ),
    ),
    'PluginMonologMetadata' => 
    array (
      'description' => 'Adds logging capabilities to Matomo.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLoginMetadata' => 
    array (
      'description' => 'Login_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUsersManagerMetadata' => 
    array (
      'description' => 'UsersManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSitesManagerMetadata' => 
    array (
      'description' => 'SitesManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInstallationMetadata' => 
    array (
      'description' => 'Installation_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreUpdaterMetadata' => 
    array (
      'description' => 'CoreUpdater_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreConsoleMetadata' => 
    array (
      'description' => 'CoreConsole_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginScheduledReportsMetadata' => 
    array (
      'description' => 'ScheduledReports_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMapMetadata' => 
    array (
      'description' => 'UserCountryMap_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLiveMetadata' => 
    array (
      'description' => 'Live_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCustomVariablesMetadata' => 
    array (
      'description' => 'CustomVariables_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginPrivacyManagerMetadata' => 
    array (
      'description' => 'PrivacyManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginImageGraphMetadata' => 
    array (
      'description' => 'ImageGraph_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAnnotationsMetadata' => 
    array (
      'description' => 'Annotations_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMobileMessagingMetadata' => 
    array (
      'description' => 'MobileMessaging_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginOverlayMetadata' => 
    array (
      'description' => 'Overlay_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSegmentEditorMetadata' => 
    array (
      'description' => 'SegmentEditor_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInsightsMetadata' => 
    array (
      'description' => 'Insights_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMorpheusMetadata' => 
    array (
      'description' => 'Morpheus is the default theme of Matomo 3 designed to help you focus on your analytics. In Greek mythology, Morpheus is the God of dreams. In the Matrix movie, Morpheus is the leader of the rebel forces who fight to awaken humans from a dreamlike reality called The Matrix. ',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => true,
      'require' => 
      array (
      ),
      'name' => 'Morpheus',
      'stylesheet' => 'stylesheets/main.less',
    ),
    'PluginContentsMetadata' => 
    array (
      'description' => 'Contents_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginBulkTrackingMetadata' => 
    array (
      'description' => 'Provides ability to send several Tracking API requests in one bulk request. Makes importing a lot of data in Matomo faster.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'BulkTracking',
    ),
    'PluginResolutionMetadata' => 
    array (
      'description' => 'Resolution_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicePluginsMetadata' => 
    array (
      'description' => 'DevicePlugins_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginHeartbeatMetadata' => 
    array (
      'description' => 'Handles ping tracker requests.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginIntlMetadata' => 
    array (
      'description' => 'Intl_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMarketplaceMetadata' => 
    array (
      'description' => 'Marketplace_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginProfessionalServicesMetadata' => 
    array (
      'description' => 'Provides widgets to learn about Professional services and products for Matomo.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ProfessionalServices',
    ),
    'PluginUserIdMetadata' => 
    array (
      'description' => 'UserId_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCustomPiwikJsMetadata' => 
    array (
      'description' => 'CustomPiwikJs_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.5.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginHeatmapSessionRecordingMetadata' => 
    array (
      'description' => 'Truly understand your visitors by seeing where they click, hover, type and scroll. Replay their actions in a video and ultimately increase conversions.',
      'homepage' => 'https://www.heatmap-analytics.com',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'InnoCraft',
          'email' => 'contact@innocraft.com',
          'homepage' => 'https://www.innocraft.com',
        ),
      ),
      'license' => 'InnoCraft EULA',
      'version' => '3.2.2',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.0.4-rc1,<4.0.0-b1',
      ),
      'name' => 'HeatmapSessionRecording',
      'price' => 
      array (
        'base' => 200,
      ),
      'archive' => 
      array (
        'exclude' => 
        array (
          0 => '/tracker.js',
        ),
      ),
      'preview' => 
      array (
        'video_url' => 'https://www.youtube.com/embed/AUSXjH8U9fk',
      ),
      'keywords' => 
      array (
        0 => 'heatmap',
        1 => 'session recording',
        2 => 'session',
        3 => 'recording',
        4 => 'move',
        5 => 'scroll',
        6 => 'hover',
        7 => 'click',
        8 => 'user',
        9 => 'visitor',
        10 => 'video',
        11 => 'visit',
      ),
      'license_file' => '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/LICENSE',
    ),
    'PluginMultiChannelConversionAttributionMetadata' => 
    array (
      'description' => 'Get a clear understanding of how much credit each of your marketing channel is actually responsible for to shift your marketing efforts wisely.',
      'homepage' => 'https://www.innocraft.com',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'InnoCraft',
          'email' => 'contact@innocraft.com',
          'homepage' => 'https://www.innocraft.com',
        ),
      ),
      'license' => 'InnoCraft EULA',
      'version' => '3.0.1',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.0.0,<4.0.0-b1',
      ),
      'name' => 'MultiChannelConversionAttribution',
      'price' => 
      array (
        'base' => 75,
      ),
      'archive' => 
      array (
        'exclude' => 
        array (
          0 => '/CHANGELOG.md',
        ),
      ),
      'keywords' => 
      array (
        0 => 'attribution',
        1 => 'models',
        2 => 'channels',
        3 => 'referrer',
        4 => 'referer',
        5 => 'conversion',
        6 => 'traffic',
        7 => 'ads',
      ),
      'license_file' => '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/LICENSE',
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/IdSite.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\IdSite',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/UserId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\UserId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalBuyer',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalConverted.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalConverted',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitIp.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitIp',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDate.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDate',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfMonth',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfWeek.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfWeek',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMonth',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionQuarter.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionQuarter',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionSecond.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionSecond',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionWeekOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionWeekOfYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceFirst',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceOrder',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorReturning.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorReturning',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitsCount.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitsCount',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitTotalTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitTotalTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalActions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalActions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalInteractions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalInteractions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalSearches.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalSearches',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Campaign.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Campaign',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Keyword.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Keyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerName.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerType.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerUrl.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Website.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Website',
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserLanguage/Columns/Language.php' => 'Piwik\\Plugins\\UserLanguage\\Columns\\Language',
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserEngine.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserEngine',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserName.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserVersion',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceBrand.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceBrand',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceModel.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceModel',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceType.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/Os.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\Os',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/OsVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\OsVersion',
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/TotalEvents.php' => 'Piwik\\Plugins\\Events\\Columns\\TotalEvents',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/City.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\City',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Country.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Country',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Latitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Latitude',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Longitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Longitude',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Provider.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Provider',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Region.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Region',
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/GeoIp2/Columns/Region.php' => 'Piwik\\Plugins\\GeoIp2\\Columns\\Region',
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalMinute.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalTime.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalTime',
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitsByDaysSinceLastVisit',
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/Base.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\Base',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableName.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableValue.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableValue',
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Columns/Resolution.php' => 'Piwik\\Plugins\\Resolution\\Columns\\Resolution',
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginCookie.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginCookie',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginDirector.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginDirector',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginFlash.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginFlash',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginGears.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginGears',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginJava.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginJava',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginPdf.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginPdf',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginQuickTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginRealPlayer',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginSilverlight',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginWindowsMedia',
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserId/Columns/UserId.php' => 'Piwik\\Plugins\\UserId\\Columns\\UserId',
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginHeatmapSessionRecordingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMultiChannelConversionAttributionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionIdPages.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionIdPages',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionType.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ClickedUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ClickedUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/DownloadUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\DownloadUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/IdPageview.php' => 'Piwik\\Plugins\\Actions\\Columns\\IdPageview',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/InteractionPosition.php' => 'Piwik\\Plugins\\Actions\\Columns\\InteractionPosition',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageGenerationTime.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageGenerationTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchKeyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchKeyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/TimeSpentRefAction.php' => 'Piwik\\Plugins\\Actions\\Columns\\TimeSpentRefAction',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventAction.php' => 'Piwik\\Plugins\\Events\\Columns\\EventAction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventCategory.php' => 'Piwik\\Plugins\\Events\\Columns\\EventCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventName.php' => 'Piwik\\Plugins\\Events\\Columns\\EventName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventUrl.php' => 'Piwik\\Plugins\\Events\\Columns\\EventUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventValue.php' => 'Piwik\\Plugins\\Events\\Columns\\EventValue',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/SearchCategory.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\SearchCategory',
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentInteraction.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentInteraction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentName.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentPiece.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentPiece',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentTarget.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentTarget',
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginHeatmapSessionRecordingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMultiChannelConversionAttributionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/IdGoal.php' => 'Piwik\\Plugins\\Goals\\Columns\\IdGoal',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Revenue.php' => 'Piwik\\Plugins\\Goals\\Columns\\Revenue',
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Items.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Items',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Order.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Order',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueDiscount.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueDiscount',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Revenue.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Revenue',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueShipping.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueShipping',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueSubtotal',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueTax.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueTax',
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginHeatmapSessionRecordingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMultiChannelConversionAttributionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'AllDimensionModifyTime' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionType.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ClickedUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/DestinationPage.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/DownloadUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageTitle.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageTitle.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/IdPageview.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/InteractionPosition.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Keyword.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/KeywordwithNoSearchResult.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageGenerationTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageTitle.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageUrl.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchCategory.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchDestinationPage.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchKeyword.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchNoResultKeyword.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/TimeSpentRefAction.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalActions.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalInteractions.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalSearches.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentInteraction.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentName.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentPiece.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentTarget.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/IdSite.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionIdPages.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionId.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerMinute.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/UserId.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionMinute.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalConverted.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitId.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitIp.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfMonth.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfWeek.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfYear.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMinute.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMonth.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionQuarter.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionSecond.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionWeekOfYear.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionYear.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorId.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorReturning.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitsCount.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitTotalTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/Base.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableName.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableValue.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/SearchCategory.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/DevicePluginColumn.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginCookie.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginDirector.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginFlash.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginGears.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginJava.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginPdf.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/Plugin.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/Base.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserEngine.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserName.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserVersion.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceBrand.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceModel.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceType.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/Os.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/OsVersion.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/BaseConversion.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Items.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Order.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductCategory.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductName.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductPrice.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductQuantity.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductSku.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueDiscount.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Revenue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueShipping.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueTax.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventAction.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventCategory.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventName.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventUrl.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventValue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/TotalEvents.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/ExampleTracker/Columns/ExampleActionDimension.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/ExampleTracker/Columns/ExampleConversionDimension.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/ExampleTracker/Columns/ExampleDimension.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/ExampleTracker/Columns/ExampleVisitDimension.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/GeoIp2/Columns/Region.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/DaysToConversion.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/IdGoal.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Revenue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/VisitsUntilConversion.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Columns/ChannelType.php' => 1525859651,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Columns/Website.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Provider/Columns/Provider.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Base.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Campaign.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Keyword.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerName.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Referrer.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerType.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerUrl.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/SearchEngine.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/SocialNetwork.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/WebsitePage.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Website.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Columns/Configuration.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Columns/Resolution.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Base.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/City.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Continent.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Country.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Latitude.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Longitude.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Provider.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Region.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserId/Columns/UserId.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserLanguage/Columns/Language.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/PagesPerVisit.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitDuration.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitsbyVisitNumber.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/DayOfTheWeek.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalMinute.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalTime.php' => 1525824954,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Metrics/AveragePageGenerationTime.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Metrics/AverageTimeOnPage.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Metrics/BounceRate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Metrics/ExitRate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/Metrics/InteractionRate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/ActionsPerVisit.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/AverageTimeOnSite.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/BounceRate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/CallableProcessedMetric.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/ConversionRate.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/EvolutionMetric.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/Metrics/VisitsPercent.php' => 1525824950,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/Metrics/AverageEventValue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/AverageOrderRevenue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/AveragePrice.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/AverageQuantity.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecificProcessedMetric.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/ProductConversionRate.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/RevenuePerVisit.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/AverageOrderRevenue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/ConversionRate.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/Conversions.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/ItemsCount.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/RevenuePerVisit.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Metrics/GoalSpecific/Revenue.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/BaseMetric.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/Browser.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/Device.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/Location.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/OperatingSystem.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/SessionTime.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/TimeOnPage.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Columns/Metrics/TimeOnSite.php' => 1525859622,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Columns/Metrics/AttributionComparison.php' => 1525859651,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Columns/Metrics/Conversion.php' => 1525859651,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Columns/Metrics/Revenue.php' => 1525859651,
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Columns/Metrics/EcommerceOnlyEvolutionMetric.php' => 1525824952,
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitFrequency/Columns/Metrics/ReturningMetric.php' => 1525824954,
    ),
    'PluginCorePluginsAdminRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreAdminHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWebsiteMeasurableRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDiagnosticsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreVisualizationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProxyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Console.php' => 'Piwik\\Plugins\\API\\Renderer\\Console',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Csv.php' => 'Piwik\\Plugins\\API\\Renderer\\Csv',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Html.php' => 'Piwik\\Plugins\\API\\Renderer\\Html',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Json2.php' => 'Piwik\\Plugins\\API\\Renderer\\Json2',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Json.php' => 'Piwik\\Plugins\\API\\Renderer\\Json',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Original.php' => 'Piwik\\Plugins\\API\\Renderer\\Original',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Php.php' => 'Piwik\\Plugins\\API\\Renderer\\Php',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Rss.php' => 'Piwik\\Plugins\\API\\Renderer\\Rss',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Tsv.php' => 'Piwik\\Plugins\\API\\Renderer\\Tsv',
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Renderer/Xml.php' => 'Piwik\\Plugins\\API\\Renderer\\Xml',
    ),
    'PluginExamplePluginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWidgetizeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTransitionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLanguagesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginActionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDashboardRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMultiSitesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginReferrersRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserLanguageRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicesDetectionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginGoalsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEcommerceRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSEORendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEventsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginGeoIp2RendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitsSummaryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitFrequencyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitTimeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitorInterestRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginExampleAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginRssWidgetRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMonologRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLoginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUsersManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSitesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInstallationRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreUpdaterRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreConsoleRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginScheduledReportsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryMapRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLiveRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCustomVariablesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginPrivacyManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginImageGraphRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAnnotationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMobileMessagingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginOverlayRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSegmentEditorRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInsightsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMorpheusRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginContentsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginBulkTrackingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginResolutionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicePluginsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginHeartbeatRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginIntlRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMarketplaceRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProfessionalServicesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserIdRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCustomPiwikJsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginHeatmapSessionRecordingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMultiChannelConversionAttributionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCorePluginsAdminReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginCoreAdminHomeReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginCoreHomeReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginWebsiteMeasurableReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginDiagnosticsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginCoreVisualizationsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginProxyReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginAPIReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/API/Reports/Get.php' => 'Piwik\\Plugins\\API\\Reports\\Get',
    ),
    'PluginExamplePluginReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginWidgetizeReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginTransitionsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginLanguagesManagerReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginActionsReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetDownloads.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetDownloads',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetEntryPageTitles.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetEntryPageTitles',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetEntryPageUrls.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetEntryPageUrls',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetExitPageTitles.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetExitPageTitles',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetExitPageUrls.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetExitPageUrls',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetOutlinks.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetOutlinks',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetPageTitlesFollowingSiteSearch.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetPageTitlesFollowingSiteSearch',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetPageTitles.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetPageTitles',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetPageUrlsFollowingSiteSearch.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetPageUrlsFollowingSiteSearch',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetPageUrls.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetPageUrls',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/Get.php' => 'Piwik\\Plugins\\Actions\\Reports\\Get',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetSiteSearchCategories.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetSiteSearchCategories',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetSiteSearchKeywords.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetSiteSearchKeywords',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Reports/GetSiteSearchNoResultKeywords.php' => 'Piwik\\Plugins\\Actions\\Reports\\GetSiteSearchNoResultKeywords',
    ),
    'PluginDashboardReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginMultiSitesReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Reports/GetAll.php' => 'Piwik\\Plugins\\MultiSites\\Reports\\GetAll',
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Reports/GetOne.php' => 'Piwik\\Plugins\\MultiSites\\Reports\\GetOne',
    ),
    'PluginReferrersReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetAll.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetAll',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetCampaigns.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetCampaigns',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetKeywordsFromCampaignId.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetKeywordsFromCampaignId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetKeywordsFromSearchEngineId.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetKeywordsFromSearchEngineId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetKeywords.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetKeywords',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetReferrerType.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetReferrerType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetSearchEnginesFromKeywordId.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetSearchEnginesFromKeywordId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetSearchEngines.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetSearchEngines',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetSocials.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetSocials',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetUrlsForSocial.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetUrlsForSocial',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetUrlsFromWebsiteId.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetUrlsFromWebsiteId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Reports/GetWebsites.php' => 'Piwik\\Plugins\\Referrers\\Reports\\GetWebsites',
    ),
    'PluginUserLanguageReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserLanguage/Reports/GetLanguageCode.php' => 'Piwik\\Plugins\\UserLanguage\\Reports\\GetLanguageCode',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserLanguage/Reports/GetLanguage.php' => 'Piwik\\Plugins\\UserLanguage\\Reports\\GetLanguage',
    ),
    'PluginDevicesDetectionReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetBrand.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetBrand',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetBrowserEngines.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetBrowserEngines',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetBrowsers.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetBrowsers',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetBrowserVersions.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetBrowserVersions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetModel.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetModel',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetOsFamilies.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetOsFamilies',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetOsVersions.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetOsVersions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Reports/GetType.php' => 'Piwik\\Plugins\\DevicesDetection\\Reports\\GetType',
    ),
    'PluginGoalsReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Reports/GetDaysToConversion.php' => 'Piwik\\Plugins\\Goals\\Reports\\GetDaysToConversion',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Reports/GetMetrics.php' => 'Piwik\\Plugins\\Goals\\Reports\\GetMetrics',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Reports/Get.php' => 'Piwik\\Plugins\\Goals\\Reports\\Get',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Reports/GetVisitsUntilConversion.php' => 'Piwik\\Plugins\\Goals\\Reports\\GetVisitsUntilConversion',
    ),
    'PluginEcommerceReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetDaysToConversionAbandonedCart.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetDaysToConversionAbandonedCart',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetDaysToConversionEcommerceOrder.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetDaysToConversionEcommerceOrder',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetEcommerceAbandonedCart.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetEcommerceAbandonedCart',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetEcommerceOrder.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetEcommerceOrder',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetItemsCategory.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetItemsCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetItemsName.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetItemsName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetItemsSku.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetItemsSku',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetVisitsUntilConversionAbandonedCart.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetVisitsUntilConversionAbandonedCart',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Reports/GetVisitsUntilConversionEcommerceOrder.php' => 'Piwik\\Plugins\\Ecommerce\\Reports\\GetVisitsUntilConversionEcommerceOrder',
    ),
    'PluginSEOReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginEventsReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetActionFromCategoryId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetActionFromCategoryId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetActionFromNameId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetActionFromNameId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetAction.php' => 'Piwik\\Plugins\\Events\\Reports\\GetAction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetCategoryFromActionId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetCategoryFromActionId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetCategoryFromNameId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetCategoryFromNameId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetCategory.php' => 'Piwik\\Plugins\\Events\\Reports\\GetCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetNameFromActionId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetNameFromActionId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetNameFromCategoryId.php' => 'Piwik\\Plugins\\Events\\Reports\\GetNameFromCategoryId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Reports/GetName.php' => 'Piwik\\Plugins\\Events\\Reports\\GetName',
    ),
    'PluginUserCountryReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Reports/GetCity.php' => 'Piwik\\Plugins\\UserCountry\\Reports\\GetCity',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Reports/GetContinent.php' => 'Piwik\\Plugins\\UserCountry\\Reports\\GetContinent',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Reports/GetCountry.php' => 'Piwik\\Plugins\\UserCountry\\Reports\\GetCountry',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Reports/GetRegion.php' => 'Piwik\\Plugins\\UserCountry\\Reports\\GetRegion',
    ),
    'PluginGeoIp2Reports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginVisitsSummaryReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitsSummary/Reports/Get.php' => 'Piwik\\Plugins\\VisitsSummary\\Reports\\Get',
    ),
    'PluginVisitFrequencyReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitFrequency/Reports/Get.php' => 'Piwik\\Plugins\\VisitFrequency\\Reports\\Get',
    ),
    'PluginVisitTimeReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Reports/GetByDayOfWeek.php' => 'Piwik\\Plugins\\VisitTime\\Reports\\GetByDayOfWeek',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Reports/GetVisitInformationPerLocalTime.php' => 'Piwik\\Plugins\\VisitTime\\Reports\\GetVisitInformationPerLocalTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Reports/GetVisitInformationPerServerTime.php' => 'Piwik\\Plugins\\VisitTime\\Reports\\GetVisitInformationPerServerTime',
    ),
    'PluginVisitorInterestReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Reports/GetNumberOfVisitsByDaysSinceLast.php' => 'Piwik\\Plugins\\VisitorInterest\\Reports\\GetNumberOfVisitsByDaysSinceLast',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Reports/GetNumberOfVisitsByVisitCount.php' => 'Piwik\\Plugins\\VisitorInterest\\Reports\\GetNumberOfVisitsByVisitCount',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Reports/GetNumberOfVisitsPerPage.php' => 'Piwik\\Plugins\\VisitorInterest\\Reports\\GetNumberOfVisitsPerPage',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Reports/GetNumberOfVisitsPerVisitDuration.php' => 'Piwik\\Plugins\\VisitorInterest\\Reports\\GetNumberOfVisitsPerVisitDuration',
    ),
    'PluginExampleAPIReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginRssWidgetReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginMonologReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginLoginReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginUsersManagerReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginSitesManagerReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginInstallationReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginCoreUpdaterReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginCoreConsoleReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginScheduledReportsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginUserCountryMapReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginLiveReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Live/Reports/GetLastVisitsDetails.php' => 'Piwik\\Plugins\\Live\\Reports\\GetLastVisitsDetails',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Live/Reports/GetLastVisits.php' => 'Piwik\\Plugins\\Live\\Reports\\GetLastVisits',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Live/Reports/GetSimpleLastVisitCount.php' => 'Piwik\\Plugins\\Live\\Reports\\GetSimpleLastVisitCount',
    ),
    'PluginCustomVariablesReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Reports/GetCustomVariables.php' => 'Piwik\\Plugins\\CustomVariables\\Reports\\GetCustomVariables',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Reports/GetCustomVariablesValuesFromNameId.php' => 'Piwik\\Plugins\\CustomVariables\\Reports\\GetCustomVariablesValuesFromNameId',
    ),
    'PluginPrivacyManagerReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginImageGraphReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginAnnotationsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginMobileMessagingReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginOverlayReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginSegmentEditorReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginInsightsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginMorpheusReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginContentsReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Reports/GetContentNames.php' => 'Piwik\\Plugins\\Contents\\Reports\\GetContentNames',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Reports/GetContentPieces.php' => 'Piwik\\Plugins\\Contents\\Reports\\GetContentPieces',
    ),
    'PluginBulkTrackingReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginResolutionReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Reports/GetConfiguration.php' => 'Piwik\\Plugins\\Resolution\\Reports\\GetConfiguration',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Reports/GetResolution.php' => 'Piwik\\Plugins\\Resolution\\Reports\\GetResolution',
    ),
    'PluginDevicePluginsReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Reports/GetPlugin.php' => 'Piwik\\Plugins\\DevicePlugins\\Reports\\GetPlugin',
    ),
    'PluginHeartbeatReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginIntlReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginMarketplaceReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginProfessionalServicesReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginUserIdReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserId/Reports/GetUsers.php' => 'Piwik\\Plugins\\UserId\\Reports\\GetUsers',
    ),
    'PluginCustomPiwikJsReports\\Piwik\\Plugin\\Report' => 
    array (
    ),
    'PluginHeatmapSessionRecordingReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Reports/GetRecordedSessions.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Reports\\GetRecordedSessions',
    ),
    'PluginMultiChannelConversionAttributionReports\\Piwik\\Plugin\\Report' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Reports/GetChannelAttribution.php' => 'Piwik\\Plugins\\MultiChannelConversionAttribution\\Reports\\GetChannelAttribution',
    ),
    'PluginCorePluginsAdminCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCoreAdminHomeCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCoreHomeCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/ActionsCategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\ActionsCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/VisitorsCategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\VisitorsCategory',
    ),
    'PluginWebsiteMeasurableCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginDiagnosticsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCoreVisualizationsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginProxyCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginAPICategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginExamplePluginCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginWidgetizeCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginTransitionsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginLanguagesManagerCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginActionsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginDashboardCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Dashboard/Categories/DashboardCategory.php' => 'Piwik\\Plugins\\Dashboard\\Categories\\DashboardCategory',
    ),
    'PluginMultiSitesCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Categories/MultiSitesCategory.php' => 'Piwik\\Plugins\\MultiSites\\Categories\\MultiSitesCategory',
    ),
    'PluginReferrersCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/ReferrersCategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\ReferrersCategory',
    ),
    'PluginUserLanguageCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginDevicesDetectionCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginGoalsCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Categories/GoalsCategory.php' => 'Piwik\\Plugins\\Goals\\Categories\\GoalsCategory',
    ),
    'PluginEcommerceCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Categories/EcommerceCategory.php' => 'Piwik\\Plugins\\Ecommerce\\Categories\\EcommerceCategory',
    ),
    'PluginSEOCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginEventsCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Categories/EventsCategory.php' => 'Piwik\\Plugins\\Events\\Categories\\EventsCategory',
    ),
    'PluginUserCountryCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Categories/LocationsCategory.php' => 'Piwik\\Plugins\\UserCountry\\Categories\\LocationsCategory',
    ),
    'PluginGeoIp2Categories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginVisitsSummaryCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginVisitFrequencyCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginVisitTimeCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginVisitorInterestCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginExampleAPICategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginRssWidgetCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginMonologCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginLoginCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginUsersManagerCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginSitesManagerCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginInstallationCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCoreUpdaterCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCoreConsoleCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginScheduledReportsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginUserCountryMapCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginLiveCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Live/Categories/LiveCategory.php' => 'Piwik\\Plugins\\Live\\Categories\\LiveCategory',
    ),
    'PluginCustomVariablesCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Categories/CustomVariablesCategory.php' => 'Piwik\\Plugins\\CustomVariables\\Categories\\CustomVariablesCategory',
    ),
    'PluginPrivacyManagerCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginImageGraphCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginAnnotationsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginMobileMessagingCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginOverlayCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginSegmentEditorCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginInsightsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginMorpheusCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginContentsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginBulkTrackingCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginResolutionCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginDevicePluginsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginHeartbeatCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginIntlCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginMarketplaceCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginProfessionalServicesCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginUserIdCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCustomPiwikJsCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginHeatmapSessionRecordingCategories\\Piwik\\Category\\Category' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Categories/HeatmapCategory.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Categories\\HeatmapCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Categories/SessionRecordingsCategory.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Categories\\SessionRecordingsCategory',
    ),
    'PluginMultiChannelConversionAttributionCategories\\Piwik\\Category\\Category' => 
    array (
    ),
    'PluginCorePluginsAdminCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginCoreAdminHomeCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginCoreHomeCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/DevicesSubcategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\DevicesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/EngagementSubcategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\EngagementSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/SoftwareSubcategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\SoftwareSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Categories/VisitorsOverviewSubcategory.php' => 'Piwik\\Plugins\\CoreHome\\Categories\\VisitorsOverviewSubcategory',
    ),
    'PluginWebsiteMeasurableCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginDiagnosticsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginCoreVisualizationsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginProxyCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginAPICategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginExamplePluginCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginWidgetizeCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginTransitionsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginLanguagesManagerCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginActionsCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/DownloadsSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\DownloadsSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/EntryPagesSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\EntryPagesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/ExitPagesSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\ExitPagesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/OutlinksSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\OutlinksSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/PagesSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\PagesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/PageTitlesSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\PageTitlesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Categories/SiteSearchSubcategory.php' => 'Piwik\\Plugins\\Actions\\Categories\\SiteSearchSubcategory',
    ),
    'PluginDashboardCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginMultiSitesCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginReferrersCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/AllReferrersSubcategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\AllReferrersSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/CampaignsSubcategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\CampaignsSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/ReferrersOverviewSubcategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\ReferrersOverviewSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/SearchEnginesSubcategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\SearchEnginesSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Categories/WebsitesSubcategory.php' => 'Piwik\\Plugins\\Referrers\\Categories\\WebsitesSubcategory',
    ),
    'PluginUserLanguageCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginDevicesDetectionCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginGoalsCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Categories/AddANewGoalSubcategory.php' => 'Piwik\\Plugins\\Goals\\Categories\\AddANewGoalSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Categories/GoalsOverviewSubcategory.php' => 'Piwik\\Plugins\\Goals\\Categories\\GoalsOverviewSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Categories/ManageGoalsSubcategory.php' => 'Piwik\\Plugins\\Goals\\Categories\\ManageGoalsSubcategory',
    ),
    'PluginEcommerceCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Categories/EcommerceLogSubcategory.php' => 'Piwik\\Plugins\\Ecommerce\\Categories\\EcommerceLogSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Categories/EcommerceOverviewSubcategory.php' => 'Piwik\\Plugins\\Ecommerce\\Categories\\EcommerceOverviewSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Categories/ProductSubcategory.php' => 'Piwik\\Plugins\\Ecommerce\\Categories\\ProductSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Categories/SalesSubcategory.php' => 'Piwik\\Plugins\\Ecommerce\\Categories\\SalesSubcategory',
    ),
    'PluginSEOCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginEventsCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Categories/EventsSubcategory.php' => 'Piwik\\Plugins\\Events\\Categories\\EventsSubcategory',
    ),
    'PluginUserCountryCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Categories/LocationsSubcategory.php' => 'Piwik\\Plugins\\UserCountry\\Categories\\LocationsSubcategory',
    ),
    'PluginGeoIp2Categories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginVisitsSummaryCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginVisitFrequencyCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginVisitTimeCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Categories/TimesSubcategory.php' => 'Piwik\\Plugins\\VisitTime\\Categories\\TimesSubcategory',
    ),
    'PluginVisitorInterestCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginExampleAPICategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginRssWidgetCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginMonologCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginLoginCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginUsersManagerCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginSitesManagerCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginInstallationCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginCoreUpdaterCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginCoreConsoleCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginScheduledReportsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginUserCountryMapCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountryMap/Categories/RealTimeMapSubcategory.php' => 'Piwik\\Plugins\\UserCountryMap\\Categories\\RealTimeMapSubcategory',
    ),
    'PluginLiveCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Live/Categories/VisitorLogSubcategory.php' => 'Piwik\\Plugins\\Live\\Categories\\VisitorLogSubcategory',
    ),
    'PluginCustomVariablesCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Categories/CustomVariablesSubcategory.php' => 'Piwik\\Plugins\\CustomVariables\\Categories\\CustomVariablesSubcategory',
    ),
    'PluginPrivacyManagerCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginImageGraphCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginAnnotationsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginMobileMessagingCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginOverlayCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginSegmentEditorCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginInsightsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginMorpheusCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginContentsCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Categories/ContentsSubcategory.php' => 'Piwik\\Plugins\\Contents\\Categories\\ContentsSubcategory',
    ),
    'PluginBulkTrackingCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginResolutionCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginDevicePluginsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginHeartbeatCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginIntlCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginMarketplaceCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginProfessionalServicesCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginUserIdCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserId/Categories/VisitorsUserSubcategory.php' => 'Piwik\\Plugins\\UserId\\Categories\\VisitorsUserSubcategory',
    ),
    'PluginCustomPiwikJsCategories\\Piwik\\Category\\Subcategory' => 
    array (
    ),
    'PluginHeatmapSessionRecordingCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Categories/ManageHeatmapSubcategory.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Categories\\ManageHeatmapSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Categories/ManageSessionRecordingSubcategory.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Categories\\ManageSessionRecordingSubcategory',
    ),
    'PluginMultiChannelConversionAttributionCategories\\Piwik\\Category\\Subcategory' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Categories/EcommerceAttributionSubcategory.php' => 'Piwik\\Plugins\\MultiChannelConversionAttribution\\Categories\\EcommerceAttributionSubcategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Categories/GoalsAttributionSubcategory.php' => 'Piwik\\Plugins\\MultiChannelConversionAttribution\\Categories\\GoalsAttributionSubcategory',
    ),
    'PluginCorePluginsAdminTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCoreAdminHomeTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCoreHomeTrackerPiwik\\Tracker\\LogTable' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Tracker/LogTable/Action.php' => 'Piwik\\Plugins\\CoreHome\\Tracker\\LogTable\\Action',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Tracker/LogTable/ConversionItem.php' => 'Piwik\\Plugins\\CoreHome\\Tracker\\LogTable\\ConversionItem',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Tracker/LogTable/Conversion.php' => 'Piwik\\Plugins\\CoreHome\\Tracker\\LogTable\\Conversion',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Tracker/LogTable/LinkVisitAction.php' => 'Piwik\\Plugins\\CoreHome\\Tracker\\LogTable\\LinkVisitAction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Tracker/LogTable/Visit.php' => 'Piwik\\Plugins\\CoreHome\\Tracker\\LogTable\\Visit',
    ),
    'PluginWebsiteMeasurableTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginDiagnosticsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCoreVisualizationsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginProxyTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginAPITrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginExamplePluginTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginWidgetizeTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginTransitionsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginLanguagesManagerTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginActionsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginDashboardTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginMultiSitesTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginReferrersTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginUserLanguageTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginDevicesDetectionTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginGoalsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginEcommerceTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginSEOTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginEventsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginUserCountryTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginGeoIp2TrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginVisitsSummaryTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginVisitFrequencyTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginVisitTimeTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginVisitorInterestTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginExampleAPITrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginRssWidgetTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginMonologTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginLoginTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginUsersManagerTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginSitesManagerTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginInstallationTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCoreUpdaterTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCoreConsoleTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginScheduledReportsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginUserCountryMapTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginLiveTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCustomVariablesTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginPrivacyManagerTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginImageGraphTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginAnnotationsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginMobileMessagingTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginOverlayTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginSegmentEditorTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginInsightsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginMorpheusTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginContentsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginBulkTrackingTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginResolutionTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginDevicePluginsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginHeartbeatTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginIntlTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginMarketplaceTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginProfessionalServicesTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginUserIdTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCustomPiwikJsTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginHeatmapSessionRecordingTrackerPiwik\\Tracker\\LogTable' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Tracker/LogTable/LogHsrBlob.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Tracker\\LogTable\\LogHsrBlob',
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Tracker/LogTable/LogHsrEvent.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Tracker\\LogTable\\LogHsrEvent',
      '/var/www/my.campaigns.io/public_html/stats/plugins/HeatmapSessionRecording/Tracker/LogTable/LogHsr.php' => 'Piwik\\Plugins\\HeatmapSessionRecording\\Tracker\\LogTable\\LogHsr',
    ),
    'PluginMultiChannelConversionAttributionTrackerPiwik\\Tracker\\LogTable' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/IdSite.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\IdSite',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionIdPages.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionIdPages',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/LinkVisitActionId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/ServerTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/UserId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\UserId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalBuyer',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitGoalConverted.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalConverted',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitIp.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitIp',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDate.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDate',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfMonth',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfWeek.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfWeek',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionDayOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMonth',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionQuarter.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionQuarter',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionSecond.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionSecond',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionWeekOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionWeekOfYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitLastActionYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionYear',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceFirst',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceOrder',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorId',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitorReturning.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorReturning',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitsCount.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitsCount',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CoreHome/Columns/VisitTotalTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitTotalTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionType.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ActionUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ClickedUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ClickedUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/DestinationPage.php' => 'Piwik\\Plugins\\Actions\\Columns\\DestinationPage',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/DownloadUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\DownloadUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/EntryPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/ExitPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/IdPageview.php' => 'Piwik\\Plugins\\Actions\\Columns\\IdPageview',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/InteractionPosition.php' => 'Piwik\\Plugins\\Actions\\Columns\\InteractionPosition',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/Keyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\Keyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/KeywordwithNoSearchResult.php' => 'Piwik\\Plugins\\Actions\\Columns\\KeywordwithNoSearchResult',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageGenerationTime.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageGenerationTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageTitle',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/PageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchCategory.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchDestinationPage.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchDestinationPage',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchKeyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchKeyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/SearchNoResultKeyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchNoResultKeyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/TimeSpentRefAction.php' => 'Piwik\\Plugins\\Actions\\Columns\\TimeSpentRefAction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalActions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalActions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalInteractions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalInteractions',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Actions/Columns/VisitTotalSearches.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalSearches',
    ),
    'PluginDashboardColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiSites/Columns/Website.php' => 'Piwik\\Plugins\\MultiSites\\Columns\\Website',
    ),
    'PluginReferrersColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Campaign.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Campaign',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Keyword.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Keyword',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerName.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Referrer.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Referrer',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerType.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/ReferrerUrl.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/SearchEngine.php' => 'Piwik\\Plugins\\Referrers\\Columns\\SearchEngine',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/SocialNetwork.php' => 'Piwik\\Plugins\\Referrers\\Columns\\SocialNetwork',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/WebsitePage.php' => 'Piwik\\Plugins\\Referrers\\Columns\\WebsitePage',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Referrers/Columns/Website.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Website',
    ),
    'PluginUserLanguageColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserLanguage/Columns/Language.php' => 'Piwik\\Plugins\\UserLanguage\\Columns\\Language',
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserEngine.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserEngine',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserName.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/BrowserVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserVersion',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceBrand.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceBrand',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceModel.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceModel',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/DeviceType.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceType',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/Os.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\Os',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicesDetection/Columns/OsVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\OsVersion',
    ),
    'PluginGoalsColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/DaysToConversion.php' => 'Piwik\\Plugins\\Goals\\Columns\\DaysToConversion',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/IdGoal.php' => 'Piwik\\Plugins\\Goals\\Columns\\IdGoal',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/Revenue.php' => 'Piwik\\Plugins\\Goals\\Columns\\Revenue',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Goals/Columns/VisitsUntilConversion.php' => 'Piwik\\Plugins\\Goals\\Columns\\VisitsUntilConversion',
    ),
    'PluginEcommerceColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Items.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Items',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Order.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Order',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductCategory.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\ProductCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductName.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\ProductName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductPrice.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\ProductPrice',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductQuantity.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\ProductQuantity',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/ProductSku.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\ProductSku',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueDiscount.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueDiscount',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/Revenue.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Revenue',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueShipping.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueShipping',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueSubtotal',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Ecommerce/Columns/RevenueTax.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueTax',
    ),
    'PluginSEOColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventAction.php' => 'Piwik\\Plugins\\Events\\Columns\\EventAction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventCategory.php' => 'Piwik\\Plugins\\Events\\Columns\\EventCategory',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventName.php' => 'Piwik\\Plugins\\Events\\Columns\\EventName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventUrl.php' => 'Piwik\\Plugins\\Events\\Columns\\EventUrl',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/EventValue.php' => 'Piwik\\Plugins\\Events\\Columns\\EventValue',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Events/Columns/TotalEvents.php' => 'Piwik\\Plugins\\Events\\Columns\\TotalEvents',
    ),
    'PluginUserCountryColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/City.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\City',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Continent.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Continent',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Country.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Country',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Latitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Latitude',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Longitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Longitude',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Provider.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Provider',
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserCountry/Columns/Region.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Region',
    ),
    'PluginGeoIp2Columns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/GeoIp2/Columns/Region.php' => 'Piwik\\Plugins\\GeoIp2\\Columns\\Region',
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/DayOfTheWeek.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\DayOfTheWeek',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalMinute.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalMinute',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitTime/Columns/LocalTime.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalTime',
    ),
    'PluginVisitorInterestColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/PagesPerVisit.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\PagesPerVisit',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitDuration.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitDuration',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitsByDaysSinceLastVisit',
      '/var/www/my.campaigns.io/public_html/stats/plugins/VisitorInterest/Columns/VisitsbyVisitNumber.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitsbyVisitNumber',
    ),
    'PluginExampleAPIColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/Base.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\Base',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableName.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/CustomVariableValue.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableValue',
      '/var/www/my.campaigns.io/public_html/stats/plugins/CustomVariables/Columns/SearchCategory.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\SearchCategory',
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentInteraction.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentInteraction',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentName.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentName',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentPiece.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentPiece',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Contents/Columns/ContentTarget.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentTarget',
    ),
    'PluginBulkTrackingColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Columns/Configuration.php' => 'Piwik\\Plugins\\Resolution\\Columns\\Configuration',
      '/var/www/my.campaigns.io/public_html/stats/plugins/Resolution/Columns/Resolution.php' => 'Piwik\\Plugins\\Resolution\\Columns\\Resolution',
    ),
    'PluginDevicePluginsColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginCookie.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginCookie',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginDirector.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginDirector',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginFlash.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginFlash',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginGears.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginGears',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginJava.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginJava',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginPdf.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginPdf',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/Plugin.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\Plugin',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginQuickTime',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginRealPlayer',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginSilverlight',
      '/var/www/my.campaigns.io/public_html/stats/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginWindowsMedia',
    ),
    'PluginHeartbeatColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/UserId/Columns/UserId.php' => 'Piwik\\Plugins\\UserId\\Columns\\UserId',
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginHeatmapSessionRecordingColumns\\Piwik\\Columns\\Dimension' => 
    array (
    ),
    'PluginMultiChannelConversionAttributionColumns\\Piwik\\Columns\\Dimension' => 
    array (
      '/var/www/my.campaigns.io/public_html/stats/plugins/MultiChannelConversionAttribution/Columns/ChannelType.php' => 'Piwik\\Plugins\\MultiChannelConversionAttribution\\Columns\\ChannelType',
    ),
  ),
);